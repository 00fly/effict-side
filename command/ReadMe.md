# 工程打包启动

## 一. 概况

| 项目 | 插件 | 说明 | 格式 |
| :------| ------ | :----- | ------ |
| java-junit4 | **maven-assembly-plugin** | 打包java、test代码 | jar、zip、tar.gz |
| java-no-depend | **maven-jar-plugin** | 打包java | jar |
| java-with-depend | **maven-jar-plugin、maven-dependency-plugin** | 打包java、lib | jar + /lib |
| springboot-with-depend | **maven-dependency-plugin** | 打包boot | jar |

## 二.文件格式预览

### 1. java-junit4

#### ① `mvn clean package`
不包含测试代码、不包含junit包

```shell
#结构
├── com
│   └── fly
│       ├── simple
│       │   ├── MainRun2.class
│       │   └── MainRun.class
│       └── utils
│           └── Executor.class
├── log4j2.xml
├── Log4j-charsets.properties
├── Log4j-config.xsd
├── Log4j-events.dtd
├── Log4j-events.xsd
├── Log4j-levels.xsd

```

#### ② `mvn clean package -f pom-junit-test.xml`
包含测试代码、junit包

```shell
#结构
├── com
│   └── fly
│       ├── base
│       │   └── FileMoveTest.class
│       ├── simple
│       │   ├── MainRun2.class
│       │   └── MainRun.class
│       ├── test ①测试代码
│       │   ├── JunitRunner.class
│       │   ├── MainRunner2.class
│       │   └── MainRunner.class
│       └── utils
│           └── Executor.class
├── junit ②junit包
│   ├── extensions
│   │   ├── ActiveTestSuite$1.class
│   │   ├── ActiveTestSuite.class
│   │   ├── RepeatedTest.class
│   │   ├── TestDecorator.class
│   │   ├── TestSetup$1.class
```


#### ③ `mvn clean package -f pom-main-test.xml`
包含测试代码、不包含junit包

```shell
#结构
├── com
│   └── fly
│       ├── base
│       │   └── FileMoveTest.class 不可运行
│       ├── simple
│       │   ├── MainRun2.class
│       │   └── MainRun.class
│       ├── test ①测试代码
│       │   ├── JunitRunner.class  不可运行
│       │   ├── MainRunner2.class
│       │   └── MainRunner.class
│       └── utils
│           └── Executor.class
├── log4j2.xml
├── Log4j-charsets.properties
├── Log4j-config.xsd
├── Log4j-events.dtd
├── Log4j-events.xsd
├── Log4j-levels.xsd
```

#### ④ `mvn clean package -f pom-pack-test.xml`
包含测试代码、junit包、正式发布包

```shell
#结构
├── com ①测试代码
│   └── fly
│       ├── base 
│       │   └── FileMoveTest.class
│       └── test
│           ├── JunitRunner.class
│           ├── MainRunner2.class
│           └── MainRunner.class
├── commons-io-2.5.jar
├── commons-lang3-3.5.jar
├── hamcrest-core-1.3.jar
├── java-junit4-0.0.1.jar ③正式发布包
├── junit-4.12.jar        ②junit包
├── log4j-api-2.12.1.jar
├── log4j-core-2.12.1.jar
├── log4j-slf4j-impl-2.12.1.jar
└── slf4j-api-1.7.25.jar

```

### 2. java-no-depend （略）

### 3. java-with-depend
#### ① `mvn clean package`
不包含测试代码

```shell
#结构
├── java-with-depend.jar
└── lib
    ├── commons-lang3-3.5.jar
    ├── hamcrest-core-1.3.jar
    ├── junit-4.12.jar
    ├── log4j-api-2.12.1.jar
    ├── log4j-core-2.12.1.jar
    ├── log4j-slf4j-impl-2.12.1.jar
    ├── lombok-1.18.12.jar
    └── slf4j-api-1.7.25.jar
```

#### ② `mvn clean package -f pom-same-dir.xml`
不包含测试代码

```shell
#结构
├── commons-lang3-3.5.jar
├── hamcrest-core-1.3.jar
├── java-with-depend.jar
├── junit-4.12.jar
├── log4j-api-2.12.1.jar
├── log4j-core-2.12.1.jar
├── log4j-slf4j-impl-2.12.1.jar
├── lombok-1.18.12.jar
└── slf4j-api-1.7.25.jar
```

### 4. springboot-with-depend
