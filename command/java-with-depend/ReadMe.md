## docker环境下优先使用docker-compose中environment值

**BaseDataSource**

## 带依赖java工程

### 一，文件运行

```shell
cd classes

#windows
java -cp .;../lib/* com.fly.simple.MainRun
java -cp .;../lib/* com.fly.simple.MainRun2

#linux
java -cp .:../lib/* com.fly.simple.MainRun
java -cp .:../lib/* com.fly.simple.MainRun2
```

### 二，jar运行
**maven-jar-plugin定义默认mainClass和依赖包位置**

#### 1. `mvn clean package`
```shell
│  java-with-depend.jar （运行包）
└──lib （依赖包目录）

#直接运行
java -jar java-with-depend.jar

#指定mainClass
java -cp java-with-depend.jar com.fly.simple.MainRun2
java -cp java-with-depend.jar com.fly.simple.MainRun3
```

#### 2. `mvn clean package -f pom-same-dir.xml`
```shell
   java-with-depend.jar （运行包）
   commons-io-2.5.jar
   commons-lang3-3.5.jar
   hamcrest-core-1.3.jar
   junit-4.12.jar
   ...

#直接运行
java -jar java-with-depend.jar

#指定mainClass
java -cp ./* com.fly.simple.MainRun
java -cp ./* com.fly.simple.MainRun2
java -cp ./* com.fly.simple.MainRun3
```

### 三，参考
[https://maven.apache.org/plugins/maven-dependency-plugin/](https://maven.apache.org/plugins/maven-dependency-plugin/)