package com.fly.simple;

import java.util.Timer;
import java.util.TimerTask;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author 00fly
 *
 */
@Slf4j
public class TimerRun
{
    /**
     * Timer线程安全, 但只会单线程执行, 如果执行时间过长, 就错过下次任务了, 抛出异常时, task会终止
     * 
     * @param args
     */
    public static void main(String[] args)
    {
        log.info("======== Timer");
        new Timer().scheduleAtFixedRate(new TimerTask()
        {
            @Override
            public void run()
            {
                log.info("Hello !!!");
            }
        }, 10000L, 5000L);
    }
}
