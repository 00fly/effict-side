package com.fly.simple;

import java.util.Scanner;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author 00fly
 *
 */
@Slf4j
public class ScannerRun
{
    /**
     * 非交互方式下不能保证程序一直运行
     * 
     * @param args
     */
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        do
        {
            log.info("------------输入x退出,回车换行继续------------");
        } while (!"x".equalsIgnoreCase(sc.nextLine()));
        log.info("------------成功退出------------");
        sc.close();
    }
}
