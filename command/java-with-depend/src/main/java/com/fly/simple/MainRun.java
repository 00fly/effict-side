package com.fly.simple;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;

import com.fly.common.ds.BaseDataSource;
import com.fly.common.utils.Executor;
import com.mysql.cj.jdbc.MysqlDataSource;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MainRun
{
    /**
     * 线程池保证程序一直运行
     * 
     * @param args
     */
    public static void main(String[] args)
    {
        printSystemInfo(args);
        Executors.newScheduledThreadPool(2).scheduleAtFixedRate(() -> {
            try
            {
                Executor.execute(new String[] {"dir", "ls"}).forEach(log::info);
                System.out.println(10 / 0);
            }
            catch (Exception e)
            {
                log.error(e.getMessage(), e);
            }
        }, 10, 2, TimeUnit.SECONDS);
    }
    
    /**
     * 打印系统信息
     * 
     * @param args
     */
    private static void printSystemInfo(String[] args)
    {
        // 输出System env
        log.info("----------- env ------------------");
        System.getenv()
            .keySet()
            .stream()
            .sorted() // 排序
            .forEach(k -> log.info("{} -> {}", k, System.getenv().get(k)));
        
        if (args.length > 0)
        {
            // java -jar xxx.jar p1=1 p2=2 --noweb
            log.info("------------ args --------------");
            log.info("args: {}", StringUtils.join(args, ","));
        }
        
        // java -Dxxx=test -DprocessType=1 -jar xxx.jar
        log.info("------------Properties------------------");
        System.getProperties().keySet().stream().forEach(key -> log.info("{}: {}", key, System.getProperty(String.valueOf(key))));
        
        // 打印DataSource
        MysqlDataSource dataSource = BaseDataSource.getDataSource();
        log.info("{}", dataSource.getUrl());
        log.info("{}", dataSource.getUser());
        log.info("{}", dataSource.getPassword());
    }
}
