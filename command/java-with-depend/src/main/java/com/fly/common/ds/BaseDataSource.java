package com.fly.common.ds;

import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;

import com.mysql.cj.jdbc.MysqlDataSource;

public class BaseDataSource
{
    private static MysqlDataSource dataSource = new MysqlDataSource();
    
    /**
     * 获取DataSource对象
     * 
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static MysqlDataSource getDataSource()
    {
        return dataSource;
    }
    
    // 静态初始化 DataSource
    static
    {
        //docker环境下优先使用docker-compose中environment值
        Map<String, String> env = System.getenv();
        ResourceBundle config = ResourceBundle.getBundle("jdbc");
        String url = StringUtils.defaultIfBlank(env.get("JDBC_URL"), config.getString("jdbc.url"));
        String userName = StringUtils.defaultIfBlank(env.get("JDBC_USERNAME"), config.getString("jdbc.username"));
        String passWord = StringUtils.defaultIfBlank(env.get("JDBC_PASSWORD"), config.getString("jdbc.password"));
        dataSource.setUrl(url);
        dataSource.setUser(userName);
        dataSource.setPassword(passWord);
    }
}
