package com.fly.simple;

import java.util.Scanner;

/**
 * 
 * @author 00fly
 *
 */
public class MainRun3
{
    /**
     * 此方式非交互方式下不能保证程序一直运行
     * 
     * @param args
     */
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        do
        {
            System.out.println("------------输入x退出,回车换行继续------------");
        } while (!"x".equalsIgnoreCase(sc.nextLine()));
        System.out.println("------------成功退出------------");
        sc.close();
    }
}
