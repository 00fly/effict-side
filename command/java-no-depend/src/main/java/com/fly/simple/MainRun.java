package com.fly.simple;

import java.io.IOException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MainRun
{
    /**
     * 线程池保证程序一直运行
     * 
     * @param args
     */
    public static void main(String[] args)
    {
        ScheduledExecutorService service = new ScheduledThreadPoolExecutor(1);
        String[] command = {"dir", "ls"};
        service.scheduleAtFixedRate(() -> {
            try
            {
                Executor.execute(command).forEach(System.out::println);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }, 2, 10, TimeUnit.SECONDS);
    }
}
