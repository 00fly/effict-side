package com.fly.simple;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 快速启动
 */
public class Quickstart
{
    /**
     * 遍历jar目录下的zipkin-server-xxx-exec.jar,选择对应序号的jar启动
     * 
     * @param args
     * @throws IOException
     */
    public static void main(String[] args)
        throws IOException
    {
        String path = new File("").getCanonicalPath();
        String[] files = new File(path).list();
        if (files != null && files.length > 0)
        {
            List<String> jarList = Stream.of(files).filter(f -> f.endsWith("-exec.jar")).sorted().collect(Collectors.toList());
            
            // 构造map
            int count = 1;
            Map<Integer, String> map = new HashMap<>();
            for (String jar : jarList)
            {
                map.put(count++, jar);
            }
            
            // 选项选择
            String input;
            Scanner sc = new Scanner(System.in);
            do
            {
                System.out.println("请输入序号选择文件！");
                map.forEach((k, v) -> System.out.println(k + " ===> " + v));
                input = sc.nextLine();
            } while (input == null || input.trim().length() == 0 || !map.containsKey(Integer.parseInt(input)));
            String jar = map.get(Integer.parseInt(input));
            System.out.println("你选择了：" + jar);
            
            // 执行脚本，注意：加&会在后台运行
            String command = "java -jar " + jar + " --server.port=8083&";
            Executor.execute(command, command);
            sc.close();
        }
    }
}
