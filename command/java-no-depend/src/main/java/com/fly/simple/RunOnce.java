package com.fly.simple;

public class RunOnce
{
    /**
     * 验证: syso输出内容，jar运行可写入文件<br>
     * 指定：pom.xml文件mainClass为com.fly.simple.RunOnce<br>
     * 运行：java -jar java-no-depend-0.0.1.jar > /work/1.txt
     * 
     * @param args
     */
    public static void main(String[] args)
    {
        String jsonData =
            "{\"_index\":\"book_shop\",\"_type\":\"it_book\",\"_id\":\"1\",\"_score\":1.0, \"_source\":{\"name\": \"Java编程思想（第4版）\",\"author\": \"[美] Bruce Eckel\",\"category\": \"编程语言\", \"price\": 109.0,\"publisher\": \"机械工业出版社\",\"date\": \"2007-06-01\",\"tags\": [ \"Java\", \"编程语言\" ]}}";
        System.out.println(jsonData);
    }
}
