## java工程无依赖

### 一. class文件运行
```shell
#进入classes 直接运行
java com.fly.simple.MainRun
java com.fly.simple.MainRun2
```

### 二. jar运行
```shell
#打包
mvn clean package

#直接运行
java -jar java-no-depend.jar

#指定mainClass运行
java -cp java-no-depend.jar com.fly.simple.MainRun
java -cp java-no-depend.jar com.fly.simple.MainRun2
```