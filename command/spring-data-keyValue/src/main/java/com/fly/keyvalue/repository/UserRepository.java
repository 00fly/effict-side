package com.fly.keyvalue.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.fly.keyvalue.entity.User;

public interface UserRepository extends PagingAndSortingRepository<User, Long>
{
    List<User> findByAgeGreaterThan(int age);
}
