package com.fly.keyvalue;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fly.KeyValueApplication;
import com.fly.keyvalue.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest(classes = KeyValueApplication.class)
public class ServiceTest
{
    @Autowired
    UserService userService;
    
    @Test
    public void test()
    {
        log.info("userService.get(1L)： {}", userService.get(1L));
        log.info("userService.page() ： {}", ToStringBuilder.reflectionToString(userService.page(),ToStringStyle.SIMPLE_STYLE));
    }
}
