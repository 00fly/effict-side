<!-- 在jar包都有的前提下EL表达式原样输出，不被解析  原因是  page指令中确少 isELIgnored="false" servlet3.0默认关闭了el表达式的解析 -->
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<table>
	<tr>
		<th colspan="${fn:length(urls)+1}">Navigate</th>
	</tr>
	<tr>
		<td><a target="blank" href="${pageContext.request.contextPath}/druid/">druid monitor</a></td>
		<c:forEach var="item" items="${urls}">
			<td><a href="${pageContext.request.contextPath}${item}/">${item}</a></td>
		</c:forEach>
	</tr>
</table>
