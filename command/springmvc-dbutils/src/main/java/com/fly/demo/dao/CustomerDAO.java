package com.fly.demo.dao;

import java.sql.SQLException;
import java.util.List;

import com.fly.common.PaginationSupport;
import com.fly.demo.entity.Customer;

/**
 * 
 * CustomerDAO 接口
 * 
 * @author 00fly
 * @version [版本号, 2018-09-26]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public interface CustomerDAO
{
    
    /**
     * 根据条件删除数据
     * 
     * @param criteria 条件对象
     * @return
     * @throws SQLException
     */
    long deleteByCriteria(Customer criteria)
        throws SQLException;
    
    /**
     * 根据主键id删除数据
     * 
     * @param id 主键
     * @return
     * @throws SQLException
     */
    long deleteById(Integer id)
        throws SQLException;
    
    /**
     * 根据主键id列表删除数据
     * 
     * @param ids 主键列表
     * @return
     * @throws SQLException
     */
    long deleteById(Integer[] ids)
        throws SQLException;
    
    /**
     * 根据主键id列表删除数据
     * 
     * @param ids 主键列表
     * @return
     * @throws SQLException
     */
    long deleteById(List<Integer> ids)
        throws SQLException;
    
    /**
     * 增加记录(插入全字段)
     * 
     * @param bean 待插入对象
     * @return
     * @throws SQLException
     */
    boolean insert(Customer bean)
        throws SQLException;
    
    /**
     * 增加记录(仅插入非空字段)
     * 
     * @param bean 待插入对象
     * @return
     * @throws SQLException
     */
    boolean insertSelective(Customer bean)
        throws SQLException;
    
    /**
     * 查询全部
     * 
     * @return
     * @throws SQLException
     */
    List<Customer> queryAll()
        throws SQLException;
    
    /**
     * 根据条件查询
     * 
     * @param criteria 条件对象
     * @return
     * @throws SQLException
     */
    List<Customer> queryByCriteria(Customer criteria)
        throws SQLException;
    
    /**
     * 根据id查找数据
     * 
     * @param id 主键
     * @return
     * @throws SQLException
     */
    Customer queryById(Integer id)
        throws SQLException;
    
    /**
     * 根据条件分页查询
     * 
     * @param criteria 条件对象
     * @param pageNo 页号
     * @param pageSize 页大小
     * @return
     * @throws SQLException
     */
    PaginationSupport<Customer> queryForPagination(Customer criteria, int pageNo, int pageSize)
        throws SQLException;
    
    /**
     * 根据条件查询数据条数
     * 
     * @param criteria 条件对象
     * @return
     * @throws SQLException
     */
    long queryTotal(Customer criteria)
        throws SQLException;
    
    /**
     * 根据复杂条件更新全字段数据
     * 
     * @param bean 待更新对象
     * @param criteria 条件对象
     * @return
     * @throws SQLException
     */
    boolean updateByCriteria(Customer bean, Customer criteria)
        throws SQLException;
    
    /**
     * 根据复杂条件更新非空字段数据
     * 
     * @param bean 待更新对象
     * @param criteria 条件对象
     * @return
     * @throws SQLException
     */
    boolean updateByCriteriaSelective(Customer bean, Customer criteria)
        throws SQLException;
    
    /**
     * 根据id更新全部数据
     * 
     * @param bean 待更新对象
     * @return
     * @throws SQLException
     */
    boolean updateById(Customer bean)
        throws SQLException;
    
    /**
     * 根据id更新非空字段数据
     * 
     * @param bean 待更新对象
     * @return
     * @throws SQLException
     */
    boolean updateByIdSelective(Customer bean)
        throws SQLException;
    
}
