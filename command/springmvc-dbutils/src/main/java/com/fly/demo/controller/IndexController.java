package com.fly.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fly.common.PaginationSupport;
import com.fly.common.utils.AnnotationHelper;
import com.fly.demo.entity.Customer;
import com.fly.demo.service.CustomerService;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * IndexController
 * 
 * @author 00fly
 * @version [版本号, 2018-09-26]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@Controller
public class IndexController
{
    @Autowired
    ApplicationContext applicationContext;
    
    @Autowired
    CustomerService customerService;
    
    /**
     * 首页
     * 
     * @param model
     * @return
     * @throws Exception
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "/", method = {RequestMethod.GET, RequestMethod.POST})
    public String index(Model model)
        throws Exception
    {
        model.addAttribute("urls", AnnotationHelper.getRequestMappingURL(applicationContext));
        return "/index";
    }
    
    /**
     * 列表展示
     * 
     * @param model
     * @return
     * @see [类、类#方法、类#成员]
     */
    @Cacheable(value = "simpleCache", key = "#pageNo")
    @ResponseBody
    @RequestMapping(value = "/cache/user/page/{pageNo}", method = RequestMethod.GET)
    public List<Customer> listCached(@PathVariable int pageNo)
    {
        log.info("listCached run ......");
        PaginationSupport<Customer> page = customerService.queryForPagination(null, pageNo, 10);
        return page.getItems();
    }
}
