package com.fly.demo.service.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.fly.common.PaginationSupport;
import com.fly.common.exception.DataException;
import com.fly.demo.dao.CustomerDAO;
import com.fly.demo.entity.Customer;
import com.fly.demo.service.CustomerService;

/**
 * 
 * CustomerService 接口实现类
 * 
 * @author 00fly
 * @version [版本号, 2018-09-26]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Service
@Transactional
public class CustomerServiceImpl implements CustomerService
{
    @Autowired
    private CustomerDAO customerDAO;
    
    @Override
    public void insert(Customer customer)
    {
        try
        {
            customerDAO.insert(customer);
        }
        catch (SQLException e)
        {
            throw new DataException(e.getMessage(), e.getCause());
        }
    }
    
    @Override
    public void deleteById(Integer id)
    {
        try
        {
            customerDAO.deleteById(id);
        }
        catch (SQLException e)
        {
            throw new DataException(e.getMessage(), e.getCause());
        }
    }
    
    @Override
    public long deleteById(Integer[] ids)
    {
        try
        {
            return customerDAO.deleteById(ids);
        }
        catch (SQLException e)
        {
            throw new DataException(e.getMessage(), e.getCause());
        }
    }
    
    @Override
    public long deleteById(List<Integer> ids)
    {
        try
        {
            return customerDAO.deleteById(ids);
        }
        catch (SQLException e)
        {
            throw new DataException(e.getMessage(), e.getCause());
        }
    }
    
    @Override
    public void update(Customer customer)
    {
        try
        {
            customerDAO.updateById(customer);
        }
        catch (SQLException e)
        {
            throw new DataException(e.getMessage(), e.getCause());
        }
    }
    
    @Override
    public void saveOrUpdate(Customer customer)
    {
        try
        {
            if (customer.getId() != null && StringUtils.isNotBlank(Objects.toString(customer.getId())))
            {
                customerDAO.updateById(customer);
            }
            else
            {
                customerDAO.insert(customer);
            }
        }
        catch (SQLException e)
        {
            throw new DataException(e.getMessage(), e.getCause());
        }
    }
    
    @Override
    public Customer queryById(Integer id)
    {
        try
        {
            return customerDAO.queryById(id);
        }
        catch (SQLException e)
        {
            throw new DataException(e.getMessage(), e.getCause());
        }
    }
    
    @Override
    public List<Customer> queryAll()
    {
        try
        {
            return customerDAO.queryAll();
        }
        catch (SQLException e)
        {
            throw new DataException(e.getMessage(), e.getCause());
        }
    }
    
    /**
     * 根据条件分页查询
     * 
     * @param customer 条件对象
     * @param pageNo 页号
     * @param pageSize 页大小
     * @return
     */
    @Override
    public PaginationSupport<Customer> queryForPagination(Customer customer, int pageNo, int pageSize)
    {
        try
        {
            return customerDAO.queryForPagination(customer, pageNo, pageSize);
        }
        catch (SQLException e)
        {
            throw new DataException(e.getMessage(), e.getCause());
        }
    }
    
    /**
     * 事务方法
     * 
     * @see [类、类#方法、类#成员]
     */
    @Override
    public void testTrans()
    {
        try
        {
            List<Customer> list = queryAll();
            for (Customer customer : list)
            {
                customerDAO.insert(customer);
            }
            Assert.isTrue(false, "not true");
        }
        catch (Exception e)
        {
            throw new DataException(e.getMessage(), e.getCause());
        }
    }
}
