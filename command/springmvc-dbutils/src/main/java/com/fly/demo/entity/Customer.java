package com.fly.demo.entity;

import javax.validation.constraints.NotBlank;

/**
 * 
 * customer表对应的Customer实体
 * 
 * @author 00fly
 * @version [版本号, 2018-09-26]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class Customer
{
    // id
    private Integer id;
    
    // customerName
    @NotBlank(message = "用户名不能为空")
    private String customername;
    
    // email
    @NotBlank(message = "邮箱不能为空")
    private String email;
    
    public Integer getId()
    {
        return id;
    }
    
    public void setId(Integer id)
    {
        this.id = id;
    }
    
    public void setCustomername(String customername)
    {
        this.customername = customername;
    }
    
    public String getCustomername()
    {
        return this.customername;
    }
    
    public void setEmail(String email)
    {
        this.email = email;
    }
    
    public String getEmail()
    {
        return this.email;
    }
}
