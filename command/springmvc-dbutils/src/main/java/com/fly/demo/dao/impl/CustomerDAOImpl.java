package com.fly.demo.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fly.common.PaginationSupport;
import com.fly.core.DbutilsTemplate;
import com.fly.demo.dao.CustomerDAO;
import com.fly.demo.entity.Customer;

/**
 * 
 * CustomerDAO 接口实现类
 * 
 * @author 00fly
 * @version [版本号, 2018-09-26]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Repository
@SuppressWarnings("unchecked")
public class CustomerDAOImpl implements CustomerDAO
{
    @Autowired
    DbutilsTemplate dbutilsTemplate;
    
    /**
     * 构造whereClause
     * 
     * @param criteria
     * @return
     */
    private String buildWhereClause(Customer criteria)
    {
        if (criteria == null)
        {
            return "";
        }
        StringBuilder whereClause = new StringBuilder();
        if (criteria.getId() != null)
        {
            whereClause.append(" and id=?");
        }
        if (criteria.getCustomername() != null)
        {
            whereClause.append(" and customerName=?");
        }
        if (criteria.getEmail() != null)
        {
            whereClause.append(" and email=?");
        }
        if (whereClause.length() > 4)
        {
            return whereClause.substring(4);
        }
        return "";
    }
    
    /**
     * 构造whereParams
     * 
     * @param criteria
     * @return
     * 
     */
    private List<Object> buildWhereParams(Customer criteria)
    {
        if (criteria == null)
        {
            return Collections.emptyList();
        }
        List<Object> whereParams = new ArrayList<>();
        if (criteria.getId() != null)
        {
            whereParams.add(criteria.getId());
        }
        if (criteria.getCustomername() != null)
        {
            whereParams.add(criteria.getCustomername());
        }
        if (criteria.getEmail() != null)
        {
            whereParams.add(criteria.getEmail());
        }
        return whereParams;
    }
    
    /**
     * 根据条件删除数据
     * 
     * @param criteria 条件对象
     * @return
     * @throws SQLException
     */
    @Override
    public long deleteByCriteria(Customer criteria)
        throws SQLException
    {
        StringBuilder sql = new StringBuilder("delete from customer");
        String whereClause = buildWhereClause(criteria);
        List<Object> whereParams = buildWhereParams(criteria);
        if (StringUtils.isNotEmpty(whereClause))
        {
            sql.append(" where ").append(whereClause);
        }
        return dbutilsTemplate.update(sql.toString(), whereParams.toArray());
    }
    
    /**
     * 根据主键id删除数据
     * 
     * @param id 主键
     * @return
     * @throws SQLException
     */
    @Override
    public long deleteById(Integer id)
        throws SQLException
    {
        String sql = "delete from customer where id=?";
        return dbutilsTemplate.update(sql, id);
    }
    
    /**
     * 根据主键id列表删除数据
     * 
     * @param ids 主键列表
     * @return
     * @throws SQLException
     */
    @Override
    public long deleteById(Integer[] ids)
        throws SQLException
    {
        Integer[][] idsArr = new Integer[ids.length][1];
        for (int i = 0; i < ids.length; i++)
        {
            idsArr[i][0] = ids[i];
        }
        String sql = "delete from customer where id=?";
        return dbutilsTemplate.batch(sql, idsArr).length;
    }
    
    /**
     * 根据主键id列表删除数据
     * 
     * @param ids 主键列表
     * @return
     * @throws SQLException
     */
    @Override
    public long deleteById(List<Integer> ids)
        throws SQLException
    {
        Integer[][] idsArr = new Integer[ids.size()][1];
        for (int i = 0; i < ids.size(); i++)
        {
            idsArr[i][0] = ids.get(i);
        }
        String sql = "delete from customer where id=?";
        return dbutilsTemplate.batch(sql, idsArr).length;
    }
    
    /**
     * 增加记录(插入全字段)
     * 
     * @param bean 待插入对象
     * @return
     * @throws SQLException
     */
    @Override
    public boolean insert(Customer bean)
        throws SQLException
    {
        String sql = "insert into customer (customerName, email ) values(?, ?)";
        return dbutilsTemplate.update(sql, bean.getCustomername(), bean.getEmail()) > 0;
    }
    
    /**
     * 增加记录(仅插入非空字段)
     * 
     * @param bean 待插入对象
     * @return
     * @throws SQLException
     */
    @Override
    public boolean insertSelective(Customer bean)
        throws SQLException
    {
        StringBuilder columns = new StringBuilder();
        StringBuilder values = new StringBuilder();
        List<Object> params = new ArrayList<>();
        if (bean.getCustomername() != null)
        {
            columns.append(", customerName");
            values.append(", ?");
            params.add(bean.getCustomername());
        }
        if (bean.getEmail() != null)
        {
            columns.append(", email");
            values.append(", ?");
            params.add(bean.getEmail());
        }
        StringBuilder sql = new StringBuilder("insert into customer (").append(columns.substring(1)).append(")");
        sql.append(" values(").append(values.substring(1)).append(")");
        return dbutilsTemplate.update(sql.toString(), params.toArray()) > 0;
    }
    
    /**
     * 查询全部
     * 
     * @return
     * @throws SQLException
     */
    @Override
    public List<Customer> queryAll()
        throws SQLException
    {
        String sql = "select id, customerName, email from customer";
        return dbutilsTemplate.query(Customer.class, sql);
    }
    
    /**
     * 根据条件查询
     * 
     * @param criteria 条件对象
     * @return
     * @throws SQLException
     */
    @Override
    public List<Customer> queryByCriteria(Customer criteria)
        throws SQLException
    {
        StringBuilder sql = new StringBuilder("select id, customerName, email from customer ");
        String whereClause = buildWhereClause(criteria);
        List<Object> whereParams = buildWhereParams(criteria);
        if (StringUtils.isNotEmpty(whereClause))
        {
            sql.append(" where ").append(whereClause);
        }
        return dbutilsTemplate.query(Customer.class, sql.toString(), whereParams.toArray());
    }
    
    /**
     * 根据id查找数据
     * 
     * @param id 主键
     * @return
     * @throws SQLException
     */
    @Override
    public Customer queryById(Integer id)
        throws SQLException
    {
        String sql = "select id, customerName, email from customer where id=?";
        return (Customer)dbutilsTemplate.queryFirst(Customer.class, sql, id);
    }
    
    /**
     * 根据条件分页查询
     * 
     * @param criteria 条件对象
     * @param pageNo 页号
     * @param pageSize 页大小
     * @return
     * @throws SQLException
     */
    @Override
    public PaginationSupport<Customer> queryForPagination(Customer criteria, int pageNo, int pageSize)
        throws SQLException
    {
        StringBuilder sql = new StringBuilder("select id, customerName, email from customer");
        String whereClause = buildWhereClause(criteria);
        List<Object> whereParams = buildWhereParams(criteria);
        if (StringUtils.isNotEmpty(whereClause))
        {
            sql.append(" where ").append(whereClause);
        }
        return dbutilsTemplate.queryForPagination(Customer.class, sql.toString(), pageNo, pageSize, whereParams.toArray());
    }
    
    /**
     * 根据条件查询数据条数
     * 
     * @param criteria 条件对象
     * @return
     * @throws SQLException
     */
    @Override
    public long queryTotal(Customer criteria)
        throws SQLException
    {
        StringBuilder sql = new StringBuilder("select count(1) from customer");
        String whereClause = buildWhereClause(criteria);
        List<Object> whereParams = buildWhereParams(criteria);
        if (StringUtils.isNotEmpty(whereClause))
        {
            sql.append(" where ").append(whereClause);
        }
        return dbutilsTemplate.queryForLong(sql.toString(), whereParams.toArray());
    }
    
    /**
     * 根据复杂条件更新全字段数据
     * 
     * @param bean 待更新对象
     * @param criteria 条件对象
     * @return
     * @throws SQLException
     */
    @Override
    public boolean updateByCriteria(Customer bean, Customer criteria)
        throws SQLException
    {
        StringBuilder sql = new StringBuilder("update customer set customerName=?, email=?");
        List<Object> params = new ArrayList<>();
        params.add(bean.getCustomername());
        params.add(bean.getEmail());
        String whereClause = buildWhereClause(criteria);
        List<Object> whereParams = buildWhereParams(criteria);
        if (StringUtils.isNotEmpty(whereClause))
        {
            sql.append(" where ").append(whereClause);
        }
        params.addAll(whereParams);
        return dbutilsTemplate.update(sql.toString(), params.toArray()) > 0;
    }
    
    /**
     * 根据复杂条件更新非空字段数据
     * 
     * @param bean 待更新对象
     * @param criteria 条件对象
     * @return
     * @throws SQLException
     */
    @Override
    public boolean updateByCriteriaSelective(Customer bean, Customer criteria)
        throws SQLException
    {
        StringBuilder sql = new StringBuilder("update customer set");
        StringBuilder columns = new StringBuilder();
        List<Object> params = new ArrayList<>();
        if (bean.getCustomername() != null)
        {
            columns.append(", customerName=?");
            params.add(bean.getCustomername());
        }
        if (bean.getEmail() != null)
        {
            columns.append(", email=?");
            params.add(bean.getEmail());
        }
        sql.append(columns.substring(1));
        
        String whereClause = buildWhereClause(criteria);
        List<Object> whereParams = buildWhereParams(criteria);
        if (StringUtils.isNotEmpty(whereClause))
        {
            sql.append(" where ").append(whereClause);
        }
        params.addAll(whereParams);
        return dbutilsTemplate.update(sql.toString(), params.toArray()) > 0;
    }
    
    /**
     * 根据id更新全部数据
     * 
     * @param bean 待更新对象
     * @return
     * @throws SQLException
     */
    @Override
    public boolean updateById(Customer bean)
        throws SQLException
    {
        String sql = "update customer set customerName=?, email=? where id=?";
        return dbutilsTemplate.update(sql, bean.getCustomername(), bean.getEmail(), bean.getId()) > 0;
    }
    
    /**
     * 根据id更新非空字段数据
     * 
     * @param bean 待更新对象
     * @return
     * @throws SQLException
     */
    @Override
    public boolean updateByIdSelective(Customer bean)
        throws SQLException
    {
        StringBuilder sql = new StringBuilder("update customer set");
        StringBuilder columns = new StringBuilder();
        List<Object> params = new ArrayList<>();
        if (bean.getCustomername() != null)
        {
            columns.append(", customerName=?");
            params.add(bean.getCustomername());
        }
        if (bean.getEmail() != null)
        {
            columns.append(", email=?");
            params.add(bean.getEmail());
        }
        sql.append(columns.substring(1));
        sql.append(" where id=?");
        params.add(bean.getId());
        return dbutilsTemplate.update(sql.toString(), params.toArray()) > 0;
    }
}
