package com.fly.core.config;

import java.util.List;

import org.springframework.cache.Cache;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheFactoryBean;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * CacheManagerConfig
 * 
 * @author 00fly
 * @version [版本号, 2021年2月28日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Configuration
@EnableCaching
public class CacheManagerConfig
{
    
    @Bean
    public SimpleCacheManager cacheManager(List<Cache> caches)
    {
        SimpleCacheManager simpleCacheManager = new SimpleCacheManager();
        simpleCacheManager.setCaches(caches);
        return simpleCacheManager;
    }
    
    @Bean
    public ConcurrentMapCacheFactoryBean simpleCache()
    {
        ConcurrentMapCacheFactoryBean simpleCache = new ConcurrentMapCacheFactoryBean();
        simpleCache.setName("simpleCache");
        return simpleCache;
    }
}

//等价配置
//<!-- 启用缓存 -->
//<cache:annotation-driven cache-manager="cacheManager"/>
//<bean id="cacheManager" class="org.springframework.cache.support.SimpleCacheManager">
//    <property name="caches">
//        <set>
//            <!-- 可以配置多个 -->
//            <bean name="simpleCache" class="org.springframework.cache.concurrent.ConcurrentMapCacheFactoryBean"/>
//        </set>
//    </property>
//</bean>
