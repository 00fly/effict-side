package com.fly.test;

import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.fly.demo.dao.CustomerDAO;
import com.fly.demo.entity.Customer;
import com.fly.demo.service.CustomerService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Transactional
@RunWith(SpringRunner.class)
@ContextConfiguration({"/applicationContext.xml"})
public class ServiceTest
{
    @Autowired
    CustomerService customerService;
    
    @Autowired
    CustomerDAO customerDAO;
    
    /**
     * 事务测试(去掉@Transactional数据被插入或更新)
     * 
     * @see [类、类#方法、类#成员]
     */
    @Test
    public void testTrans()
    {
        try (Scanner sc = new Scanner(System.in))
        {
            do
            {
                List<Customer> list = customerService.queryAll();
                for (Customer customer : list)
                {
                    log.info("customer => {}", ReflectionToStringBuilder.toString(customer, ToStringStyle.JSON_STYLE));
                    customer.setCustomername("customer_" + RandomStringUtils.randomNumeric(2, 8));
                    customerService.saveOrUpdate(customer);
                    
                    customerService.queryById(customer.getId());
                    log.info("customer => {}", ReflectionToStringBuilder.toString(customer, ToStringStyle.JSON_STYLE));
                }
                
                log.info("------------输入x退出,回车换行继续------------");
            } while (!StringUtils.equalsIgnoreCase(sc.nextLine(), "x"));
            log.info("------------成功退出------------");
        }
        
        // customerDAO.queryAll();
        // customerService.testTrans();
    }
}
