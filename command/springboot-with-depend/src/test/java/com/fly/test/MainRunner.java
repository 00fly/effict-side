package com.fly.test;

import java.util.Calendar;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import com.fly.simple.BootApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest(classes = BootApplication.class, webEnvironment = WebEnvironment.NONE)
public class MainRunner
{
    @Test
    public void test()
    {
        int curHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        log.info("========>>>>> huors：");
        IntStream.range(curHour, 24).forEach(h -> log.info("{}", h));
    }
}
