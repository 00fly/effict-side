package com.fly.simple.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * ShowJob
 * 
 * @author 00fly
 * @version [版本号, 2022年11月30日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@Component
@Configuration
public class ShowJob
{
    /**
     * 通过SPEL注入jobIndexConfig对象的jobIndex属性值
     */
    @Value("#{jobIndexConfig.jobIndex}")
    String index;
    
    @Scheduled(fixedRate = 10 * 1000L)
    public void run()
    {
        log.info("ShowJob:: {}", index);
    }
}
