package com.fly.simple.task;

import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PostConstruct;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@ConditionalOnExpression("${system.job.index}==2")
public class MyTimer
{
    /**
     * Timer线程安全, 但只会单线程执行, 如果执行时间过长, 就错过下次任务了, 抛出异常时, task会终止
     */
    @PostConstruct
    public void init()
    {
        TimerTask task = new TimerTask()
        {
            @Override
            public void run()
            {
                log.info("Hello World!");
            }
        };
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(task, 10000L, 5000L);
        log.info("======== Timer started!");
    }
}
