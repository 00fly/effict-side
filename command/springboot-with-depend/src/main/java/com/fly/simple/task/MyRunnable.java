package com.fly.simple.task;

import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@ConditionalOnExpression("${system.job.index}==1")
public class MyRunnable implements Runnable
{
    @Override
    public void run()
    {
        while (true)
        {
            try
            {
                TimeUnit.SECONDS.sleep(10);
                log.info("Hello Runnable!");
            }
            catch (Exception e)
            {
                log.error(e.getMessage(), e);
            }
        }
    }
    
    @PostConstruct
    public void init()
    {
        Runnable runnable = new MyRunnable();
        Thread thread = new Thread(runnable);
        thread.start();
    }
}
