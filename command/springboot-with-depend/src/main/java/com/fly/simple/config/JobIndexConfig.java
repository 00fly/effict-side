package com.fly.simple.config;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@Component
public class JobIndexConfig
{
    @Value("${random.int(1,5)}")
    String jobIndex;
    
    @PostConstruct
    public void init()
    {
        log.info("###### JobIndexConfig::jobIndex： {}", jobIndex);
    }
    
    // @DependsOn("jobIndexConfig")
    // @ConditionalOnExpression("'#{jobIndexConfig.jobIndex}'.equals('1')")
}
