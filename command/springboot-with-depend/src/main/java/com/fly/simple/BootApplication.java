package com.fly.simple;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@EnableScheduling
@SpringBootApplication
public class BootApplication implements CommandLineRunner
{
    @Value("${system.job.index}")
    String jobIndex;
    
    public static void main(String[] args)
    {
        SpringApplication.run(BootApplication.class, args);
    }
    
    @Override
    public void run(String... args)
    {
        log.info("###### jobIndex： {}", jobIndex);
    }
}