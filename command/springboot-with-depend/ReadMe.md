# SpringBoot工程

## 一.class文件运行
进入 `\target\classes` 执行 `注意系统间隔符的差异 windows; linux:` 

**windows**

```shell 
java -cp .;../lib/* com.fly.simple.BootApplication
```

**linux**

```shell
java -cp .:../lib/* com.fly.simple.BootApplication
```



## 二.jar运行
### 1. `mvn clean package` SpringBoot 最常见方式，略。。。

### 2. `mvn clean package -f pom-jar-with-lib.xml` 打包

maven-dependency-plugin、maven-jar-plugin指定depend、mainClass直接运行

```shell
java -jar springboot-with-depend.jar
```


## 三.其他

**springboot工程jar打包并运行junit单元测试方法的支持，不是太迫切，因为有knife4j等工具辅助测试**