package com.fly.test;

import com.fly.base.FileMoveTest;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MainRunner
{
    /**
     * main方式测试
     * 
     * @throws Exception
     */
    public static void main(String[] args)
        throws Exception
    {
        log.info("MainRunner......");
        new FileMoveTest().test();
    }
}