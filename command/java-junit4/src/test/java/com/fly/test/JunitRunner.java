package com.fly.test;

import org.apache.commons.lang3.RandomUtils;
import org.junit.runner.JUnitCore;
import org.junit.runner.Request;
import org.junit.runner.Result;

import com.fly.base.FileMoveTest;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JunitRunner
{
    /**
     * junit4测试方法
     * 
     * @param args
     */
    public static void main(String[] args)
    {
        log.info("Hello world!");
        Result result;
        if (RandomUtils.nextBoolean())
        {
            // 测试类的全部方法
            result = JUnitCore.runClasses(FileMoveTest.class);
        }
        else
        {
            // 测试类的指定方法
            Request request = Request.method(FileMoveTest.class, "test");
            result = new JUnitCore().run(request);
        }
        System.exit(result.wasSuccessful() ? 0 : 1);
    }
}