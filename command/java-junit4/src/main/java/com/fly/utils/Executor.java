package com.fly.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Executor
{
    public static final boolean IS_OS_WINDOWS = SystemUtils.IS_OS_WINDOWS;
    
    /**
     * execute命令
     * 
     * @param command
     * @throws IOException
     * @see [类、类#方法、类#成员]
     */
    public static List<String> execute(String[] command)
        throws IOException
    {
        List<String> resultList = new ArrayList<>();
        String[] cmd = IS_OS_WINDOWS ? new String[] {"cmd", "/c", command[0]} : new String[] {"/bin/sh", "-c", command[1]};
        Process ps = Runtime.getRuntime().exec(cmd);
        log.info("========>>>>> 即将执行命令：{}", StringUtils.join(cmd, " "));
        try (BufferedReader br = new BufferedReader(new InputStreamReader(ps.getInputStream(), "GBK")))
        {
            String line;
            while ((line = br.readLine()) != null)
            {
                resultList.add(line);
            }
        }
        return resultList;
    }
}
