## 带依赖java工程

### 一.常规打包（不带测试代码）
```shell
mvn clean package

#直接运行
java -jar java-junit4.jar

#运行时指定类
java -cp java-junit4.jar com.fly.simple.MainRun
java -cp java-junit4.jar com.fly.simple.MainRun2
```

### 二.带测试代码打包（使用maven-assembly-plugin）

#### 1.Jar运行junit测试（推荐）

**推荐理由：运行包和依赖包均为解压添加进Jar，支持自主指定运行类**

assembly配置文件需要打包Junit依赖,scope设为`test` **已解決打包排除lombok**

```shell
mvn clean package -f pom-junit-test.xml

#直接运行
java -jar java-junit4-junit-test.jar

#运行时指定junit相关类
java -cp java-junit4-junit-test.jar org.junit.runner.JUnitCore com.fly.base.FileMoveTest
java -cp java-junit4-junit-test.jar com.fly.test.JunitRunner

#运行时指定main类
java -cp java-junit4-junit-test.jar com.fly.simple.MainRun
java -cp java-junit4-junit-test.jar com.fly.simple.MainRun2

java -cp java-junit4-junit-test.jar com.fly.test.MainRunner
java -cp java-junit4-junit-test.jar com.fly.test.MainRunner2
```


#### 2.多格式压缩包解压运行（推荐）

**推荐理由：运行包和依赖包均为jar、测试包为文件，支持自主指定运行类**

##### ①准备

```shell
#打包
mvn clean package -f pom-pack-test.xml

#解压zip
unzip java-junit4-junit-test.zip

#解压tar.gz
tar -zxvf java-junit4-junit-test.tar.gz

#解压后
├── com ①测试代码
│   └── fly
│       ├── base 
│       │   └── FileMoveTest.class
│       └── test
│           ├── JunitRunner.class
│           ├── MainRunner2.class
│           └── MainRunner.class
├── commons-io-2.5.jar
├── commons-lang3-3.5.jar
├── hamcrest-core-1.3.jar
├── java-junit4-0.0.1.jar ③正式发布包
├── junit-4.12.jar        ②junit包
├── log4j-api-2.12.1.jar
├── log4j-core-2.12.1.jar
├── log4j-slf4j-impl-2.12.1.jar
└── slf4j-api-1.7.25.jar
```
##### ②windows运行
```shell
java -cp .;* com.fly.simple.MainRun
java -cp .;* com.fly.simple.MainRun2

java -cp .;* com.fly.test.JunitRunner
java -cp .;* com.fly.test.MainRunner
java -cp .;* com.fly.test.MainRunner2

java -cp .;* org.junit.runner.JUnitCore com.fly.base.FileMoveTest
```

##### ③linux运行
```shell
java -cp .:* com.fly.simple.MainRun
java -cp .:* com.fly.simple.MainRun2

java -cp .:* com.fly.test.JunitRunner
java -cp .:* com.fly.test.MainRunner
java -cp .:* com.fly.test.MainRunner2

java -cp .:* org.junit.runner.JUnitCore com.fly.base.FileMoveTest
```

#### 3.test包内main方式测试（不常用，不推荐）
assembly配置文件不需要Junit依赖,只需包含test-classes，scope设为`runtime` **打包不包含 lombok**

```shell
mvn clean package -f pom-main-test.xml

#直接运行
java -jar java-junit4-main-test.jar

#运行时指定类
java -cp java-junit4-main-test.jar com.fly.test.MainRunner
java -cp java-junit4-main-test.jar com.fly.test.MainRunner2
```

### 三.参考文档
[Pre-defined Descriptor Files](https://maven.apache.org/plugins/maven-assembly-plugin/descriptor-refs.html)