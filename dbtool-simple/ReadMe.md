## java web maven工程
启动方式：
1. 工程右键 Run with Jetty
2. 命令行    mvn tomcat7:run


## tomcat配置多域名

1.配置hosts文件


```shell
127.0.0.1 test.00fly.online
127.0.0.1 demo.00fly.online
```


2. tomcat 配置server.xml

```xml
      <Host name="test.00fly.online"  appBase="webapps" unpackWARs="true" autoDeploy="true"> 
        <Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs"
               prefix="localhost_access_log" suffix=".txt"
               pattern="%h %l %u %t &quot;%r&quot; %s %b" />
      </Host>
      <Host name="demo.00fly.online"  appBase="webapps" unpackWARs="true" autoDeploy="true">
        <Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs"
               prefix="localhost_access_log" suffix=".txt"
               pattern="%h %l %u %t &quot;%r&quot; %s %b" />
      </Host>
```