package com.fly.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import org.apache.commons.lang3.SystemUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@WebServlet(value = {"/start"}, loadOnStartup = 1)
public class Startup extends HttpServlet
{
    private static final long serialVersionUID = -5156345638165284301L;
    
    /**
     * 打开浏览器
     * 
     * @see [类、类#方法、类#成员]
     */
    @Override
    public void init()
        throws ServletException
    {
        try
        {
            if (SystemUtils.IS_OS_WINDOWS)
            {
                log.info("now open Browser...");
                Runtime.getRuntime().exec("cmd /c start /min http://127.0.0.1:8080/");
            }
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
}