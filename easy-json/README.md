# Jar快速导出json数据

## 一、**Jar运行**

### 1.**windows运行出现乱码： dos窗口输出的日志中出现乱码和程序返回数据出现乱码**

```shell
#将控制台输出编码改为UTF8
chcp 65001

#解决程序返回数据出现乱码
java -Dfile.encoding=utf-8 -jar easy-json-0.0.1.jar > data.json
```

### 2.**unix运行**

```shell
java -jar easy-json-0.0.1.jar > /work/data.json
```

## 二、**docker 运行**

```shell
docker run registry.cn-shanghai.aliyuncs.com/00fly/easy-json:0.0.1 > data.json
```

## 三、**unix下结果输出**

```shell
echo 'hello world!' > 1.txt

echo "{\"_index\":\"book_shop\",\"_type\":\"it_book\",\"_id\":\"1\",\"_score\":1.0, \"_source\":{\"name\": \"Java编程思想（第4版）\",\"author\": \"[美] Bruce Eckel\",\"category\": \"编程语言\", \"price\": 109.0,\"publisher\": \"机械工业出版社\",\"date\": \"2007-06-01\",\"tags\": [ \"Java\", \"编程语言\" ]}}" > 2.txt
```
