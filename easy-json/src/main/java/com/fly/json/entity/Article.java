package com.fly.json.entity;

import java.util.Objects;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Article
{
    Long articleId;
    
    String title;
    
    Long viewCount;
    
    String url;
    
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Article other = (Article)obj;
        return Objects.equals(articleId, other.articleId);
    }
    
    @Override
    public int hashCode()
    {
        return Objects.hash(articleId);
    }
}
