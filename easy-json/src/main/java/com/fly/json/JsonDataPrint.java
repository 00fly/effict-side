package com.fly.json;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.fly.json.entity.Article;
import com.fly.json.entity.BlogData;
import com.fly.json.entity.Record;
import com.fly.json.utils.JsonBeanUtils;
import com.fly.json.utils.ShellExecutor;

import lombok.extern.slf4j.Slf4j;

/**
 * 使用slf4j-simple（或slf4j-nop）规避log4j-slf4j-impl将日志打印到console<br>
 * slf4j-simple默认情況下只输出info以上（包含info）级別的日志, 且仅把日志输入到System.err
 * 
 */
@Slf4j
public class JsonDataPrint
{
    /**
     * 验证: syso输出内容，jar运行可写入文件<br>
     * 运行：java -jar easy-json-0.0.1.jar > /work/1.txt
     * 
     * @param args
     * @throws IOException
     */
    public static void main(String[] args)
        throws IOException
    {
        // 解决windows下jar运行返回数据乱码
        System.setProperty("file.encoding", "utf-8");
        
        int pageSize = 50;
        int total = parseToBlogData(ShellExecutor.execute("curl -e \"blog.csdn.net\" \"https://blog.csdn.net/community/home-api/v1/get-business-list?page=1&size=1&businessType=blog&username=qq_16127313")).getData().getTotal();
        int pageNum = (total + pageSize - 1) / pageSize;
        
        List<Article> articles = IntStream.rangeClosed(1, pageNum)
            .mapToObj(i -> String.format("curl -e \"blog.csdn.net\" \"https://blog.csdn.net/community/home-api/v1/get-business-list?page=%s&size=%s&businessType=blog&username=qq_16127313\"", i, pageSize))
            .map(cmd -> ShellExecutor.execute(cmd))
            .map(json -> parseToBlogData(json))
            .filter(blog -> blog != null)
            .map(blog -> blog.getData().getList())
            .flatMap(List::stream)
            .collect(Collectors.toList());
        
        // 构造对象
        BlogData blogData = new BlogData().setData(new Record().setList(articles));
        String jsonData = JsonBeanUtils.beanToJson(blogData, true);
        System.out.println(jsonData);
    }
    
    /**
     * 解析json为BlogData
     * 
     * @param json
     * @return
     */
    private static BlogData parseToBlogData(String json)
    {
        try
        {
            return JsonBeanUtils.jsonToBean(json, BlogData.class, true);
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
            return null;
        }
    }
}
