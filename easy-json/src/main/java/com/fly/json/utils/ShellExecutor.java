package com.fly.json.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ShellExecutor
{
    /**
     * execute命令
     * 
     * @param command
     * @throws IOException
     */
    public static void exec(String command)
    {
        try
        {
            log.info("∮∮∮∮∮ WILL EXECUTE: {}", command);
            String[] cmd = SystemUtils.IS_OS_WINDOWS ? new String[] {"cmd", "/c", command} : new String[] {"/bin/sh", "-c", command};
            Runtime.getRuntime().exec(cmd);
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    /**
     * execute命令
     * 
     * @param command
     * @return 执行结果
     * @throws IOException
     */
    public static String execute(String command)
    {
        try
        {
            log.info("∮∮∮∮∮ WILL EXECUTE: {}", command);
            String[] cmd = SystemUtils.IS_OS_WINDOWS ? new String[] {"cmd", "/c", command} : new String[] {"/bin/sh", "-c", command};
            List<String> resultList = new ArrayList<>();
            try (BufferedReader br = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec(cmd).getInputStream())))
            {
                String line;
                while ((line = br.readLine()) != null)
                {
                    resultList.add(line);
                }
            }
            return StringUtils.join(resultList, System.lineSeparator());
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
            return "";
        }
    }
}
