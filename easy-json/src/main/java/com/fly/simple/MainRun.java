package com.fly.simple;

public class MainRun
{
    /**
     * 使用mvn clean package -f pom-simple.xml编译
     * 
     * @param args
     */
    public static void main(String[] args)
    {
        String jsonData =
            "{\"_index\":\"book_shop\",\"_type\":\"it_book\",\"_id\":\"1\",\"_score\":1.0, \"_source\":{\"name\": \"Java编程思想（第4版）\",\"author\": \"[美] Bruce Eckel\",\"category\": \"编程语言\", \"price\": 109.0,\"publisher\": \"机械工业出版社\",\"date\": \"2007-06-01\",\"tags\": [ \"Java\", \"编程语言\" ]}}";
        System.out.println(jsonData);
    }
}
