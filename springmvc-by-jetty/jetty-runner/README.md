#### Jetty Runner说明

##### 仅在jetty-runner-9.3.25.v20180904版本上有此问题

- jetty-runner是jetty发布包里自带的运行工具 [官方文档链接](https://www.eclipse.org/jetty/documentation/jetty-9/index.html#runner)
- jetty-runner[源码](https://repo1.maven.org/maven2/org/eclipse/jetty/jetty-runner/9.3.25.v20180904/)只有一个java文件`org.eclipse.jetty.runner.Runner.java`，本质上是将此文件和及其依赖包解压打包到 `jetty-runner-<version>.jar`
- 解决JSP支持问题，只需在`jetty-runner-<version>.jar`的`META-INF\services\javax.servlet.ServletContainerInitializer`文件中添加下面语句即可：

```shell
org.eclipse.jetty.apache.jsp.JettyJasperInitializer
```

#### 问题定位过程
  **1.** 通过JettyRunner.main方法启动war发现jsp支持没问题

  **2.** 通过引入`maven-dependency-plugin`插件，将项目依赖包复制到<outputDirectory>/lib目录下，通过执行下面的命令(指定depend、mainClass运行)，依次删除`apache-jsp-8.0.33.jar、apache-jsp-9.3.25.v20180904.jar`测试，发现导致jsp报错原因是`apache-jsp-9.3.25.v20180904.jar`没加载。

``` shell
 java -cp lib/* org.eclipse.jetty.runner.Runner springmvc-java-config.war
```
  **3.** 引入`maven-shade-plugin`插件，，将项目依赖包解压打包为一个Jar和官方Jar对比,发现 `META-INF/services/javax.servlet.ServletContainerInitializer`内容被覆盖，正是此文件的问题导致JSP编译异常。
 

#### 解决办法有下面几种，任选其一

 - 添加 `maven-shade-plugin` 插件，使用AppendingTransformer用来处理重名配置文件的合并，使用当前工程生成的 `jetty-runner.jar`
 
    
 - 修改 `jetty-runner-9.3.25.v20180904.Jar` 在 `META-INF\services\javax.servlet.ServletContainerInitializer`文件中添加 `org.eclipse.jetty.apache.jsp.JettyJasperInitializer`
 
 
 - 改用其他版本 `jetty-runner.jar` 如：`9.4.49.v20220914`