package com.fly;

import org.eclipse.jetty.runner.Runner;

@SuppressWarnings("deprecation")
public class JettyRunner
{
    public static void main(String[] args)
    {
        Runner.main(new String[] {"../war-examples/dbtool_simple.war"});
    }
}
