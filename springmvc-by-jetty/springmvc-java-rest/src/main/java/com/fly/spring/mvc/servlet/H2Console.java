package com.fly.spring.mvc.servlet;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;

/**
 * 
 * 注册H2Console
 * 
 * @author 00fly
 * @version [版本号, 2023年4月13日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@WebServlet(value = "/h2-console/*", initParams = {@WebInitParam(name = "webAllowOthers", value = "true")})
public class H2Console extends org.h2.server.web.WebServlet
{
    private static final long serialVersionUID = -8107456131947221626L;
}