package com.fly.spring.mvc.controller;

import java.util.List;
import java.util.stream.IntStream;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fly.spring.jdbc.service.UserService;
import com.fly.spring.jdbc.vo.User;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "用户操作接口")
@RestController
public class RestUserController
{
    @Autowired
    UserService userService;
    
    @PostConstruct
    public void init()
    {
        userService.createUserTable();
        userService.saveUser(new User("Way Lau", 30));
        userService.saveUser(new User("Rod Johnson", 45));
        IntStream.range(1, 10).forEach(i -> userService.saveUser(new User("user_00" + i, 18 + i)));
    }
    
    @ApiOperation("查询全部用户")
    @GetMapping("/users")
    public List<User> users()
    {
        return userService.listUser();
    }
}
