package com.fly.spring.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 
 * IndexController
 * 
 * @author 00fly
 * @version [版本号, 2018-09-26]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Controller
public class IndexController
{
    /**
     * 首页
     * 
     * @param model
     * @return
     * @see [类、类#方法、类#成员]
     */
    @GetMapping(value = "/")
    public String index(Model model)
    {
        model.addAttribute("urls", new String[] {"/h2-console", "/servlet/test"});
        return "index";
    }
}
