package com.fly.spring;

import java.net.InetAddress;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.SystemUtils;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class SpringApplication
{
    private static final int PORT = 8080;
    
    @PostConstruct
    private void openBrowser()
    {
        try
        {
            if (SystemUtils.IS_OS_WINDOWS)
            {
                log.info("now open Browser...");
                String ip = InetAddress.getLocalHost().getHostAddress();
                String url = "http://" + ip + ":" + PORT;
                Runtime.getRuntime().exec("cmd /c start " + url + "/users");
                Runtime.getRuntime().exec("cmd /c start " + url + "/doc.html");
                Runtime.getRuntime().exec("cmd /c start " + url + "/h2-console");
                Runtime.getRuntime().exec("cmd /c start " + url + "/servlet/test");
            }
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
        }
    }
}
