package com.fly.spring.jdbc.dao;

import java.util.List;

import com.fly.spring.jdbc.vo.User;

public interface UserDao
{
    /**
     * 初始化User表
     */
    void createUserTable();
    
    /**
     * 保存用户
     * 
     * @param user
     */
    void saveUser(User user);
    
    /**
     * 查询用户
     * 
     * @return
     */
    List<User> listUser();
    
}
