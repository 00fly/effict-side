package com.fly.spring.jdbc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fly.spring.jdbc.dao.UserDao;
import com.fly.spring.jdbc.service.UserService;
import com.fly.spring.jdbc.vo.User;

@Service
public class UserServiceImpl implements UserService
{
    
    private UserDao userdao;
    
    @Autowired
    public void setUserDao(UserDao userdao)
    {
        this.userdao = userdao;
    }
    
    public void createUserTable()
    {
        userdao.createUserTable();
    }
    
    public void saveUser(User user)
    {
        userdao.saveUser(user);
    }
    
    public List<User> listUser()
    {
        return userdao.listUser();
    }
    
}
