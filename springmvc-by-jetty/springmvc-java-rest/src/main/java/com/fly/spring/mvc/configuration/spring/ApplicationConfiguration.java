package com.fly.spring.mvc.configuration.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RestController;

import com.fly.spring.mvc.configuration.SwaggerConfig;

/**
 * 
 * 取代applicationContext.xml
 * 
 * @author 00fly
 * @version [版本号, 2023年4月21日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Configuration
@EnableScheduling
@EnableAspectJAutoProxy
@EnableTransactionManagement
@ImportResource("classpath:applicationContext.xml")
@ComponentScan(basePackages = "com.fly.spring", useDefaultFilters = true, excludeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION, value = {Controller.class, RestController.class, ControllerAdvice.class}),
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = SwaggerConfig.class)})
public class ApplicationConfiguration
{
}
