package com.fly.core.druid;

import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.JdkRegexpMethodPointcut;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.alibaba.druid.support.spring.stat.DruidStatInterceptor;

/**
 * 
 * 配置druid Spring关联监控
 * 
 * @author 00fly
 * @version [版本号, 2018年9月20日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Configuration
public class SpringMethodAspect
{
    @Bean
    public DruidStatInterceptor druidStatInterceptor()
    {
        return new DruidStatInterceptor();
    }
    
    /**
     * 定义切点
     * 
     * @return
     * @see [类、类#方法、类#成员]
     */
    @Bean
    @Scope("prototype")
    public JdkRegexpMethodPointcut druidStatPointcut()
    {
        JdkRegexpMethodPointcut pointcut = new JdkRegexpMethodPointcut();
        pointcut.setPatterns(new String[] {"com.fly.*.service.*", "com.fly.*.dao.*"});
        return pointcut;
    }
    
    @Bean
    public DefaultPointcutAdvisor druidStatAdvisor(DruidStatInterceptor druidStatInterceptor, JdkRegexpMethodPointcut druidStatPointcut)
    {
        DefaultPointcutAdvisor defaultPointAdvisor = new DefaultPointcutAdvisor();
        defaultPointAdvisor.setPointcut(druidStatPointcut);
        defaultPointAdvisor.setAdvice(druidStatInterceptor);
        return defaultPointAdvisor;
    }
}
