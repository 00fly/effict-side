package com.fly.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fly.common.PaginationSupport;
import com.fly.demo.entity.Customer;
import com.fly.demo.service.CustomerService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * RestCustomerController
 * 
 * @author 00fly
 * @version [版本号, 2018-09-26]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@RestController
@Api(tags = "用户操作接口")
@RequestMapping("/rest/customer")
public class RestCustomerController
{
    @Autowired
    CustomerService customerService;
    
    /**
     * 列表展示
     * 
     * @param model
     * @return
     * @see [类、类#方法、类#成员]
     */
    @ApiOperation("分页查询用户")
    @GetMapping("/page")
    public PaginationSupport<Customer> list(@RequestParam(defaultValue = "1") int pageNo)
    {
        return customerService.queryForPagination(null, pageNo, 10);
    }
}
