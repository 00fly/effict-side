package com.fly.demo.service;

import java.util.List;

import com.fly.common.PaginationSupport;
import com.fly.demo.entity.Customer;

/**
 * 
 * CustomerService 接口
 * 
 * @author 00fly
 * @version [版本号, 2018-09-26]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public interface CustomerService
{
    /**
     * 新增
     * 
     * @param customer
     * @see [类、类#方法、类#成员]
     */
    void insert(Customer customer);
    
    /**
     * 根据id删除
     * 
     * @param id
     * @see [类、类#方法、类#成员]
     */
    void deleteById(Integer id);
    
    /**
     * 根据主键id列表删除数据
     * 
     * @param ids 主键列表
     * @return
     */
    long deleteById(Integer[] ids);
    
    /**
     * 根据主键id列表删除数据
     * 
     * @param ids 主键列表
     * @return
     */
    long deleteById(List<Integer> ids);
    
    /**
     * 根据id更新
     * 
     * @param customer
     * @see [类、类#方法、类#成员]
     */
    void update(Customer customer);
    
    /**
     * 新增/根据id更新
     * 
     * @param customer
     * @see [类、类#方法、类#成员]
     */
    void saveOrUpdate(Customer customer);
    
    /**
     * 根据id查询
     * 
     * @param id
     * @return
     * @see [类、类#方法、类#成员]
     */
    Customer queryById(Integer id);
    
    /**
     * 查询全部
     * 
     * @return
     */
    List<Customer> queryAll();
    
    /**
     * 根据条件分页查询
     * 
     * @param criteria 条件对象
     * @param pageNo 页号，最小值1
     * @param pageSize 页大小，最小值1
     * @return
     */
    PaginationSupport<Customer> queryForPagination(Customer criteria, int pageNo, int pageSize);
    
    /**
     * 事务方法
     * 
     * @see [类、类#方法、类#成员]
     */
    void testTrans();
}
