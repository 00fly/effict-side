package com.fly;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.jar.JarArchiveEntry;
import org.apache.commons.compress.archivers.jar.JarArchiveInputStream;
import org.apache.commons.compress.archivers.jar.JarArchiveOutputStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * Apache Commons Compress测试待补充<br>
 * https://commons.apache.org/proper/commons-compress/examples.html
 * 
 * 
 * @author 00fly
 *
 */
@Slf4j
public class CompressTest
{
    @Test
    public void test()
        throws IOException
    {
        String src = getClass().getResource("/apache-jstl.jar").getPath();
        String add1 = getClass().getResource("/servlet-api.jar").getPath();
        String add2 = getClass().getResource("/log4j2.xml").getPath();
        String newJar = src.replace(".jar", DateFormatUtils.format(System.currentTimeMillis(), "_HHmmssSSS") + ".jar");
        log.info("源文件: {}", src);
        log.info("++新增: {}", add1);
        log.info("++新增: {}", add2);
        log.info("新文件: {}", newJar);
        
        try (JarArchiveInputStream jarInput = new JarArchiveInputStream(new FileInputStream(src)); ArchiveOutputStream outputStream = new JarArchiveOutputStream(new FileOutputStream(newJar)))
        {
            JarArchiveEntry jarEntry;
            while ((jarEntry = jarInput.getNextJarEntry()) != null)
            {
                outputStream.putArchiveEntry(jarEntry);
                IOUtils.copy(jarInput, outputStream);
                
                // TODO：写入字节数组，如何定位读入的数据到达jarEntry结尾
                // byte[] buffer = new byte[1024];
                // int bytesRead = 0;
                // while ((bytesRead = jarInput.read(buffer)) != -1)
                // {
                // outputStream.write(buffer, 0, bytesRead);
                // }
            }
            outputStream.flush();
            
            // 追加addFiles
            String[] addFiles = {add1, add2};
            for (String addFile : addFiles)
            {
                JarArchiveEntry addEntry = new JarArchiveEntry("add/" + StringUtils.substringAfterLast(addFile, "/"));
                outputStream.putArchiveEntry(addEntry);
                try (InputStream entryInputStream = new FileInputStream(addFile))
                {
                    IOUtils.copy(entryInputStream, outputStream);
                }
            }
            
            // 追加add/001.txt
            JarArchiveEntry entry = new JarArchiveEntry("add/001.txt");
            outputStream.putArchiveEntry(entry);
            outputStream.write("org.apache.commons.compress.archivers.jar.JarArchiveOutputStream;".getBytes(StandardCharsets.UTF_8));
            outputStream.closeArchiveEntry();
            outputStream.finish();
        }
    }
}
