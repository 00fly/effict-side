package com.fly;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * @author 00fly
 *
 */
@Slf4j
public class JarEntryTest
{
    /**
     * JDK-API实现-将addFiles添加到srcJar并重命名为newJar
     * 
     * @param srcJar
     * @param newJar
     * @param addFiles
     * @throws IOException
     */
    private void addFilesToJar(File srcJar, String newJar, File... addFiles)
        throws IOException
    {
        try (JarInputStream jis = new JarInputStream(new FileInputStream(srcJar)); JarOutputStream jos = new JarOutputStream(new FileOutputStream(newJar), jis.getManifest()))
        {
            JarEntry jarEntry;
            while ((jarEntry = jis.getNextJarEntry()) != null)
            {
                jos.putNextEntry(jarEntry);
                IOUtils.copy(jis, jos);
            }
            
            // 追加文件
            for (File addFile : addFiles)
            {
                jarEntry = new JarEntry("add/" + addFile.getName());
                jos.putNextEntry(jarEntry);
                try (InputStream entryInputStream = new FileInputStream(addFile))
                {
                    IOUtils.copy(entryInputStream, jos);
                }
            }
        }
    }
    
    /**
     * 拷贝 Jar
     * 
     * @param fromJar
     * @param toJar
     * @throws IOException
     */
    private void copyJar(String fromJar, String toJar)
        throws IOException
    {
        try (JarInputStream jis = new JarInputStream(new BufferedInputStream(new FileInputStream(fromJar))); JarOutputStream jos = new JarOutputStream(new BufferedOutputStream(new FileOutputStream(toJar)), jis.getManifest()))
        {
            JarEntry je;
            while ((je = jis.getNextJarEntry()) != null)
            {
                jos.putNextEntry(je);
                int iRead;
                while ((iRead = jis.read()) != -1)
                {
                    jos.write(iRead);
                }
            }
            jis.closeEntry();
            jos.finish();
        }
    }
    
    @Test
    public void testAddFiles()
    {
        try
        {
            String src = getClass().getResource("/apache-jstl.jar").getPath();
            String add1 = getClass().getResource("/servlet-api.jar").getPath();
            String add2 = getClass().getResource("/log4j2.xml").getPath();
            String newJar = src.replace(".jar", DateFormatUtils.format(System.currentTimeMillis(), "_HHmmssSSS") + ".jar");
            log.info("源文件: {}", src);
            log.info("++新增: {}", add1);
            log.info("++新增: {}", add2);
            log.info("新文件: {}", newJar);
            addFilesToJar(new File(src), newJar, new File(add1), new File(add2));
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @Test
    public void testCopy()
    {
        try
        {
            String src = getClass().getResource("/servlet-api.jar").getPath();
            String newJar = src.replace(".jar", DateFormatUtils.format(System.currentTimeMillis(), "_HHmmssSSS") + ".jar");
            log.info("源文件: {}", src);
            log.info("新文件: {}", newJar);
            copyJar(src, newJar);
            
            if (SystemUtils.IS_OS_WINDOWS)
            {
                Runtime.getRuntime().exec("cmd /c start " + new File(newJar).getParentFile().getCanonicalPath());
            }
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    /**
     * 遍历打印
     * 
     * @throws IOException
     */
    @Test
    public void testList()
        throws IOException
    {
        String src = getClass().getResource("/servlet-api.jar").getPath();
        try (JarInputStream jis = new JarInputStream(new BufferedInputStream(new FileInputStream(src))))
        {
            JarEntry je;
            while ((je = jis.getNextJarEntry()) != null)
            {
                System.out.println(je.getName());
            }
            jis.closeEntry();
        }
    }
}
