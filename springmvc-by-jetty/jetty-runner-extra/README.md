#### Jetty Runner说明


** jetty-runner-extra 支持将war打包进Jar运行 **

#### 步骤

- 将war放入resources文件夹
- 执行`mvn clean package`生成发布包`jetty-runner-extra.jar`
- 执行`java -jar jetty-runner-extra.jar`运行


#### 运行逻辑

- 遍历Jar找到war拷贝到当前目录

- 调用 `org.eclipse.jetty.runner.Runner` 运行war


#### 优化方向

- 执行 `java -jar jetty-runner-extra.jar`  搜索jar内存在的war运行，否则给出提示 `war不存在`，程序终止。


- 执行 `java -jar jetty-runner-extra.jar --addwar` 交互运行，列出当前目录(包含子目录)war供选择添加。


- 执行 `java -jar jetty-runner-extra.jar --addwar war/simple.war` 如指定的war存在，直接添加，否则给出提示 `war不存在`，程序终止。
 
 
#### 远程调试

JDK5-8调试模式参数 `java -agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=8000 -jar jetty-runner-extra.jar --addwar`
 