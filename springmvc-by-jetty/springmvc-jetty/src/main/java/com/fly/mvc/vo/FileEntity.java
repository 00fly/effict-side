package com.fly.mvc.vo;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class FileEntity
{
    private MultipartFile image;
    
    private String userName;
    
}
