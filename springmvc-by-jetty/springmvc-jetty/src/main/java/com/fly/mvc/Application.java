package com.fly.mvc;

import java.io.IOException;
import java.net.InetAddress;

import org.apache.commons.lang3.SystemUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Application
{
    public static void main(String[] args)
        throws Exception
    {
        openBrowser();
        new JettyServer().run();
    }
    
    private static void openBrowser()
        throws IOException
    {
        if (SystemUtils.IS_OS_WINDOWS)
        {
            log.info("★★★★★★★★  now open Browser ★★★★★★★★ ");
            String url = "http://" + InetAddress.getLocalHost().getHostAddress() + ":8080";
            Runtime.getRuntime().exec("cmd /c start " + url + "/hello");
            Runtime.getRuntime().exec("cmd /c start " + url + "/doc.html");
        }
    }
}
