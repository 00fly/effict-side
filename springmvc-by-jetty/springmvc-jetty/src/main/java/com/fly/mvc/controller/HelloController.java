package com.fly.mvc.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fly.mvc.vo.User;

@RestController
public class HelloController
{
    @GetMapping("/hello")
    public String hello()
    {
        return "Hello World!";
    }
    
    @GetMapping("/hello/way")
    public User helloWay()
    {
        return new User("Way Lau", 30);
    }
}
