package com.leo.demo.model;

/**
 * @ClassName: UserInfo
 * @Description: 用户信息表
 * @Author: leo825
 * @Date: 2020-01-02 11:10
 * @Version: 1.0
 */
public class UserInfo
{
    /**
     * 用户id
     */
    Integer id;
    
    /**
     * 用户名称
     */
    String name;
    
    /**
     * 用户性别
     */
    String gender;
    
    /**
     * 用户年龄
     */
    String age;
    
    /**
     * 备注
     */
    String remarks;
    
    public Integer getId()
    {
        return id;
    }
    
    public void setId(Integer id)
    {
        this.id = id;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public String getGender()
    {
        return gender;
    }
    
    public void setGender(String gender)
    {
        this.gender = gender;
    }
    
    public String getAge()
    {
        return age;
    }
    
    public void setAge(String age)
    {
        this.age = age;
    }
    
    public String getRemarks()
    {
        return remarks;
    }
    
    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }
    
    @Override
    public String toString()
    {
        return "UserInfo{" + "id=" + id + ", name='" + name + '\'' + ", gender='" + gender + '\'' + ", age='" + age + '\'' + ", remarks='" + remarks + '\'' + '}';
    }
}
