package com.leo.demo.service;

import java.util.List;

import com.leo.demo.model.UserInfo;

/**
 * @ClassName: UserInfoService
 * @Description: ${description}
 * @Author: leo825
 * @Date: 2020-01-02 11:20
 * @Version: 1.0
 */
public interface UserInfoService
{
    /**
     * 增加用户信息
     * 
     * @param userInfo
     */
    void insertUserInfo(UserInfo userInfo);
    
    /**
     * 删除用户信息
     * 
     * @param id
     */
    void deleteUserInfo(Integer id);
    
    /**
     * 修改用户信息
     * 
     * @param id 旧用户信息的id
     * @param newUserInfo
     */
    void updateUserInfo(Integer id, UserInfo newUserInfo);
    
    /**
     * 查询用户信息
     * 
     * @return
     */
    List<UserInfo> getUserInfoList();
}
