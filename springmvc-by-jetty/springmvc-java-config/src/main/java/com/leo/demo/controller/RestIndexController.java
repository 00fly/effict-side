package com.leo.demo.controller;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * IndexController
 * 
 * @author 00fly
 * @version [版本号, 2018-11-06]
 */
@Api(tags = "演示接口")
@RestController
@RequestMapping("rest")
public class RestIndexController
{
    @ApiOperation("首页数据")
    @GetMapping("/index")
    public Map<String, String> index()
        throws UnknownHostException
    {
        Map<String, String> map = new HashMap<>();
        map.put("msg", "来自spring-mvc的信息");
        map.put("Server IP", InetAddress.getLocalHost().getHostAddress());
        return map;
    }
}
