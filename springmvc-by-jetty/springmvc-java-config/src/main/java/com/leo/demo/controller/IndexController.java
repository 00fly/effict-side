package com.leo.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.leo.demo.service.UserInfoService;

/**
 * 
 * IndexController
 * 
 * @author 00fly
 * @version [版本号, 2018-09-26]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Controller
public class IndexController
{
    @Autowired
    UserInfoService userInfoService;
    
    /**
     * 首页
     * 
     * @param model
     * @return
     * @throws Exception
     * @see [类、类#方法、类#成员]
     */
    @GetMapping(value = "/")
    public String index(Model model)
        throws Exception
    {
        model.addAttribute("urls", new String[] {"/helloworld", "/getUserInfoList", "/h2-console", "/servlet/test"});
        return "index";
    }
    
    @RequestMapping(value = "/helloworld")
    public String helloWorld()
    {
        return "success";
    }
    
    @GetMapping(value = "/getUserInfoList", produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getUserInfoList()
    {
        return userInfoService.getUserInfoList().toString();
    }
    
}
