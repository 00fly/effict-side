package com.leo.demo.servlet;

import java.io.IOException;
import java.net.InetAddress;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/servlet/test")
public class TestServlet extends HttpServlet
{
    private static final long serialVersionUID = 7704687959170309245L;
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        String ip = InetAddress.getLocalHost().getHostAddress();
        response.getWriter().append("HttpServlet Process!  Server IP: ").append(ip);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        doGet(request, response);
    }
}
