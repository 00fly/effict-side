package com.leo.core.config.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RestController;

import com.leo.core.config.SwaggerConfig;

/**
 * 
 * 取代 applicationContext.xml
 * 
 * @author 00fly
 * @version [版本号, 2023年4月21日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Configuration
@EnableScheduling
@EnableTransactionManagement
@ComponentScan(basePackages = "com.leo", useDefaultFilters = true, excludeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION, value = {Controller.class, RestController.class, ControllerAdvice.class}),
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = SwaggerConfig.class)})
public class ApplicationConfiguration
{
}
