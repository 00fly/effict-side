package com.leo.core.config.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.leo.core.config.SwaggerConfig;

/**
 * 
 * 取代spring-mvc.xml
 * 
 * @author 00fly
 * @version [版本号, 2023年4月21日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.leo.demo.controller", "com.leo.core.config"}, useDefaultFilters = false, includeFilters = {
    @ComponentScan.Filter(type = FilterType.ANNOTATION, value = {Controller.class, RestController.class, ControllerAdvice.class}), @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = SwaggerConfig.class)})
public class SpringMvcConfiguration extends WebMvcConfigurerAdapter
{
    /**
     * set静态资源处置策略
     * 
     * @param configurer
     */
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer)
    {
        configurer.enable();
    }
    
    @Override
    public void configureViewResolvers(ViewResolverRegistry registry)
    {
        registry.jsp("/WEB-INF/views/", ".jsp");
    }
}
