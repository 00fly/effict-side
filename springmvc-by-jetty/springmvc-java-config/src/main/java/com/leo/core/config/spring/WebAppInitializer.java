package com.leo.core.config.spring;

import java.nio.charset.StandardCharsets;

import javax.servlet.Filter;

import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * 
 * 取代web.xml配置Spring、Spring MVC，和WebXmlInitializer二选一
 * 
 * @author 00fly
 * @version [版本号, 2023年4月27日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer
{
    /**
     * 加载 Spring 配置类中的信息，初始化 Spring 容器
     * 
     * @return
     */
    @Override
    protected Class<?>[] getRootConfigClasses()
    {
        return new Class[] {ApplicationConfiguration.class};
    }
    
    /**
     * 加载 Spring MVC 配置类中的信息，初始化 Spring MVC 容器
     * 
     * @return
     */
    @Override
    protected Class<?>[] getServletConfigClasses()
    {
        return new Class[] {SpringMvcConfiguration.class};
    }
    
    /**
     * 配置 DispatcherServlet 的映射路径
     * 
     * @return
     */
    @Override
    protected String[] getServletMappings()
    {
        return new String[] {"/"};
    }
    
    @Override
    protected Filter[] getServletFilters()
    {
        return new Filter[] {encodingFilter()};
    }
    
    public CharacterEncodingFilter encodingFilter()
    {
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding(StandardCharsets.UTF_8.toString());
        characterEncodingFilter.setForceEncoding(true);
        return characterEncodingFilter;
    }
}
