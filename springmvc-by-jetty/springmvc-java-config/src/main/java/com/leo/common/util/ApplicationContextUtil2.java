package com.leo.common.util;

import org.springframework.context.support.ApplicationObjectSupport;

/**
 * @ClassName: ApplicationContextUtil2
 * @Description: ${description}
 * @Author: leo825
 * @Date: 2020-02-01 18:53
 * @Version: 1.0
 */
public class ApplicationContextUtil2 extends ApplicationObjectSupport
{
    public <T> T getBean(Class<T> clazz)
    {
        return super.getApplicationContext().getBean(clazz);
    }
}
