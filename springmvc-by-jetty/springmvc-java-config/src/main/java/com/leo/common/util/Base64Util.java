package com.leo.common.util;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: Base64Util
 * @Description: Base64编码，是我们程序开发中经常使用到的编码方法。它是一种基于用64个可打印字符来表示二进制数据的表示方法
 * @Author: leo825
 * @Date: 2020-02-12 17:32
 * @Version: 1.0
 */
@Slf4j
public class Base64Util
{
    /**
     * 使用base64加密
     *
     * @param content
     * @return
     */
    public static String encryptData(String content)
        throws Exception
    {
        return Base64.getEncoder().encodeToString(content.getBytes(StandardCharsets.UTF_8));
    }
    
    /**
     * 使用base64解密
     *
     * @param encryptData
     * @return
     */
    public static String decodeDate(String encryptData)
        throws Exception
    {
        return new String(Base64.getDecoder().decode(encryptData.getBytes()), StandardCharsets.UTF_8);
    }
    
    public static void main(String[] args)
        throws Exception
    {
        String str = "root";
        String encodeStr = encryptData(str);
        log.info("原字符串：{}, base64加密后：{}, 解密后：{}", str, encodeStr, decodeDate(encodeStr));
    }
}
