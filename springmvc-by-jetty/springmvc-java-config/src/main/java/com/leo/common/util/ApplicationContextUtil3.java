package com.leo.common.util;

import org.springframework.web.context.support.WebApplicationObjectSupport;

/**
 * @ClassName: ApplicationContextUtil2
 * @Description: ${description}
 * @Author: leo825
 * @Date: 2020-02-01 18:53
 * @Version: 1.0
 */
public class ApplicationContextUtil3 extends WebApplicationObjectSupport
{
    public <T> T getBean(Class<T> clazz)
    {
        return super.getWebApplicationContext().getBean(clazz);
    }
}
