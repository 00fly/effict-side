package com.leo.common.filter;

import java.io.IOException;

import org.springframework.core.io.Resource;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.TypeFilter;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: MyTypeFilter
 * @Description: 实现TypeFilter接口达到排除类的目的
 * @Author: leo825
 * @Date: 2020-05-17 11:20
 * @Version: 1.0
 */
@Slf4j
public class MyTypeFilter implements TypeFilter
{
    /**
     * 过滤当前类的信息，如果包含的则不需要扫描
     *
     * @param metadataReader 读取当前正在扫描的信息
     * @param metadataReaderFactory 可以获取到其他任何类的信息
     * @return
     * @throws IOException
     */
    @Override
    public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory)
        throws IOException
    {
        // 获取当前类注解的信息
        AnnotationMetadata annotationMetadata = metadataReader.getAnnotationMetadata();
        // 获取当前正在扫描的类信息
        ClassMetadata classMetadata = metadataReader.getClassMetadata();
        // 获取当前类资源（类的路径）
        Resource resource = metadataReader.getResource();
        String className = classMetadata.getClassName();
        // 包含这个包路径的则被拦截
        if (className.startsWith("com.leo.config"))
        {
            log.info("===============>{}", className);
            return false;
        }
        return true;
    }
}
