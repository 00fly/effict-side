#### 项目介绍
 ```shell 
jetty-runner           jetty-runner调用示例
jetty-runner-extra     jetty-runner优化                                         
springmvc-java-config  零配置springmvc示例1                                           
springmvc-java-rest    零配置springmvc示例2                                         
springmvc-simple       springmvc示例1                                           
springmvc-dbutils      springmvc示例2                                         
 ```

似乎springmvc项目war最佳执行方式是使用 `jetty-runner.jar`方式:


 ```shell
 java -jar jetty-runner-9.4.49.v20220914.jar springmvc-java-config.war
 ```

 [jetty-runner下载地址]( https://repo1.maven.org/maven2/org/eclipse/jetty/jetty-runner/)

##### 内嵌jetty服务代码待解决问题

- @WebServlet 注解支持
- Swagger+knife4j整合doc.html打不开
- JSP编译
- 使用plugin生成可执行war



 ```xml
<!-- 将项目依赖包复制到项目lib目录下 -->
<plugin>
	<groupId>org.apache.maven.plugins</groupId>
	<artifactId>maven-dependency-plugin</artifactId>
	<version>3.1.2</version>
	<executions>
		<execution>
			<id>copy-dependencies</id>
			<phase>package</phase>
			<goals>
				<goal>copy-dependencies</goal>
			</goals>
			<configuration>
				<outputDirectory>${project.build.directory}/lib</outputDirectory>
			</configuration>
		</execution>
	</executions>
</plugin>
 ```
 
 
 
 
####  最佳实践
 
 
 springmvc项目去除jetty，执行`mvn clean package`生成如下发布包：
 
 ```shell 
[INFO] jetty-runner                                                       [jar]
[INFO] springmvc-java-config                                              [war]
[INFO] springmvc-java-rest                                                [war]
[INFO] springmvc-simple                                                   [war]
[INFO] springmvc-dbutils                                                  [war]  
 ```
 
 将jar和war放到同一级目录
 
  ```shell
2023/04/22  09:40         6,393,050 jetty-runner.jar
2023/04/22  09:40        23,709,421 springmvc-java-config.war
2023/04/22  09:40        25,542,834 springmvc-java-rest.war
2023/04/22  09:40         9,226,287 springmvc-simple.war
2023/04/22  09:40        31,835,379 springmvc-dbutils.war
 ```
 
 执行如下命令:
 
  ```shell
 java -jar jetty-runner.jar springmvc-java-config.war
  ```
 