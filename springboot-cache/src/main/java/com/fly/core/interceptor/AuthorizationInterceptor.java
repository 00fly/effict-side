package com.fly.core.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * 
 * AuthorizationInterceptor
 * 
 * @author 00fly
 * @version [版本号, 2019年7月21日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Component
@ConditionalOnWebApplication
public class AuthorizationInterceptor extends HandlerInterceptorAdapter
{
    @Autowired
    ApplicationContext applicationContext;
    
    @Value("${server.port}")
    String port;
    
    @Autowired
    HttpSession session;
    
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
        throws Exception
    {
        if (session.getAttribute("urls") == null)
        {
            session.setAttribute("urls", AnnotationHelper.getRequestMappingURL(applicationContext));
            session.setAttribute("port", port);
        }
        return true;
    }
}
