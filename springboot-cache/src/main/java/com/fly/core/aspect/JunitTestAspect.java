package com.fly.core.aspect;

import java.lang.reflect.Method;
import java.util.Scanner;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * aop单元测试增强
 *
 * @author 00fly
 * @version [版本号, 2018年12月10日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
//@Aspect
@Component
public class JunitTestAspect
{
    @Around("@annotation(org.junit.Test)")
    public Object around(ProceedingJoinPoint joinPoint)
        throws Throwable
    {
        String className = joinPoint.getTarget().getClass().getName();
        MethodSignature signature = (MethodSignature)joinPoint.getSignature();
        Method method = signature.getMethod();
        String methodName = new StringBuffer(className).append(".").append(method.getName()).toString();
        Object result = null;
        log.info("aop单元测试增强执行{}", methodName);
        try (Scanner sc = new Scanner(System.in))
        {
            do
            {
                result = joinPoint.proceed();
                log.info("------------输入x退出,回车换行继续------------");
            } while (!"x".equalsIgnoreCase(sc.nextLine()));
            log.info("------------成功退出------------");
        }
        return result;
    }
}
