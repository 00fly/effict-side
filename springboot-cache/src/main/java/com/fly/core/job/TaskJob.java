package com.fly.core.job;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fly.core.log.annotation.Log;
import com.fly.core.log.enums.BusinessType;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class TaskJob
{
    /**
     * 无默认值会抛出IllegalArgumentException
     */
    @Value("${welcome.msg:hello, 00fly!}")
    private String welcome;
    
    /**
     * 测试日志打印
     */
    @Scheduled(initialDelay = 30 * 60000L, fixedRate = 10000L)
    @Log(title = "定时任务", businessType = BusinessType.OTHER)
    public void printLog()
    {
        log.trace("{}", welcome);
        log.debug("{}", welcome);
        log.info("{}", welcome);
        log.warn("{}", welcome);
        log.error("{}", welcome);
    }
}
