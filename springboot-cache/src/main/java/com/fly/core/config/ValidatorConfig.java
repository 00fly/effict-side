package com.fly.core.config;

import java.nio.charset.StandardCharsets;

import org.hibernate.validator.HibernateValidator;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 * 
 * 配置validation的国际化
 * 
 * @author 00fly
 * @version [版本号, 2023年2月9日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Configuration
public class ValidatorConfig
{
    @Bean
    public MessageSource messageSource()
    {
        ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();
        resourceBundleMessageSource.setUseCodeAsDefaultMessage(false);
        resourceBundleMessageSource.setDefaultEncoding(StandardCharsets.UTF_8.name());
        resourceBundleMessageSource.setCacheMillis(-1);// 缓存时间，-1表示不过期
        resourceBundleMessageSource.setBasenames("ValidationMessages");
        return resourceBundleMessageSource;
    }
    
    @Bean
    public LocalValidatorFactoryBean validator()
    {
        LocalValidatorFactoryBean validatorFactoryBean = new LocalValidatorFactoryBean();
        validatorFactoryBean.setProviderClass(HibernateValidator.class);
        validatorFactoryBean.setValidationMessageSource(messageSource());
        return validatorFactoryBean;
    }
}