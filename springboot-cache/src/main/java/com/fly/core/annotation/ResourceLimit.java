package com.fly.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * 资源调用限制注解
 * 
 * @author 00fly
 * @version [版本号, 2022年3月18日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ResourceLimit
{
    /**
     * 统计周期，单位分钟
     */
    long circle();
    
    /**
     * 条件，成功或失败
     */
    Status condition();
    
    /**
     * 次数上限
     */
    long maxCount();
    
    /**
     * 下次运行时间间隔，单位分钟
     */
    long next();
}
