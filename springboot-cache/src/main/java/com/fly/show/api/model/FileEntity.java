package com.fly.show.api.model;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class FileEntity
{
    private MultipartFile file;
    
    private Long id;
    
    private String name;
}
