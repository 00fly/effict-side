package com.fly.show.model;

import com.fasterxml.jackson.annotation.JsonView;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 
 * 结果对象
 * 
 * @author 00fly
 * @version [版本号, 2021年5月2日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Data
@ApiModel(description = "Json格式消息体")
public class JsonResult<T>
{
    public interface SuccessWithData extends SuccessNoData
    {
    }
    
    public interface SuccessNoData
    {
    }
    
    public interface Error
    {
    }
    
    @JsonView(SuccessWithData.class)
    @ApiModelProperty("数据对象")
    private T data;
    
    @JsonView({SuccessNoData.class, Error.class})
    @ApiModelProperty(value = "是否成功", required = true, example = "true")
    private boolean success;
    
    @JsonView(Error.class)
    @ApiModelProperty("错误码")
    private String errorCode;
    
    @JsonView({Error.class})
    @ApiModelProperty("提示信息")
    private String message;
    
    public JsonResult()
    {
        super();
    }
    
    public static <T> JsonResult<T> success(T data)
    {
        JsonResult<T> r = new JsonResult<>();
        if (data instanceof String)
        {
            r.setMessage((String)data);
        }
        else
        {
            r.setData(data);
        }
        r.setSuccess(true);
        return r;
    }
    
    public static JsonResult<?> success()
    {
        JsonResult<Object> r = new JsonResult<>();
        r.setSuccess(true);
        return r;
    }
    
    public static JsonResult<Object> error(String code, String msg)
    {
        JsonResult<Object> r = new JsonResult<>();
        r.setSuccess(false);
        r.setErrorCode(code);
        r.setMessage(msg);
        return r;
    }
    
    public static JsonResult<Object> error(String msg)
    {
        return error("500", msg);
    }
}
