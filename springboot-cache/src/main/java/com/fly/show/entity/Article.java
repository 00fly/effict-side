package com.fly.show.entity;

import lombok.Data;

@Data
public class Article
{
    String title;
    
    String url;
    
    Long viewCount;
}
