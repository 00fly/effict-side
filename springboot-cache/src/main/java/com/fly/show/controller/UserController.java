package com.fly.show.controller;

import javax.validation.Valid;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fly.show.model.User;
import com.fly.show.service.IUserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags = "用户操作接口")
@Controller
@RequestMapping("/user")
public class UserController
{
    @Autowired
    private IUserService userService;
    
    @ResponseBody
    @GetMapping("get/{id:\\d+}")
    public User get(@PathVariable Long id)
    {
        return userService.get(id);
    }
    
    /**
     * 新增/更新数据
     * 
     * @param user
     * @return
     * @see [类、类#方法、类#成员]
     */
    @PostMapping("/add")
    public String add(@Valid @ModelAttribute("item") User user, Errors errors, Model model)
    {
        if (errors.hasErrors())
        {
            // 待定位，空值[年龄不能为空]提示2次
            for (ObjectError error : errors.getAllErrors())
            {
                log.info("error message: {}", error.getDefaultMessage());
            }
            model.addAttribute("items", userService.list());
            return "/user/show";
        }
        userService.saveOrUpdate(user);
        return "redirect:/user/list";
    }
    
    @ApiOperation("测试删除")
    @ResponseBody
    @DeleteMapping("/delete/{id:\\d+}")
    public String delete1(@PathVariable Long id)
    {
        userService.delete(id);
        return userService.get(id) == null ? "success" : "fail";
    }
    
    @GetMapping("/delete/{id:\\d+}")
    public String delete(@PathVariable Long id)
    {
        userService.delete(id);
        return "redirect:/user/list";
    }
    
    @GetMapping({"/", "/list"})
    public String list(Model model)
    {
        User user = new User();
        if (RandomUtils.nextInt(1, 10) > 1)
        {
            user.setUserName(RandomStringUtils.randomNumeric(5));
            user.setAge(RandomUtils.nextInt(10, 70));
            user.setDesc(RandomStringUtils.randomNumeric(5));
        }
        model.addAttribute("item", user);
        model.addAttribute("items", userService.list());
        return "/user/show";
    }
    
    /**
     * 编辑数据
     * 
     * @param id
     * @param model
     * @return
     * @see [类、类#方法、类#成员]
     */
    @GetMapping("/update/{id:\\d+}")
    public String update(@PathVariable Long id, Model model)
    {
        model.addAttribute("item", userService.get(id));
        model.addAttribute("items", userService.list());
        return "/user/show";
    }
}
