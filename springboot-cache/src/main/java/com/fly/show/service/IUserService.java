package com.fly.show.service;

import java.util.List;

import com.fly.show.model.User;

public interface IUserService
{
    void saveOrUpdate(User user);
    
    void delete(Long userId);
    
    User get(Long userId);
    
    List<User> list();
    
}
