package com.fly.test.restful;

import java.util.Scanner;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

import com.fly.CacheApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@WebAppConfiguration
@SpringBootTest(classes = CacheApplication.class)
public class ServiceTest
{
    @Test
    public void test()
    {
        try (Scanner sc = new Scanner(System.in))
        {
            do
            {
                log.info("------------输入x退出,回车换行继续------------");
            } while (!"x".equalsIgnoreCase(sc.nextLine()));
            log.info("------------成功退出------------");
        }
    }
    
    @Test
    public void test1()
    {
        System.out.println(1234);
    }
}
