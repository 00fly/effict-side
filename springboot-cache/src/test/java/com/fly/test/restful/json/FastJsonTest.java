package com.fly.test.restful.json;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

import com.alibaba.fastjson.JSONObject;
import com.fly.test.restful.DataEntity;
import com.github.xiaoymin.knife4j.annotations.Ignore;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FastJsonTest
{
    private String jsonBody = "{\"id\":\"1\",\"name\":\"00fly.online\"}";
    
    /**
     * FastJson
     * 
     * @see [类、类#方法、类#成员]
     */
    @Ignore
    @Test
    public void test1()
    {
        // json字符串转Java对象
        DataEntity dataEntity = JSONObject.parseObject(jsonBody, DataEntity.class);
        log.info("dataEntity: {} ", dataEntity);
        
        // Java对象转字符串
        dataEntity.setName("00fly");
        String jsonStr = JSONObject.toJSONString(dataEntity);
        log.info("jsonStr: {} ", jsonStr);
        
        // Java List对象转字符串
        List<DataEntity> list = new ArrayList<DataEntity>();
        list.add(dataEntity);
        list.add(new DataEntity().setId("2").setName("jack"));
        jsonStr = JSONObject.toJSONString(list);
        log.info("jsonStr: {} ", jsonStr);
        
        List<DataEntity> dataEntitys = JSONObject.parseArray(jsonStr, DataEntity.class);
        log.info("dataEntitys: {} ", dataEntitys);
    }
    
    @Test
    public void test2()
        throws IOException
    {
        String jsonStr = IOUtils.resourceToString("/data/json.txt", StandardCharsets.UTF_8);
        List<Map> list = JSONObject.parseArray(jsonStr, Map.class);
        log.info("list: {} ", list);
        Map<String, Object> map = new HashMap<>();
        list.forEach(c -> {
            String keyName = "";
            for (Object key : c.keySet())
            {
                if (key.toString().startsWith("value"))
                {
                    keyName = key.toString();
                    break;
                }
            }
            map.put(c.get("field").toString(), c.get(keyName));
        });
        log.info("{}", map);
    }
    
    @Test
    public void test3()
        throws IOException
    {
        String str = "{\"id\":\"75\",\"shoppingCartItemList\":[{\"id\":\"407\",\"num\":\"10\"}]}";
        JSONObject jsonObject = JSONObject.parseObject(str);
        String r = jsonObject.getString("shoppingCartItemList");
        log.info(r);
    }
}