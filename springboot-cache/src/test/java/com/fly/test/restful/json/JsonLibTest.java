package com.fly.test.restful.json;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.fly.test.restful.DataEntity;

import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Slf4j
public class JsonLibTest
{
    private String jsonBody = "{\"id\":\"1\",\"name\":\"00fly.online\"}";
    
    /**
     * lombok注解导致运行出错：Property 'id' of class DataEntity has no write method. SKIPPED. <br>
     * <br>
     * 不建议使用JsonLib 来做转换<br>
     * 
     * @see [类、类#方法、类#成员]
     */
    @Test
    @Deprecated
    @SuppressWarnings("unchecked")
    public void test1()
    {
        // json字符串转Java对象
        JSONObject jsonObject = JSONObject.fromObject(jsonBody);
        DataEntity dataEntity = (DataEntity)JSONObject.toBean(jsonObject, DataEntity.class);
        log.info("dataEntity: {} ", dataEntity);
        
        // Java对象转字符串
        dataEntity.setName("00fly");
        String jsonStr = JSONObject.fromObject(dataEntity).toString();
        log.info("jsonStr: {} ", jsonStr);
        
        // Java List对象转字符串
        List<DataEntity> list = new ArrayList<DataEntity>();
        list.add(dataEntity);
        list.add(dataEntity);
        JSONArray jsonArray = JSONArray.fromObject(list);
        jsonStr = jsonArray.toString();
        log.info("jsonStr: {} ", jsonStr);
        
        JSONArray jsonobj = JSONArray.fromObject(jsonStr);
        List<DataEntity> dataEntitys = (List<DataEntity>)JSONArray.toList(jsonobj, DataEntity.class);
        log.info("dataEntitys: {} ", dataEntitys);
        
    }
}
