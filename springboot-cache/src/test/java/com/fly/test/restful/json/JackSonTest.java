package com.fly.test.restful.json;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fly.core.utils.JsonBeanUtils;
import com.fly.test.restful.DataEntity;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JackSonTest
{
    private String jsonBody = "{\"id\":\"1\",\"name\":\"00fly.online\"}";
    
    /**
     * JackSon
     * 
     * @throws IOException
     * 
     * @see [类、类#方法、类#成员]
     */
    @Test
    public void test1()
        throws IOException
    {
        // json字符串转Java对象
        DataEntity dataEntity = JsonBeanUtils.jsonToBean(jsonBody, DataEntity.class);
        log.info("dataEntity: {} ", dataEntity);
        
        // Java对象转字符串
        dataEntity.setName("00fly");
        String jsonStr = JsonBeanUtils.beanToJson(dataEntity);
        log.info("jsonStr: {} ", jsonStr);
        
        // Java List对象转字符串
        List<DataEntity> list = new ArrayList<DataEntity>();
        list.add(dataEntity);
        list.add(new DataEntity().setId("2").setName("jack"));
        jsonStr = JsonBeanUtils.beanToJson(list);
        log.info("jsonStr: {} ", jsonStr);
        
        JavaType javaType = TypeFactory.defaultInstance().constructParametricType(List.class, DataEntity.class);
        List<DataEntity> dataEntitys = JsonBeanUtils.jsonToBean(jsonStr, javaType);
        log.info("dataEntitys: {} ", dataEntitys);
    }
}
