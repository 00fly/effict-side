package com.fly.test.restful;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import lombok.extern.slf4j.Slf4j;

/**
 * see: https://blog.csdn.net/u013066244/article/details/72783575
 */
@Slf4j
public class FileControllerTest
{
    Resource resource = new ClassPathResource("data/nginx-1.25.3.tar.gz");
    
    @Test
    public void test()
        throws IOException
    {
        // ①先解压tar.gz成tar, ②再对tar进行解压
        try (TarArchiveInputStream tis = new TarArchiveInputStream(new GzipCompressorInputStream(new ByteArrayInputStream(IOUtils.toByteArray(resource.getInputStream())))))
        {
            TarArchiveEntry tarEntry;
            while ((tarEntry = tis.getNextTarEntry()) != null)
            {
                log.info("fileName: {}", tarEntry.getName());
            }
        }
    }
    
    @Test
    public void test2()
        throws IOException
    {
        decompressTarGz(resource.getFile(), resource.getFile().getParentFile());
    }
    
    private void decompressTarGz(File sourceTarGzFile, File targetDir)
        throws IOException
    {
        // 解压成tar包
        try (TarArchiveInputStream tarArchiveInputStream = new TarArchiveInputStream(new GzipCompressorInputStream(Files.newInputStream(sourceTarGzFile.toPath()))))
        {
            // 读取tar包里的文件
            TarArchiveEntry entry;
            while ((entry = tarArchiveInputStream.getNextTarEntry()) != null)
            {
                if (entry.isDirectory())
                {
                    continue;
                }
                File targetFile = new File(targetDir, entry.getName());
                targetFile.getParentFile().mkdirs();
                IOUtils.copy(tarArchiveInputStream, Files.newOutputStream(targetFile.toPath()));
            }
        }
    }
}
