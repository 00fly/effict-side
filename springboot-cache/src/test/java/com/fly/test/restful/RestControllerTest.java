package com.fly.test.restful;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import com.fly.CacheApplication;
import com.fly.show.service.IUserService;
import com.github.xiaoymin.knife4j.annotations.Ignore;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@WebAppConfiguration
@SpringBootTest(classes = CacheApplication.class)
public class RestControllerTest
{
    
    @Autowired
    WebApplicationContext wac;
    
    @Autowired
    IUserService userService;
    
    MockMvc mockMvc;
    
    @Autowired
    RestTemplate restTemplate;
    
    @BeforeEach
    public void setup()
    {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        log.info("★★★★★★★★ WebApplicationContext = {}", wac);
        int i = 1;
        for (String beanName : wac.getBeanDefinitionNames())
        {
            log.info("{}.\t{}", i, beanName);
            i++;
        }
    }
    
    /**
     * 测试
     * 
     * @throws Exception
     * 
     * @see [类、类#方法、类#成员]
     */
    @Ignore
    @Test
    public void test()
        throws Exception
    {
        String url = "http://127.0.0.1:8080/swagger/queryList";
        
        // Get(数组[]转List)
        DataEntity[] data = restTemplate.getForObject(url, DataEntity[].class);
        List<DataEntity> list = Arrays.asList(data);
        log.info("list: {}", list);
        
        // Post(数组[]转List)
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(headers);
        data = restTemplate.postForObject(url, requestEntity, DataEntity[].class);
        list = Arrays.asList(data);
        log.info("list: {}", list);
    }
    
    /**
     * 测试 RestAPI
     * 
     * @throws Exception
     * 
     * @see [类、类#方法、类#成员]
     */
    @Test
    public void testRestAPI()
        throws Exception
    {
        log.info("★★★★★★★★ userService.queryAll() = {}", userService.list());
        // get
        mockMvc.perform(get("/query/1")).andDo(MockMvcResultHandlers.print());
    }
}
