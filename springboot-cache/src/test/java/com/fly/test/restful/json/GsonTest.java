package com.fly.test.restful.json;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.fly.test.restful.DataEntity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GsonTest
{
    private String jsonBody = "{\"id\":\"1\",\"name\":\"00fly.online\"}";
    
    Gson gson = new Gson();
    
    /**
     * Gson
     * 
     * @see [类、类#方法、类#成员]
     */
    @Test
    public void test1()
    {
        // json字符串转Java对象
        DataEntity dataEntity = gson.fromJson(jsonBody, DataEntity.class);
        log.info("dataEntity: {} ", dataEntity);
        
        // Java对象转字符串
        dataEntity.setName("00fly");
        String jsonStr = gson.toJson(dataEntity);
        log.info("jsonStr: {} ", jsonStr);
        
        // Java List对象转字符串
        List<DataEntity> list = new ArrayList<DataEntity>();
        list.add(dataEntity);
        list.add(new DataEntity().setId("2").setName("jack"));
        jsonStr = gson.toJson(list);
        log.info("jsonStr: {} ", jsonStr);
        
        List<DataEntity> dataEntitys = gson.fromJson(jsonStr, new TypeToken<List<DataEntity>>()
        {
        }.getType());
        log.info("dataEntitys: {} ", dataEntitys);
    }
}