package com.fly.test.restful;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.condition.RequestMethodsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.fly.CacheApplication;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@WebAppConfiguration
@SpringBootTest(classes = CacheApplication.class)
public class MappingURLTest
{
    @Autowired
    RequestMappingHandlerMapping mapping;
    
    @Test
    public void test()
    {
        List<URLInfo> urlList = new ArrayList<>();
        Map<RequestMappingInfo, HandlerMethod> map = mapping.getHandlerMethods();
        for (Map.Entry<RequestMappingInfo, HandlerMethod> m : map.entrySet())
        {
            URLInfo urlInfo = new URLInfo();
            RequestMappingInfo info = m.getKey();
            HandlerMethod method = m.getValue();
            PatternsRequestCondition p = info.getPatternsCondition();
            for (String url : p.getPatterns())
            {
                urlInfo.setUrl(url);
            }
            urlInfo.setClassName(method.getMethod().getDeclaringClass().getName());
            urlInfo.setMethod(method.getMethod().getName());
            RequestMethodsRequestCondition methodsCondition = info.getMethodsCondition();
            String type = methodsCondition.toString();
            if (type != null && type.startsWith("[") && type.endsWith("]"))
            {
                type = type.substring(1, type.length() - 1);
                urlInfo.setType(type);
            }
            urlList.add(urlInfo);
        }
        urlList.forEach(url -> {
            log.info("{}", url);
        });
    }
}

@Data
class URLInfo
{
    private String url;
    
    private String method;
    
    private String type;
    
    private String className;
}
