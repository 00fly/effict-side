package com.fly.test.restful.json;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.alibaba.fastjson2.JSONObject;
import com.alibaba.fastjson2.JSONWriter;
import com.fly.test.restful.DataEntity;

import lombok.extern.slf4j.Slf4j;

/**
 * 待调试 fastjson2
 */
@Slf4j
public class FastJson2Test
{
    private static List<DataEntity> list = new ArrayList<>();
    
    @BeforeAll
    public static void init()
    {
        list.add(new DataEntity().setId("1"));
        list.add(new DataEntity().setId("2").setName("name2"));
        list.add(new DataEntity().setId("3").setName("name3"));
    }
    
    @Test
    public void test()
    {
        String jsonStr = JSONObject.toJSONString(list);
        log.info("jsonStr: {} ", jsonStr);
    }
    
    @Test
    public void test1()
    {
        String jsonStr = JSONObject.toJSONString(list, JSONWriter.Feature.WriteNulls);
        log.info("jsonStr: {} ", jsonStr);
    }
    
    @Test
    public void test2()
    {
        String jsonStr = JSONObject.toJSONString(list, JSONWriter.Feature.WriteNullStringAsEmpty);
        log.info("jsonStr: {} ", jsonStr);
    }
    
    @Test
    public void test3()
    {
        DataEntity dataEntity = new DataEntity().setId("2");
        String jsonStr = JSONObject.toJSONString(dataEntity, JSONWriter.Feature.WriteNullStringAsEmpty);
        Object res = JSONObject.parseObject(jsonStr).entrySet();
        log.info("res: {} ", res);
    }
}