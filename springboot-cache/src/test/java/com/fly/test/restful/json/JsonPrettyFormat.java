package com.fly.test.restful.json;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import lombok.extern.slf4j.Slf4j;

/**
 * Json字符串格式化
 */
@Slf4j
public class JsonPrettyFormat
{
    // thread-safe
    ObjectMapper mapper = new ObjectMapper();
    
    private String jsonString =
        "{\"_index\":\"book_shop\",\"_type\":\"it_book\",\"_id\":\"1\",\"_score\":1.0, \"_source\":{\"name\": \"Java编程思想（第4版）\",\"author\": \"[美] Bruce Eckel\",\"category\": \"编程语言\", \"price\": 109.0,\"publisher\": \"机械工业出版社\",\"date\": \"2007-06-01\",\"tags\": [ \"Java\", \"编程语言\" ]}}";
    
    @Test
    public void fastjsonTest()
    {
        JSONObject object = JSONObject.parseObject(jsonString);
        String pretty = JSON.toJSONString(object, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue, SerializerFeature.WriteDateUseDateFormat);
        log.info("{} ", pretty);
    }
    
    @Test
    public void gsonTest()
    {
        JsonObject jsonObject = JsonParser.parseString(jsonString).getAsJsonObject();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String pretty = gson.toJson(jsonObject);
        log.info("{} ", pretty);
    }
    
    @Test
    public void jacksonTest()
        throws JsonMappingException, JsonProcessingException
    {
        Object obj = mapper.readValue(jsonString, Object.class);
        String pretty = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
        log.info("{} ", pretty);
        
        JsonNode jsonNode = mapper.readTree(jsonString);
        pretty = jsonNode.toPrettyString();
        log.info("{} ", pretty);
    }
    
    @Test
    public void printTest()
        throws IOException
    {
        JsonNode jsonNode = mapper.readTree(jsonString);
        String pretty = jsonNode.toPrettyString();
        List<String> lines = IOUtils.readLines(new StringReader(pretty));
        lines.stream().forEach(log::info);
    }
}
