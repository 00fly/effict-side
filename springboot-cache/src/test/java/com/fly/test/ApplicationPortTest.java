package com.fly.test;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import com.fly.CacheApplication;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * Application 运行端口测试 <br>
 * <br>
 * docker run --name tomcat -it -d -p 8081:8080 -p 8080:8080 tomcat:alpine
 * 
 * @author 00fly
 * @version [版本号, 2022年11月25日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@SpringBootTest(classes = CacheApplication.class, webEnvironment = WebEnvironment.DEFINED_PORT)
public class ApplicationPortTest
{
    @Value("${server.search.url}")
    String baseUrl;
    
    @Value("${server.search.port.begin}")
    Integer portBegin;
    
    @Value("${server.search.port.end}")
    Integer portEnd;
    
    @Value("${server.search.visit-list}")
    String[] visits;
    
    @Autowired
    RestTemplate restTemplate;
    
    @Autowired
    WebClient webClient;
    
    /**
     * 线程池方式测试
     * 
     * @throws InterruptedException
     * 
     * @see [类、类#方法、类#成员]
     */
    @Test
    public void test()
        throws InterruptedException
    {
        // 线程池
        // ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
        ExecutorService cachedThreadPool = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60L, TimeUnit.SECONDS, new SynchronousQueue<Runnable>(), new CustomizableThreadFactory("cached-pool-"));
        for (int i = portBegin; i < portEnd; i++)
        {
            final int port = i;
            cachedThreadPool.execute(() -> openBrowser(baseUrl, port, visits));
        }
        TimeUnit.SECONDS.sleep(5);
    }
    
    @Test
    public void test2()
        throws InterruptedException
    {
        IntStream.range(portBegin, portEnd).forEach(port -> openBrowserByWebclient(baseUrl, port, visits));
        TimeUnit.SECONDS.sleep(10); // 重要，等待异步调用完成
    }
    
    private void openBrowserByWebclient(String baseUrl, int port, String[] visits)
    {
        Arrays.stream(visits).map(v -> String.format("%s:%s/%s", baseUrl, port, v)).forEach(url -> {
            webClient.get().uri(url).exchange().subscribe(reponse -> {
                if (reponse.statusCode() == HttpStatus.OK)
                {
                    try
                    {
                        log.info("#### now open url: {}", url);
                        Runtime.getRuntime().exec("cmd /c start /min " + url);
                    }
                    catch (IOException e)
                    {
                    }
                }
            });
        });
    }
    
    private void openBrowser(String baseUrl, int port, String[] visits)
    {
        Arrays.stream(visits).forEach(path -> {
            try
            {
                String url = baseUrl + ":" + port + "/" + path;
                ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class);
                if (responseEntity.getStatusCode() == HttpStatus.OK)
                {
                    log.info("now open url: {}", url);
                    Runtime.getRuntime().exec("cmd /c start /min " + url);
                }
            }
            catch (Exception e)
            {
            }
        });
    }
}
