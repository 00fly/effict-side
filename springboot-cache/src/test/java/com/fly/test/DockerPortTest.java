package com.fly.test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import com.fly.core.utils.ShellExecutor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DockerPortTest
{
    String dockerCmd = "docker ps --format \"{{.Names}} {{.Ports}}\"";
    
    /**
     * 输出docker应用映射端口
     * 
     * @throws IOException
     * @see [类、类#方法、类#成员]
     */
    @Test
    public void printPorts()
        throws IOException
    {
        Map<String, Set<String>> map = new TreeMap<>();
        for (String line : ShellExecutor.execute2(dockerCmd))
        {
            log.info("{}", line);
            String name = StringUtils.substringBefore(line, " ");
            Set<String> ports = new TreeSet<>();
            String portText = StringUtils.substringAfter(line, " ");
            for (String it : portText.split(","))
            {
                String port = StringUtils.substringBetween(it, ":", "->");
                if (StringUtils.isNotBlank(port))
                {
                    ports.add(port.replace(":", ""));
                }
            }
            map.put(name, ports);
        }
        log.info("######## {}", map);
    }
    
    /**
     * 获取docker相关信息
     * 
     * @throws IOException
     */
    @Test
    public void printPorts1()
        throws IOException
    {
        Map<String, Set<String>> map = new TreeMap<>();
        for (String line : ShellExecutor.execute2(dockerCmd))
        {
            log.info("{}", line);
            String name = StringUtils.substringBefore(line, " ");
            Set<String> ports = Stream.of(StringUtils.substringAfter(line, " ").split(",")).map(p -> StringUtils.substringBetween(p, ":", "->")).filter(StringUtils::isNotBlank).map(p -> p.replace(":", "")).sorted().collect(Collectors.toSet());
            map.put(name, ports);
        }
        log.info("######## {}", map);
    }
    
    /**
     * mysql5 0.0.0.0:3306->3306/tcp, 33060/tcp, 0.0.0.0:13306->3306/tcp<br/>
     * 
     * 获取docker相关信息
     * 
     * @throws IOException
     */
    @Test
    public void printPorts2()
        throws IOException
    {
        Map<String, Set<String>> map = new TreeMap<>();
        ShellExecutor.execute2(dockerCmd)
            .stream()
            .map(line -> Collections.singletonMap(StringUtils.substringBefore(line, " "),
                Stream.of(StringUtils.substringAfter(line, " ").split(",")).map(p -> StringUtils.substringBetween(p, ":", "->")).filter(StringUtils::isNotBlank).map(p -> p.replace(":", "")).sorted().collect(Collectors.toSet())))
            .forEach(it -> map.putAll(it));
        log.info("######## {}", map);
    }
    
    @Test
    public void printPorts3()
        throws IOException
    {
        List<String> lines;
        try (InputStream in = new ClassPathResource("docker_out.txt").getInputStream())
        {
            lines = IOUtils.readLines(in, StandardCharsets.UTF_8);
        }
        Map<String, Set<String>> map = new TreeMap<>();
        lines.stream()
            .map(line -> Collections.singletonMap(StringUtils.substringBefore(line, " "),
                Stream.of(StringUtils.substringAfter(line, " ").split(",")).map(p -> StringUtils.substringBetween(p, ":", "->")).filter(StringUtils::isNotBlank).map(p -> p.replace(":", "")).sorted().collect(Collectors.toSet())))
            .forEach(it -> map.putAll(it));
        log.info("######## {}", map);
    }
}
