# docker compose运行步骤
1. pom引入docker-maven-plugin插件，配置镜像参数
2. 编写Dockerfile文件定义打包步骤
3. 执行 `sh rebuid` 打包镜像
4. 进入docker目录执行 `sh restart.sh|stop.sh` 重启、停止容器

### 其他

#### 1. wait-for测试


```bash
docker run -it -d --name jdk openjdk:8-jre-alpine
docker cp wait-for.sh jdk:/
docker exec -it jdk sh

sh wait-for.sh www.baidu.com:80 -- echo "baidu is up"
```

### 2. 指定网络、固定ip

参考 docker-compose.yml 

### 3. Docker Compose仅适合开发，生产环境请使用Docker Stack或K8S

