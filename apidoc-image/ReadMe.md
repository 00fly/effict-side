# docker运行apidoc


## 一，构建镜像

**构建较慢，单独脚本运行**

```shell
#windows
build.sh

#lunix
sh build.sh

```

## 二，推送镜像

```shell
mvn clean package
```

## 三，运行镜像

```shell
#在src目录运行，扫描src目录在doc目录生成接口文档
docker run --rm -v $(pwd):/home/node/apidoc apidoc/apidoc -i src -o doc

docker run --rm -v $(pwd):/home/node/apidoc registry.cn-shanghai.aliyuncs.com/00fly/apidoc -i src -o doc

```

**待解决：error: EACCES: permission denied, mkdir 'doc/'**

**镜像内用户为node，解决办法如下：**

```shell
#创建node用户，设置密码
adduser node
passwd node

#创建禁止登录用户（推荐）
#－s：指定用户登入后所使用的shell
#－M：不要自动建立用户的登入目录
adduser node -s /sbin/nologin -M

#赋权到目录
chown -R node apidoc-image/
```

**txyun运行失败原因：node用户uid不是1000**

```shell
#查看用户列表，查看用户
cat /etc/passwd
id node

#删除uid为1000的用户xxx,重新添加node
userdel -r xxx
userdel -r node
```

# 非docker运行apidoc

**[apidoc](https://apidocjs.com/) 是一个可以将源代码中的注释直接生成api接口文档的工具,对现有代码无侵入**

## 1.安装
```
#安装apidoc之前要先安装node.js、npm
yum install -y nodejs
yum install -y npm

#安装apidoc
npm install -g apidoc

#验证
apidoc -v
apidoc -h

```
## 2.配置
典型 apidoc.json

```json
{
	"name": "接口文档",
	"version": "1.0.0",
	"description": "接口文档",
	"title": "接口演示文档",
	"url": "http://127.0.0.1:8080",
	"sampleUrl": "http://127.0.0.1:8080",
	"template": {
		"forceLanguage": "zh_cn",
		"showRequiredLabels": true,
		"withCompare": false,
		"withGenerator": true
	}
}

```

## 3.生成文档

```shell
#扫描src目录在doc目录生成接口文档(default)
apidoc
apidoc -i src -o doc

#扫描src目录在apidoc目录生成接口文档
apidoc -i src -o apidoc
```
