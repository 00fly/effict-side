package com.fly.web;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.Data;

@RestController
@RequestMapping("/demo")
public class DemoController
{
    /**
     * @api {post} /knife/post/test post测试
     * @apiName post演示
     * @apiGroup 演示API
     * @apiParam {String} name="msg" 名称
     * @apiParam {String} [value] 取值
     * @apiSuccess {String} name 名称
     * @apiSuccess {String} value 取值
     * 
     */
    @PostMapping("/post/test")
    public OutPutBean test(InputBean input)
    {
        return new OutPutBean();
    }
    
    /**
     * @api {post} /knife/json/test json测试
     * @apiGroup 演示API
     * @apiBody {String} name="msg" 名称
     * @apiBody {String} [value] 取值
     * @apiSuccess {String} name 名称
     * @apiSuccess {String} value 取值
     * 
     */
    @PostMapping("/json/test")
    public OutPutBean jsonTest(@RequestBody InputBean input)
    {
        return new OutPutBean();
    }
}

@Data
class InputBean
{
    private String name;
    
    private String value;
}

@Data
class OutPutBean
{
    private String name;
    
    private String value;
}