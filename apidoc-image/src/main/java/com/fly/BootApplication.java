package com.fly;

import java.io.IOException;

import org.apache.commons.lang3.SystemUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
public class BootApplication implements CommandLineRunner
{
    public static void main(String[] args)
    {
        SpringApplication.run(BootApplication.class, args);
    }
    
    @Override
    public void run(String... args)
        throws IOException
    {
        if (SystemUtils.IS_OS_WINDOWS)
        {
            log.info("★★★★★★★★  now open Browser ★★★★★★★★ ");
            Runtime.getRuntime().exec("cmd /c start /min http://127.0.0.1:8080");
        }
    }
}