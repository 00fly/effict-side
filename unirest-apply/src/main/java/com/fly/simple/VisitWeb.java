package com.fly.simple;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.fly.utils.CmdExecutor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class VisitWeb
{
    static List<String> urls;
    
    /**
     * 静态代码块初始化
     */
    static
    {
        try
        {
            urls = IOUtils.readLines(VisitWeb.class.getClassLoader().getResourceAsStream("urls.txt"), StandardCharsets.UTF_8)
                .stream()
                .filter(line -> StringUtils.isNotBlank(line)) // 过滤空白行
                .collect(Collectors.toList());
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    public static void main(String[] args)
    {
        urls.forEach(line -> CmdExecutor.execute("curl " + line, true));
    }
}
