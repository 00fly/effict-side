# unirest-show

普通java工程，通过unirest向接口发送post请求，接口地址通过命令行传入

如： `java -jar unirest-show-0.0.1.jar --server.url=http://124.71.129.204:8083/post`




## 一、lunix调试
**注意分割符lunix:windows;**

### 1. class文件运行

**支持指定MainClass**

```bash
#!bin/bash
mvn clean package
cd target/classes
java -cp .:../lib/* com.fly.simple.VisitWeb

```

### 2. jar运行

#### ① 一体包
```bash
#默认MainClass
java -jar unirest-apply-0.0.1-jar-with-dependencies.jar

#指定MainClass
java -cp unirest-apply-0.0.1-jar-with-dependencies.jar com.fly.simple.VisitWeb
```

#### ② lib分离包

**保证lib和unirest-apply-0.0.1.jar同级**

```bash
#默认MainClass
java -jar unirest-apply-0.0.1.jar

#指定MainClass
java -cp unirest-apply-0.0.1.jar:lib/* com.fly.simple.VisitWeb
```