package com.fly.socket.sample3;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MyServer
{
    private final static Logger logger = Logger.getLogger(MyServer.class.getName());
    
    public static void main(String[] args)
    {
        try (ServerSocket server = new ServerSocket(10000))
        {
            while (true)
            {
                Socket socket = server.accept();
                invoke(socket);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    
    private static void invoke(final Socket socket)
        throws IOException
    {
        new Thread(() -> {
            try (ObjectInputStream is = new ObjectInputStream(new BufferedInputStream(socket.getInputStream())); ObjectOutputStream os = new ObjectOutputStream(socket.getOutputStream()))
            {
                Object obj = is.readObject();
                User user = (User)obj;
                System.out.println("user: " + user.getName() + "/" + user.getPassword());
                
                user.setName(user.getName() + "_new");
                user.setPassword(user.getPassword() + "_new");
                
                os.writeObject(user);
                os.flush();
            }
            catch (IOException | ClassNotFoundException ex)
            {
                logger.log(Level.SEVERE, null, ex);
            }
        }).start();
    }
}