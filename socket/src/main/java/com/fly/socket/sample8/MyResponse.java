package com.fly.socket.sample8;

import java.io.Serializable;

public interface MyResponse extends Serializable
{
    
    Object getResult();
}
