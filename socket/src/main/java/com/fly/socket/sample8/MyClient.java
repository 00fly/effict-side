package com.fly.socket.sample8;

public interface MyClient
{
    
    public <T> T execute(MyRequest request, MyResponseHandler<T> handler);
    
    public MyResponse execute(MyRequest request);
    
}
