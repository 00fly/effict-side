package com.fly.socket.sample8;

public interface MyResponseHandler<T>
{
    T handle(MyResponse response);
}
