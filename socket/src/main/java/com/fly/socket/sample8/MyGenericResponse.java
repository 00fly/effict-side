package com.fly.socket.sample8;

public class MyGenericResponse implements MyResponse
{
    
    private Object obj = null;
    
    public MyGenericResponse(Object obj)
    {
        this.obj = obj;
    }
    
    @Override
    public Object getResult()
    {
        return obj;
    }
}
