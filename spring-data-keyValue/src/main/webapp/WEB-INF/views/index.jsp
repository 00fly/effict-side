<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<style>
body {
	margin: 10;
	font-size: 62.5%;
	line-height: 1.5;
}

.blue-button {
	background: #25A6E1;
	padding: 3px 20px;
	color: #fff;
	font-size: 10px;
	border-radius: 2px;
	-moz-border-radius: 2px;
	-webkit-border-radius: 4px;
	border: 1px solid #1A87B9
}

table {
	width: 70%;
}

th {
	background: SteelBlue;
	color: white;
}

td, th {
	border: 1px solid gray;
	font-size: 12px;
	text-align: left;
	padding: 5px 10px;
	overflow: hidden;
	white-space: nowrap;
	text-overflow: ellipsis;
	max-width: 200px;
	white-space: nowrap;
	text-overflow: ellipsis;
	text-overflow: ellipsis;
}
</style>
</head>
<title>00fly功能演示</title>
<body>
    <table>
	  <tr>
		<th colspan="${fn:length(urls)+3}">Navigate</th>
	  </tr>
	  <tr>
		<c:forEach var="item" items="${urls}">
			<td><a href="${pageContext.request.contextPath}${item}/">${item}</a></td>
		</c:forEach>
			<td><a href="/${pageContext.request.contextPath}404">404</a></td>
			<td><a target="_blank" href="/${pageContext.request.contextPath}doc.html">doc.html</a></td>
	  </tr>
    </table>
	<table>
		<tr>
			<th colspan="2">Docker Application Links</th>
		</tr>
		<c:forEach var="item" items="${map}">
			<c:set var="name" value="${item.key}" />
			<tr>
				<td>${name}</td>
				<td><c:forEach var="port" items="${item.value}">
						<a target="_blank" href="${baseUrl}:${port}/">${port}</a>&nbsp;&nbsp;&nbsp;&nbsp;
	      </c:forEach></td>
			</tr>
		</c:forEach>
		<tr>
			<th colspan="2">Controller Links</th>
		</tr>
		<c:forEach var="item" items="${urlMap}">
			<c:set var="name" value="${item.key}" />
			<tr>
				<td>${name}</td>
				<td><c:forEach var="url" items="${item.value}">
						<a target="_blank" href="${url}">${url}</a>
						<br />
					</c:forEach></td>
			</tr>
		</c:forEach>

	</table>
</body>
</html>
