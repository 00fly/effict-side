<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<style>
body {
	margin: 10;
	font-size: 62.5%;
	line-height: 1.5;
}

.blue-button {
	background: #25A6E1;
	padding: 3px 20px;
	color: #fff;
	font-size: 10px;
	border-radius: 2px;
	-moz-border-radius: 2px;
	-webkit-border-radius: 4px;
	border: 1px solid #1A87B9
}

table {
	width: 70%;
}

th {
	background: SteelBlue;
	color: white;
}

td, th {
	border: 1px solid gray;
	font-size: 12px;
	text-align: left;
	padding: 5px 10px;
	overflow: hidden;
	white-space: nowrap;
	text-overflow: ellipsis;
	max-width: 200px;
	white-space: nowrap;
	text-overflow: ellipsis;
	text-overflow: ellipsis;
}
</style>
</head>
<title>00fly功能演示</title>
<script type="text/javascript">
function formReset() {
	window.top.location.href = "${pageContext.request.contextPath}/user/";
}
</script>
<body>
	<center>
    <table>
	  <tr>
		<th colspan="${fn:length(urls)+3}">Navigate</th>
	  </tr>
	  <tr>
		<c:forEach var="item" items="${urls}">
			<td><a href="${pageContext.request.contextPath}${item}/">${item}</a></td>
		</c:forEach>
			<td><a href="/${pageContext.request.contextPath}404">404</a></td>
			<td><a target="_blank" href="/${pageContext.request.contextPath}doc.html">doc.html</a></td>
	  </tr>
    </table>    
	<form:form method="post" modelAttribute="item" action="${pageContext.request.contextPath}/user/add">
		<table>
			<tr>
				<th colspan="3">Add or Edit Item</th>
				<form:hidden path="userId" />
			</tr>
			<tr>
				<td><form:label path="userName">userName:</form:label></td>
				<td><form:input path="userName" size="30" maxlength="30"></form:input></td>
				<td>String <form:errors path="userName" cssStyle="color:red" /></td>
			</tr>
			<tr>
				<td><form:label path="age">age:</form:label></td>
				<td><form:input path="age" size="30" maxlength="30"></form:input></td>
				<td>Integer <form:errors path="age" cssStyle="color:red" /></td>
			</tr>
			<tr>
				<td><form:label path="desc">desc:</form:label></td>
				<td><form:input path="desc" size="30" maxlength="30"></form:input></td>
				<td>String <form:errors path="desc" cssStyle="color:red" /></td>
			</tr>
			<tr>
				<td colspan="3" style="text-align: center;"><input type="submit" class="blue-button" /> <input type="reset" class="blue-button" onclick="formReset()" /></td>
			</tr>
		</table>
	</form:form>
	<h3>Data List</h3>
	<c:if test="${!empty page.content}">
			<table>
				<tr>
					<td align="left">&nbsp;共 <b><font color="red">${page.totalElements}</font> </b>条记录- 共 <b><font color="red">${page.totalPages }</font> </b>页 - 这是第 <b> <font
							color="red">${page.number+1}</font>
					</b>页&nbsp; | &nbsp; <a href="./list?pageNo=0">首页</a>&nbsp; <a href="./list?pageNo=${page.number-1}">前一页</a>&nbsp; <a
							href="./list?pageNo=${page.number+1 gt page.totalPages-1?page.totalPages-1:page.number+1}">后一页</a>&nbsp; <a href="./list?pageNo=${page.totalPages-1}">末页</a>&nbsp;
					</td>
				</tr>
			</table>
			<table>
				<tr>
				<th width="5">序号</th>
				<th width="5">Edit</th>
				<th width="5">Delete</th>
				<th width="100">userId</th>
				<th width="150">userName</th>
				<th width="150">age</th>
				<th width="150">desc</th>
			</tr>
			<c:forEach items="${page.content}" var="it" varStatus="vs">
				<tr>
					<td>${page.number*page.size + vs.count}</td>
					<td><a href="<c:url value='/user/update/${it.userId}' />">Edit</a></td>
					<td><a href="<c:url value='/user/delete/${it.userId}' />">Delete</a></td>
					<td>${it.userId}</td>
					<td>${it.userName}</td>
					<td>${it.age}</td>
					<td>${it.desc}</td>
				</tr>
			</c:forEach>
		</table>
	</c:if>
	<h3>
		应用端口：<font color="red">${port}</font>
	</h3>
	</center>
</body>
</html>
