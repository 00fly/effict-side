package com.fly.keyvalue.service;

import org.springframework.data.domain.Page;

import com.fly.keyvalue.entity.User;

public interface UserService
{
    void saveOrUpdate(User user);
    
    void delete(Long userId);
    
    User get(Long userId);
    
    Page<User> page();
    
    Page<User> page(int pageNo);
}
