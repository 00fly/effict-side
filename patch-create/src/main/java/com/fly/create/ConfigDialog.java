package com.fly.create;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

public class ConfigDialog extends JDialog
{
    /**
     * 
     */
    private static final long serialVersionUID = -8705354806404364273L;
    
    public ConfigDialog(JFrame owner, String title)
    {
        super(owner, title, true);
        
        // 设置宽高
        setSize(500, 240);
        // 得到当前系统screenSize
        Dimension screenSize = getToolkit().getScreenSize();
        // 得到当前frameSize
        Dimension frameSize = this.getSize();
        
        // 自适应处理
        frameSize.height = Math.min(screenSize.height, frameSize.height);
        frameSize.width = Math.min(screenSize.width, frameSize.width);
        setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
        
        String[][] data = new String[0][2];
        String[] name = {"项目工程目录", "补丁文件存放目录"};
        DefaultTableModel tableModel = new MyTableModel(data, name);
        JTable portalTable = new JTable(tableModel);
        portalTable.getTableHeader().setReorderingAllowed(false);
        portalTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        portalTable.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent event)
            {
                if (event.getSource() instanceof JTable)
                {
                    JTable t = (JTable)event.getSource();
                    t.getSelectedRow();
                }
            }
        });
        JScrollPane scrollPane = new JScrollPane(portalTable);
        scrollPane.setBounds(20, 100, 480, 120);
        add(scrollPane);
        
        setResizable(false);
        setVisible(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }
    
    // 扩展DefaultTableModel类
    class MyTableModel extends DefaultTableModel
    {
        private static final long serialVersionUID = -4520646865448106746L;
        
        public MyTableModel(String[][] data, String[] name)
        {
            super(data, name);
        }
        
        // 禁止编辑
        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex)
        {
            return false;
        }
    }
    
}
