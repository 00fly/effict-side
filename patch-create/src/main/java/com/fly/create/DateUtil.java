package com.fly.create;

import java.text.SimpleDateFormat;

public class DateUtil
{
    private static final String TIMEFORMAT = "yyyy-MM-dd HH:mm:ss";
    
    private DateUtil()
    {
        super();
    }
    
    private static final String getCurrDateTimeStr(String formatStr)
    {
        SimpleDateFormat sdf = new SimpleDateFormat(formatStr);
        return sdf.format(System.currentTimeMillis());
    }
    
    public static final String getCurrDateTimeStr()
    {
        return getCurrDateTimeStr(TIMEFORMAT);
    }
    
    public static final String getDateMark()
    {
        return getCurrDateTimeStr("_MMdd_HH");
    }
}
