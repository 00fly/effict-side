# PatchCreate
为了方便根据svn提交日志分析文件目录自动拷贝出修改文件，自己动手使用swing 编写了该工具。

 具体使用方法请参考工具内帮助菜单。

        1.   支持 web工程目录名为 WebRoot 或 WebContent 补丁创建 

         2.   支持class 、jsp、图片、css、html、jar等补丁文件创建。

         3.   支持带内部类 、嵌套类补丁文件创建。

![输入图片说明](https://gitee.com/uploads/images/2018/0202/225113_c182a911_722815.jpeg "15bd5738-b19d-378c-ab68-62e9f878cb53.jpg")


![输入图片说明](https://gitee.com/uploads/images/2018/0202/225128_7569339a_722815.jpeg "91afcff1-cb37-3f8c-ac8e-068713dfd89e.jpg")


![输入图片说明](https://gitee.com/uploads/images/2018/0202/225141_3bf51a5e_722815.jpeg "907aa4a7-6bb4-302e-b97a-7b1b5bf9b38d.jpg")



![输入图片说明](https://gitee.com/uploads/images/2018/0202/225147_c485b401_722815.jpeg "ca9d9b1e-9a64-31cb-b0dc-22b198aa183b.jpg")


![输入图片说明](https://gitee.com/uploads/images/2018/0202/225154_f680f8b4_722815.jpeg "fa938577-6a3c-3046-89f8-1b50b5cc1cac.jpg")


![输入图片说明](https://gitee.com/uploads/images/2018/0202/225135_765c0e2f_722815.jpeg "832cf6ab-21bc-343a-8443-223d6a6746b6.jpg")

欢迎点击链接加入技术讨论群【Java 爱码少年】：https://jq.qq.com/?_wv=1027&k=4AuWuZu