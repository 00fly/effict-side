# 二维码应用工程


## 一、文件版二维码生成

 1. 选择需要生成二维码图片的原始文本文件，主要包含包含xml、java、yaml、md等
 2. 读取选择的文件文本内容，转换成二维码图片

![文件版二维码](images/qr001.png)

![文件版二维码](images/qr002.png)


## 注意

如JFrame打成jar后，系统图标不显示，请确认在jar根目录下是否存在 /img/icon.gif, 原因可能为resources文件夹设置有问题，eg: Excluded：* 

## 二、文字版二维码生成


![文字版二维码](images/qr003.png)