package com.fly.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.apache.commons.lang3.StringUtils;

import com.fly.utils.QRCodeUtil;

/**
 * 
 * 文本二维码生成简化版本
 * 
 * @author 00fly
 * @version [版本号, 2023年3月3日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class SimpleQrCodeUI extends JFrame
{
    private static final long serialVersionUID = -708209618541039567L;
    
    JTextArea textArea = new JTextArea();
    
    JButton qrButton = new JButton("生 成 二 维 码");
    
    public SimpleQrCodeUI()
        throws HeadlessException
    {
        initComponent();
        
        // 设定用户界面的外观
        try
        {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            SwingUtilities.updateComponentTreeUI(this);
        }
        catch (Exception e)
        {
        }
        
        // 加载图标
        URL imgURL = getClass().getResource("/img/icon.gif");
        if (imgURL != null)
        {
            Image image = getToolkit().createImage(imgURL);
            setIconImage(image);
        }
        this.setTitle("二维码应用工具 V1.0");
        this.setBounds(400, 200, 1200, 550);
        this.setResizable(true);
        this.setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
    /**
     * 组件初始化
     * 
     * @see [类、类#方法、类#成员]
     */
    private void initComponent()
    {
        textArea.setToolTipText("输入需要生成二维码图片的原始文本内容");
        
        // JTextArea不自带滚动条，因此就需要把文本区放到一个滚动窗格中
        JScrollPane scroll = new JScrollPane(textArea);
        scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        
        this.add(scroll, BorderLayout.CENTER);
        this.add(qrButton, BorderLayout.SOUTH);
        qrButton.setPreferredSize(new Dimension(100, 40));
        qrButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                try
                {
                    String content = textArea.getText();
                    if (StringUtils.isNotBlank(content))
                    {
                        content = content.replaceAll("\t", " ").replaceAll("((\r\n)|\n)[\\s\t ]*(\\1)+", "$1");
                        BufferedImage image = QRCodeUtil.createImage(content, null, false);
                        new ShowDialog(image);
                    }
                }
                catch (Exception e1)
                {
                    JOptionPane.showMessageDialog(null, e1.getMessage(), "二维码生成失败", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }
    
    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(() -> new SimpleQrCodeUI());
    }
}
