package com.fly.ui;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;

/**
 * 
 * qr显示弹出窗口
 * 
 * @author 00fly
 * @version [版本号, 2023年3月3日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class ShowDialog extends JDialog
{
    private static final long serialVersionUID = 8010838747205419843L;
    
    public ShowDialog(BufferedImage image)
    {
        super();
        // 加载图标
        URL imgURL = getClass().getResource("/img/icon.gif");
        if (imgURL != null)
        {
            setIconImage(getToolkit().createImage(imgURL));
        }
        setTitle("请扫描二维码");
        setSize(510, 530);
        
        // 自适应居中处理
        Dimension screenSize = getToolkit().getScreenSize();
        Dimension dialogSize = getSize();
        dialogSize.height = Math.min(screenSize.height, dialogSize.height);
        dialogSize.width = Math.min(screenSize.width, dialogSize.width);
        setLocation((screenSize.width - dialogSize.width) / 2, (screenSize.height - dialogSize.height) / 2);
        
        // 生成二维码图片
        add(new JLabel(new ImageIcon(image)));
        
        setVisible(true);
        setResizable(false);
        setAlwaysOnTop(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
}
