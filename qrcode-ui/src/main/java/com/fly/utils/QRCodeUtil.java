package com.fly.utils;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Shape;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Hashtable;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.Result;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class QRCodeUtil
{
    /**
     * 二维码尺寸
     */
    private static final int QRCODE_SIZE = 500;
    
    /**
     * LOGO宽度
     */
    private static final int WIDTH = 60;
    
    /**
     * LOGO高度
     */
    private static final int HEIGHT = 60;
    
    /**
     * 给定内容、图标生成二维码图片
     * 
     * @param content 內容
     * @param imgURL 图标
     * @param needCompress 是否压缩尺寸
     * @return
     * @throws Exception
     * @see [类、类#方法、类#成员]
     */
    public static BufferedImage createImage(String content, URL imgURL, boolean needCompress)
        throws Exception
    {
        Hashtable<EncodeHintType, Object> hints = new Hashtable<>();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        hints.put(EncodeHintType.CHARACTER_SET, StandardCharsets.UTF_8);
        hints.put(EncodeHintType.MARGIN, 1);
        BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, QRCODE_SIZE, QRCODE_SIZE, hints);
        int width = bitMatrix.getWidth();
        int height = bitMatrix.getHeight();
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                image.setRGB(x, y, bitMatrix.get(x, y) ? 0xFF000000 : 0xFFFFFFFF);
            }
        }
        if (imgURL == null)
        {
            return image;
        }
        // 插入图片
        insertImage(image, imgURL, needCompress);
        return image;
    }
    
    private static void insertImage(BufferedImage source, URL imgURL, boolean needCompress)
        throws Exception
    {
        if (imgURL == null)
        {
            System.err.println("文件不存在！");
            return;
        }
        Image src = ImageIO.read(imgURL);
        int width = src.getWidth(null);
        int height = src.getHeight(null);
        if (needCompress)
        {
            // 压缩LOGO
            width = Math.min(width, WIDTH);
            height = Math.min(height, HEIGHT);
            Image image = src.getScaledInstance(width, height, Image.SCALE_SMOOTH);
            BufferedImage tag = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics g = tag.getGraphics();
            g.drawImage(image, 0, 0, null);
            g.dispose();
            src = image;
        }
        // 插入LOGO
        Graphics2D graph = source.createGraphics();
        int x = (QRCODE_SIZE - width) / 2;
        int y = (QRCODE_SIZE - height) / 2;
        graph.drawImage(src, x, y, width, height, null);
        Shape shape = new RoundRectangle2D.Float(x, y, width, width, 6, 6);
        graph.setStroke(new BasicStroke(3f));
        graph.draw(shape);
        graph.dispose();
    }
    
    /**
     * 解析二维码图
     * 
     * @param file
     * @return
     * @throws Exception
     * @see [类、类#方法、类#成员]
     */
    public static String decode(File file)
        throws Exception
    {
        BufferedImage image = ImageIO.read(file);
        if (image == null)
        {
            return null;
        }
        BufferedImageLuminanceSource source = new BufferedImageLuminanceSource(image);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        Hashtable<DecodeHintType, String> hints = new Hashtable<>();
        hints.put(DecodeHintType.CHARACTER_SET, StandardCharsets.UTF_8.name());
        Result result = new MultiFormatReader().decode(bitmap, hints);
        return result.getText();
    }
    
    /**
     * 解析二维码图
     * 
     * @param path
     * @return
     * @throws Exception
     * @see [类、类#方法、类#成员]
     */
    public static String decode(String path)
        throws Exception
    {
        return decode(new File(path));
    }
}