package com.fly.core.utils;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fly.demo.entity.Article;
import com.fly.demo.entity.BlogData;

import lombok.extern.slf4j.Slf4j;

/**
 * JsonBeanUtils测试
 */
@Slf4j
public class JsonBeanUtilsTest
{
    static String jsonText;
    
    static Properties properties;
    
    @BeforeAll
    public static void init()
    {
        try
        {
            jsonText = ShellExecutor.execute("curl -e \"blog.csdn.net\" \"https://blog.csdn.net/community/home-api/v1/get-business-list?page=1&size=20&businessType=blog&username=qq_16127313\"");
            properties = PropertiesLoaderUtils.loadProperties(new ClassPathResource("jdbc-h2.properties"));
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @Test
    public void propsTest()
        throws IOException
    {
        String propsJson = JsonBeanUtils.beanToJson(properties);
        log.info("propsJson : {} ", propsJson);
        log.info("properties: {} ", JsonBeanUtils.jsonToBean(propsJson, Properties.class));
    }
    
    @Test
    public void beanToJson001()
        throws IOException
    {
        BlogData blogData = JsonBeanUtils.jsonToBean(jsonText, BlogData.class, true);
        log.info("blogData: {} ", blogData);
        log.info("blogData: {} ", JsonBeanUtils.beanToJson(blogData));
        log.info("blogData.data: {} ", JsonBeanUtils.beanToJson(blogData.getData()));
        log.info("blogData.data.list: {} ", JsonBeanUtils.beanToJson(blogData.getData().getList()));
    }
    
    @Test
    public void beanToJson002()
        throws IOException
    {
        BlogData blogData = JsonBeanUtils.jsonToBean(jsonText, BlogData.class, true);
        String jsonText = JsonBeanUtils.beanToJson(blogData);
        log.info("blogData: {} ", blogData);
        log.info("jsonText: {} ", jsonText);
    }
    
    /**
     * 测试 JsonBeanUtils.jsonToBean(String jsonText, JavaType javaType)
     * 
     * @throws IOException
     */
    @Test
    public void jsonToBean001()
        throws IOException
    {
        BlogData blogData = JsonBeanUtils.jsonToBean(jsonText, BlogData.class, true);
        String jsonStr = JsonBeanUtils.beanToJson(blogData.getData().getList());
        
        // List<SubList>
        JavaType javaType = TypeFactory.defaultInstance().constructParametricType(List.class, Article.class);
        List<Article> dataEntitys = JsonBeanUtils.jsonToBean(jsonStr, javaType);
        log.info("dataEntitys: {} ", dataEntitys);
    }
    
    /**
     * 测试 JsonBeanUtils.jsonToBean(String jsonText, TypeReference<T> typeRef)
     * 
     * @throws IOException
     */
    @Test
    public void jsonToBean002()
        throws IOException
    {
        BlogData blogData = JsonBeanUtils.jsonToBean(jsonText, BlogData.class, true);
        String jsonStr = JsonBeanUtils.beanToJson(blogData.getData().getList());
        
        // List<SubList>
        TypeReference<List<Article>> typeRef = new TypeReference<List<Article>>()
        {
        };
        List<Article> dataEntitys = JsonBeanUtils.jsonToBean(jsonStr, typeRef);
        log.info("dataEntitys: {} ", dataEntitys);
    }
}
