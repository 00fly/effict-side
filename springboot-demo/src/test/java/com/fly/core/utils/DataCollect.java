package com.fly.core.utils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;

import com.fly.demo.entity.Article;
import com.fly.demo.entity.BlogData;
import com.fly.demo.entity.Record;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DataCollect
{
    @Test
    public void apiDataInit()
        throws IOException
    {
        List<Article> articles = IntStream.rangeClosed(1, 2)
            .mapToObj(i -> String.format("curl -e \"blog.csdn.net\" \"https://blog.csdn.net/community/home-api/v1/get-business-list?page=%s&size=100&businessType=blog&username=qq_16127313\"", i))
            .map(cmd -> ShellExecutor.execute(cmd))
            .map(json -> parseToArticles(json))
            .flatMap(List::stream)
            .collect(Collectors.toList());
        BlogData blogData = new BlogData().setData(new Record().setList(articles));
        FileUtils.writeStringToFile(new File("data.json"), JsonBeanUtils.beanToJson(blogData, true), Charset.defaultCharset(), false);
        ShellExecutor.exec("start .");
    }
    
    /**
     * 解析json为List
     * 
     * @param json
     * @return
     */
    private List<Article> parseToArticles(String json)
    {
        try
        {
            return JsonBeanUtils.jsonToBean(json, BlogData.class, true).getData().getList();
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
            return Collections.emptyList();
        }
    }
}
