package com.fly.core;

import java.net.URL;

import org.junit.jupiter.api.Test;
import org.springframework.util.ResourceUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProtocolTest
{
    /**
     * 判断是否在Jar内运行相关API
     */
    @Test
    public void test()
    {
        URL url = getClass().getProtectionDomain().getCodeSource().getLocation();
        log.info("{}", url);
        log.info("    isJarURL: {}", ResourceUtils.isJarURL(url));
        log.info("   isFileURL: {}", ResourceUtils.isFileURL(url));
        log.info("isJarFileURL: {}", ResourceUtils.isJarFileURL(url));
        
        url = getClass().getResource("");
        log.info("{}", url);
        log.info("    isJarURL: {}", ResourceUtils.isJarURL(url));
        log.info("   isFileURL: {}", ResourceUtils.isFileURL(url));
        log.info("isJarFileURL: {}", ResourceUtils.isJarFileURL(url));
    }
}
