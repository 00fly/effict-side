package com.fly.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import com.fly.demo.entity.Article;
import com.fly.demo.service.init.DataInitor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest(args = "--knife4j.enable=false", webEnvironment = WebEnvironment.NONE)
public class DataInitTest
{
    @Autowired
    List<DataInitor> dataInitors;
    
    @BeforeEach
    public void before()
    {
        AtomicInteger count = new AtomicInteger();
        dataInitors.stream().forEach(d -> log.info("{}. {}", count.incrementAndGet(), d));
    }
    
    @Test
    public void testStream()
    {
        // lambda写法, 串行流至少有一个DataInitor执行init成功
        List<Article> articles = new ArrayList<>();
        dataInitors.stream()
            .peek(d -> log.info("{}", d.getClass().getName())) // debug
            .anyMatch(d -> d.init(articles));
        log.info("############## articles.size: {} ", articles.size());
    }
    
    @Test
    public void testCommon()
    {
        // 传统写法
        List<Article> articles = new ArrayList<>();
        for (DataInitor dataInitor : dataInitors)
        {
            log.info("{}", dataInitor.getClass().getName());
            if (dataInitor.init(articles))
            {
                log.info("############## articles.size: {} ", articles.size());
                return;
            }
        }
    }
}
