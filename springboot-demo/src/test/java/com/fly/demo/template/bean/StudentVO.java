package com.fly.demo.template.bean;

import java.util.Date;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class StudentVO
{
    private Long id;
    
    /**
     * 支持stu_name、StuName对应数据库表字段stu_name
     */
    private String stuName;
    
    private Date time;
}
