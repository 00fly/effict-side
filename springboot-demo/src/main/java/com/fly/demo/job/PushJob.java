package com.fly.demo.job;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fly.demo.service.DataService;
import com.fly.demo.sse.SSEServer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class PushJob
{
    @Autowired
    DataService dataService;
    
    @Scheduled(initialDelay = 1000, fixedRate = 1000)
    public void run()
    {
        try
        {
            long index = (System.currentTimeMillis() / 1000) % 21;
            SSEServer.batchSendMessage(index * 5);
            if (index < 1)
            {
                List<String> urls = dataService.getUrls();
                SSEServer.batchSendMessage("json", urls.get(RandomUtils.nextInt(0, urls.size())));
            }
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
}
