package com.fly.demo.service.thread;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.Callable;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import com.fly.core.utils.SpringContextUtils;

import reactor.core.publisher.Mono;

/**
 * 带返回值的ping接口检查线程
 */
public class PingCallable implements Callable<String>
{
    private String ip;
    
    public PingCallable(String ip)
    {
        super();
        this.ip = ip;
    }
    
    @Override
    public String call()
    {
        Mono<String> mono = SpringContextUtils.getBean(WebClient.class)
            .get()
            .uri(uriBuilder -> uriBuilder.scheme("http").host(ip).port("2375").path("/_ping").build())// URI
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToMono(String.class);
        return StringUtils.equals("OK", mono.block()) ? ip : null;
    }
}
