package com.fly.demo.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;

/**
 * DataService
 */
@Slf4j
@Service
public class DataService
{
    @Autowired
    private WebClient webClient;
    
    /**
     * 获取url数据列表
     * 
     * @return
     * @throws IOException
     */
    @Cacheable(cacheNames = "data", key = "'urls'", sync = true)
    public List<String> getUrls()
        throws IOException
    {
        log.info("★★★★★★★★ getData from webApi ★★★★★★★★");
        String resp = webClient.get().uri("https://00fly.online/upload/data.json").acceptCharset(StandardCharsets.UTF_8).accept(MediaType.APPLICATION_JSON).retrieve().bodyToMono(String.class).block();
        String jsonBody = resp.replace("{", "{\n").replace("}", "}\n").replace(",", ",\n");
        try (InputStream is = new ByteArrayInputStream(jsonBody.getBytes(StandardCharsets.UTF_8)))
        {
            return IOUtils.readLines(is, StandardCharsets.UTF_8)
                .stream()
                .filter(line -> StringUtils.contains(line, "\"url\""))
                .map(line -> StringUtils.substringBetween(StringUtils.deleteWhitespace(line), ":\"", "\"")) // 清理空白字符后截取
                .collect(Collectors.toList());
        }
    }
}