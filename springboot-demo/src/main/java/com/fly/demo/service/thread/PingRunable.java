package com.fly.demo.service.thread;

import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import com.fly.core.utils.SpringContextUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * ping接口检查线程
 */
@Slf4j
public class PingRunable implements Runnable
{
    private String ip;
    
    private List<String> serverIp;
    
    public PingRunable(String ip, List<String> serverIp)
    {
        super();
        this.ip = ip;
        this.serverIp = serverIp;
    }
    
    @Override
    public void run()
    {
        SpringContextUtils.getBean(WebClient.class)
            .get()
            .uri(uriBuilder -> uriBuilder.scheme("http").host(ip).port("2375").path("/_ping").build())// URI
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToMono(String.class)
            .subscribe(resp -> {
                log.info("serverIp: {} ===> resp: {}", ip, resp);
                if (StringUtils.equals("OK", resp))
                {
                    serverIp.add(ip);
                }
            });
    }
}