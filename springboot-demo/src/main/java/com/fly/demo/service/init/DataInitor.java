package com.fly.demo.service.init;

import java.util.List;

import com.fly.demo.entity.Article;

/**
 * 数据初始化
 */
public interface DataInitor
{
    /**
     * 执行初始化
     * 
     * @param articles
     * @return 是否成功
     */
    boolean init(List<Article> articles);
}
