package com.fly.demo.web;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fly.demo.service.NodeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags = "模拟负载均衡")
@RequestMapping("/node")
public class NodeController
{
    @Autowired
    private NodeService nodeService;
    
    @ApiOperation("选择活跃节点")
    @GetMapping("/choose")
    public List<String> choose()
        throws InterruptedException
    {
        return nodeService.choose();
    }
    
    /**
     * ExecutorService invokeAll实现
     * 
     * @return
     * @throws InterruptedException
     */
    @ApiOperation("invokeAll")
    @GetMapping("/invokeAll")
    public List<String> invokeAll()
        throws InterruptedException
    {
        return nodeService.invokeAll();
    }
    
    /**
     * ExecutorService invokeAny实现
     * 
     * @return
     * @throws InterruptedException
     * @throws ExecutionException
     * @throws TimeoutException
     */
    @ApiOperation("invokeAny")
    @GetMapping("/invokeAny")
    public String invokeAny()
        throws InterruptedException, ExecutionException, TimeoutException
    {
        return nodeService.invokeAny();
    }
}
