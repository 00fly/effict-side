package com.fly.demo.web;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fly.demo.JsonResult;
import com.fly.demo.service.UserService;
import com.fly.demo.web.convert.DateEditor;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

@Api(tags = "用户数据接口")
@RestController
@RequestMapping("/api")
public class ApiController
{
    @Autowired
    UserService userService;
    
    @InitBinder
    public void dateTypeBinder(WebDataBinder webDataBinder)
    {
        // 方式3，自定义日期属性编辑器，支持多种格式 yyyy-MM-dd, yyyy-MM-dd HH:mm:ss
        webDataBinder.registerCustomEditor(Date.class, new DateEditor());
    }
    
    @ApiOperation("用户查询")
    @GetMapping("/query")
    public JsonResult<?> query(@Valid QueryReq queryReq)
    {
        List<Map<String, Object>> list = userService.query(queryReq.getName(), queryReq.getStartTime(), queryReq.getEndTime());
        return JsonResult.success(list);
    }
}

@Data
class QueryReq
{
    @NotBlank(message = "名称不能为空")
    @ApiModelProperty(value = "名称", example = "user")
    String name;
    
    @NotNull(message = "时间不能为空")
    @Past(message = "开始时间必须小于当前时间")
    @ApiModelProperty(value = "开始时间", allowableValues = "2024-07-10,2024-07-10 08:00:00")
    private Date startTime;
    
    @NotNull(message = "时间不能为空")
    @ApiModelProperty(value = "结束时间", allowableValues = "2030-12-30,2030-12-30 23:59:59")
    private Date endTime;
}