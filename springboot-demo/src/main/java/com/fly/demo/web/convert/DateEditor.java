package com.fly.demo.web.convert;

import java.beans.PropertyEditorSupport;
import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 自定义日期属性编辑器
 * 
 * @author 00fly
 * @version [版本号, 2023年11月9日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
public class DateEditor extends PropertyEditorSupport
{
    /**
     * 将字符串转换成Date
     * 
     * @param text
     */
    @Override
    public void setAsText(String text)
    {
        try
        {
            Date date = DateUtils.parseDate(text, "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss");
            setValue(date);
        }
        catch (ParseException e)
        {
            log.error(e.getMessage(), e);
        }
    }
}
