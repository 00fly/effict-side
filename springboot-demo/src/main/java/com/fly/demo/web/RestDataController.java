package com.fly.demo.web;

import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fly.demo.JsonResult;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.google.common.collect.ImmutableMap;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * Swagger 注解演示
 * 
 * @author 00fly
 * @version [版本号, 2021年9月11日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Api(tags = "RestData")
@RestController
@RequestMapping("/data")
@ConditionalOnWebApplication
public class RestDataController
{
    @ApiOperationSupport(order = 10)
    @ApiOperation("get001")
    @GetMapping("/get001")
    public JsonResult<?> get001()
    {
        // 借用MapSqlParameterSource实现chain map
        MapSqlParameterSource map = new MapSqlParameterSource().addValue("k1", "v1").addValue("k2", "v2");
        return JsonResult.success(map.getValues());
    }
    
    @ApiOperationSupport(order = 20)
    @ApiOperation("get002")
    @PostMapping("/get002")
    public JsonResult<?> get002()
    {
        // 借用MapSqlParameterSource实现chain map
        MapSqlParameterSource map = new MapSqlParameterSource("stuName", "jackson").addValue("k1", "v1").addValue("k2", "v2");
        return JsonResult.success(map.getValues());
    }
    
    @ApiOperationSupport(order = 30)
    @ApiOperation("get003")
    @PostMapping("/get003")
    public JsonResult<?> get003()
    {
        // guava
        Map<String, String> immutableMap = new ImmutableMap.Builder<String, String>().put("q1", "java").put("q2", "python").build();
        return JsonResult.success(immutableMap);
    }
}
