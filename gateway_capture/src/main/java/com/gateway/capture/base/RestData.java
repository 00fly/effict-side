package com.gateway.capture.base;

import lombok.Data;

/**
 * @Description:统一返回数据类型
 * @param
 * @author:hutao
 * @mail:hutao_2017@aliyun.com
 * @date:2021年2月23日
 */
@Data
public class RestData<T>
{
    
    private int code;
    
    private String msg;
    
    private T data;
    
    /**
     * @Description:
     * @param
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年2月23日
     */
    public static <T> RestData<T> success()
    {
        RestData<T> response = new RestData<T>();
        response.setCode(200);
        response.setMsg("操作成功");
        return response;
    }
    
    /**
     * @Description:
     * @param
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年2月23日
     */
    public static <T> RestData<T> success(String msg)
    {
        RestData<T> response = new RestData<T>();
        response.setCode(200);
        response.setMsg(msg);
        return response;
    }
    
    /**
     * @Description:
     * @param
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年2月23日
     */
    public static <T> RestData<T> success(int code, String msg)
    {
        RestData<T> response = new RestData<T>();
        response.setCode(code);
        response.setMsg(msg);
        return response;
    }
    
    /**
     * @Description:
     * @param
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年2月23日
     */
    public static <T> RestData<T> error(int code, String msg, T t)
    {
        RestData<T> response = new RestData<T>();
        response.setCode(code);
        response.setMsg(msg);
        response.setData(t);
        return response;
    }
    
    /**
     * @Description:
     * @param
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年2月23日
     */
    public static <T> RestData<T> success(T t)
    {
        RestData<T> response = new RestData<T>();
        response.setCode(200);
        response.setMsg("操作成功");
        response.setData(t);
        return response;
    }
    
    /**
     * @Description:
     * @param
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年2月23日
     */
    public static <T> RestData<T> error()
    {
        RestData<T> response = new RestData<T>();
        response.setCode(500);
        response.setMsg("系统异常");
        return response;
    }
    
    /**
     * @Description:
     * @param
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年2月23日
     */
    public static <T> RestData<T> error(String msg)
    {
        RestData<T> response = new RestData<T>();
        response.setCode(500);
        response.setMsg(msg);
        return response;
    }
    
    /**
     * @Description:
     * @param
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年2月23日
     */
    public static <T> RestData<T> error(int code, String msg)
    {
        RestData<T> response = new RestData<T>();
        response.setCode(code);
        response.setMsg(msg);
        return response;
    }
}
