package com.gateway.capture.config;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * @Description:http RestTemplate 请求配置
 * @author:hutao
 * @mail:hutao_2017@aliyun.com
 * @date:2021年2月23日
 */
@SpringBootConfiguration
public class RestTemplateConfig
{
    @Bean
    public RestTemplate restTemplate()
    {
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(6000);
        factory.setReadTimeout(2000);
        return new RestTemplate(factory);
    }
}