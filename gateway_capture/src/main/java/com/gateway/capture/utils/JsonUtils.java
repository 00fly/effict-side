package com.gateway.capture.utils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import org.springframework.core.io.ClassPathResource;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.ObjectUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * @Description:json解析工具
 * @author:hutao
 * @mail:hutao_2017@aliyun.com
 * @date:2021年3月3日
 */
public class JsonUtils
{
    
    private static ObjectMapper objectMapper = new ObjectMapper();
    
    /**
     * @Description:读取json文件到java对象中
     * @author hutao
     * @mail hutao_2017@aliyun.com
     * @date 2021年2月24日
     */
    public static <T> T readJsonFromClassPath(String path, Class<T> type)
        throws IOException
    {
        ClassPathResource resource = new ClassPathResource(path);
        InputStream inputStream = resource.getInputStream();
        byte[] bdata = FileCopyUtils.copyToByteArray(inputStream);
        String data = new String(bdata, StandardCharsets.UTF_8);
        return objectMapper.readValue(data, type);
    }
    
    /**
     * @Description:读取json字符串到java对象中
     * @author hutao
     * @mail hutao_2017@aliyun.com
     * @date 2021年2月24日
     */
    public static <T> T stringToJavaBean(String strJson, Class<T> type)
        throws IOException
    {
        return objectMapper.readValue(strJson, type);
    }
    
    /**
     * @Description:jackson的ObjectNode节点转json字符
     * @author hutao
     * @mail:hutao_2017@aliyun.com
     * @date 2021年2月24日
     */
    public static String objectNodeToString(ObjectNode jsonObjec)
        throws JsonProcessingException
    {
        if (jsonObjec == null)
        {
            return "";
        }
        return objectMapper.writeValueAsString(jsonObjec);
    }
    
    /**
     * @Description:json字符串转ObjectNode
     * @author hutao
     * @mail:hutao_2017@aliyun.com
     * @date 2021年2月24日
     */
    public static ObjectNode stringToObjectNode(String str)
        throws JsonMappingException, JsonProcessingException
    {
        if (ObjectUtils.isEmpty(str))
        {
            return null;
        }
        return objectMapper.readValue(str, ObjectNode.class);
    }
    
    /**
     * @Description:json字符串转Map
     * @author hutao
     * @mail:hutao_2017@aliyun.com
     * @date 2021年2月24日
     */
    public static Map<String, String> stringToMap(String str)
        throws JsonMappingException, JsonProcessingException
    {
        if (ObjectUtils.isEmpty(str))
        {
            return null;
        }
        return objectMapper.readValue(str, Map.class);
    }
    
    /**
     * @Description:json字符串转List<Map<String, Object>>
     * @author hutao
     * @mail:hutao_2017@aliyun.com
     * @date 2021年2月24日
     */
    public static List<Map<String, Object>> stringToListMap(String str)
        throws JsonMappingException, JsonProcessingException
    {
        if (ObjectUtils.isEmpty(str))
        {
            return null;
        }
        return objectMapper.readValue(str, new TypeReference<List<Map<String, Object>>>()
        {
        });
    }
    
    /**
     * @Description:json字符串转List<Bean>
     * @author LiGuang
     * @date 2021年3月04日
     */
    public static <T> List<T> jsonToListBean(String jsonData, Class<T> beanType)
        throws JsonProcessingException
    {
        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(List.class, beanType);
        List<T> list = objectMapper.readValue(jsonData, javaType);
        return list;
        
    }
    
    /**
     * @Description:
     * @author hutao
     * @mail:hutao_2017@aliyun.com
     * @date 2021年2月24日
     */
    public static <T> String javaBeanToString(Object javaBean)
        throws JsonProcessingException
    {
        if (javaBean == null)
        {
            return "";
        }
        return new ObjectMapper().writeValueAsString(javaBean);
    }
    
    /**
     * @Description:读取json文件
     * @author:LiGuang
     * @date:2021-03-03 19:52
     */
    public static String readFile(InputStream inputStream)
        throws IOException
    {
        byte[] data = FileCopyUtils.copyToByteArray(inputStream);
        String str = new String(data, StandardCharsets.UTF_8);
        return str;
    }
    
}
