package com.gateway.capture.utils;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import com.sun.management.OperatingSystemMXBean;

/**
 * @Description:操作系统工具类
 * @author:hutao
 * @mail:hutao_2017@aliyun.com
 * @date:2021年9月13日
 */
public class OsUtils
{
    /**
     * @Description:获取操作系统的MAC地址
     * @param
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年9月10日
     */
    public static String getLocalMac()
    {
        try
        {
            InetAddress ia = InetAddress.getLocalHost();
            // 获取网卡，获取地址
            byte[] mac = NetworkInterface.getByInetAddress(ia).getHardwareAddress();
            StringBuffer sb = new StringBuffer("");
            for (int i = 0; i < mac.length; i++)
            {
                if (i != 0)
                {
                    sb.append(":");
                }
                // 字节转换为整数
                int temp = mac[i] & 0xff;
                String str = Integer.toHexString(temp);
                if (str.length() == 1)
                {
                    sb.append("0" + str);
                }
                else
                {
                    sb.append(str);
                }
            }
            String localMAC = sb.toString().toUpperCase();
            return localMAC;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * @Description:获取操作系统的运行内存
     * @param
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年9月13日
     */
    public static long getRamTotal()
    {
        OperatingSystemMXBean mem = (OperatingSystemMXBean)ManagementFactory.getOperatingSystemMXBean();
        return mem.getTotalPhysicalMemorySize() / 1024 / 1024 / 1024;
    }
    
    /**
     * @Description:获取操作系统的硬盘容量，单位:G
     * @param
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年9月13日
     */
    public static long getDiskTotal()
    {
        long sum = 0;
        File[] disks = File.listRoots();
        for (File file : disks)
        {
            sum += file.getTotalSpace() / 1024 / 1024 / 1024;
        }
        return sum;
    }
    
    /**
     * @Description:将Mac地址的数组形式转换为字符串形式 (适用于线程安全的情况下)
     * @param
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年9月9日
     */
    public static String macBytesToString(byte[] macBytes)
    {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < macBytes.length; i++)
        {
            String hexString = Integer.toHexString(0xFF & macBytes[i]);
            if (hexString.length() < 2)
            {
                hexString = "0" + hexString;
            }
            builder.append(':').append(hexString);
        }
        return builder.substring(1).toUpperCase();
    }
    
    public static String getHostAddress()
    {
        Enumeration<NetworkInterface> nis;
        String ip = null;
        try
        {
            nis = NetworkInterface.getNetworkInterfaces();
            for (; nis.hasMoreElements();)
            {
                NetworkInterface ni = nis.nextElement();
                Enumeration<InetAddress> ias = ni.getInetAddresses();
                for (; ias.hasMoreElements();)
                {
                    InetAddress ia = ias.nextElement();
                    // ia instanceof Inet6Address && !ia.equals("")
                    if (ia instanceof Inet4Address && !ia.getHostAddress().equals("127.0.0.1"))
                    {
                        ip = ia.getHostAddress();
                    }
                }
            }
        }
        catch (SocketException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return ip;
    }
}
