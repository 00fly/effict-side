package com.gateway.capture.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;

/**
 * @Description:对象复制工具
 * @author:hutao
 * @mail:hutao_2017@aliyun.com
 * @date:2021年4月16日
 */
public class CopyUtils
{
    
    /**
     * @Description:将Source对象复制到Target对象
     * @author:hutao
     * @return
     * @mail:hutao_2017@aliyun.com
     * @date:2021年4月16日
     */
    public static <T> T copyBean(Object source, Class<T> targetClazz)
    {
        T target = null;
        try
        {
            target = targetClazz.newInstance();
            BeanUtils.copyProperties(source, target);
        }
        catch (InstantiationException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IllegalAccessException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return target;
    }
    
    /**
     * @Description:将List<Source> 集合变成 List<target>集合
     * @param
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年4月16日
     */
    public static <T> List<T> copyList(List<?> listSource, Class<T> targetClazz)
    {
        if (listSource == null)
        {
            return new ArrayList<>();
        }
        List<T> listTarget = new ArrayList<>();
        listSource.forEach(temp -> {
            T newInstance;
            try
            {
                newInstance = targetClazz.newInstance();
                BeanUtils.copyProperties(temp, newInstance);
                listTarget.add(newInstance);
            }
            catch (InstantiationException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (IllegalAccessException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        });
        return listTarget;
    }
    
    /**
     * @Description:将Map中属性复制到对象中（key-> class的属性名相同）
     * @param
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年9月13日
     */
    public static <T> T copyMapToObject(Map<String, String> map, Class<T> targetClazz)
    {
        T target = null;
        if (map == null)
        {
            return target;
        }
        try
        {
            target = targetClazz.newInstance();
            Field[] fields = target.getClass().getDeclaredFields();
            for (Field field : fields)
            {
                int mod = field.getModifiers();
                if (Modifier.isStatic(mod) || Modifier.isFinal(mod))
                {
                    continue;
                }
                field.setAccessible(true);
                field.set(target, map.get(field.getName()));
            }
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return target;
    }
}
