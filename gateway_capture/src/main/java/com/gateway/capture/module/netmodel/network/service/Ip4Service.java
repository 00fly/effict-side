package com.gateway.capture.module.netmodel.network.service;

import org.jnetpcap.packet.PcapPacket;

import com.gateway.capture.module.netmodel.network.pojo.CustomIp4;

/**
 * @Description:
 * @author:hutao
 * @mail:hutao_2017@aliyun.com
 * @date:2021年9月13日
 */
public interface Ip4Service
{
    /**
     * @Description:提取IP4协议信息
     * @param
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年9月10日
     */
    public CustomIp4 extractIp4Info(PcapPacket packet);
}
