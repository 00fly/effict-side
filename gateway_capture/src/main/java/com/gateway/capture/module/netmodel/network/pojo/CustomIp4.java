package com.gateway.capture.module.netmodel.network.pojo;

import lombok.Data;

/**
 * @Description:网络传输层IP信息 IPv4是一种无连接的协议，操作在使用分组交换的链路层（如以太网）上。
 *                        <P>
 *                        此协议会尽最大努力交付数据包，意即它不保证任何数据包均能送达目的地，也不保证所有数据包均按照正确的顺序无重复地到达。
 *                        <P>
 *                        这些方面是由上层的传输协议（如传输控制协议）处理的。
 * @author:hutao
 * @mail:hutao_2017@aliyun.com
 * @date:2021年9月9日
 */
@Data
public class CustomIp4
{
    /**
     * 协议类型
     */
    private int version;
    
    /**
     * 协议类型描述
     */
    private String versionDes;
    
    /**
     * 目的地址 MAC地址
     */
    private String destination;
    
    /**
     * 源地址 MAC地址
     */
    private String source;
}
