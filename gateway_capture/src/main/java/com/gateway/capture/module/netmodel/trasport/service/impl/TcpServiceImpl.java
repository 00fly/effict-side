package com.gateway.capture.module.netmodel.trasport.service.impl;

import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.protocol.tcpip.Tcp;
import org.springframework.stereotype.Service;

import com.gateway.capture.module.netmodel.trasport.pojo.CustomTcp;
import com.gateway.capture.module.netmodel.trasport.service.TcpService;

@Service
public class TcpServiceImpl implements TcpService
{
    
    /**
     * @Description:提取TCP信息
     * @param
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年9月13日
     */
    @Override
    public CustomTcp extractTcpInfo(PcapPacket packet)
    {
        if (!packet.hasHeader(Tcp.ID))
        {
            return null;
        }
        Tcp tcp = packet.getHeader(new Tcp());
        CustomTcp tcpInfo = new CustomTcp();
        tcpInfo.setSourcePort(tcp.source());
        tcpInfo.setDestinationPort(tcp.destination());
        return tcpInfo;
    }
}
