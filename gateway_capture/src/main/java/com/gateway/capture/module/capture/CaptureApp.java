package com.gateway.capture.module.capture;

import java.util.List;

import org.jnetpcap.PcapIf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.gateway.capture.module.capture.service.CaptureService;

/**
 * @Description:抓包程序
 * @author:hutao
 * @mail:hutao_2017@aliyun.com
 * @date:2021年9月13日
 */
@Order(value = 2)
@Component
public class CaptureApp implements ApplicationRunner
{
    
    @Autowired
    private CaptureService captureService;
    
    @Override
    public void run(ApplicationArguments args)
        throws Exception
    {
        List<PcapIf> pcapIf = captureService.getPcapIf();
        captureService.capturePcap(pcapIf.get(0));
    }
}