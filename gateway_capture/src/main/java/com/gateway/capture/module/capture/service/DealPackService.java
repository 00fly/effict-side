package com.gateway.capture.module.capture.service;

import org.jnetpcap.packet.PcapPacket;

/**
 * @Description:处理数据包
 * @author:hutao
 * @mail:hutao_2017@aliyun.com
 * @date:2021年12月20日
 */
public interface DealPackService
{
    
    /**
     * @Description:处理http数据包
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年12月20日
     */
    void dealPcapPacket(PcapPacket packet);
    
}
