package com.gateway.capture.module.netmodel.datalink.pojo;

import lombok.Data;

/**
 * @Description:数据链路层Ethernet信息 ethernet:以太网是一种计算机局域网技术。IEEE组织的IEEE 802.3标准制定了以太网的技术标准，它规定了包括物理层的连线、电子信号和介质访问层协议的内容
 * @author:hutao
 * @mail:hutao_2017@aliyun.com
 * @date:2021年9月9日
 */
@Data
public class CustomEthernet
{
    
    /**
     * 协议类型
     */
    private int type;
    
    /**
     * 协议类型描述
     */
    private String typeDes;
    
    /**
     * 目的地址 MAC地址
     */
    private String destination;
    
    /**
     * 源地址 MAC地址
     */
    private String source;
}
