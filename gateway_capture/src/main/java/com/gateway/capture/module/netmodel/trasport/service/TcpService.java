package com.gateway.capture.module.netmodel.trasport.service;

import org.jnetpcap.packet.PcapPacket;

import com.gateway.capture.module.netmodel.trasport.pojo.CustomTcp;

public interface TcpService
{
    
    /**
     * @Description:提取TCP信息
     * @param
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年9月13日
     */
    CustomTcp extractTcpInfo(PcapPacket packet);
    
}
