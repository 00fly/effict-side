package com.gateway.capture.module.capture.handler;

import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;
import org.jnetpcap.protocol.tcpip.Http;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gateway.capture.module.capture.service.DealPackService;
import com.gateway.capture.module.netmodel.datalink.service.EthernetService;

@Component
public class CustomPcapHandler<T> implements PcapPacketHandler<Object>
{
    
    @Autowired
    private EthernetService ethernetService;
    
    @Autowired
    private DealPackService dealPackService;
    
    @Override
    public void nextPacket(PcapPacket packet, Object user)
    {
        
        if (packet.hasHeader(Http.ID))
        {
            dealPackService.dealPcapPacket(packet);
        }
    }
}
