package com.gateway.capture.module.capture.pojo;

import lombok.Data;

/**
 * @Description:发送给后台系统的数据包
 * @author:hutao
 * @mail:hutao_2017@aliyun.com
 * @date:2021年9月14日
 */
@Data
public class CaptureDataDTO<T>
{
    
    private String protocolType;
    
    private T data;
    
}
