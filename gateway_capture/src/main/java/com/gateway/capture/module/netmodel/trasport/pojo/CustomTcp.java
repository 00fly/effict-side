package com.gateway.capture.module.netmodel.trasport.pojo;

import lombok.Data;

/**
 * @Description:TCP信息
 * @author:hutao
 * @mail:hutao_2017@aliyun.com
 * @date:2021年9月13日
 */
@Data
public class CustomTcp
{
    
    /**
     * 当前操作系统执行此进程时分配的端口
     */
    private int sourcePort;
    
    /**
     * 目的地端口号。TCP端口号的作用用于进程寻址依据
     */
    private int destinationPort;
    
}
