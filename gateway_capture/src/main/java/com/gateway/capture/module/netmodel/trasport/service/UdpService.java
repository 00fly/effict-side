package com.gateway.capture.module.netmodel.trasport.service;

import org.jnetpcap.packet.PcapPacket;

import com.gateway.capture.module.netmodel.trasport.pojo.CustomUdp;

public interface UdpService
{
    
    CustomUdp extractUdpInfo(PcapPacket packet);
}
