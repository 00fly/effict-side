package com.gateway.capture.module.netmodel.datalink.service;

import org.jnetpcap.packet.PcapPacket;

import com.gateway.capture.module.netmodel.datalink.pojo.CustomEthernet;

/**
 * @Description:
 * @author:hutao
 * @mail:hutao_2017@aliyun.com
 * @date:2021年9月13日
 */
public interface EthernetService
{
    /**
     * @Description:提取数据链路层Ethernet信息中的的协议类型，目标mac地址，源mac地址等信息
     * @param
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年9月9日
     */
    public CustomEthernet extractEthernetInfo(PcapPacket packet);
}
