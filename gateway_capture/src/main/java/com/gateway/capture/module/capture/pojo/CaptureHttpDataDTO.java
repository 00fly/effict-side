package com.gateway.capture.module.capture.pojo;

import com.gateway.capture.module.netmodel.application.pojo.CustomHttp;
import com.gateway.capture.module.netmodel.datalink.pojo.CustomEthernet;
import com.gateway.capture.module.netmodel.network.pojo.CustomIp4;
import com.gateway.capture.module.netmodel.trasport.pojo.CustomTcp;

import lombok.Data;

/**
 * @Description:从数据包中抓取的数据信息
 * @author:hutao
 * @mail:hutao_2017@aliyun.com
 * @date:2021年9月14日
 */
@Data
public class CaptureHttpDataDTO
{
    
    private CustomEthernet ethernetInfo;
    
    private CustomIp4 Ip4info;
    
    private CustomTcp tcpInfo;
    
    private CustomHttp httpInfo;
    
    private int packetSize;
    
}
