package com.gateway.capture.module.netmodel.trasport.pojo;

import lombok.Data;

@Data
public class CustomUdp
{
    /**
     * 当前操作系统执行此进程时分配的端口
     */
    private int sourcePort;
    
    /**
     * 目的地端口号。TCP端口号的作用用于进程寻址依据
     */
    private int destinationPort;
}
