package com.gateway.capture.module.netmodel.trasport.service.impl;

import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.protocol.tcpip.Udp;
import org.springframework.stereotype.Service;

import com.gateway.capture.module.netmodel.trasport.pojo.CustomUdp;
import com.gateway.capture.module.netmodel.trasport.service.UdpService;

@Service
public class UdpServiceIpl implements UdpService
{
    @Override
    public CustomUdp extractUdpInfo(PcapPacket packet)
    {
        if (!packet.hasHeader(Udp.ID))
        {
            return null;
        }
        Udp udp = packet.getHeader(new Udp());
        CustomUdp customUdp = new CustomUdp();
        customUdp.setDestinationPort(udp.destination());
        customUdp.setSourcePort(udp.source());
        return customUdp;
    }
}
