package com.gateway.capture.module.netmodel.network.service.impl;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.protocol.network.Ip4;
import org.springframework.stereotype.Service;

import com.gateway.capture.module.netmodel.network.pojo.CustomIp4;
import com.gateway.capture.module.netmodel.network.service.Ip4Service;

@Service
public class Ip4ServiceImpl implements Ip4Service
{
    
    /**
     * @Description:提取IP4协议信息
     * @param
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年9月10日
     */
    @Override
    public CustomIp4 extractIp4Info(PcapPacket packet)
    {
        if (!packet.hasHeader(Ip4.ID))
        {
            return null;
        }
        Ip4 ip4 = packet.getHeader(new Ip4());
        CustomIp4 customIp = new CustomIp4();
        try
        {
            InetAddress sourceAddress = InetAddress.getByAddress(ip4.source());
            InetAddress destinationAddress = InetAddress.getByAddress(ip4.destination());
            customIp.setDestination(destinationAddress.getHostAddress());
            customIp.setSource(sourceAddress.getHostAddress());
        }
        catch (UnknownHostException e)
        {
            e.printStackTrace();
        }
        customIp.setVersion(ip4.version());
        customIp.setVersionDes(ip4.getDescription());
        return customIp;
    }
}
