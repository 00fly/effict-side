package com.gateway.capture.module.netmodel.datalink.service.impl;

import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.protocol.lan.Ethernet;
import org.springframework.stereotype.Service;

import com.gateway.capture.module.netmodel.datalink.pojo.CustomEthernet;
import com.gateway.capture.module.netmodel.datalink.service.EthernetService;
import com.gateway.capture.utils.OsUtils;

@Service
public class EthernetServiceImpl implements EthernetService
{
    
    /**
     * @Description:提取数据链路层Ethernet信息中的的协议类型，目标mac地址，源mac地址等信息
     * @param
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年9月9日
     */
    @Override
    public CustomEthernet extractEthernetInfo(PcapPacket packet)
    {
        if (!packet.hasHeader(Ethernet.ID))
        {
            return null;
        }
        Ethernet ethernet = packet.getHeader(new Ethernet());
        CustomEthernet customEthernet = new CustomEthernet();
        if (ethernet.typeEnum() != null)
        {
            customEthernet.setType(ethernet.typeEnum().getId());
            customEthernet.setTypeDes(ethernet.typeEnum().getDescription());
            customEthernet.setSource(OsUtils.macBytesToString(ethernet.source()));
            customEthernet.setDestination(OsUtils.macBytesToString(ethernet.destination()));
        }
        return customEthernet;
    }
}
