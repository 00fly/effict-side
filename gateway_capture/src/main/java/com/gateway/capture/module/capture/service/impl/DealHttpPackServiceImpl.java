package com.gateway.capture.module.capture.service.impl;

import org.jnetpcap.packet.PcapPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.gateway.capture.module.capture.pojo.CaptureDataDTO;
import com.gateway.capture.module.capture.pojo.CaptureHttpDataDTO;
import com.gateway.capture.module.capture.service.DealPackService;
import com.gateway.capture.module.netmodel.application.pojo.CustomHttp;
import com.gateway.capture.module.netmodel.application.service.HttpService;
import com.gateway.capture.module.netmodel.datalink.pojo.CustomEthernet;
import com.gateway.capture.module.netmodel.datalink.service.EthernetService;
import com.gateway.capture.module.netmodel.network.pojo.CustomIp4;
import com.gateway.capture.module.netmodel.network.service.Ip4Service;
import com.gateway.capture.module.netmodel.trasport.pojo.CustomTcp;
import com.gateway.capture.module.netmodel.trasport.service.TcpService;

@Service
public class DealHttpPackServiceImpl implements DealPackService
{
    
    private final static Logger logger = LoggerFactory.getLogger(DealHttpPackServiceImpl.class);
    
    @Autowired
    private EthernetService ethernetService;
    
    @Autowired
    private Ip4Service ip4Service;
    
    @Autowired
    private TcpService tcpService;
    
    @Autowired
    private HttpService httpService;
    
    @Autowired
    private RestTemplate restTemplate;
    
    /**
     * @Description:处理http数据包
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年12月20日
     */
    @Override
    public void dealPcapPacket(PcapPacket packet)
    {
        CustomEthernet ethernetInfo = ethernetService.extractEthernetInfo(packet);
        CustomIp4 ip4Info = ip4Service.extractIp4Info(packet);
        CustomTcp tcpInfo = tcpService.extractTcpInfo(packet);
        CustomHttp httpInfo = httpService.extractHttpInfo(packet);
        
        CaptureHttpDataDTO httpData = new CaptureHttpDataDTO();
        httpData.setEthernetInfo(ethernetInfo);
        httpData.setIp4info(ip4Info);
        httpData.setTcpInfo(tcpInfo);
        httpData.setHttpInfo(httpInfo);
        httpData.setPacketSize(packet.size());
        CaptureDataDTO<CaptureHttpDataDTO> captureDataDTO = new CaptureDataDTO<>();
        captureDataDTO.setProtocolType("HTTP");
        captureDataDTO.setData(httpData);
        try
        {
            logger.info("发送的Http数据包为：{}", captureDataDTO);
            System.out.println("抓取的Http数据包为：" + captureDataDTO);
            // ResponseEntity<RestData> res = restTemplate.postForEntity("", captureDataDTO, RestData.class);
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
    }
}
