package com.gateway.capture.module.netmodel.application.pojo;

import lombok.Data;

/**
 * @Description:Http协议参数
 * @author:hutao
 * @mail:hutao_2017@aliyun.com
 * @date:2021年9月13日
 */
@Data
public class CustomHttp
{
    /**
     * 指定客户端能够接收的内容类型 Accept: text/plain, text/html
     */
    private String Accept;
    
    /**
     * 浏览器可以接受的字符编码集。 Accept-Charset: iso-8859-5
     */
    private String Accept_Charset;
    
    /**
     * 指定浏览器可以支持的web服务器返回内容压缩编码类型。 Accept-Encoding: compress, gzip
     */
    private String Accept_Encoding;
    
    /**
     * 可以请求网页实体的一个或者多个子范围字段 Accept-Ranges: bytes
     */
    private String Accept_Ranges;
    
    /**
     * 浏览器可接受的语言 Accept-Language: en,zh
     */
    private String Accept_Language;
    
    /**
     * HTTP授权的授权证书 Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==
     */
    private String Authorization;
    
    /**
     * 指定请求和响应遵循的缓存机制 Cache-Control: no-cache
     */
    private String Cache_Control;
    
    /**
     * 表示是否需要持久连接。（HTTP 1.1默认进行持久连接） Connection: close
     */
    private String Connection;
    
    /**
     * HTTP请求发送时，会把保存在该请求域名下的所有cookie值一起发送给web服务器。 Cookie: $Version=1; Skin=new;
     */
    /**
     * 请求的内容长度 Content-Length: 348
     */
    private String Content_Length;
    
    /**
     * 请求的与实体对应的MIME信息 Content-Type: application/x-www-form-urlencoded
     */
    private String Content_Type;
    
    /**
     * HTTP请求发送时，会把保存在该请求域名下的所有cookie值一起发送给web服务器。 Cookie: $Version=1; Skin=new;
     */
    private String Cookie;
    
    /**
     * 请求发送的日期和时间 Date: Tue, 15 Nov 2010 08:12:31 GMT
     */
    private String Date;
    
    /**
     * 指定请求的服务器的域名和端口号 Host: www.zcmhi.com
     */
    private String Host;
    
    /**
     * 如果内容未改变返回304代码，参数为服务器先前发送的Etag，与服务器回应的Etag比较判断是否改变 If-None-Match: “737060cd8c284d8af7ad3082f209582d”
     */
    private String If_None_Match;
    
    /**
     * 如果请求的部分在指定时间之后被修改则请求成功，未被修改则返回304代码 If-Modified-Since: Sat, 29 Oct 2010 19:43:31 GMT
     */
    private String If_Modified_Since;
    
    private String UA_CPU;
    
    /**
     * 代理连接
     */
    private String Proxy_Connection;
    
    /**
     * 先前网页的地址，当前请求网页紧随其后,即来路 Referer: http://www.zcmhi.com/archives/71.html
     */
    private String Referer;
    
    /**
     * 请求方式
     */
    private String RequestMethod;
    
    /**
     * 请求地址
     */
    private String RequestUrl;
    
    /**
     * 请求版本
     */
    private String RequestVersion;
    
    /**
     * 用户代理
     */
    private String User_Agent;
    
    /**
     * 请求参数
     */
    private Object data;
}
