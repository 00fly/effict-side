package com.gateway.capture.module.netmodel.application.service;

import org.jnetpcap.packet.PcapPacket;

import com.gateway.capture.module.netmodel.application.pojo.CustomHttp;

public interface HttpService
{
    
    /**
     * @Description:提取http中的信息
     * @param
     * @author:hutao
     * @mail:hutao_2017@aliyun.com
     * @date:2021年9月13日
     */
    CustomHttp extractHttpInfo(PcapPacket packet);
    
}
