package com.gateway.capture;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GatewayCaptureApp
{
    
    public static void main(String[] args)
    {
        SpringApplication.run(GatewayCaptureApp.class, args);
    }
}
