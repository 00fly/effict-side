package com.fly.demo;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

@ShellComponent
public class MyCommands
{
    @ShellMethod("Add two integers together.")
    public long add(long a, long b)
    {
        return a + b;
    }
}