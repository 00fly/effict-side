package com.fly.test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fly.RunApplication;
import com.fly.core.XsqlBuilderSQLTemplate;
import com.fly.core.dyn.DynSQL;
import com.fly.core.dyn.SqlXmlUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * Junit 单元测试
 * 
 * @author 00fly
 * @version [版本号, 2018-10-31]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RunApplication.class)
public class XsqlBuilderSQLTemplateTest
{
    @Autowired
    DynSQL dynSQL;
    
    @Autowired
    SqlXmlUtil sqlXmlUtil;
    
    @Autowired
    XsqlBuilderSQLTemplate xSqlTemplate;
    
    @Test
    public void testDynSQL()
    {
        String dynSqlText = dynSQL.getSqlById("demo.sql001");
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("name", "name%");
        params.put("age", 18);
        
        List<Map<String, Object>> list = xSqlTemplate.queryForList(dynSqlText, params);
        log.info("★★★★ execute: result = {}", list);
    }
}
