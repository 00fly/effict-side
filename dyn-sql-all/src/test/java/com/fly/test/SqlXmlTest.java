package com.fly.test;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fly.RunApplication;
import com.fly.common.NamedSqlExecutor;
import com.fly.core.dyn.SqlXmlUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * Junit 单元测试
 * 
 * @author 00fly
 * @version [版本号, 2018-10-31]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RunApplication.class)
public class SqlXmlTest
{
    @Autowired
    NamedSqlExecutor namedSqlExecutor;
    
    @Autowired
    private SqlXmlUtil sqlXmlUtil;
    
    @Test
    public void testSqlXmlUtil()
    {
        try (Scanner sc = new Scanner(System.in))
        {
            String input;
            do
            {
                log.info("------------输入x退出,其他值继续------------");
                input = StringUtils.trimToEmpty(sc.nextLine());
                try
                {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("startDate", defaultOrNull("2017-01-01"));
                    params.put("endDate", defaultOrNull("2018-02-01"));
                    params.put("deptCode", defaultOrNull("001"));
                    params.put("itemName", defaultOrNull("%部门%"));
                    params.put("start", 0);
                    params.put("size", 10);
                    String namedParamSQL = sqlXmlUtil.getSqlById("efficientdata.itemDelayFinishNum", params);
                    log.info("parmas={},sql={}", params, namedParamSQL);
                    // namedSqlExecutor.queryForList(namedParamSQL, params);
                }
                catch (Exception e)
                {
                    log.error(e.getMessage(), e);
                }
            } while (!StringUtils.equalsIgnoreCase(input, "x"));
            log.info("------------成功退出------------");
        }
    }
    
    /**
     * 返回默认值或null
     * 
     * @param defaultValue
     * @return
     * @see [类、类#方法、类#成员]
     */
    private Object defaultOrNull(Object defaultValue)
    {
        if (RandomUtils.nextBoolean())
        {
            return defaultValue;
        }
        if (String.class.isInstance(defaultValue))
        {
            // 字符串, null 或 "" 视为空值
            return RandomUtils.nextBoolean() ? null : "";
        }
        if (List.class.isInstance(defaultValue))
        {
            // List对象, null或 emptyList视为空值
            return RandomUtils.nextBoolean() ? null : Collections.emptyList();
        }
        return null;
    }
}
