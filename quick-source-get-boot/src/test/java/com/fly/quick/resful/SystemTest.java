package com.fly.quick.resful;

import java.util.Enumeration;
import java.util.ResourceBundle;

import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SystemTest
{
    ResourceBundle env = ResourceBundle.getBundle("env");
    
    ResourceBundle prop = ResourceBundle.getBundle("prop");
    
    @Test
    public void printEnv()
    {
        log.info("----------------- printEnv");
        Enumeration<String> e = env.getKeys();
        while (e.hasMoreElements())
        {
            String key = e.nextElement();
            log.info(" {}: {}", env.getObject(key), System.getenv(key));
        }
    }
    
    @Test
    public void printProperty()
    {
        log.info("----------------- printProperty");
        Enumeration<String> e = prop.getKeys();
        while (e.hasMoreElements())
        {
            String key = e.nextElement();
            log.info(" {}: {}", prop.getObject(key), System.getProperty(key));
        }
    }
}
