package com.fly.core.utils;

import java.util.Scanner;

import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExecutorTest
{
    @Test
    public void test1()
        throws Exception
    {
        try (Scanner sc = new Scanner(System.in))
        {
            String value;
            do
            {
                value = sc.nextLine();
                Executor.execute(value).forEach(log::info);
                log.info("------------输入x退出,回车换行继续------------");
            } while (!"x".equalsIgnoreCase(value));
            log.info("------------成功退出------------");
        }
    }
}
