package com.fly.quick.runner;

import java.io.IOException;

import org.apache.commons.lang3.SystemUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fly.core.utils.SpringContextUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@ConditionalOnWebApplication
public class WebStartedRunner
{
    @Value("${server.port}")
    Integer port;
    
    @Bean
    CommandLineRunner run()
    {
        return args -> {
            openBrowser();
        };
    }
    
    private void openBrowser()
        throws IOException
    {
        if (SystemUtils.IS_OS_WINDOWS && port > 0)
        {
            log.info("★★★★★★★★  now open Browser ★★★★★★★★ ");
            Runtime.getRuntime().exec("cmd /c start /min " + SpringContextUtils.getServerBaseURL());
        }
    }
}
