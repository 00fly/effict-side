package com.fly.core.config;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * mvc配置
 * 
 * @author 00fly
 * @version [版本号, 2021年4月23日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@Configuration
@ConditionalOnWebApplication
public class MvcConfiguration
{
    /**
     * 实例化WebMvcConfigurer接口
     *
     * @return
     */
    @Bean
    public WebMvcConfigurer webMvcConfigurer()
    {
        return new WebMvcConfigurer()
        {
            @Override
            public void configureMessageConverters(List<HttpMessageConverter<?>> converters)
            {
                converters.add(this.stringHttpMessageConverter());
                converters.add(this.mappingJackson2HttpMessageConverter());
            }
            
            @Override
            public void configureContentNegotiation(ContentNegotiationConfigurer configurer)
            {
                configurer.defaultContentType(MediaType.APPLICATION_JSON);
                configurer.ignoreUnknownPathExtensions(false);
                configurer.favorPathExtension(true);
                configurer.favorParameter(false);
                Map<String, MediaType> mediaTypes = new HashMap<>(3);
                mediaTypes.put("atom", MediaType.APPLICATION_ATOM_XML);
                mediaTypes.put("html", MediaType.TEXT_HTML);
                mediaTypes.put("json", MediaType.APPLICATION_JSON);
                configurer.mediaTypes(mediaTypes);
            }
            
            @Bean
            public StringHttpMessageConverter stringHttpMessageConverter()
            {
                StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter();
                return stringHttpMessageConverter;
            }
            
            @Bean
            public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter()
            {
                MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
                List<MediaType> list = new ArrayList<>();
                list.add(MediaType.APPLICATION_JSON);
                list.add(MediaType.APPLICATION_XML);
                list.add(MediaType.TEXT_HTML);
                list.add(MediaType.TEXT_PLAIN);
                list.add(MediaType.TEXT_XML);
                messageConverter.setSupportedMediaTypes(list);
                return messageConverter;
            }
            
            /**
             * 等价于mvc中<mvc:view-controller path="/" view-name="redirect:doc.html" /><br>
             * 等价于mvc中<mvc:view-controller path="/index" view-name="index" />
             * 
             * @param registry
             */
            @Override
            public void addViewControllers(ViewControllerRegistry registry)
            {
                registry.addViewController("/").setViewName("redirect:doc.html");
                registry.addViewController("/index").setViewName("index.html");
                registry.addViewController("/in").setViewName("auto.html");
            }
            
            /**
             * 加载静态资源文件或文件映射
             */
            @Override
            public void addResourceHandlers(final ResourceHandlerRegistry registry)
            {
                try
                {
                    File file = new File("./upload/");
                    // file.mkdirs();
                    registry.addResourceHandler("/upload/**").addResourceLocations("file:" + file.getCanonicalPath() + "/");
                }
                catch (IOException e)
                {
                    log.error(e.getMessage(), e);
                }
            }
        };
    }
}
