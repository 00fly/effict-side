package com.fly;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.fly.core.utils.SpringContextUtils;

@SpringBootApplication
public class QuickSrcApplication
{
    @Autowired
    SpringContextUtils springContextUtils;
    
    public static void main(String[] args)
    {
        SpringApplication.run(QuickSrcApplication.class, args);
    }
}