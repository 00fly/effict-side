package com.websystique.springboot;

import java.net.URI;
import java.util.LinkedHashMap;
import java.util.List;

import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import com.websystique.springboot.model.User;

public class SpringBootRestTestClient
{
    String REST_SERVICE_URI = "http://localhost:8080/SpringBootRestApi/api";
    
    RestTemplate restTemplate = new RestTemplate();
    
    /* GET */
    @SuppressWarnings("unchecked")
    private void listAllUsers()
    {
        System.out.println("Testing listAllUsers API-----------");
        List<LinkedHashMap<String, Object>> usersMap = restTemplate.getForObject(REST_SERVICE_URI + "/user/", List.class);
        if (usersMap != null)
        {
            for (LinkedHashMap<String, Object> map : usersMap)
            {
                System.out.println("User : id=" + map.get("id") + ", Name=" + map.get("name") + ", Age=" + map.get("age") + ", Salary=" + map.get("salary"));
            }
        }
        else
        {
            System.out.println("No user exist----------");
        }
    }
    
    /* GET */
    private void getUser()
    {
        System.out.println("Testing getUser API----------");
        User user = restTemplate.getForObject(REST_SERVICE_URI + "/user/1", User.class);
        System.out.println(user);
    }
    
    /* POST */
    private void createUser()
    {
        System.out.println("Testing create User API----------");
        User user = new User(0, "Sarah", 51, 134);
        URI uri = restTemplate.postForLocation(REST_SERVICE_URI + "/user/", user, User.class);
        System.out.println("Location : " + uri.toASCIIString());
    }
    
    /* PUT */
    private void updateUser()
    {
        System.out.println("Testing update User API----------");
        User user = new User(1, "Tomy", 33, 70000);
        restTemplate.put(REST_SERVICE_URI + "/user/1", user);
        System.out.println(user);
    }
    
    /* DELETE */
    private void deleteUser()
    {
        System.out.println("Testing delete User API----------");
        restTemplate.delete(REST_SERVICE_URI + "/user/3");
    }
    
    /* DELETE */
    private void deleteAllUsers()
    {
        System.out.println("Testing all delete Users API----------");
        restTemplate.delete(REST_SERVICE_URI + "/user/");
    }
    
    @Test
    public void test()
    {
        listAllUsers();
        getUser();
        createUser();
        listAllUsers();
        updateUser();
        listAllUsers();
        deleteUser();
        listAllUsers();
        deleteAllUsers();
        listAllUsers();
    }
}