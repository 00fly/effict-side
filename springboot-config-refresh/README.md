
# 一，springboot配置文件实时刷新

1. FileChangedReloadingStrategy（不推荐）

2. FileAlterationMonitor（开发时、使用`spring.config.location`时推荐）

3. 数据库配置更新后手动刷新（推荐）


## welcome.message更新顺序

1. h2   
2. application-dev.yml
3. welcome.properties
4. /show/refresh

`如需要测试数据库配置，请在application.yml设置spring.profiles.active为test或者prod`


## 二，几种jar运行方式示例

### 1. Jar直接运行

```shell
java -jar springboot-config-refresh-1.0.0.jar
```

### 2. 指定环境变量、加载外部配置文件运行

```shell
java -jar -Dspring.profiles.active=dev springboot-config-refresh-1.0.0.jar --spring.config.location=./application-other.yml
java -jar springboot-config-refresh-1.0.0.jar --spring.profiles.active=dev --spring.config.location=./application-other.yml
```
配置文件建议`持久化到db` 或 `外部配置文件（需另外单独处理）`