package com.fly.refresh.prop;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import com.fasterxml.jackson.dataformat.javaprop.JavaPropsMapper;
import com.fly.core.utils.YamlUtils;
import com.fly.refresh.entity.Result;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JavaPropsMapperTest
{
    /**
     * thread-safe
     */
    JavaPropsMapper javaPropsMapper = new JavaPropsMapper();
    
    /**
     * Properties to Bean
     * 
     * @throws IOException
     */
    @Test
    public void testPropToBean()
        throws IOException
    {
        Properties complex = PropertiesLoaderUtils.loadProperties(new ClassPathResource("prop/complex.properties"));
        
        // 多层结构转换成了嵌套Map
        Map<?, ?> map = javaPropsMapper.readPropertiesAs(complex, Map.class);
        log.info("***** PropToBean：{} => {}", complex, map);
        
        Result javaBean = javaPropsMapper.readPropertiesAs(complex, Result.class);
        log.info("***** PropToBean：{} => {}", complex, javaBean);
    }
    
    /**
     * Properties to Bean
     * 
     * @throws IOException
     */
    @Test
    public void testPropToBean2()
        throws IOException
    {
        String text = IOUtils.toString(new ClassPathResource("yaml/sample.yml").getURL(), StandardCharsets.UTF_8);
        Properties props = YamlUtils.yamlToProperties(text);
        props.keySet().forEach(key -> {
            log.info("{} => {}", key, props.get(key));
        });
        
        Result result = javaPropsMapper.readPropertiesAs(props, Result.class);
        log.info("***** PropToBean：{}", result);
    }
}
