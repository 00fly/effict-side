package com.fly.refresh.yaml;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import com.fly.refresh.entity.Result;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class YAMLMapperTest
{
    /**
     * thread-safe
     */
    YAMLMapper yamlMapper = new YAMLMapper();
    
    @Test
    public void test()
        throws IOException
    {
        Resource resource = new ClassPathResource("yaml/complex.yml");
        String text = IOUtils.toString(resource.getURL(), StandardCharsets.UTF_8);
        log.info("***** complex.yml yamlText => {}", text);
        
        Result javaBean = yamlMapper.readValue(text, Result.class);
        log.info("***** toJavaBean => {}", javaBean);
        
        // 报错com.fasterxml.jackson.databind.exc.MismatchedInputException
        // Properties prop = yamlMapper.readValue(text, Properties.class);
        // log.info("***** toJavaBean => {}", prop);
    }
}
