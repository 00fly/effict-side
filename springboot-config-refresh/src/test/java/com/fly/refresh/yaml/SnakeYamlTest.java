package com.fly.refresh.yaml;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.yaml.snakeyaml.Yaml;

import com.fly.core.utils.YamlUtils;
import com.fly.refresh.entity.Result;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SnakeYamlTest
{
    private String text;
    
    @BeforeEach
    public void init()
    {
        try
        {
            Resource resource = new ClassPathResource("yaml/complex.yml");
            text = IOUtils.toString(resource.getURL(), StandardCharsets.UTF_8);
            log.info("yamlText => {}", text);
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e.getCause());
        }
    }
    
    @Test
    public void test()
    {
        Yaml yaml = new Yaml();
        Result javaBean = yaml.loadAs(text, Result.class);
        log.info("***** toJavaBean => {}", javaBean);
    }
    
    /**
     * 注意区别
     */
    @Test
    public void testPk()
        throws IOException
    {
        Yaml yaml = new Yaml();
        Properties prop1 = yaml.loadAs(text, Properties.class);
        Properties prop2 = YamlUtils.yamlToProperties(text);
        log.info("** PK ** {} <=> {}", prop1, prop2);
        // {welcome={message=Hello 00fly in test2.yml}} <=> {welcome.message=Hello 00fly in test2.yml}
    }
}