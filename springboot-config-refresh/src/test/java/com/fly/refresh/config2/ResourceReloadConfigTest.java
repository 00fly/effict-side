package com.fly.refresh.config2;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.ReloadingFileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.builder.fluent.PropertiesBuilderParameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.configuration2.io.ClasspathLocationStrategy;
import org.apache.commons.configuration2.io.FileLocationStrategy;
import org.apache.commons.configuration2.reloading.PeriodicReloadingTrigger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import lombok.extern.slf4j.Slf4j;

/**
 * Configuration2配置文件实时刷新 https://www.geek-share.com/detail/2727072209.html
 * 
 * @author 00fly
 * @version [版本号, 2017年4月25日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
public class ResourceReloadConfigTest
{
    ReloadingFileBasedConfigurationBuilder<PropertiesConfiguration> builder;
    
    /**
     * 初始化
     */
    @BeforeEach
    public void init()
    {
        // 文件扫描策略
        // FileLocationStrategy strategy = new CombinedLocationStrategy(Arrays.asList(new ClasspathLocationStrategy(), new FileSystemLocationStrategy()));
        FileLocationStrategy strategy = new ClasspathLocationStrategy();
        PropertiesBuilderParameters propertiesBuilderParameters = new Parameters().properties()
            .setEncoding(StandardCharsets.UTF_8.name())
            .setPath(new ClassPathResource("job.properties").getPath())
            .setLocationStrategy(strategy)
            .setListDelimiterHandler(new DefaultListDelimiterHandler(','))
            .setReloadingRefreshDelay(2000L)
            .setThrowExceptionOnMissing(true);
        builder = new ReloadingFileBasedConfigurationBuilder<PropertiesConfiguration>(PropertiesConfiguration.class).configure(propertiesBuilderParameters);
        PeriodicReloadingTrigger trigger = new PeriodicReloadingTrigger(builder.getReloadingController(), null, 60, TimeUnit.SECONDS);
        trigger.start();
    }
    
    @Test
    public void read()
        throws ConfigurationException
    {
        // 直接读取
        Configurations configs = new Configurations();
        FileBasedConfigurationBuilder.setDefaultEncoding(PropertiesConfiguration.class, StandardCharsets.UTF_8.name());
        PropertiesConfiguration propConfig = configs.properties(new ClassPathResource("job.properties").getPath());
        log.info("propConfig：{}", propConfig.getString("schedule.myjob.cron"));
    }
    
    /**
     * https://cloud.tencent.com/developer/article/1600688
     * 
     * @throws ConfigurationException
     */
    @Test
    public void test()
        throws ConfigurationException
    {
        PropertiesConfiguration configuration = builder.getConfiguration();
        log.info("{}", configuration.getString("schedule.myjob.cron"));
    }
}