package com.fly.refresh.job;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fly.refresh.entity.Person;
import com.fly.refresh.entity.Welcome;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * SimpleJob
 * 
 * @author 00fly
 * @version [版本号, 2022年11月30日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@Component
public class SimpleJob
{
    @Autowired
    private Person person;
    
    @Autowired
    private Welcome welcome;
    
    /**
     * 不能实时刷新
     */
    // 注意用法区别：@Value("${welcome.message:hello, 00fly in java!}")
    @Value("#{welcome.message}")
    private String message;
    
    @Scheduled(cron = "*/10 * * * * ?")
    public void run()
    {
        log.info("{}, {}, #{welcome.message} = {} ", ToStringBuilder.reflectionToString(welcome), ToStringBuilder.reflectionToString(person), message);
    }
}
