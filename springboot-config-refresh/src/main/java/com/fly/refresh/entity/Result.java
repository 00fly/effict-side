package com.fly.refresh.entity;

import java.util.Map;

import lombok.Data;

/**
 * 定义Result实体绑定Person、Welcome<br>
 * 与下面的Spring配置等价<br>
 * <br>
 * ＠Component<br>
 * ＠ConfigurationProperties(prefix = "person")<br>
 * public class Person { ... }<br>
 * <br>
 * ＠Component<br>
 * ＠ConfigurationProperties(prefix = "welcome")<br>
 * public class Welcome { ... }
 */
@Data
public class Result
{
    private Person person;
    
    private Welcome welcome;
    
    private Map<String, Object> spring;
}