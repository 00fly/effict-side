CREATE TABLE IF NOT EXISTS `sys_config` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `key` varchar(100),
  `value` varchar(200),
  `description` varchar(200),
  `status` varchar(20),
  `version` bigint,
  `creater` varchar(50),
  `create_time` datetime,
  `modifier` varchar(50),
  `modify_time` datetime,
  PRIMARY KEY (`id`)
);

INSERT INTO `sys_config` VALUES ('1', 'welcome.message',  CONCAT('hello from db, rand ' ,CAST(RAND()*65536 AS INT)), '系统提示语', '1', '0', 'admin', now(), 'admin', now());