package com.fly.api.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

/**
 * 
 * API模块1Controller
 * 
 * @author 00fly
 * @version [版本号, 2018-9-19]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Controller
@Api(value = "API模块1", description = "API模块1")
public class V1Controller
{
    
    /**
     * v1/delete.jsp
     * 
     * @param param1 param1
     * @param param2 param2
     * @param param3 param3
     * @param param4 param4
     * @return String
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "v1/delete.jsp", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "v1/delete.jsp", notes = "v1/delete.jsp详细说明")
    public String v1_delete_jsp(@RequestParam(required = true) @ApiParam(value = "param1") String param1, @RequestParam(required = true) @ApiParam(value = "param2") String param2,
        @RequestParam(required = true) @ApiParam(value = "param3") String param3, @RequestParam(required = true) @ApiParam(value = "param4") String param4)
    {
        return "v1/delete.jsp";
    }
    
    /**
     * v1/insert.jsp
     * 
     * @param param1 param1
     * @param param2 param2
     * @param param3 param3
     * @param param4 param4
     * @return String
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "v1/insert.jsp", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "v1/insert.jsp", notes = "v1/insert.jsp详细说明")
    public String v1_insert_jsp(@RequestParam(required = true) @ApiParam(value = "param1") String param1, @RequestParam(required = true) @ApiParam(value = "param2") String param2,
        @RequestParam(required = true) @ApiParam(value = "param3") String param3, @RequestParam(required = true) @ApiParam(value = "param4") String param4)
    {
        return "v1/insert.jsp";
    }
    
    /**
     * v1/query.jsp
     * 
     * @param param1 param1
     * @param param2 param2
     * @param param3 param3
     * @param param4 param4
     * @return String
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "v1/query.jsp", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "v1/query.jsp", notes = "v1/query.jsp详细说明")
    public String v1_query_jsp(@RequestParam(required = true) @ApiParam(value = "param1") String param1, @RequestParam(required = true) @ApiParam(value = "param2") String param2,
        @RequestParam(required = true) @ApiParam(value = "param3") String param3, @RequestParam(required = true) @ApiParam(value = "param4") String param4)
    {
        return "v1/query.jsp";
    }
    
    /**
     * v1/update.jsp
     * 
     * @param param1 param1
     * @param param2 param2
     * @param param3 param3
     * @param param4 param4
     * @return String
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "v1/update.jsp", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "v1/update.jsp", notes = "v1/update.jsp详细说明")
    public String v1_update_jsp(@RequestParam(required = true) @ApiParam(value = "param1") String param1, @RequestParam(required = true) @ApiParam(value = "param2") String param2,
        @RequestParam(required = true) @ApiParam(value = "param3") String param3, @RequestParam(required = true) @ApiParam(value = "param4") String param4)
    {
        return "v1/update.jsp";
    }
    
}
