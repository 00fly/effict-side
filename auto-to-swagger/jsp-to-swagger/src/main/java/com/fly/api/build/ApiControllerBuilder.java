package com.fly.api.build;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 接口api-doc自动生成类
 * 
 * @author 00fly
 * @version [版本号, 2017-07-31]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
public class ApiControllerBuilder
{
    private static Configuration configuration = new Configuration();
    static
    {
        configuration.setDefaultEncoding(StandardCharsets.UTF_8.name());
        configuration.setDateFormat("yyyy-MM-dd HH:mm:ss");
        configuration.setNumberFormat("#0.#");
    }
    
    /**
     * 解析模板生成内容
     * 
     * @param template
     * @param model
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    public static String renderTemplate(Template template, Object model)
        throws TemplateException, IOException
    {
        StringWriter result = new StringWriter();
        template.process(model, result);
        return result.toString();
    }
    
    /**
     * 注册模板路径
     * 
     * @param directory
     * @return
     * @throws IOException
     */
    public static Configuration buildConfiguration()
    {
        Configuration cfg = new Configuration();
        cfg.setTemplateLoader(new ClassTemplateLoader(ClassLoader.class, "/"));
        return cfg;
    }
    
    /**
     * 测试
     * 
     * @param args
     * @throws IOException
     * @throws ConfigurationException
     * @throws TemplateException
     */
    @SuppressWarnings("unchecked")
    public static void main(String[] args)
        throws IOException, ConfigurationException, TemplateException
    {
        Map<String, Object> model = new HashMap<>();
        model.put("date", new Date());
        model.put("models", JSPFileUtils.queryJspModel());
        
        // 读取资源文件描述信息
        Map<String, String> map = new TreeMap<>();
        ResourceBundle config = ResourceBundle.getBundle("build-config.api-desc");
        for (String key : config.keySet())
        {
            String text = config.getString(key).replace(" ", "&nbsp;");
            text = text.replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;").replace("\r\n", "<br>").replace("\"", "\\\"");
            map.put(key, StringUtils.trimToEmpty(text));
        }
        model.put("desc", map);
        
        File file;
        String content;
        // 根据模板生成代码
        Configuration cfg = ApiControllerBuilder.buildConfiguration();
        Template template = cfg.getTemplate("build-config\\api-controller.ftl");
        PropertiesConfiguration ctrl = new PropertiesConfiguration("src\\main\\resources\\build-config\\ctrl-reg.properties");
        int size = ctrl.getInt("controller.size", 0);
        for (int i = 1; i <= size; i++)
        {
            String key = "controller" + i;
            String ctrlName = ctrl.getString(key + ".name");
            String ctrlPath = ctrl.getString(key + ".path");
            model.put("ctrl_desc", ctrl.getString(key + ".desc"));
            model.put("ctrl_name", ctrlName);
            model.put("ctrl_path", ctrlPath);
            content = ApiControllerBuilder.renderTemplate(template, model);
            file = new File(String.format("src\\main\\java\\com\\fly\\api\\controller\\%s.java", ctrlName));
            FileUtils.writeStringToFile(file, content);
            log.info("filePath: {}", file.getAbsolutePath());
            // 将已经处理的url移除
            Map<String, String> urlPattern = (Map<String, String>)model.get("models");
            Map<String, String> tModel = new HashMap<>();
            tModel.putAll(urlPattern);
            for (String url : urlPattern.keySet())
            {
                if (url.startsWith(ctrlPath))
                {
                    tModel.remove(url);
                }
            }
            model.put("models", tModel);
        }
        model.put("ctrl_desc", ctrl.getString("default.desc"));
        model.put("ctrl_name", ctrl.getString("default.name"));
        model.put("ctrl_path", ctrl.getString("default.path", ""));
        content = ApiControllerBuilder.renderTemplate(template, model);
        file = new File(String.format("src\\main\\java\\com\\fly\\api\\controller\\%s.java", ctrl.getString("default.name")));
        FileUtils.writeStringToFile(file, content);
        log.info("filePath: {}", file.getAbsolutePath());
        log.info("------------Controller 文件生成完成------------");
    }
}
