package com.fly.api;

import java.io.IOException;

import javax.servlet.ServletContext;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * Spring在启动完成后执行方法
 * 
 * @author 00fly
 * @version [版本号, 2017-07-31]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@Component
public class Startup implements ApplicationListener<ContextRefreshedEvent>
{
    /**
     * 事件回调
     * 
     * @param event
     */
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event)
    {
        ApplicationContext applicationContext = event.getApplicationContext();
        log.info("★★★★★★★★ {}", applicationContext);
        
        // 忽略spring context, 仅在servlet容器初始化完成时执行
        if (applicationContext.getParent() == null)
        {
            return;
        }
        try
        {
            log.info("now open Browser...");
            ServletContext servletContext = applicationContext.getBean(ServletContext.class);
            Runtime.getRuntime().exec("cmd.exe /c start /min http://127.0.0.1:8080" + servletContext.getContextPath() + "/api-doc/");
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
}
