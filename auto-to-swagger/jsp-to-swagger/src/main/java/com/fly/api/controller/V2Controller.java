package com.fly.api.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

/**
 * 
 * API模块2Controller
 * 
 * @author 00fly
 * @version [版本号, 2018-9-19]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Controller
@Api(value = "API模块2", description = "API模块2")
public class V2Controller
{
    
    /**
     * v2/delete.jsp
     * 
     * @param param1 param1
     * @param param2 param2
     * @param param3 param3
     * @param param4 param4
     * @return String
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "v2/delete.jsp", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "v2/delete.jsp", notes = "v2/delete.jsp详细说明")
    public String v2_delete_jsp(@RequestParam(required = true) @ApiParam(value = "param1") String param1, @RequestParam(required = true) @ApiParam(value = "param2") String param2,
        @RequestParam(required = true) @ApiParam(value = "param3") String param3, @RequestParam(required = true) @ApiParam(value = "param4") String param4)
    {
        return "v2/delete.jsp";
    }
    
    /**
     * v2/query.jsp
     * 
     * @param param1 param1
     * @param param2 param2
     * @param param3 param3
     * @param param4 param4
     * @return String
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "v2/query.jsp", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "v2/query.jsp", notes = "v2/query.jsp详细说明")
    public String v2_query_jsp(@RequestParam(required = true) @ApiParam(value = "param1") String param1, @RequestParam(required = true) @ApiParam(value = "param2") String param2,
        @RequestParam(required = true) @ApiParam(value = "param3") String param3, @RequestParam(required = true) @ApiParam(value = "param4") String param4)
    {
        return "v2/query.jsp";
    }
    
    /**
     * v2/update.jsp
     * 
     * @param param1 param1
     * @param param2 param2
     * @param param3 param3
     * @param param4 param4
     * @return String
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "v2/update.jsp", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "v2/update.jsp", notes = "v2/update.jsp详细说明")
    public String v2_update_jsp(@RequestParam(required = true) @ApiParam(value = "param1") String param1, @RequestParam(required = true) @ApiParam(value = "param2") String param2,
        @RequestParam(required = true) @ApiParam(value = "param3") String param3, @RequestParam(required = true) @ApiParam(value = "param4") String param4)
    {
        return "v2/update.jsp";
    }
    
    /**
     * v2/insert.jsp
     * 
     * @param param1 param1
     * @param param2 param2
     * @param param3 param3
     * @param param4 param4
     * @return String
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "v2/insert.jsp", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "v2/insert.jsp", notes = "v2/insert.jsp详细说明")
    public String v2_insert_jsp(@RequestParam(required = true) @ApiParam(value = "param1") String param1, @RequestParam(required = true) @ApiParam(value = "param2") String param2,
        @RequestParam(required = true) @ApiParam(value = "param3") String param3, @RequestParam(required = true) @ApiParam(value = "param4") String param4)
    {
        return "v2/insert.jsp";
    }
    
}
