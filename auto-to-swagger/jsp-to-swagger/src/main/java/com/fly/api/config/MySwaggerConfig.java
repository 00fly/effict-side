package com.fly.api.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mangofactory.swagger.configuration.SpringSwaggerConfig;
import com.mangofactory.swagger.models.dto.ApiInfo;
import com.mangofactory.swagger.plugin.EnableSwagger;
import com.mangofactory.swagger.plugin.SwaggerSpringMvcPlugin;

@Configuration
@EnableSwagger
public class MySwaggerConfig
{
    @Autowired
    private SpringSwaggerConfig springSwaggerConfig;
    
    @Bean
    public SwaggerSpringMvcPlugin customImplementation()
    {
        return new SwaggerSpringMvcPlugin(this.springSwaggerConfig).apiInfo(apiInfo()).includePatterns(".*?");
    }
    
    private ApiInfo apiInfo()
    {
        return new ApiInfo("API接口管理", "业务接口", "My Apps API terms of service", "00fly 有问题请及时联系我", null, null);
    }
}