package com.fly.api.controller;

import org.springframework.stereotype.Controller;

import com.wordnik.swagger.annotations.Api;

/**
 * 
 * App默认模块Controller
 * 
 * @author 00fly
 * @version [版本号, 2018-9-19]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Controller
@Api(value = "App默认模块", description = "App默认模块")
public class AppController
{
}
