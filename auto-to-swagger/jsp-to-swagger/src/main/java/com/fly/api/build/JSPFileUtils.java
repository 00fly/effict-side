package com.fly.api.build;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

/**
 * 
 * 接口jsp文件处理
 * 
 * @author 00fly
 * @version [版本号, 2017-07-31]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class JSPFileUtils
{
    /**
     * 测试
     * 
     * @param args
     * @throws IOException
     * @throws ConfigurationException
     */
    public static void main(String[] args)
        throws IOException, ConfigurationException
    {
        buildApiDesc(queryJspModel());
    }
    
    /**
     * 解析jsp文件接口参数名称, 以map数据格式返回
     * 
     * @throws IOException
     * @throws ConfigurationException
     */
    public static Map<String, String> queryJspModel()
        throws IOException
    {
        Map<String, String> map = new TreeMap<>();
        String[] extensions = {"jsp"};
        Collection<File> files = FileUtils.listFiles(new File("src\\main\\webapp\\WEB-INF\\api"), extensions, true);
        for (File file : files)
        {
            // key: 接口相对路径
            String path = StringUtils.substringAfter(file.getAbsolutePath(), "\\api\\").replace("\\", "/");
            List<String> params = new ArrayList<>();
            List<String> lines = FileUtils.readLines(file, StandardCharsets.UTF_8);
            for (String line : lines)
            {
                if (line.contains("request.getParameter(\""))
                {
                    String[] textArr = line.split(",");
                    for (String text : textArr)
                    {
                        if (StringUtils.isNotBlank(text) && text.contains("request.getParameter(\""))
                        {
                            String param = StringUtils.substringBetween(text, "request.getParameter(\"", "\")");
                            if (!params.contains(param))
                            {
                                params.add(param);
                            }
                        }
                    }
                }
            }
            String value = StringUtils.deleteWhitespace(params.toString());
            value = StringUtils.substringBetween(value, "[", "]");
            map.put(path, value);
        }
        return map;
    }
    
    /**
     * 构建ApiDesc
     * 
     * @param map
     * @throws ConfigurationException
     * @see [类、类#方法、类#成员]
     */
    public static void buildApiDesc(Map<String, String> map)
        throws ConfigurationException
    {
        // 处理api接口描述资源文件
        PropertiesConfiguration config = new PropertiesConfiguration("src\\main\\resources\\build-config\\api-desc.properties");
        for (String key : map.keySet())
        {
            String propertiesKey = key.replace("/", "_").replace(".", "_");
            if (!config.containsKey(propertiesKey))
            {
                config.setProperty(propertiesKey, key);
            }
            String note = propertiesKey + "._note";
            if (!config.containsKey(note))
            {
                config.setProperty(note, key + "详细说明");
            }
            String[] values = StringUtils.trimToEmpty(map.get(key)).split(",");
            for (String value : values)
            {
                if (StringUtils.isNotEmpty(value))
                {
                    String paramKey = propertiesKey + "." + value;
                    String paramRequired = paramKey + ".required";
                    if (!config.containsKey(paramKey))
                    {
                        config.setProperty(paramKey, value);
                    }
                    if (!config.containsKey(paramRequired))
                    {
                        config.setProperty(paramRequired, true);
                    }
                }
            }
        }
        config.save();
    }
    
}
