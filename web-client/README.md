
# WebClient功能简单演示项目

自 Spring 5 以来，WebClient已成为Spring WebFlux的一部分，并且是发出 HTTP 请求的首选方式。它是经典RestTemplate的首选替代方案，后者自 Spring 5.0 以来一直处于维护模式。

## 1. RestTemplate
RestTemplate本质上是阻塞的，并使用 Java Servlet API 的每个请求一个线程模型。这意味着RestTemplate一旦向远程服务器发送请求，就会等待响应。默认情况下，每次RestTemplate都会创建新的，并在收到并处理响应后关闭连接。Httpconnection 创建和关闭 URL 连接是一项成本高昂的操作。为了在生产类应用程序中有效地使用RestTemplate ，我们必须使用HTTP 连接池，否则性能会快速下降。当应用程序中有大量请求时，线程和连接的数量也会按比例增加。这会给服务器资源带来负担。如果服务器速度缓慢，用户很快就会发现应用程序性能下降，甚至无响应。

请注意，RestTemplate 是线程安全的，并且可以随时在多个连接之间共享单个实例。

## 2. WebClient

WebClient本质上是异步且非阻塞的。它遵循 Spring WebFlux 反应式框架的事件驱动架构。使用WebClient，客户端无需等待响应返回。相反，当服务器有响应时，它将使用回调方法收到通知。

当我们通过WebClient调用返回 Mono或 Flux 的API 时，API 会立即返回。而调用结果将通过 mono 或 flux 回调传递给调用端。

请注意，如果需要，我们可以通过WebClient.block()方法实现类似RestTemplate的同步处理。
