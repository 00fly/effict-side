package com.fly.http;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RunMain
{
    private static FluxWebClient webClient = new FluxWebClient();
    
    /**
     * 程序运行入口
     * 
     */
    public static void main(String[] args)
    {
        scheduledThreadPoolExecutorStart();
    }
    
    private static void scheduledThreadPoolExecutorStart()
    {
        new ScheduledThreadPoolExecutor(2).scheduleAtFixedRate(() -> {
            webClient.visitAll();
        }, 0L, 30, TimeUnit.SECONDS);
        log.info("======== ScheduledThreadPoolExecutor started!");
    }
    
    /**
     * Timer线程安全, 但单线程执行, 抛出异常时, task会终止
     */
    @Deprecated
    protected static void timeStart()
    {
        new Timer().scheduleAtFixedRate(new TimerTask()
        {
            @Override
            public void run()
            {
                webClient.visitAll();
            }
        }, 0L, 30 * 1000L);
        log.info("======== Timer started!");
    }
}
