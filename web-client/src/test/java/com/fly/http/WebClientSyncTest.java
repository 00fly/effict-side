package com.fly.http;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.RandomUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import com.fly.http.bean.BlogData;
import com.fly.http.bean.ImageShowDialog;
import com.fly.http.bean.JsonBeanUtils;
import com.fly.http.bean.SearchReq;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * http请求WebClient同步调用实现
 */
@Slf4j
public class WebClientSyncTest
{
    private WebClient webClient = WebClient.builder().codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(-1)).build();
    
    private void openImage(Resource resource)
    {
        try
        {
            new ImageShowDialog(ImageIO.read(resource.getInputStream()));
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @BeforeClass
    public static void init()
    {
        new File("download").mkdirs();
    }
    
    /**
     * WebClient同步调用
     * 
     * @throws IOException
     */
    @Test
    public void testDownFile()
        throws IOException
    {
        Mono<ClientResponse> mono = webClient.get().uri("https://00fly.online/upload/urls.txt").accept(MediaType.IMAGE_JPEG).exchange();
        ClientResponse response = mono.block();
        log.info("----- headers: {}", response.headers());
        log.info("----- statusCode: {}", response.statusCode());
        
        // 保存到本地
        Resource resource = response.bodyToMono(Resource.class).block();
        FileCopyUtils.copy(resource.getInputStream(), new FileOutputStream("download/urls.txt"));
    }
    
    /**
     * WebClient同步调用
     * 
     * @throws IOException
     * @throws InterruptedException
     */
    @Test
    public void testDownImg001()
        throws IOException, InterruptedException
    {
        Mono<Resource> mono = webClient.get()
            .uri("https://00fly.online/upload/2019/02/201902262129360274AKuFZcUfip.jpg")
            .accept(MediaType.IMAGE_JPEG)
            .retrieve() // 获取响应体
            .bodyToMono(Resource.class)
            .timeout(Duration.ofSeconds(10)); // 对于每个请求，我们可以设置超时
        openImage(mono.block());
        TimeUnit.SECONDS.sleep(10);
    }
    
    /**
     * WebClient同步调用
     * 
     * @throws IOException
     */
    @Test
    public void testDownImg002()
        throws IOException
    {
        Mono<Resource> mono = webClient.get()
            .uri("https://00fly.online/upload/2019/02/201902262129360274AKuFZcUfip.jpg")
            .accept(MediaType.IMAGE_JPEG)
            .retrieve() // 获取响应体
            .bodyToMono(Resource.class);
        
        // 保存到本地
        Resource resource = mono.block();
        File dest = new File(String.format("download/img_%s.jpg", System.currentTimeMillis()));
        FileCopyUtils.copy(resource.getInputStream(), new FileOutputStream(dest));
        if (Desktop.isDesktopSupported())
        {
            Desktop.getDesktop().open(dest.getParentFile());
        }
    }
    
    /**
     * WebClient同步调用
     */
    @Test
    public void testExchange001()
    {
        // get
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("q1", "java");
        params.add("q2", "python");
        Mono<ClientResponse> monoGet = webClient.get()
            .uri(uriBuilder -> uriBuilder.scheme("https")
                .host("httpbin.org")
                .path("/get")
                .queryParams(params) // 等价 queryParam("q1", "java").queryParam("q2", "python")
                .build())
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .exchange();
        
        ClientResponse clientResponse = monoGet.block(); // 获取完整的响应对象
        log.info("----- headers: {}", clientResponse.headers());
        log.info("----- statusCode: {}", clientResponse.statusCode());
        log.info("----- response: {}", clientResponse.bodyToMono(String.class).block());
    }
    
    /**
     * WebClient同步调用
     */
    @Test
    public void testExchange002()
    {
        // get
        Mono<ClientResponse> monoGet = webClient.get()
            .uri(uriBuilder -> uriBuilder.scheme("https").host("httpbin.org").path("/get").queryParam("q1", "java").queryParam("q2", "python").build())
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .exchange();
        ClientResponse clientResponse = monoGet.block(); // 获取完整的响应对象
        log.info("----- headers: {}", clientResponse.headers());
        log.info("----- statusCode: {}", clientResponse.statusCode());
        log.info("----- response: {}", clientResponse.bodyToMono(String.class).block());
        
        // formData post
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("q1", "java");
        params.add("q2", "python");
        Mono<ClientResponse> monoPost = webClient.post()
            .uri("https://httpbin.org/post")
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .body(BodyInserters.fromFormData(params)) // 设置formData,等价 BodyInserters.fromFormData("q1", "java").with("q2", "python")
            .exchange();
        ClientResponse clientResponse2 = monoPost.block(); // 获取完整的响应对象
        log.info("----- headers: {}", clientResponse2.headers());
        log.info("----- statusCode: {}", clientResponse2.statusCode());
        log.info("----- response: {}", clientResponse2.bodyToMono(String.class).block());
    }
    
    /**
     * WebClient同步调用
     */
    @Test
    public void testFormDataPost()
    {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("q1", "java");
        params.add("q2", "python");
        Mono<String> mono = webClient.post()
            .uri("https://httpbin.org/post")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .body(BodyInserters.fromFormData(params)) // 设置formData,等价 BodyInserters.fromFormData("q1", "java").with("q2", "python")
            .retrieve() // 获取响应体
            .bodyToMono(String.class); // 响应数据类型转换
        log.info("response: {}", mono.block());
    }
    
    /**
     * WebClient同步调用
     */
    @Test
    public void testGet001()
    {
        Mono<String> mono = webClient.get()
            .uri("https://httpbin.org/{method}", "get") // {任意命名}
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .retrieve()
            .bodyToMono(String.class);
        log.info("response: {}", mono.block());
    }
    
    /**
     * WebClient同步调用，结果转换为BlogData对象
     */
    @Test
    public void testGet002()
    {
        Mono<BlogData> mono = webClient.get()
            .uri("https://00fly.online/upload/data.json")
            .acceptCharset(StandardCharsets.UTF_8)
            .exchange()
            .doOnSuccess(clientResponse -> log.info("----- headers: {}", clientResponse.headers()))
            .doOnSuccess(clientResponse -> log.info("----- statusCode: {}", clientResponse.statusCode()))
            .flatMap(clientResponse -> clientResponse.bodyToMono(BlogData.class));
        log.info("response: {}", mono.block());
    }
    
    /**
     * WebClient同步调用
     * 
     */
    @Test
    public void testGet003()
    {
        Mono<String> mono = webClient.get()
            .uri("https://httpbin.org/get")
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .exchange()
            .doOnSuccess(clientResponse -> log.info("----- headers: {}", clientResponse.headers()))
            .doOnSuccess(clientResponse -> log.info("----- statusCode: {}", clientResponse.statusCode()))
            .flatMap(clientResponse -> clientResponse.bodyToMono(String.class));
        log.info("response: {}", mono.block());
    }
    
    /**
     * WebClient同步调用, https://httpbin.org/get?q=java
     * 
     */
    @Test
    public void testGet004()
    {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("q", "java");
        String uri = UriComponentsBuilder.fromUriString("https://httpbin.org/get").queryParams(params).toUriString();
        
        // 注意比较
        // uri = UriComponentsBuilder.fromUriString("https://httpbin.org/get").queryParam("q", "java", "python").toUriString();
        // uri = UriComponentsBuilder.fromUriString("https://httpbin.org/get").queryParam("q1", "java").queryParam("q2", "python").toUriString();
        
        Mono<String> mono = webClient.get()
            .uri(uri)
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .exchange()
            .doOnSuccess(clientResponse -> log.info("----- headers: {}", clientResponse.headers()))
            .doOnSuccess(clientResponse -> log.info("----- statusCode: {}", clientResponse.statusCode()))
            .flatMap(clientResponse -> clientResponse.bodyToMono(String.class));
        log.info("response: {}", mono.block());
    }
    
    /**
     * WebClient同步调用
     * 
     */
    @Test
    public void testGet005()
    {
        Mono<String> mono = webClient.get()
            .uri(uriBuilder -> uriBuilder.scheme("https").host("httpbin.org").path("/get").queryParam("q1", "java").queryParam("q2", "python").build())
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .retrieve()
            .bodyToMono(String.class);
        log.info("response: {}", mono.block());
    }
    
    /**
     * WebClient同步调用
     * 
     */
    @Test
    public void testJsonBody001()
    {
        Mono<String> mono = webClient.post()
            .uri(uriBuilder -> uriBuilder.scheme("https").host("httpbin.org").path("/post").build())
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(Collections.singletonMap("q", "java"))
            .retrieve()
            .bodyToMono(String.class);
        log.info("response: {}", mono.block());
    }
    
    /**
     * WebClient同步调用
     * 
     * @throws IOException
     */
    @Test
    public void testJsonBody002()
        throws IOException
    {
        Mono<String> mono;
        int num = RandomUtils.nextInt(1, 4);
        switch (num)
        {
            case 1: // 方式1,javaBean
                SearchReq req = new SearchReq();
                req.setPageNo(1);
                req.setPageSize(10);
                req.setKeyword("1");
                mono = webClient.post()
                    .uri("https://httpbin.org/post")
                    .acceptCharset(StandardCharsets.UTF_8)
                    .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
                    .bodyValue(req) // 设置JsonBody
                    .retrieve()
                    .bodyToMono(String.class);
                log.info("response: {}", mono.block());
                break;
            
            case 2: // 方式2,HashMap
                Map<String, String> params = new HashMap<>();
                params.put("pageNo", "2");
                params.put("pageSize", "20");
                params.put("keyword", "2");
                mono = webClient.post()
                    .uri("https://httpbin.org/post")
                    .acceptCharset(StandardCharsets.UTF_8)
                    .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
                    .bodyValue(params) // 设置JsonBody
                    .retrieve()
                    .bodyToMono(String.class);
                log.info("response: {}", mono.block());
                break;
            
            case 3: // 方式3,json字符串
                Map<String, String> params2 = new HashMap<>();
                params2.put("pageNo", "2");
                params2.put("pageSize", "20");
                params2.put("keyword", "2");
                mono = webClient.post()
                    .uri("https://httpbin.org/post")
                    .acceptCharset(StandardCharsets.UTF_8)
                    .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
                    .body(BodyInserters.fromValue(JsonBeanUtils.beanToJson(params2, false))) // 设置formData
                    .retrieve()
                    .bodyToMono(String.class);
                log.info("response: {}", mono.block());
                break;
        }
    }
    
    /**
     * WebClient同步调用
     * 
     */
    @Test
    public void testUpload001()
    {
        MultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
        params.add("q1", "java");
        params.add("q2", "python");
        params.add("file", new ClassPathResource("123.jpg"));
        Mono<String> mono = webClient.post()
            .uri("https://httpbin.org/post")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .body(BodyInserters.fromMultipartData(params)) // 设置formData,等价 BodyInserters.fromFormData("q1", "java").with("q2", "python")
            .retrieve() // 获取响应体
            .bodyToMono(String.class); // 响应数据类型转换
        log.info("----- response: {}", mono.block());
    }
    
    /**
     * WebClient同步调用
     * 
     */
    @Test
    public void testUpload002()
    {
        Mono<String> mono = webClient.post()
            .uri("https://httpbin.org/post")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .body(BodyInserters.fromMultipartData("q1", "java").with("q2", "python").with("file", new ClassPathResource("123.jpg")))
            .retrieve() // 获取响应体
            .bodyToMono(String.class); // 响应数据类型转换
        log.info("----- response: {}", mono.block());
    }
}
