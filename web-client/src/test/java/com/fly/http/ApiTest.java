package com.fly.http;

import java.awt.Desktop;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.util.ResourceUtils;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ApiTest
{
    // 缓冲区默认256k，设为-1以解决报错Exceeded limit on max bytes to buffer : 262144
    private WebClient webClient = WebClient.builder().codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(-1)).build();
    
    /**
     * 写入文本文件
     * 
     * @param urls
     * @see [类、类#方法、类#成员]
     */
    private void process(List<String> urls)
    {
        try
        {
            if (ResourceUtils.isFileURL(ResourceUtils.getURL(ResourceUtils.CLASSPATH_URL_PREFIX)))
            {
                String path = ResourceUtils.getURL(ResourceUtils.CLASSPATH_URL_PREFIX).getPath() + "urls.txt";
                File dest = new File(path);
                FileUtils.writeLines(dest, StandardCharsets.UTF_8.name(), urls);
                Desktop.getDesktop().open(dest);
            }
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @Test
    public void test001()
        throws IOException
    {
        String response = webClient.get().uri("https://00fly.online/upload/data.json").acceptCharset(StandardCharsets.UTF_8).accept(MediaType.APPLICATION_JSON).retrieve().bodyToMono(String.class).block();
        IOUtils.write(response, new FileOutputStream("src/test/resources/data.json"), StandardCharsets.UTF_8);
        
        // 文本替换
        String jsonBody = response.replace("{", "{\n").replace("}", "}\n").replace(",", ",\n");
        try (InputStream is = new ByteArrayInputStream(jsonBody.getBytes(StandardCharsets.UTF_8)))
        {
            List<String> urls =
                IOUtils.readLines(is, StandardCharsets.UTF_8).stream().filter(line -> StringUtils.contains(line, "\"url\"")).map(n -> StringUtils.substringBetween(StringUtils.deleteWhitespace(n), ":\"", "\"")).collect(Collectors.toList());
            log.info("★★★★★★★★ urls: {} ★★★★★★★★", urls.size());
            process(urls);
        }
    }
    
    @Test
    public void test002()
        throws IOException
    {
        Resource resource = new ClassPathResource("data.json");
        String jsonBody = IOUtils.toString(resource.getInputStream(), StandardCharsets.UTF_8).replace("{", "{\n").replace("}", "}\n").replace(",", ",\n");
        try (InputStream is = new ByteArrayInputStream(jsonBody.getBytes(StandardCharsets.UTF_8)))
        {
            List<String> urls = IOUtils.readLines(is, StandardCharsets.UTF_8).stream().filter(line -> StringUtils.contains(line, "\"url\":")).map(n -> StringUtils.substringBetween(n, ":\"", "\",")).collect(Collectors.toList());
            log.info("★★★★★★★★ urls: {} ★★★★★★★★", urls.size());
            process(urls);
        }
    }
    
    @Test
    public void test003()
    {
        String resp = webClient.get().uri("https://00fly.online/upload/urls.txt").acceptCharset(StandardCharsets.UTF_8).accept(MediaType.TEXT_HTML).retrieve().bodyToMono(String.class).block();
        List<String> urls = Arrays.asList(StringUtils.split(resp, "\r\n"));
        AtomicInteger count = new AtomicInteger(0);
        urls.stream().forEach(url -> log.info("{}. {}", count.incrementAndGet(), url));
    }
}
