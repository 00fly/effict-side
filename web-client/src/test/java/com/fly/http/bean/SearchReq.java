package com.fly.http.bean;

import lombok.Data;

@Data
public class SearchReq
{
    Integer pageNo = 1;
    
    Integer pageSize = 10;
    
    String keyword;
}
