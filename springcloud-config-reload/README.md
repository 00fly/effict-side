# springboot工程自动刷新配置说明



## 一. 刷新配置数据来源

1. 在application.yml通过spring.profiles.active指定

2. 手动加载配置到ConfigurableEnvironment，参考SysDataBaseConfig


## 二. 刷新配置过程

调用`contextRefresher.refresh`完成刷新context后

- 使用@RefreshScope、@Value注解注入context资源


- 使用@ConfigurationProperties注解绑定context资源

## 三. 其他

禁止连接配置中心服务端，修改pom.xml

```
<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-starter-config</artifactId>
	<exclusions>
		<exclusion>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-config-client</artifactId>
		</exclusion>
	</exclusions>
</dependency>
```