package com.fly.core.config;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 数据库配置信息加载类
 * 
 * @author 00fly
 * @version [版本号, 2021年10月24日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@Configuration
public class SysDataBaseConfig
{
    @Autowired
    JdbcTemplate jdbcTemplate;
    
    @Autowired
    ConfigurableEnvironment environment;
    
    private List<SysConfig> getAll()
    {
        return jdbcTemplate.query("SELECT `key`, `value`, `status` FROM sys_config WHERE `status` = '1'", new BeanPropertyRowMapper<>(SysConfig.class));
    }
    
    @PostConstruct
    public void initDatabasePropertySource()
    {
        // 追加配置到系统变量中,name取值随意
        environment.getPropertySources()
            .addLast(new MapPropertySource("sys_config", getAll().stream()
                .filter(p -> StringUtils.isNoneEmpty(p.getKey(), p.getValue()))
                .peek(s -> log.info("{} ====> {}", s.getKey(), s.getValue())) // log
                .collect(Collectors.toMap(SysConfig::getKey, SysConfig::getValue))));
    }
}

/**
 * 
 * 配置信息实体对象
 * 
 * @author 00fly
 * @version [版本号, 2021年10月24日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Data
class SysConfig
{
    private String key;
    
    private String value;
    
    private String status;
}
