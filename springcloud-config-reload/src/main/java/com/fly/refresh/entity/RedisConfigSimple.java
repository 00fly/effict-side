package com.fly.refresh.entity;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * 
 * ConfigurationProperties 注解的配置无需RefreshScope也会自动刷新<br>
 * 数据来源：1.数据库sys_config表 2.上下文application-redis.properties
 * 
 * @author 00fly
 * @version [版本号, 2018年7月20日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Data
@Component
@ConfigurationProperties(prefix = "redis")
public class RedisConfigSimple
{
    private String hostname;
    
    private String port;
    
    private String password;
}
