package com.fly.refresh.entity;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * 
 * RefreshScope注解，设置配置文件自动刷新<br>
 * 数据来源：1.数据库sys_config表 2.上下文application-redis.properties
 * 
 * @author 00fly
 * @version [版本号, 2018年7月20日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Data
@Component
@RefreshScope
@ConfigurationProperties(prefix = "redis")
public class RedisConfig
{
    private String hostname;
    
    private String port;
    
    private String password;
    
    @Value("${welcome.msg}")
    private String message;
    
    @Value("#{${redis.port}+' or '+${port}}")
    private String allPort;
}
