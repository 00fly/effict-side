package com.fly.refresh.back;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.refresh.ContextRefresher;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.fly.core.config.SysDataBaseConfig;

@Service
public class RefreshFromDb
{
    @Autowired
    JdbcTemplate jdbcTemplate;
    
    @Autowired
    ContextRefresher contextRefresher;
    
    @Autowired
    SysDataBaseConfig sysDataBaseConfig;
    
    /**
     * 数据库配置项更新
     * 
     * @see [类、类#方法、类#成员]
     */
    public int update(String message)
    {
        int count = jdbcTemplate.update("UPDATE sys_config SET `value`=? WHERE `key` = 'welcome.msg'", message);
        if (count > 0)
        {
            sysDataBaseConfig.initDatabasePropertySource();
            contextRefresher.refresh();
        }
        return count;
    }
}
