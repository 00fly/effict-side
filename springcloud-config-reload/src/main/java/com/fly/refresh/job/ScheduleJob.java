package com.fly.refresh.job;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;

import com.fly.refresh.entity.RedisConfig;
import com.fly.refresh.entity.RedisConfigSimple;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class ScheduleJob
{
    @Autowired
    RedisConfig redisConfig;
    
    @Autowired
    RedisConfigSimple redisConfigSimple;
    
    @Bean
    ScheduledExecutorService scheduledExecutorService()
    {
        return new ScheduledThreadPoolExecutor(8, new CustomizableThreadFactory("schedule-pool-"));
    }
    
    @Scheduled(cron = "0/10 * 7-23 * * ?")
    public void run()
    {
        log.info("{}", redisConfig);
        log.info("{}\n", redisConfigSimple);
    }
}
