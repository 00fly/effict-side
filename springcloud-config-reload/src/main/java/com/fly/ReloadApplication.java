package com.fly;

import org.apache.commons.lang3.SystemUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.util.ResourceUtils;

import com.fly.core.utils.SpringContextUtils;

@EnableScheduling
@SpringBootApplication
@PropertySource(ResourceUtils.CLASSPATH_URL_PREFIX + "jdbc-h2.properties")
public class ReloadApplication
{
    /**
     * 启动 SpringBootApplication
     */
    public static void main(String[] args)
    {
        SpringApplication.run(ReloadApplication.class);
    }
    
    /**
     * 接口实现类另一种写法
     */
    @Bean
    @ConditionalOnWebApplication
    CommandLineRunner openBrowser()
    {
        return args -> {
            if (SystemUtils.IS_OS_WINDOWS)// 防止非windows系统报错，启动失败
            {
                String url = SpringContextUtils.getServerBaseURL();
                Runtime.getRuntime().exec("cmd /c start /min " + url + "/doc.html");
                Runtime.getRuntime().exec("cmd /c start /min " + url + "/h2-console");
            }
        };
    }
}