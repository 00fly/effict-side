CREATE TABLE IF NOT EXISTS `sys_config` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `key` varchar(100),
  `value` varchar(200),
  `description` varchar(200),
  `status` varchar(20),
  `version` bigint,
  `creater` varchar(50),
  `create_time` datetime,
  `modifier` varchar(50),
  `modify_time` datetime,
  PRIMARY KEY (`id`)
);

INSERT INTO `sys_config` VALUES ('1', 'redis.hostname', '127.0.0.1', 'redis host配置', '1', '0', 'admin', now(), 'admin', now());
INSERT INTO `sys_config` VALUES ('2', 'redis.port', CAST(RAND()*65536 AS INT), 'redis port配置', '1', '0', 'admin', now(), 'admin', now());
INSERT INTO `sys_config` VALUES ('3', 'welcome.msg', 'hello from db', '系统提示语', '1', '0', 'admin', now(), 'admin', now());