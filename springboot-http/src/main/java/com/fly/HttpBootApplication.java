package com.fly;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.fly.core.utils.SpringContextUtils;

@EnableScheduling
@EnableFeignClients
@EnableAspectJAutoProxy
@SpringBootApplication
public class HttpBootApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(HttpBootApplication.class, args);
    }
    
    @Bean
    @ConditionalOnWebApplication
    CommandLineRunner init()
    {
        return args -> {
            if (SystemUtils.IS_OS_WINDOWS)// 防止非windows系统报错，启动失败
            {
                String url = SpringContextUtils.getServerBaseURL();
                if (StringUtils.containsNone(url, "-")) // junit port:-1
                {
                    Runtime.getRuntime().exec("cmd /c start /min " + url);
                    Runtime.getRuntime().exec("cmd /c start /min " + url + "/doc.html");
                }
            }
        };
    }
}
