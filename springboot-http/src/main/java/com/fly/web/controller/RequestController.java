package com.fly.web.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fly.core.entity.JsonResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

@Api(tags = "通用HTTP接口")
@RestController
@RequestMapping("/http")
public class RequestController
{
    @Autowired
    RestTemplate restTemplate;
    
    @ApiOperation("服务器端请求转发")
    @PostMapping("/send")
    public JsonResult<?> send(@Valid HttpRequest request)
    {
        ResponseEntity<JsonResult> responseEntity;
        if (StringUtils.equals(request.getMethod(), "get"))
        {
            responseEntity = restTemplate.getForEntity(request.getUrl(), JsonResult.class);
        }
        else
        {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            HttpEntity<String> requestEntity = new HttpEntity<>(request.getJsonBody(), headers);
            responseEntity = restTemplate.postForEntity(request.getUrl(), requestEntity, JsonResult.class);
        }
        return responseEntity.getBody();
    }
    
    @ApiOperation("服务器端请求转发2")
    @PostMapping("/send2")
    public void send2(@Valid HttpRequest request, HttpServletResponse response)
        throws IOException
    {
        ResponseEntity<String> responseEntity;
        if (StringUtils.equals(request.getMethod(), "get"))
        {
            responseEntity = restTemplate.getForEntity(request.getUrl(), String.class);
        }
        else
        {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            HttpEntity<String> requestEntity = new HttpEntity<>(request.getJsonBody(), headers);
            responseEntity = restTemplate.postForEntity(request.getUrl(), requestEntity, String.class);
        }
        response.setCharacterEncoding(StandardCharsets.UTF_8.name());
        response.setContentType(MediaType.TEXT_HTML_VALUE);
        response.getWriter().write(responseEntity.getBody());
    }
}

@Data
@ApiModel("请求对象")
class HttpRequest
{
    @NotBlank(message = "实际请求URL不能为空")
    @URL(message = "请输入合法的URL")
    @ApiModelProperty(value = "实际请求URL ", required = true, position = 1)
    private String url;
    
    @NotBlank(message = "请求方式不能为空")
    @Pattern(regexp = "post|get", message = "请求方式只能为post、get")
    @ApiModelProperty(value = "请求方式", example = "post", required = true, position = 2)
    private String method;
    
    @ApiModelProperty(value = "jsonBody请求体", position = 3)
    private String jsonBody;
}