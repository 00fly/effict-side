package com.fly.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.fly.core.annotation.Item;
import com.fly.core.annotation.OpenApi;
import com.fly.web.feign.MyFeignClient;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags = "测试FeignClient")
@RestController
public class FeignClientController
{
    @Autowired
    private MyFeignClient feignClient;
    
    @OpenApi(checkItems = Item.ALL)
    @ApiOperation("关键字搜索")
    @PostMapping("query")
    public String query(@RequestHeader String token, @RequestHeader String timestamp, @RequestHeader String sign, String keyword)
    {
        log.info("####### feignClient.search({})", keyword);
        return feignClient.search(keyword);
    }
}
