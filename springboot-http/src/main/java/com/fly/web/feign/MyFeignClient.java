package com.fly.web.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 注意name这里是非cloud项目，随意写一个就好，但是要注意名字必须有。
 **/
@FeignClient(name = "myFeignClient", url = "http://124.71.129.204:8083")
//@FeignClient(name = "myFeignClient", url = "http://127.0.0.1:${server.port}")
public interface MyFeignClient
{
    /**
     * OpenFeign在构造请求时需要@RequestMapping/@RequestParam/@PathVariable/@RequestHeader等来构造http请求<BR>
     * 否则报错 feign.FeignException: [405] during [GET]
     * 
     * @param q
     * @return
     */
    @GetMapping("/get")
    String search(@RequestParam String q);
}
