package com.fly.web.job;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * ScheduleJob
 * 
 * @author 00fly
 * @version [版本号, 2022年11月30日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@Component
public class ScheduleJob
{
    @Autowired
    RestTemplate restTemplate;
    
    /**
     * 向本机外网发送数据，验证流量收发
     */
    // @Scheduled(fixedRate = 60000L)
    public void run()
    {
        try
        {
            log.info("run...");
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            MultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
            Resource resource = new ClassPathResource("data/nginx-1.25.3.tar.gz");
            params.add("file", resource);
            params.add("id", "1");
            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(params, headers);
            String uploadUrl = "http://124.71.129.204:8083/post";
            restTemplate.postForEntity(uploadUrl, requestEntity, String.class);
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
        }
    }
}
