package com.fly.core.config.extend;

import java.net.URI;

import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpUriRequest;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

/**
 * 定义HttpGetRequestWithEntity实现HttpEntityEnclosingRequestBase抽象类，以支持GET请求携带body数据<br>
 * <br>
 * 参考：https://blog.csdn.net/CDWLX/article/details/124315229
 */
public class HttpComponentsClientRestfulHttpRequestFactory extends HttpComponentsClientHttpRequestFactory
{
    @Override
    protected HttpUriRequest createHttpUriRequest(HttpMethod httpMethod, URI uri)
    {
        if (httpMethod == HttpMethod.GET)
        {
            return new HttpGetRequestWithEntity(uri);
        }
        return super.createHttpUriRequest(httpMethod, uri);
    }
    
    /**
     * 定义HttpGetRequestWithEntity实现HttpEntityEnclosingRequestBase抽象类，以支持GET请求携带body数据
     */
    private static final class HttpGetRequestWithEntity extends HttpEntityEnclosingRequestBase
    {
        public HttpGetRequestWithEntity(final URI uri)
        {
            super.setURI(uri);
        }
        
        @Override
        public String getMethod()
        {
            return HttpMethod.GET.name();
        }
    }
}
