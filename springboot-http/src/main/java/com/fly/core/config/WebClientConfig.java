package com.fly.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import reactor.netty.http.client.HttpClient;

/**
 * 注册WebClient
 */
@Configuration
public class WebClientConfig
{
    @Bean
    WebClient webClient()
    {
        // 配置连接超时、读写超时
        // TcpClient tcpClient = TcpClient.create().option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000).doOnConnected(conn -> conn.addHandlerLast(new ReadTimeoutHandler(10)).addHandlerLast(new WriteTimeoutHandler(10)));
        // HttpClient httpClient = HttpClient.from(tcpClient);
        
        // 配置连接超时、读写超时
        HttpClient httpClient =
            HttpClient.create().tcpConfiguration(client -> client.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 10000).doOnConnected(conn -> conn.addHandlerLast(new ReadTimeoutHandler(10)).addHandlerLast(new WriteTimeoutHandler(10))));
        
        // 缓冲区默认256k，设为-1以解决报错Exceeded limit on max bytes to buffer : 262144
        return WebClient.builder().clientConnector(new ReactorClientHttpConnector(httpClient)).codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(-1)).build();
    }
}
