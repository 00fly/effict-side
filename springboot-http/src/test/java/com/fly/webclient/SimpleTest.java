package com.fly.webclient;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * https://blog.csdn.net/zzhongcy/article/details/105412842
 * 
 */
@Slf4j
@DisplayName("WebClient测试")
public class SimpleTest
{
    static WebClient webClient;
    
    @BeforeAll
    public static void setup()
    {
        webClient = WebClient.builder().baseUrl("https://api.github.com").defaultHeader(HttpHeaders.CONTENT_TYPE, "application/vnd.github.v3+json").defaultHeader(HttpHeaders.USER_AGENT, "Spring 5 WebClient").build();
    }
    
    @Test
    public void testGet()
        throws InterruptedException
    {
        Mono<String> resp = WebClient.create().method(HttpMethod.GET).uri("https://www.baidu.com").cookie("token", "xxxx").header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE).retrieve().bodyToMono(String.class);
        resp.subscribe(log::info);
        TimeUnit.SECONDS.sleep(2);
    }
    
    @Test
    public void testFormParam()
        throws InterruptedException
    {
        MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
        formData.add("name1", "value1");
        formData.add("name2", "value2");
        Mono<String> resp = WebClient.create().post().uri("http://www.w3school.com.cn/test/demo_form.asp").contentType(MediaType.APPLICATION_FORM_URLENCODED).body(BodyInserters.fromFormData(formData)).retrieve().bodyToMono(String.class);
        resp.subscribe(log::info);
        TimeUnit.SECONDS.sleep(2);
    }
}
