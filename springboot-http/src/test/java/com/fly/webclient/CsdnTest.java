package com.fly.webclient;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;

/**
 * https://blog.csdn.net/zzhongcy/article/details/105412842
 * 
 */
@Slf4j
@DisplayName("CSDN测试")
public class CsdnTest
{
    static WebClient webClient;
    
    @BeforeAll
    public static void init()
    {
        // 默认底层使用Netty
        log.info("--- init ---");
        webClient = WebClient.builder().codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(-1)).build();
        
        // 内置支持Jetty反应性HttpClient实现，手工编码指定
        // webClient = WebClient.builder().baseUrl("https://blog.csdn.net").clientConnector(new JettyClientHttpConnector()).build();
    }
    
    @Test
    @DisplayName("get请求")
    public void testGet()
        throws InterruptedException
    {
        visit();
        TimeUnit.SECONDS.sleep(10); // 重要，等待异步调用完成
    }
    
    @Test
    @DisplayName("Timer")
    public void timeStart()
        throws InterruptedException
    {
        // Timer线程安全, 但单线程执行, 抛出异常时, task会终止
        new Timer().scheduleAtFixedRate(new TimerTask()
        {
            @Override
            public void run()
            {
                visit();
            }
        }, 0L, 30 * 1000L);
        log.info("======== Timer started!");
        TimeUnit.SECONDS.sleep(60); // 重要，等待异步调用完成
    }
    
    @Test
    @DisplayName("ScheduledThreadPool")
    public void scheduledThreadPoolExecutorStart()
        throws InterruptedException
    {
        ScheduledExecutorService service = new ScheduledThreadPoolExecutor(2);
        service.scheduleAtFixedRate(() -> {
            visit();
        }, 0L, 30, TimeUnit.SECONDS);
        log.info("======== ScheduledThreadPoolExecutor started!");
        TimeUnit.SECONDS.sleep(60); // 重要，等待异步调用完成
    }
    
    private void visit()
    {
        String resp = webClient.get().uri("https://00fly.online/upload/urls.txt").acceptCharset(StandardCharsets.UTF_8).accept(MediaType.TEXT_HTML).retrieve().bodyToMono(String.class).block();
        List<String> urls = Arrays.asList(StringUtils.split(resp, "\r\n"));
        urls.stream().forEach(log::info);
        
        // 异步访问
        AtomicInteger count = new AtomicInteger(0);
        urls.stream()
            .forEach(url -> webClient.get()
                .uri(url)
                .acceptCharset(StandardCharsets.UTF_8)
                .accept(MediaType.TEXT_HTML)
                .retrieve()
                .bodyToMono(String.class)
                .subscribe(r -> log.info("process complted: {}. {}", count.incrementAndGet(), url), e -> log.error(e.getMessage())));
    }
}
