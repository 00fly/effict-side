package com.fly.webclient.bean;

import java.util.List;

import lombok.Data;

@Data
public class BlogData
{
    private Integer code;
    
    private String message;
    
    private String traceId;
    
    private Record data;
}

@Data
class Record
{
    private List<SubList> list;
    
    private Long total;
}

@Data
class SubList
{
    String articleId;
    
    String title;
    
    String description;
    
    String url;
    
    Integer type;
    
    String top;
    
    String forcePlan;
    
    Long viewCount;
    
    Long commentCount;
    
    String editUrl;
    
    String postTime;
    
    Long diggCount;
    
    String formatTime;
    
    Object picList;
    
    Long collectCount;
}