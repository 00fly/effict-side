package com.fly.webclient;

import java.awt.Desktop;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.util.ResourceUtils;
import org.springframework.web.reactive.function.client.WebClient;

import com.fly.HttpBootApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest(classes = HttpBootApplication.class)
public class ApiTest
{
    @Autowired
    WebClient webClient;
    
    /**
     * 写入文本文件
     * 
     * @param urls
     * @see [类、类#方法、类#成员]
     */
    private void process(List<String> urls)
    {
        try
        {
            if (ResourceUtils.isFileURL(ResourceUtils.getURL(ResourceUtils.CLASSPATH_URL_PREFIX)))
            {
                String path = ResourceUtils.getURL(ResourceUtils.CLASSPATH_URL_PREFIX).getPath() + "urls.txt";
                File dest = new File(path);
                FileUtils.writeLines(dest, StandardCharsets.UTF_8.name(), urls);
                log.info("path: {}", dest.getCanonicalPath());
                if (SystemUtils.IS_OS_WINDOWS) // open父目录
                {
                    Runtime.getRuntime().exec("cmd /c start " + dest.getParent());
                }
                if (Desktop.isDesktopSupported())
                {
                    Desktop.getDesktop().open(dest);
                }
            }
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @Test
    public void test001()
        throws IOException
    {
        String response = webClient.get().uri("https://00fly.online/upload/data.json").acceptCharset(StandardCharsets.UTF_8).accept(MediaType.APPLICATION_JSON).retrieve().bodyToMono(String.class).block();
        IOUtils.write(response, new FileOutputStream("src/test/resources/data.json"), StandardCharsets.UTF_8);
        
        // 文本替换
        String jsonBody = response.replace("{", "{\n").replace("}", "}\n").replace(",", ",\n");
        try (InputStream is = new ByteArrayInputStream(jsonBody.getBytes(StandardCharsets.UTF_8)))
        {
            List<String> urls = IOUtils.readLines(is, StandardCharsets.UTF_8).stream().filter(line -> StringUtils.contains(line, "\"url\":")).map(n -> StringUtils.substringBetween(n, ":\"", "\",")).collect(Collectors.toList());
            log.info("★★★★★★★★ urls: {} ★★★★★★★★", urls.size());
            process(urls);
        }
    }
    
    @Test
    public void test002()
        throws IOException
    {
        Resource resource = new ClassPathResource("data.json");
        String jsonBody = IOUtils.toString(resource.getInputStream(), StandardCharsets.UTF_8).replace("{", "{\n").replace("}", "}\n").replace(",", ",\n");
        try (InputStream is = new ByteArrayInputStream(jsonBody.getBytes(StandardCharsets.UTF_8)))
        {
            List<String> urls = IOUtils.readLines(is, StandardCharsets.UTF_8).stream().filter(line -> StringUtils.contains(line, "\"url\":")).map(n -> StringUtils.substringBetween(n, ":\"", "\",")).collect(Collectors.toList());
            log.info("★★★★★★★★ urls: {} ★★★★★★★★", urls.size());
            process(urls);
        }
    }
    
    @Test
    public void test003()
    {
        String resp = webClient.get().uri("https://00fly.online/upload/urls.txt").acceptCharset(StandardCharsets.UTF_8).accept(MediaType.TEXT_HTML).retrieve().bodyToMono(String.class).block();
        List<String> urls = Arrays.asList(StringUtils.split(resp, "\r\n"));
        AtomicInteger count = new AtomicInteger(0);
        urls.stream().forEach(url -> log.info("{}. {}", count.incrementAndGet(), url));
    }
}
