package com.fly.webclient;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import com.fly.core.utils.JsonBeanUtils;
import com.fly.rest.bean.SearchReq;
import com.fly.webclient.bean.BlogData;
import com.fly.webclient.ui.ImageShowDialog;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * http请求WebClient异步调用实现
 */
@Slf4j
public class WebClientAsyncTest
{
    private WebClient webClient = WebClient.builder().codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(-1)).build();
    
    private void openImage(Resource resource)
    {
        try
        {
            new ImageShowDialog(ImageIO.read(resource.getInputStream()));
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @BeforeAll
    public static void init()
    {
        new File("download").mkdirs();
    }
    
    /**
     * WebClient异步调用
     * 
     * @throws IOException
     * @throws InterruptedException
     */
    @Test
    public void testDownFile()
        throws IOException, InterruptedException
    {
        Mono<Resource> mono = webClient.get()
            .uri("https://00fly.online/upload/urls.txt")
            .accept(MediaType.IMAGE_JPEG)
            .exchange()
            .doOnSuccess(clientResponse -> log.info("----- headers: {}", clientResponse.headers()))
            .doOnSuccess(clientResponse -> log.info("----- statusCode: {}", clientResponse.statusCode()))
            .flatMap(clientResponse -> clientResponse.bodyToMono(Resource.class))
            .timeout(Duration.ofSeconds(10)); // 对于每个请求设置超时
        
        // 保存到本地
        mono.subscribe(resource -> {
            try
            {
                FileCopyUtils.copy(resource.getInputStream(), new FileOutputStream("download/urls.txt"));
            }
            catch (IOException e)
            {
                log.error(e.getMessage(), e);
            }
        });
        TimeUnit.SECONDS.sleep(2);
    }
    
    /**
     * WebClient异步调用
     * 
     * @throws IOException
     * @throws InterruptedException
     */
    @Test
    public void testDownImg001()
        throws IOException, InterruptedException
    {
        Mono<Resource> mono = webClient.get()
            .uri("https://00fly.online/upload/2019/02/201902262129360274AKuFZcUfip.jpg")
            .accept(MediaType.IMAGE_JPEG)
            .retrieve() // 获取响应体
            .bodyToMono(Resource.class)
            .timeout(Duration.ofSeconds(10)); // 对于每个请求设置超时
        mono.subscribe(resource -> openImage(resource));
        TimeUnit.SECONDS.sleep(10);
    }
    
    /**
     * WebClient异步调用
     * 
     * @throws IOException
     * @throws InterruptedException
     */
    @Test
    public void testDownImg002()
        throws IOException, InterruptedException
    {
        Mono<Resource> mono = webClient.get()
            .uri("https://00fly.online/upload/2019/02/201902262129360274AKuFZcUfip.jpg")
            .accept(MediaType.IMAGE_JPEG)
            .retrieve() // 获取响应体
            .bodyToMono(Resource.class);
        
        // 保存到本地
        mono.subscribe(resource -> {
            try
            {
                File dest = new File(String.format("download/img_%s.jpg", System.currentTimeMillis()));
                FileCopyUtils.copy(resource.getInputStream(), new FileOutputStream(dest));
                if (Desktop.isDesktopSupported())
                {
                    Desktop.getDesktop().open(dest.getParentFile());
                }
            }
            catch (IOException e)
            {
                log.error(e.getMessage(), e);
            }
        });
        TimeUnit.SECONDS.sleep(2);
    }
    
    /**
     * WebClient异步调用
     * 
     * @throws InterruptedException
     */
    @Test
    public void testExchange001()
        throws InterruptedException
    {
        // get
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("q1", "java");
        params.add("q2", "python");
        Mono<ClientResponse> monoGet = webClient.get()
            .uri(uriBuilder -> uriBuilder.scheme("https")
                .host("httpbin.org")
                .path("/get")
                .queryParams(params) // 等价 queryParam("q1", "java").queryParam("q2", "python")
                .build())
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .exchange();
        
        monoGet.subscribe(clientResponse -> {
            log.info("----- headers: {}", clientResponse.headers());
            log.info("----- statusCode: {}", clientResponse.statusCode());
            clientResponse.bodyToMono(String.class).subscribe(body -> log.info("----- response: {}", body));
        });
        TimeUnit.SECONDS.sleep(2);
    }
    
    /**
     * WebClient异步调用
     * 
     * @throws InterruptedException
     */
    @Test
    public void testExchange002()
        throws InterruptedException
    {
        // get
        Mono<ClientResponse> monoGet = webClient.get()
            .uri(uriBuilder -> uriBuilder.scheme("https").host("httpbin.org").path("/get").queryParam("q1", "java").queryParam("q2", "python").build())
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .exchange();
        monoGet.subscribe(clientResponse -> {
            log.info("----- headers: {}", clientResponse.headers());
            log.info("----- statusCode: {}", clientResponse.statusCode());
            clientResponse.bodyToMono(String.class).subscribe(body -> log.info("----- response: {}", body));
        });
        
        // formData post
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("q1", "java");
        params.add("q2", "python");
        Mono<ClientResponse> monoPost = webClient.post()
            .uri("https://httpbin.org/post")
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .body(BodyInserters.fromFormData(params)) // 设置formData,等价 BodyInserters.fromFormData("q1", "java").with("q2", "python")
            .exchange();
        monoPost.subscribe(clientResponse -> {
            log.info("----- headers: {}", clientResponse.headers());
            log.info("----- statusCode: {}", clientResponse.statusCode());
            clientResponse.bodyToMono(String.class).subscribe(body -> log.info("----- response: {}", body));
        });
        TimeUnit.SECONDS.sleep(2);
    }
    
    /**
     * WebClient异步调用
     * 
     * @throws InterruptedException
     */
    @Test
    public void testFormDataPost()
        throws InterruptedException
    {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("q1", "java");
        params.add("q2", "python");
        Mono<String> mono = webClient.post()
            .uri("https://httpbin.org/post")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .body(BodyInserters.fromFormData(params)) // 设置formData,等价 BodyInserters.fromFormData("q1", "java").with("q2", "python")
            .retrieve() // 获取响应体
            .bodyToMono(String.class); // 响应数据类型转换
        mono.subscribe(body -> log.info("----- response: {}", body));
        TimeUnit.SECONDS.sleep(2); // 重要，等待异步调用完成
    }
    
    /**
     * WebClient异步调用
     * 
     * @throws InterruptedException
     */
    @Test
    public void testGet001()
        throws InterruptedException
    {
        Mono<String> mono = webClient.get()
            .uri("https://httpbin.org/{method}", "get") // {任意命名}
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .retrieve()
            .bodyToMono(String.class);
        mono.subscribe(body -> log.info("----- response: {}", body));
        TimeUnit.SECONDS.sleep(2); // 重要，等待异步调用完成
    }
    
    /**
     * WebClient异步调用，结果转换为BlogData对象
     * 
     * @throws InterruptedException
     */
    @Test
    public void testGet002()
        throws InterruptedException
    {
        Mono<ClientResponse> mono = webClient.get()
            .uri("https://00fly.online/upload/data.json")
            .acceptCharset(StandardCharsets.UTF_8) // 编码
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .exchange();
        mono.subscribe(response -> {
            log.info("----- headers: {}", response.headers());
            log.info("----- statusCode: {}", response.statusCode());
            response.bodyToMono(BlogData.class).subscribe(body -> log.info("----- response: {}", body));
        });
        TimeUnit.SECONDS.sleep(2); // 重要，等待异步调用完成
    }
    
    /**
     * WebClient异步调用
     * 
     * @throws InterruptedException
     */
    @Test
    public void testGet003()
        throws InterruptedException
    {
        Mono<String> mono = webClient.get()
            .uri("https://httpbin.org/get")
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .exchange()
            .doOnSuccess(clientResponse -> log.info("----- headers: {}", clientResponse.headers()))
            .doOnSuccess(clientResponse -> log.info("----- statusCode: {}", clientResponse.statusCode()))
            .flatMap(clientResponse -> clientResponse.bodyToMono(String.class));
        mono.subscribe(body -> log.info("----- response: {}", body));
        TimeUnit.SECONDS.sleep(2); // 重要，等待异步调用完成
    }
    
    /**
     * WebClient异步调用, https://httpbin.org/get?q=java
     * 
     * @throws InterruptedException
     */
    @Test
    public void testGet004()
        throws InterruptedException
    {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("q", "java");
        String uri = UriComponentsBuilder.fromUriString("https://httpbin.org/get").queryParams(params).toUriString();
        
        // 注意比较
        // uri = UriComponentsBuilder.fromUriString("https://httpbin.org/get").queryParam("q", "java", "python").toUriString();
        // uri = UriComponentsBuilder.fromUriString("https://httpbin.org/get").queryParam("q1", "java").queryParam("q2", "python").toUriString();
        
        Mono<String> mono = webClient.get()
            .uri(uri)
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .exchange()
            .doOnSuccess(clientResponse -> log.info("----- headers: {}", clientResponse.headers()))
            .doOnSuccess(clientResponse -> log.info("----- statusCode: {}", clientResponse.statusCode()))
            .flatMap(clientResponse -> clientResponse.bodyToMono(String.class));
        mono.subscribe(body -> log.info("----- response: {}", body));
        TimeUnit.SECONDS.sleep(2); // 重要，等待异步调用完成
    }
    
    /**
     * WebClient异步调用
     * 
     * @throws InterruptedException
     */
    @Test
    public void testGet005()
        throws InterruptedException
    {
        Mono<String> mono = webClient.get()
            .uri(uriBuilder -> uriBuilder.scheme("https").host("httpbin.org").path("/get").queryParam("q1", "java").queryParam("q2", "python").build())
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .retrieve()
            .bodyToMono(String.class);
        mono.subscribe(body -> log.info("----- response: {}", body));
        TimeUnit.SECONDS.sleep(2); // 重要，等待异步调用完成
    }
    
    /**
     * WebClient异步调用
     * 
     * @throws InterruptedException
     */
    @Test
    public void testJsonBody001()
        throws InterruptedException
    {
        Mono<String> mono = webClient.post()
            .uri(uriBuilder -> uriBuilder.scheme("https").host("httpbin.org").path("/post").build())
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(Collections.singletonMap("q", "java"))
            .retrieve()
            .bodyToMono(String.class);
        mono.subscribe(body -> log.info("----- response: {}", body));
        TimeUnit.SECONDS.sleep(2); // 重要，等待异步调用完成
    }
    
    /**
     * WebClient异步调用
     * 
     * @throws IOException
     * @throws InterruptedException
     */
    @Test
    public void testJsonBody002()
        throws IOException, InterruptedException
    {
        Mono<String> mono;
        int num = RandomUtils.nextInt(1, 4);
        switch (num)
        {
            case 1: // 方式1,javaBean
                SearchReq req = new SearchReq();
                req.setPageNo(1);
                req.setPageSize(10);
                req.setKeyword("1");
                mono = webClient.post()
                    .uri("https://httpbin.org/post")
                    .acceptCharset(StandardCharsets.UTF_8)
                    .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
                    .bodyValue(req) // 设置JsonBody
                    .retrieve()
                    .bodyToMono(String.class);
                mono.subscribe(body -> log.info("response: {}", body));
                break;
            
            case 2: // 方式2,HashMap
                Map<String, String> params = new HashMap<>();
                params.put("pageNo", "2");
                params.put("pageSize", "20");
                params.put("keyword", "2");
                mono = webClient.post()
                    .uri("https://httpbin.org/post")
                    .acceptCharset(StandardCharsets.UTF_8)
                    .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
                    .bodyValue(params) // 设置JsonBody
                    .retrieve()
                    .bodyToMono(String.class);
                mono.subscribe(body -> log.info("response: {}", body));
                break;
            
            case 3: // 方式3,json字符串
                Map<String, String> params2 = new HashMap<>();
                params2.put("pageNo", "2");
                params2.put("pageSize", "20");
                params2.put("keyword", "2");
                mono = webClient.post()
                    .uri("https://httpbin.org/post")
                    .acceptCharset(StandardCharsets.UTF_8)
                    .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
                    .body(BodyInserters.fromValue(JsonBeanUtils.beanToJson(params2, false))) // 设置formData
                    .retrieve()
                    .bodyToMono(String.class);
                mono.subscribe(body -> log.info("response: {}", body));
                break;
        }
        TimeUnit.SECONDS.sleep(2); // 重要，等待异步调用完成
    }
    
    /**
     * WebClient异步调用
     * 
     * @throws InterruptedException
     */
    @Test
    public void testUpload001()
        throws InterruptedException
    {
        MultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
        params.add("q1", "java");
        params.add("q2", "python");
        params.add("file", new ClassPathResource("123.jpg"));
        Mono<String> mono = webClient.post()
            .uri("https://httpbin.org/post")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .body(BodyInserters.fromMultipartData(params)) // 设置formData,等价 BodyInserters.fromFormData("q1", "java").with("q2", "python")
            .retrieve() // 获取响应体
            .bodyToMono(String.class); // 响应数据类型转换
        mono.subscribe(body -> log.info("----- response: {}", body));
        TimeUnit.SECONDS.sleep(2); // 重要，等待异步调用完成
    }
    
    /**
     * WebClient异步调用
     * 
     * @throws InterruptedException
     */
    @Test
    public void testUpload002()
        throws InterruptedException
    {
        Mono<String> mono = webClient.post()
            .uri("https://httpbin.org/post")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML)
            .body(BodyInserters.fromMultipartData("q1", "java").with("q2", "python").with("file", new ClassPathResource("123.jpg")))
            .retrieve() // 获取响应体
            .bodyToMono(String.class); // 响应数据类型转换
        mono.subscribe(body -> log.info("----- response: {}", body));
        TimeUnit.SECONDS.sleep(2); // 重要，等待异步调用完成
    }
}
