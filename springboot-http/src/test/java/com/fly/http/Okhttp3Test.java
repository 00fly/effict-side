package com.fly.http;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import lombok.extern.slf4j.Slf4j;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@Slf4j
public class Okhttp3Test
{
    private static String uploadUrl = "http://124.71.129.204:8083/post";
    
    private static OkHttpClient client = new OkHttpClient();
    
    Resource resource = new ClassPathResource("data/nginx-1.25.3.tar.gz");
    
    @Test
    public void testPOST()
        throws IOException, InterruptedException
    {
        // File不能在Jar内定位文件，只适用与IDE开发阶段
        MultipartBody multipartBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
            .addFormDataPart("id", String.valueOf(System.currentTimeMillis() % 1000))
            .addFormDataPart("file", "nginx-1.25.3.tar.gz", RequestBody.create(MediaType.parse("application/octet-stream"), resource.getFile()))
            .build();
        Request request = new Request.Builder().url(uploadUrl).post(multipartBody).build();
        client.newCall(request).enqueue(new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e)
            {
                log.error(e.getMessage(), e);
            }
            
            @Override
            public void onResponse(Call call, Response response)
                throws IOException
            {
                log.info("{}", response.code());
            }
        });
        TimeUnit.SECONDS.sleep(2);
    }
}
