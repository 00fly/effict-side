package com.fly.http;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * UnirestTest
 * 
 * @author 00fly
 * @version [版本号, 2018年11月10日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
public class UnirestTest
{
    Resource resource = new ClassPathResource("data/nginx-1.25.3.tar.gz");
    
    @Test
    public void testGET()
    {
        try
        {
            HttpResponse<String> jsonResponse = Unirest.get("http://httpbin.org/post")
                .header("accept", "application/json")
                .queryString("id", "123456")// url后拼接的参数
                .asString();
            log.info("{}", jsonResponse.getStatus());
            log.info("{}", jsonResponse.getStatusText());
            log.info("{}", jsonResponse.getHeaders());
            log.info("{}", jsonResponse.getBody());
        }
        catch (UnirestException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @Test
    public void testAsynGet()
    {
        try
        {
            Unirest.get("http://httpbin.org/post")
                .header("accept", "application/json")
                .queryString("id", "123456")// url后拼接的参数
                .asStringAsync(response -> {
                    log.info("{}", response.getStatus());
                    log.info("{}", response.getStatusText());
                    log.info("{}", response.getHeaders());
                    log.info("{}", response.getBody());
                });
        }
        catch (UnirestException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @Test
    public void testPOST()
    {
        try
        {
            HttpResponse<JsonNode> jsonResponse = Unirest.post("http://httpbin.org/post")
                .header("accept", "application/json")
                .queryString("id", "123456")// url后拼接的参数
                .field("last", "Polo")
                .asJson();
            log.info("{}", jsonResponse.getStatus());
            log.info("{}", jsonResponse.getStatusText());
            log.info("{}", jsonResponse.getHeaders());
            log.info("{}", jsonResponse.getBody());
        }
        catch (UnirestException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @Test
    public void testPostFile()
    {
        try
        {
            HttpResponse<JsonNode> jsonResponse = Unirest.post("http://httpbin.org/post")
                .header("accept", "application/json")
                .field("id", "123456")
                .field("file", resource.getFile()) // 文件
                .asJson();
            log.info("{}", jsonResponse.getStatus());
            log.info("{}", jsonResponse.getStatusText());
            log.info("{}", jsonResponse.getHeaders());
            log.info("{}", jsonResponse.getBody());
        }
        catch (UnirestException | IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @Test
    public void testDownFile()
    {
        try
        {
            Unirest.get("https://00fly.online/upload/2019/02/201902262129360274AKuFZcUfip.jpg").asFile("D://001.jpg");
            
            HttpResponse<byte[]> response = Unirest.get("https://00fly.online/upload/2019/02/201902262129360274AKuFZcUfip.jpg").asBytes();
            ByteArrayInputStream bis = new ByteArrayInputStream(response.getBody());
            BufferedImage image = ImageIO.read(bis);
            ImageIO.write(image, "jpg", new File("D://saved.jpg"));
        }
        catch (UnirestException | IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
}
