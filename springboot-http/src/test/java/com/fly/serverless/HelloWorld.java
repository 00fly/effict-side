package com.fly.serverless;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
public class HelloWorld
{
    private WebClient webClient = WebClient.builder().codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(-1)).build();
    
    /**
     * WebClient异步调用
     */
    @Test
    public void testGet001()
    {
        Scanner sc = new Scanner(System.in);
        do
        {
            Mono<String> mono = webClient.get().uri("https://1304185684-e23ugl6eb6-sh.scf.tencentcs.com").acceptCharset(StandardCharsets.UTF_8).accept(MediaType.APPLICATION_JSON, MediaType.TEXT_HTML).retrieve().bodyToMono(String.class);
            mono.subscribe(log::info);
            log.info("------------输入x退出,回车换行继续------------");
        } while (!StringUtils.equalsIgnoreCase(sc.nextLine(), "x"));
        log.info("------------成功退出------------");
        sc.close();
    }
}
