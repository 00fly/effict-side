package com.fly.dns;

import java.io.IOException;
import java.util.Properties;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import io.leopard.javahost.JavaHost;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * JavaHostTest
 * 
 * @author 00fly
 * @version [版本号, 2023年3月15日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@Deprecated
public class JavaHostTest
{
    private RestTemplate restTemplate = new RestTemplate();
    
    @BeforeAll
    public static void init()
    {
        try
        {
            // 有BUG不建议使用，基本上已经停止维护，最后版本2018年
            // windows hosts文件有空行或#注释，报错：非法host记录[﻿].
            Resource resource = new ClassPathResource("vdns.properties");
            Properties props = PropertiesLoaderUtils.loadProperties(resource);
            JavaHost.updateVirtualDns(props);
            JavaHost.printAllVirtualDns();
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @Test
    public void test1()
    {
        String url = "http://top.00fly.online:8081/get";
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class);
        log.info("ResponseEntity StatusCode={}", responseEntity.getStatusCode());
        log.info("ResponseEntity={}", responseEntity);
        log.info("ResponseBody = {}", responseEntity.getBody());
    }
}
