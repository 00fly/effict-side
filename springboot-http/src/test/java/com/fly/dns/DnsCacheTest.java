package com.fly.dns;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.alibaba.dcm.DnsCache;
import com.alibaba.dcm.DnsCacheManipulator;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * DnsCacheTest
 * 
 * @author 00fly
 * @version [版本号, 2023年3月15日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
public class DnsCacheTest
{
    private RestTemplate restTemplate = new RestTemplate();
    
    @BeforeAll
    public static void init()
    {
        DnsCacheManipulator.loadDnsCacheConfig();
        DnsCache dnsCache = DnsCacheManipulator.getWholeDnsCache();
        log.info("dnsCache={}", dnsCache);
    }
    
    @Test
    public void test1()
    {
        String url = "http://top.00fly.online:8081/get";
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class);
        log.info("ResponseEntity StatusCode={}", responseEntity.getStatusCode());
        log.info("ResponseEntity={}", responseEntity);
        log.info("ResponseBody = {}", responseEntity.getBody());
    }
}
