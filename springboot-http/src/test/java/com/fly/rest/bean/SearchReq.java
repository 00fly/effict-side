package com.fly.rest.bean;

import lombok.Data;

/**
 * 
 * RequestBody对象
 * 
 * @author 00fly
 * @version [版本号, 2018年11月10日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Data
public class SearchReq
{
    Integer pageNo = 1;
    
    Integer pageSize = 10;
    
    String keyword;
}
