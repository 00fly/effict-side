package com.fly.rest.bean;

/**
 * 
 * InvokeResult
 * 
 * @author 00fly
 * @version [版本号, 2018年11月20日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class InvokeResult
{
    private Object data;
    
    private boolean success = false;
    
    private ErrorModel errorModel;
    
    /**
     * 业务成功
     */
    public static InvokeResult success(Object data)
    {
        InvokeResult result = new InvokeResult();
        result.data = data;
        return result;
    }
    
    /**
     * 业务异常 :
     */
    public static InvokeResult bys(String code)
    {
        InvokeResult result = new InvokeResult();
        result.success = false;
        result.setErrorModel(ErrorModel.bys(code));
        return result;
    }
    
    /**
     * 系统异常 sys:
     * 
     */
    public static InvokeResult sys(String code)
    {
        InvokeResult result = new InvokeResult();
        result.success = false;
        result.setErrorModel(ErrorModel.sys(code));
        return result;
    }
    
    public Object getData()
    {
        return data;
    }
    
    public boolean isSuccess()
    {
        return success;
    }
    
    public void setSuccess(boolean success)
    {
        this.success = success;
    }
    
    public void setData(Object data)
    {
        this.data = data;
    }
    
    public ErrorModel getErrorModel()
    {
        return errorModel;
    }
    
    public void setErrorModel(ErrorModel errorModel)
    {
        this.errorModel = errorModel;
    }
}
