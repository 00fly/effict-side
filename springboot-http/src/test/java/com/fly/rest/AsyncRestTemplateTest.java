package com.fly.rest;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.AsyncRestTemplate;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * Junit 单元测试
 * 
 * @author 00fly
 * @version [版本号, 2021年4月15日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@SuppressWarnings("deprecation")
public class AsyncRestTemplateTest
{
    public AsyncRestTemplate asyncRestTemplate()
    {
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        // 设置链接超时时间
        factory.setConnectTimeout(1000);
        // 设置读取资料超时时间
        factory.setReadTimeout(2000);
        // 设置异步任务（线程不会重用，每次调用时都会重新启动一个新的线程）
        factory.setTaskExecutor(new SimpleAsyncTaskExecutor());
        return new AsyncRestTemplate(factory);
    }
    
    /**
     * 通过网络环境测试，需要先启动应用
     * 
     * @throws InterruptedException
     * @see [类、类#方法、类#成员]
     */
    @Test
    public void test1()
        throws InterruptedException
    {
        // 异步发送
        String url = "http://localhost:8080/get";
        ListenableFuture<ResponseEntity<String>> entity = asyncRestTemplate().getForEntity(url, String.class);
        entity.addCallback(result -> log.info("return => {}", result.getBody()), e -> log.error(e.getMessage()));
        log.info("do something else!!!");
        TimeUnit.SECONDS.sleep(10);
    }
}
