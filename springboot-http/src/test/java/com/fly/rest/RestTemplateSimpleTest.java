package com.fly.rest;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fly.core.utils.JsonBeanUtils;
import com.fly.rest.bean.SearchReq;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RestTemplateSimpleTest
{
    private static RestTemplate restTemplate;
    
    static
    {
        int type = RandomUtils.nextInt(1, 5);
        log.info("--------- type：{}", type);
        switch (type)
        {
            case 1:
                // 使用默认客户端构造RestTemplate,写法1
                /*** Java HttpURLConnection (默认RestTemplate采用，不支持HTTP2) ***/
                restTemplate = new RestTemplate();
                break;
            
            case 2:
                // 使用默认客户端构造RestTemplate,写法2
                restTemplate = new RestTemplate(new SimpleClientHttpRequestFactory());
                break;
            
            case 3:
                // 使用默认客户端构造RestTemplate,写法3
                restTemplate = new RestTemplate();
                restTemplate.setRequestFactory(new SimpleClientHttpRequestFactory());
                break;
            
            case 4:
                // 使用默认客户端构造RestTemplate, 配置proxy，connectTimeout，readTimeout等参数
                SimpleClientHttpRequestFactory clientHttpRquestFactory = new SimpleClientHttpRequestFactory();
                clientHttpRquestFactory.setConnectTimeout(5000);
                clientHttpRquestFactory.setReadTimeout(65000);
                restTemplate = new RestTemplate(clientHttpRquestFactory);
                break;
            default:
        }
    }
    
    @Test
    public void testFormData001()
    {
        // POST请求只能用MultiValueMap
        String url = "https://httpbin.org/post";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("q1", "java");
        params.add("q2", "python");
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, headers);
        
        if (RandomUtils.nextBoolean())
        {
            String response = restTemplate.postForObject(url, requestEntity, String.class);
            log.info("ResponseBody={}", response);
        }
        else
        {
            ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, requestEntity, String.class);
            log.info("ResponseEntity={}", responseEntity.getBody());
        }
    }
    
    @Test
    public void testGet()
    {
        String url = "https://httpbin.org/get";
        String responseBody = restTemplate.getForObject(url, String.class);
        log.info("responseBody={}", responseBody);
    }
    
    /**
     * 演示@RequestBody请求
     * 
     * @throws IOException
     */
    @Test
    public void testJsonRequestBody()
        throws IOException
    {
        String url = "https://httpbin.org/post";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        Map<String, String> map = new HashMap<>();
        ResponseEntity<String> responseEntity;
        
        int num = RandomUtils.nextInt(1, 4);
        switch (num)
        {
            case 1: // 方式1,javaBean
                SearchReq req = new SearchReq();
                req.setPageNo(1);
                req.setPageSize(5);
                req.setKeyword("0");
                responseEntity = restTemplate.postForEntity(url, new HttpEntity<>(req, headers), String.class);
                log.info("ResponseEntity = {}", responseEntity);
                break;
            
            case 2: // 方式2,HashMap
                map.clear();
                map.put("pageNo", "2");
                map.put("pageSize", "10");
                map.put("keyword", "1");
                responseEntity = restTemplate.postForEntity(url, new HttpEntity<>(map, headers), String.class);
                log.info("ResponseEntity={}", responseEntity);
                break;
            
            case 3: // 方式3,Json字符串
                map.clear();
                map.put("pageNo", "3");
                map.put("pageSize", "15");
                map.put("keyword", "2");
                responseEntity = restTemplate.postForEntity(url, new HttpEntity<>(JsonBeanUtils.beanToJson(map, false), headers), String.class);
                log.info("ResponseEntity={}", responseEntity);
                break;
        }
    }
    
    @Test
    public void testUpload()
    {
        // POST请求只能用MultiValueMap
        String url = "https://httpbin.org/post";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        
        // Resource适用于文件、Jar內
        MultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
        Resource file = new ClassPathResource("123.jpg");
        // 多次调用，后台接受到的是MultipartFile[]
        params.add("file", file);
        params.add("file", file);
        
        params.add("name", "girl");
        params.add("age", "18");
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(params, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, requestEntity, String.class);
        log.info("ResponseEntity={}", responseEntity);
    }
}
