package com.fly.controller;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fly.HttpBootApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@WebAppConfiguration
@SpringBootTest(classes = HttpBootApplication.class)
public class MultipartControllerTest
{
    @Autowired
    private static WebApplicationContext wac;
    
    private static MockMvc mockMvc;
    
    @BeforeAll
    public static void setup()
    {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        log.info("★★★★★★★★ WebApplicationContext = {}", wac);
    }
    
    /**
     * 测试 RestAPI
     * 
     * @throws Exception
     * 
     * @see [类、类#方法、类#成员]
     */
    @Test
    public void testRestAPI()
        throws Exception
    {
        // get、post
        mockMvc.perform(MockMvcRequestBuilders.get("/get")).andDo(MockMvcResultHandlers.print());
        
        Resource resource = new ClassPathResource("123.jpg");
        MockMultipartFile file = new MockMultipartFile("file", "18.jpg", MediaType.MULTIPART_FORM_DATA_VALUE, resource.getInputStream());
        mockMvc.perform(MockMvcRequestBuilders.multipart("/post").file(file).param("name", "girl").param("age", "18")).andDo(MockMvcResultHandlers.print());
    }
}
