package com.fly.shell;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.fly.core.utils.ShellExecutor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ShellTest
{
    Resource resource = new ClassPathResource("commands");
    
    @Test
    public void testShells()
        throws IOException
    {
        // Java通用写法
        // InputStream is = this.getClass().getResourceAsStream("/commands")
        try (InputStream inputStream = resource.getInputStream())
        {
            IOUtils.readLines(inputStream, StandardCharsets.UTF_8)
                .stream()
                .parallel()// 并行
                .filter(line -> StringUtils.trimToEmpty(line).toLowerCase().startsWith("curl"))
                .map(line -> ShellExecutor.execute(line))
                .forEach(log::info);
        }
    }
}
