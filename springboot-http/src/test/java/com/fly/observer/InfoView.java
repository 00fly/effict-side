package com.fly.observer;

import java.util.Observable;
import java.util.Observer;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 观察者
 * 
 * @author 00fly
 * @version [版本号, 2018年12月11日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
public class InfoView implements Observer
{
    InfoThread thread = new InfoThread();
    
    public InfoView()
    {
        super();
        thread.addObserver(this);
    }
    
    /**
     * 观察者得到通知的处理
     * 
     * @param observable
     * @param msg
     */
    @Override
    public void update(Observable observable, Object msg)
    {
        if (msg instanceof String)
        {
            log.info("接受通知消息：{}", msg);
        }
    }
    
    /**
     * 启动被观察者
     * 
     * @see [类、类#方法、类#成员]
     */
    public void run()
    {
        new Thread(thread).start();
    }
    
    /**
     * Main
     * 
     * @param args
     * @see [类、类#方法、类#成员]
     */
    public static void main(String[] args)
    {
        new InfoView().run();
    }
}
