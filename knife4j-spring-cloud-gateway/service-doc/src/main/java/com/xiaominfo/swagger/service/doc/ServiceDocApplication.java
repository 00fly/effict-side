package com.xiaominfo.swagger.service.doc;

import java.net.InetAddress;

import org.apache.commons.lang.SystemUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

@EnableDiscoveryClient
@EnableEurekaClient
@SpringBootApplication
public class ServiceDocApplication
{
    @Value("${server.port}")
    String port;
    
    public static void main(String[] args)
    {
        SpringApplication.run(ServiceDocApplication.class, args);
    }
    
    @Bean
    CommandLineRunner openBrowser()
    {
        return args -> {
            if (SystemUtils.IS_OS_WINDOWS)// 防止非windows系统报错，启动失败
            {
                String ip = InetAddress.getLocalHost().getHostAddress();
                String url = "http://" + ip + ":" + port;
                Runtime.getRuntime().exec("cmd /c start " + url + "/doc.html");
            }
        };
    }
}
