package com.fly.simple;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;

import com.fly.simple.utils.FreeMarkerUtil;
import com.fly.simple.utils.ShellExecutor;

import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TemplateRun
{
    /**
     * Args4j是一个开源的Java库，由Kohsuke Kawaguchi开发。它通过注解的方式，使得命令行参数的解析变得异常简单
     * 
     * @param args
     * @throws IOException
     * @throws TemplateException
     * @throws InterruptedException
     */
    public static void main(String[] args)
        throws IOException, TemplateException, InterruptedException
    {
        // Jar运行，必须提供参数server.net.ip
        URL url = TemplateRun.class.getProtectionDomain().getCodeSource().getLocation();
        String ip = null;
        if (url.getPath().endsWith(".jar"))
        {
            if (args.length > 0)
            {
                ip = Stream.of(args).filter(arg -> arg.contains("--server.net.ip")).map(arg -> StringUtils.substringAfter(arg, "=")).collect(Collectors.joining());
                log.info("JarPath: {}, ServerIp: {}", url.getPath(), ip);
            }
            if (StringUtils.isBlank(ip))
            {
                log.error("please start jar like:\n java -jar docker-show-jar-with-dependencies.jar --server.net.ip=124.71.129.204");
                return;
            }
        }
        
        // 写入文件
        if (SystemUtils.IS_OS_WINDOWS)
        {
            File file = new File("index.html");
            creatPage(ip, file);
            
            // 打开页面10秒后删除文件
            Runtime.getRuntime().exec("cmd /c start /min " + file.getCanonicalPath());
            TimeUnit.SECONDS.sleep(10);
            file.deleteOnExit();
            return;
        }
        if (SystemUtils.IS_OS_LINUX)
        {
            // crontab -e
            // */30 * * * * java -jar /work/gitcode/docker-run/docker-show-jar-with-dependencies.jar --server.net.ip=124.71.129.204
            creatPage(ip, new File("/usr/share/nginx/html/index.html"));
        }
    }
    
    private static void creatPage(String ip, File file)
        throws IOException, TemplateException
    {
        // 收集docker信息
        Map<String, Object> model = new HashMap<>(3);
        model.put("date", new Date());
        model.put("map", ShellExecutor.getDockerInfo());
        model.put("ip", StringUtils.defaultIfBlank(ip, "127.0.0.1"));
        
        // {mysql5=[3306, 13306], mysql8=[23306], redis-server=[6379]}
        String content = FreeMarkerUtil.renderTemplate("/templates/index.html.ftl", model);
        try (FileWriter writer = new FileWriter(file))
        {
            writer.write(content);
            writer.flush();
        }
    }
}
