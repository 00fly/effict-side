package com.fly;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;

/**
 * https://blog.csdn.net/zzhongcy/article/details/105412842
 * 
 */
@Slf4j
@DisplayName("WebClient测试")
public class WebClientTest
{
    private static WebClient webClient;
    
    @BeforeAll
    public static void init()
    {
        // 默认底层使用Netty
        log.info("--- init ---");
        webClient = WebClient.builder().codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(-1)).build();
        
        // 内置支持Jetty反应性HttpClient实现，手工编码指定
        // webClient = WebClient.builder().baseUrl("https://blog.csdn.net").clientConnector(new JettyClientHttpConnector()).build();
    }
    
    @Test
    @DisplayName("getUrls请求")
    public void testGetUrls()
        throws InterruptedException, IOException
    {
        AtomicInteger count = new AtomicInteger(0);
        getUrls().stream()
            .forEach(url -> webClient.get()
                .uri(url)
                .acceptCharset(StandardCharsets.UTF_8)
                .accept(MediaType.TEXT_HTML)
                .retrieve()
                .bodyToMono(String.class)
                .subscribe(r -> log.info("process complted: {}. {}", count.incrementAndGet(), url), e -> log.error(e.getMessage())));
        log.info("异步请求已提交...");
        TimeUnit.SECONDS.sleep(10); // 重要，等待异步调用完成
    }
    
    private List<String> getUrls()
        throws IOException
    {
        log.info("★★★★★★★★ getData from webApi ★★★★★★★★");
        String resp = webClient.get().uri("https://00fly.online/upload/data.json").acceptCharset(StandardCharsets.UTF_8).accept(MediaType.APPLICATION_JSON).retrieve().bodyToMono(String.class).block();
        String jsonBody = resp.replace("{", "{\n").replace("}", "}\n").replace(",", ",\n");
        try (InputStream is = new ByteArrayInputStream(jsonBody.getBytes(StandardCharsets.UTF_8)))
        {
            List<String> lines = IOUtils.readLines(is, StandardCharsets.UTF_8);
            return lines.stream().filter(line -> StringUtils.contains(line, "\"url\":")).map(n -> StringUtils.substringBetween(n, ":\"", "\",")).collect(Collectors.toList());
        }
    }
}
