package com.fly;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import com.fly.core.util.ShellExecutor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WebUrlTest
{
    static List<String> urls;
    
    /**
     * 静态代码块初始化
     */
    static
    {
        try (InputStream is = new ClassPathResource("urls.txt").getInputStream())
        {
            urls = IOUtils.readLines(is, StandardCharsets.UTF_8)
                .stream()
                .filter(line -> StringUtils.isNotBlank(line)) // 过滤空白行
                .collect(Collectors.toList());
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @Test
    public void test()
    {
        urls.forEach(line -> ShellExecutor.execute("curl " + line, true));
    }
}
