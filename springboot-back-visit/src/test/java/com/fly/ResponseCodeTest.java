package com.fly;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * 获取请求状态码
 */
@Slf4j
public class ResponseCodeTest
{
    private static WebClient webClient;
    
    @BeforeAll
    public static void init()
    {
        log.info("--- init ---");
        webClient = WebClient.builder().codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(-1)).build();
    }
    
    @Test
    public void test()
        throws InterruptedException
    {
        Mono<ClientResponse> monoGet = webClient.get()
            .uri("https://blog.csdn.net/qq_16127313/article/details/136742785") // url
            .acceptCharset(StandardCharsets.UTF_8)
            .accept(MediaType.TEXT_HTML)
            .exchange()
            .timeout(Duration.ofSeconds(10));
        monoGet.subscribe(clientResponse -> log.info("----- statusCode: {}", clientResponse.statusCode().value()));
        TimeUnit.SECONDS.sleep(10);
    }
}
