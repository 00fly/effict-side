package com.fly.show;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

import com.fly.core.util.JsonBeanUtils;
import com.fly.core.util.ShellExecutor;
import com.fly.visit.entity.Article;
import com.fly.visit.entity.BlogData;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ByConditionTest
{
    /**
     * 演示带条件查询数据，满足条件立即终止
     */
    @Test
    public void test()
    {
        List<Article> articles = new ArrayList<>();
        IntStream.rangeClosed(1, 20)
            .peek(i -> log.info("page: {}", i))
            .mapToObj(i -> String.format("curl -e \"blog.csdn.net\" \"https://blog.csdn.net/community/home-api/v1/get-business-list?page=%s&size=10&businessType=blog&username=qq_16127313\"", i))
            .map(cmd -> ShellExecutor.execute(cmd))
            .anyMatch(json -> !parseToBlogData(json, articles));
        log.info("articles.size: {}", articles.size());
    }
    
    /**
     * 解析json为BlogData并追加数据
     * 
     * @param json
     * @return 是否追加成功
     */
    private boolean parseToBlogData(String json, List<Article> articles)
    {
        try
        {
            BlogData blogData = JsonBeanUtils.jsonToBean(json, BlogData.class, true);
            List<Article> nowArticles = blogData.getData().getList();
            if (!nowArticles.isEmpty())
            {
                articles.addAll(nowArticles);
                return true;
            }
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
        return false;
    }
}
