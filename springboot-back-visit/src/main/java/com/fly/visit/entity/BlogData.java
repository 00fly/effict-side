package com.fly.visit.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class BlogData
{
    private Record data;
}
