package com.fly.visit.entity;

import java.io.Serializable;
import java.util.Objects;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Article implements Serializable
{
    private static final long serialVersionUID = -1258005358981249797L;
    
    Long articleId;
    
    String title;
    
    Long viewCount;
    
    String url;
    
    public void addViewCount()
    {
        viewCount++;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Article other = (Article)obj;
        return Objects.equals(articleId, other.articleId);
    }
    
    @Override
    public int hashCode()
    {
        return Objects.hash(articleId);
    }
}
