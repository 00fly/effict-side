package com.fly.visit.job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fly.visit.service.WebVisitorSevice;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * SimpleJob
 * 
 * @author 00fly
 * @version [版本号, 2022年11月30日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@Component
@Configuration
public class SimpleJob
{
    @Autowired
    WebVisitorSevice webVisitorSevice;
    
    /**
     * 
     * 每隔30秒执行一次,秒数-分钟-小时-日期-月份-星期
     */
    @Scheduled(cron = "${cron:0/30 * * * * ?}")
    public void run()
    {
        try
        {
            webVisitorSevice.backVisit();
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    /**
     * 定时执行初始化
     */
    @Scheduled(cron = "0 0 */2 * * ?")
    public void init()
    {
        webVisitorSevice.init();
    }
}
