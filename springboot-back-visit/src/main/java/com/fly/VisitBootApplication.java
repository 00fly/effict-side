package com.fly;

import java.util.stream.Stream;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.SystemUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.fly.core.util.SpringContextUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@EnableScheduling
@SpringBootApplication
public class VisitBootApplication
{
    public static void main(String[] args)
    {
        // args = new String[] {"--noweb"};
        Stream.of(args).forEach(log::info);
        boolean web = !ArrayUtils.contains(args, "--noweb");
        log.info("############### with Web Configuration: {} #############", web);
        if (web)
        {
            SpringApplication.run(VisitBootApplication.class, args);
            return;
        }
        // 禁用knife4j，与DisableWebAutoConfig、docker-compose添加command: '--noweb --knife4j.enable=false'效果一样
        args = ArrayUtils.add(args, "--knife4j.enable=false");
        new SpringApplicationBuilder(VisitBootApplication.class).web(WebApplicationType.NONE).run(args);
    }
    
    /**
     * 接口实现类另一种写法
     */
    @Bean
    @ConditionalOnWebApplication
    CommandLineRunner init()
    {
        return args -> {
            if (SystemUtils.IS_OS_WINDOWS)// 防止非windows系统报错，启动失败
            {
                String url = SpringContextUtils.getServerBaseURL();
                Runtime.getRuntime().exec("cmd /c start /min " + url);
            }
        };
    }
}