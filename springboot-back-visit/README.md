
# back-web-visit

## 一、docker-compose.yml 中 entrypoint 和command 关系

### 1.如果只定义了`entrypoint`

容器启动时会执行`entrypoint` 指定的命令或脚本。

如果没有额外的参数传递，`entrypoint` 会以默认参数运行。

### 2.如果只定义了 `command`

容器启动时会执行 `command` 指定的命令。

如果没有 `entrypoint`，`command` 就是容器的主命令。

### 3.如果同时定义了 `entrypoint` 和 `command`

容器启动时会先执行 `entrypoint` 指定的命令或脚本。

`command` 指定的参数会作为 `entrypoint` 的参数传递给它。



## 二、功能优化点

### 1.优化接口鉴权注解


### 2.责任链模式实现数据初始化

    接口 -> docker映射文件 -> Jar资源文件 


