package com.fly.hello.web;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

@Api(tags = "Knife演示接口")
@RestController
@RequestMapping("/knife")
public class KnifeController
{
    @ApiOperation("post测试")
    @PostMapping("/post/test")
    public OutPutBean test(InputBean input)
    {
        return new OutPutBean();
    }
    
    @ApiOperation("json测试")
    @PostMapping("/json/test")
    public OutPutBean jsonTest(@RequestBody InputBean input)
    {
        return new OutPutBean();
    }
}

@Data
@ApiModel("入参模型")
class InputBean
{
    @ApiModelProperty("名称")
    private String name;
    
    @ApiModelProperty("取值")
    private String value;
}

@Data
@ApiModel("出参模型")
class OutPutBean
{
    @ApiModelProperty("名称")
    private String name;
    
    @ApiModelProperty("取值")
    private String value;
}