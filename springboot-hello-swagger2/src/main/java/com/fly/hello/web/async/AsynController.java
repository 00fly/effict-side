package com.fly.hello.web.async;

import java.io.InputStream;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyEmitter;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Callback和DeferredResult用于设置单个结果<BR>
 * 如果有多个结果需要set返回给客户端时，可以使用SseEmitter以及ResponseBodyEmitter<BR>
 * StreamingResponseBody可用来直接写入到response的OutputStream<BR>
 * see: https://blog.csdn.net/f641385712/article/details/88710676
 * 
 * @author 00fly
 *
 */
@Api(tags = "异步请求接口")
@RestController
@RequestMapping("/async")
public class AsynController
{
    // ExecutorService fixedThreadPool = Executors.newFixedThreadPool(5);
    ExecutorService fixedThreadPool = new ThreadPoolExecutor(10, 20, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(), new CustomizableThreadFactory("async-pool-"));
    
    @ApiOperation("Callable")
    @GetMapping("/response-body")
    public Callable<String> callable()
    {
        return () -> {
            TimeUnit.MILLISECONDS.sleep(2000);
            return "Callable result: " + RandomStringUtils.randomAlphabetic(10);
        };
    }
    
    @ApiOperation("ResponseBodyEmitter")
    @GetMapping("/responseBodyEmitter")
    public ResponseBodyEmitter responseBodyEmitter()
    {
        ResponseBodyEmitter responseBodyEmitter = new ResponseBodyEmitter();
        fixedThreadPool.submit(() -> {
            try
            {
                responseBodyEmitter.send("demo");
                TimeUnit.MICROSECONDS.sleep(2000);
                responseBodyEmitter.send("\ntest1:" + RandomStringUtils.randomAlphabetic(10));
                TimeUnit.MICROSECONDS.sleep(2000);
                responseBodyEmitter.send("\ntest2:" + RandomStringUtils.randomAlphabetic(10));
                responseBodyEmitter.complete();
            }
            catch (Exception ignore)
            {
            }
        });
        return responseBodyEmitter;
    }
    
    Resource resource = new ClassPathResource("data/pic/18.jpg");
    
    @ApiOperation("StreamingResponseBody")
    @GetMapping(value = "/streamBody", produces = MediaType.IMAGE_JPEG_VALUE)
    public StreamingResponseBody streamBody()
    {
        return (output) -> {
            try (InputStream input = resource.getInputStream())
            {
                output.write(IOUtils.toByteArray(input));
            }
        };
    }
}
