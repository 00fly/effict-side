package com.fly.hello.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fly.core.JsonResult;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@Api(tags = "演示排序接口")
@RestController
@RequestMapping("/demo")
public class DemoController
{
    @ApiOperationSupport(order = 100)
    @ApiOperation("001 @ApiImplicitParams演示")
    @ApiImplicitParams({@ApiImplicitParam(name = "key", value = "键", required = true), @ApiImplicitParam(name = "value", value = "值", required = true)})
    @GetMapping("/first")
    public JsonResult<?> first(String key, String value)
    {
        return JsonResult.success();
    }
    
    @ApiOperationSupport(order = 80)
    @ApiOperation("002 @ApiImplicitParam演示")
    @ApiImplicitParam(name = "key", value = "键", required = true)
    @GetMapping("/second")
    public JsonResult<?> second(String key)
    {
        return JsonResult.success();
    }
    
    @ApiOperationSupport(order = 60)
    @ApiOperation("003")
    @GetMapping("/third")
    public JsonResult<?> third()
    {
        return JsonResult.success();
    }
    
    @ApiOperationSupport(order = 40)
    @ApiOperation("004")
    @GetMapping("/fourth")
    public JsonResult<?> fourth()
    {
        return JsonResult.success();
    }
}
