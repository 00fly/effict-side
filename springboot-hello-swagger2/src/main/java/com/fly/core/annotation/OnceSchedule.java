package com.fly.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 集群环境定时任务调用注解，重复定时任务仅执行1次
 * 
 * @author 00fly
 *
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OnceSchedule
{
    /**
     * 下次运行时间间隔，单位秒
     */
    long nextSeconds();
}
