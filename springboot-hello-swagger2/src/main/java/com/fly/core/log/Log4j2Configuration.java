package com.fly.core.log;

import javax.sql.DataSource;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.db.ColumnMapping;
import org.apache.logging.log4j.core.appender.db.jdbc.ColumnConfig;
import org.apache.logging.log4j.core.appender.db.jdbc.JdbcAppender;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class Log4j2Configuration implements ApplicationListener<ContextRefreshedEvent>
{
    private final DataSource dataSource;
    
    public Log4j2Configuration(DataSource dataSource)
    {
        this.dataSource = dataSource;
    }
    
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent)
    {
        final LoggerContext ctx = LoggerContext.getContext(false);
        final ColumnConfig[] cc = {ColumnConfig.newBuilder().setConfiguration(ctx.getConfiguration()).setName("event_id").setPattern("%X{id}").setUnicode(false).build(),
            ColumnConfig.newBuilder().setConfiguration(ctx.getConfiguration()).setName("event_date").setEventTimestamp(true).setUnicode(false).build(),
            ColumnConfig.newBuilder().setConfiguration(ctx.getConfiguration()).setName("thread").setPattern("%t %x").setUnicode(false).build(),
            ColumnConfig.newBuilder().setConfiguration(ctx.getConfiguration()).setName("class").setPattern("%C").setUnicode(false).build(),
            ColumnConfig.newBuilder().setConfiguration(ctx.getConfiguration()).setName("`function`").setPattern("%M").setUnicode(false).build(),
            ColumnConfig.newBuilder().setConfiguration(ctx.getConfiguration()).setName("message").setPattern("%m").setUnicode(false).build(),
            ColumnConfig.newBuilder().setConfiguration(ctx.getConfiguration()).setName("exception").setPattern("%ex{full}").setUnicode(false).build(),
            ColumnConfig.newBuilder().setConfiguration(ctx.getConfiguration()).setName("level").setPattern("%level").setUnicode(false).build(),
            ColumnConfig.newBuilder().setConfiguration(ctx.getConfiguration()).setName("time").setPattern("%d{yyyy-MM-dd HH:mm:ss.SSS}").setUnicode(false).build()};
        
        // 配置appender
        final Appender appender = JdbcAppender.newBuilder()
            .setName("databaseAppender")
            .setIgnoreExceptions(false)
            .setConnectionSource(new ConnectionFactory(dataSource))
            .setTableName("boot_log")
            .setColumnConfigs(cc)
            .setColumnMappings(new ColumnMapping[0])
            .build();
        appender.start();
        
        ctx.getConfiguration().addAppender(appender);
        
        // 指定哪些logger输出的日志保存在mysql中
        ctx.getConfiguration().getLoggerConfig("com.fly.hello.job").addAppender(appender, Level.INFO, null);
        ctx.updateLoggers();
    }
}