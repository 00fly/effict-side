package com.fly.core.log;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.logging.log4j.core.appender.db.jdbc.AbstractConnectionSource;
import org.springframework.util.Assert;

public class ConnectionFactory extends AbstractConnectionSource
{
    private final DataSource dataSource;
    
    public ConnectionFactory(DataSource dataSource)
    {
        Assert.notNull(dataSource, "dataSource can not be null");
        this.dataSource = dataSource;
    }
    
    @Override
    public Connection getConnection()
        throws SQLException
    {
        return dataSource.getConnection();
    }
}