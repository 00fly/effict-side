package com.fly.core.utils;

import java.io.IOException;
import java.util.Properties;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.dataformat.javaprop.JavaPropsMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;

/**
 * Properties 转换工具
 * 
 * @author 00fly
 *
 */
public class PropertiesUtils
{
    private static YAMLMapper yamlMapper = new YAMLMapper();
    
    private static JavaPropsMapper javaPropsMapper = new JavaPropsMapper();
    
    private PropertiesUtils()
    {
        super();
    }
    
    /**
     * properties对象转Json字符串
     * 
     * @param properties对象
     * @return
     * @throws IOException
     */
    public static String propertiesToJson(Properties properties)
        throws IOException
    {
        JsonNode jsonNode = javaPropsMapper.readPropertiesAs(properties, JsonNode.class);
        return jsonNode.toPrettyString();
    }
    
    /**
     * properties对象转yaml
     * 
     * @param properties对象
     * @return
     * @throws IOException
     */
    public static String propertiesToYaml(Properties properties)
        throws IOException
    {
        JsonNode jsonNode = javaPropsMapper.readPropertiesAs(properties, JsonNode.class);
        return yamlMapper.writeValueAsString(jsonNode);
    }
    
    /**
     * properties字符串转Json字符串
     * 
     * @param propText
     * @return
     * @throws IOException
     */
    public static String propTextToJson(String propText)
        throws IOException
    {
        JsonNode jsonNode = javaPropsMapper.readTree(propText);
        return jsonNode.toPrettyString();
    }
    
    /**
     * properties字符串转yaml
     * 
     * @param propText
     * @return
     * @throws IOException
     */
    public static String propTextToYaml(String propText)
        throws IOException
    {
        JsonNode jsonNode = javaPropsMapper.readTree(propText);
        return yamlMapper.writeValueAsString(jsonNode);
    }
}
