package com.fly.core.config;

import java.util.Collections;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Knife4jConfig
 *
 */
@Configuration
@EnableKnife4j
@EnableSwagger2
@ConditionalOnWebApplication
public class Knife4jConfig
{
    /**
     * 开发、测试环境接口文档打开
     * 
     * @return
     * @see [类、类#方法、类#成员]
     */
    @Bean
    Docket asynApi()
    {
        return new Docket(DocumentationType.SWAGGER_2).enable(true)
            .apiInfo(apiInfo())
            .groupName("asyn接口")
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.fly.hello.web.asyn"))
            .paths(PathSelectors.any())
            .build()
            .securitySchemes(security());
    }
    
    @Bean
    Docket restApi()
    {
        return new Docket(DocumentationType.SWAGGER_2).enable(true)
            .apiInfo(apiInfo())
            .groupName("web接口")
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.fly.hello.web"))
            .paths(PathSelectors.regex("(?!.*async).*")) // url中不包含async
            .build()
            .securitySchemes(security());
    }
    
    private ApiInfo apiInfo()
    {
        return new ApiInfoBuilder().title("数据接口API").description("接口文档").termsOfServiceUrl("http://00fly.online/").version("1.0.0").build();
    }
    
    private List<ApiKey> security()
    {
        return Collections.singletonList(new ApiKey("token", "token", "header"));
    }
}