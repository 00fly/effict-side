package com.fly.core.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.DefaultPropertiesPersister;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class YamlUtilsTest
{
    String yamlContent;
    
    @BeforeEach
    public void init()
    {
        try
        {
            Resource resource = new ClassPathResource("application.yml");
            yamlContent = IOUtils.toString(resource.getURL(), StandardCharsets.UTF_8);
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @Test
    public void yamlToJson()
        throws IOException
    {
        String json = YamlUtils.yamlToJson(yamlContent);
        log.info("yamlToJson: {}", json);
    }
    
    @Test
    public void yamlToMap()
        throws IOException
    {
        Map<String, String> map = YamlUtils.yamlToMap(yamlContent);
        log.info("yamlToMap: {}", map);
    }
    
    @Test
    public void yamlToProperties1()
        throws IOException
    {
        Properties properties = YamlUtils.yamlToProperties(yamlContent);
        log.info("yamlToProperties: {}", properties);
    }
    
    @Test
    public void yamlToProperties2()
        throws IOException
    {
        // yaml->String->Properties
        String propText = YamlUtils.yamlToPropText(yamlContent);
        DefaultPropertiesPersister propertiesPersister = new DefaultPropertiesPersister();
        Properties props = new Properties();
        propertiesPersister.load(props, new ByteArrayInputStream(propText.getBytes()));
        log.info("yamlToProperties: {}", props);
    }
    
    @Test
    public void yamlToPropText()
        throws IOException
    {
        String text = YamlUtils.yamlToPropText(yamlContent);
        log.info("yamlToPropText: {}", text);
    }
}
