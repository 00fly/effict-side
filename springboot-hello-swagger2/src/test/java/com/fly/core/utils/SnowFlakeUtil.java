package com.fly.core.utils;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.net.NetUtil;
import cn.hutool.core.util.IdUtil;

/**
 * 
 * 雪花算法生成全局唯一ID
 * 
 * @author 00fly
 * @version [版本号, 2023年3月27日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Component
public class SnowFlakeUtil
{
    private long workerId = 0L;
    
    private long datacenterId = 1L;
    
    private Snowflake snowflake = IdUtil.createSnowflake(workerId, datacenterId);
    
    @PostConstruct
    public void init()
    {
        try
        {
            workerId = NetUtil.ipv4ToLong(NetUtil.getLocalhostStr());
        }
        catch (Exception e)
        {
            workerId = NetUtil.getLocalhostStr().hashCode();
        }
    }
    
    public synchronized long snowflakeId(long workerId, long datacenterId)
    {
        Snowflake snowflake = IdUtil.createSnowflake(workerId, datacenterId);
        return snowflake.nextId();
    }
    
    public synchronized long snowflakeId()
    {
        return snowflake.nextId();
    }
}
