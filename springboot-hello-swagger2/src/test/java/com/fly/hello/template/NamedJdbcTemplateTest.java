package com.fly.hello.template;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Transactional
@SpringBootTest
public class NamedJdbcTemplateTest
{
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;
    
    @Autowired
    JdbcOperations jdbcOperations;
    
    RowMapper<StudentVO> rowMapper = new BeanPropertyRowMapper<>(StudentVO.class);
    
    @BeforeEach
    public void init()
    {
        /****** JdbcOperations实际就是JdbcTemplate ******/
        // JdbcOperations jdbcOperations = namedJdbcTemplate.getJdbcOperations();
        
        jdbcOperations.execute("drop table if exists student");
        jdbcOperations.execute("create table student(id int not null AUTO_INCREMENT, name varchar(50), create_date datetime, primary key(id))");
        
        String time = DateFormatUtils.format(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss");
        jdbcOperations.update("insert into student(name, create_date) values('Jack', ?)", time);
        jdbcOperations.update("insert into student(name, create_date) values('Phil', ?)", time);
        jdbcOperations.update("insert into student(name, create_date) values('Jenny', ?)", time);
        jdbcOperations.execute("delete from student where id > 100");
        log.info("before::: init success!!");
    }
    
    @Test
    public void testUpdate()
    {
        // update方法进行增删改-写法1
        String time = DateFormatUtils.format(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss");
        String sql = "insert into student(name, create_date) values(:name, :time)";
        Map<String, Object> params = new HashMap<>();
        params.put("name", RandomStringUtils.randomAlphabetic(5));
        params.put("time", time);
        int count = namedJdbcTemplate.update(sql, params);
        log.info("update count: {}, data: {}", count, jdbcOperations.queryForList("select * from student"));
        
        // update方法进行增删改-写法2
        count = namedJdbcTemplate.update(sql, new MapSqlParameterSource("name", RandomStringUtils.randomAlphabetic(5)).addValue("time", time));
        log.info("update count: {}, data: {}", count, jdbcOperations.queryForList("select * from student"));
        
        // update方法进行增删改-写法3
        count = namedJdbcTemplate.update(sql, new BeanPropertySqlParameterSource(new StudentVO().setName(RandomStringUtils.randomAlphabetic(5)).setTime(new Date())));
        log.info("update count: {}, data: {}", count, jdbcOperations.queryForList("select * from student"));
    }
    
    @Test
    public void testQuery()
    {
        // 查询主键
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedJdbcTemplate.update("insert into student(name, create_date) values(:name, :time)", new BeanPropertySqlParameterSource(new StudentVO().setName(RandomStringUtils.randomAlphabetic(5)).setTime(new Date())), keyHolder);
        long pk = keyHolder.getKey().longValue();
        log.info("pk: {}", pk);
        
        // 读取单个对象
        StudentVO studentVO = namedJdbcTemplate.queryForObject("select id, name from student where id=:id", new BeanPropertySqlParameterSource(new StudentVO().setId(pk)), new BeanPropertyRowMapper<>(StudentVO.class));
        log.info("{}", studentVO);
        
        // IN条件
        List<Map<String, Object>> list = namedJdbcTemplate.queryForList("select id, name from student where id in (:ids)", Collections.singletonMap("ids", Arrays.asList(1, 2, 3, 4, 5)));
        log.info("{}", list);
        
        // like
        log.info("----- like -----");
        log.info("{}", namedJdbcTemplate.queryForList("select * from student where name like :name", Collections.singletonMap("name", "J%")));
        log.info("{}", namedJdbcTemplate.queryForList("select * from student where name like concat('%', :name, '%')", Collections.singletonMap("name", "J%")));
    }
    
    @Test
    public void test1()
    {
        // update方法进行增删改
        int count = jdbcOperations.update("insert into student(id, name, create_date) values(?,?, now())", 4, RandomStringUtils.randomAlphabetic(5));
        log.info("{}", count);
        
        // 读取单个对象
        StudentVO studentVO = jdbcOperations.queryForObject("select id, name from student where id=?", rowMapper, 1);
        log.info("{}", studentVO);
        
        // 读取多个对象
        List<StudentVO> list = jdbcOperations.query("select id, name from student", rowMapper);
        log.info("{}", list);
        
        // 返回 List<Map<String, Object>>
        List<Map<String, Object>> data = jdbcOperations.queryForList("select id, name from student");
        log.info("{}", data);
        
        // 返回 Map<String, Object>
        Map<String, Object> map = jdbcOperations.queryForMap("select * from student where id=?", 2);
        log.info("{}", map);
        
        // 返回指定类型
        count = jdbcOperations.queryForObject("select count(*) from student", Integer.class);
        String name = jdbcOperations.queryForObject("select name from student where id=?", String.class, 4);
        log.info("{}", count);
        log.info("{}", name);
        
        // 批量插入
        List<Object[]> batchArgs = new ArrayList<Object[]>();
        
        // datetime列传时间字符串或Date都可以
        String time = DateFormatUtils.format(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        batchArgs.add(new Object[] {10, "caoyc", time});
        batchArgs.add(new Object[] {11, "zhh", now});
        batchArgs.add(new Object[] {12, "cjx", now});
        jdbcOperations.batchUpdate("insert into student(id, name, create_date) values(?,?,?)", batchArgs);
        
        // 执行批量操作
        // List<Map<String, String>> data;
        // return namedJdbcTemplate.batchUpdate(sql, SqlParameterSourceUtils.createBatch(data));
    }
}
