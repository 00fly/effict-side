package com.fly.hello.template;

import java.util.Date;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class StudentVO
{
    private Long id;
    
    private String name;
    
    private Date time;
}
