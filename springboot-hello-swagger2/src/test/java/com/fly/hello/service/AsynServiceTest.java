package com.fly.hello.service;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fly.core.JsonResult;
import com.fly.hello.service.impl.AsynServiceImpl;

public class AsynServiceTest
{
    @Mock
    RestTemplate restTemplate;
    
    @InjectMocks
    AsynServiceImpl asynService;
    
    @Test
    public void testCall()
    {
        String url = "http://127.0.0.1:8080/upload2";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        MultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
        params.add("file", new ClassPathResource("data/pic/001.jpg"));
        params.add("id", "1");
        params.add("name", "girl");
        params.add("sex", "F");
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(params, headers);
        JsonResult<Object> result = new JsonResult<>();
        result.setSuccess(true);
        result.setMessage("测试ok");
        result.setData("nothing,just test");
        ResponseEntity<JsonResult> responseEntity = new ResponseEntity<>(result, HttpStatus.OK);
        Mockito.when(restTemplate.postForEntity(url, requestEntity, JsonResult.class)).thenReturn(responseEntity);
        asynService.call();
        Assert.isTrue(responseEntity != null, "can not be null");
    }
}
