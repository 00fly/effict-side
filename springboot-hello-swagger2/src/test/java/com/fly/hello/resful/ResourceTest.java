package com.fly.hello.resful;

import java.io.IOException;
import java.net.URL;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import com.fly.HelloApplication;
import com.github.xiaoymin.knife4j.annotations.Ignore;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest(classes = HelloApplication.class)
public class ResourceTest
{
    @Autowired
    ResourceLoader resourceLoader;
    
    @Test
    public void testInProject()
        throws IOException
    {
        // 统一资源定位符 Uniform Resource Locator
        // 统一资源标志符 Uniform Resource Identifier
        Resource resource = new ClassPathResource("data/pic/18.jpg");
        log.info("--- url.path: {}", resource.getURL().getPath());
        log.info("--- uri.path: {}", resource.getURI().getPath());
        // /C:/gitee/effict-side/springboot-hello/target/classes/data/pic/18.jpg
        // /C:/gitee/effict-side/springboot-hello/target/classes/data/pic/18.jpg
        
        resource = resourceLoader.getResource("classpath:data/pic/18.jpg");
        log.info("--- url.path: {}", resource.getURL().getPath());
        log.info("--- uri.path: {}", resource.getURI().getPath());
        // /C:/gitee/effict-side/springboot-hello/target/classes/data/pic/18.jpg
        // /C:/gitee/effict-side/springboot-hello/target/classes/data/pic/18.jpg
        
        URL url = resourceLoader.getClassLoader().getResource("data/pic/18.jpg");
        log.info("--- url.path: {}", url.getPath());
        // /C:/gitee/effict-side/springboot-hello/target/classes/data/pic/18.jpg
        
        resource = new PathResource("data/pic/18.jpg");
        log.info("--- url.path: {}", resource.getURL().getPath());
        log.info("--- uri.path: {}", resource.getURI().getPath());
        // /C:/gitee/effict-side/springboot-hello/data/pic/18.jpg
        // /C:/gitee/effict-side/springboot-hello/data/pic/18.jpg
    }
    
    /**
     * InJar运行
     */
    @Test
    @Ignore
    public void testInJar()
        throws IOException
    {
        Resource resource = new ClassPathResource("data/pic/18.jpg");
        log.info("--- url.path: {}", resource.getURL().getPath());
        log.info("--- uri.path: {}", resource.getURI().getPath());
        // springboot-hello-1.0.0.jar!/BOOT-INF/classes!/data/pic/18.jpg
        // null
        
        resource = resourceLoader.getResource("classpath:data/pic/18.jpg");
        log.info("--- url.path: {}", resource.getURL().getPath());
        log.info("--- uri.path: {}", resource.getURI().getPath());
        // springboot-hello-1.0.0.jar!/BOOT-INF/classes!/data/pic/18.jpg
        // null
        
        URL url = resourceLoader.getClassLoader().getResource("data/pic/18.jpg");
        log.info("--- url.path: {}", url.getPath());
        // springboot-hello-1.0.0.jar!/BOOT-INF/classes!/data/pic/18.jpg
        
        resource = new PathResource("data/pic/18.jpg");
        log.info("--- url.path: {}", resource.getURL().getPath());
        log.info("--- uri.path: {}", resource.getURI().getPath());
        // /C:/Users/Administrator/Desktop/JAR/data/pic/18.jpg
        // /C:/Users/Administrator/Desktop/JAR/data/pic/18.jpg
    }
}
