package com.fly.hello.simple.input;

import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.util.NumberUtils;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * Application 运行端口测试 <br>
 * <br>
 * docker run --name tomcat -it -d -p 8081:8080 -p 8080:8080 tomcat:alpine
 * 
 * @author 00fly
 * @version [版本号, 2022年11月25日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
public class ApplicationPortTest
{
    RestTemplate restTemplate = new RestTemplate();
    
    @Test
    public void test()
    {
        try (Scanner sc = new Scanner(System.in))
        {
            do
            {
                runCall();
                log.info("------------输入x退出,回车换行继续------------");
            } while (!"x".equalsIgnoreCase(sc.nextLine()));
            log.info("------------成功退出------------");
        }
    }
    
    /**
     * 线程池方式测试
     * 
     * @see [类、类#方法、类#成员]
     */
    private void runCall()
    {
        ResourceBundle resource = ResourceBundle.getBundle("application");
        String baseUrl = resource.getString("server.search.url");
        Integer portBegin = NumberUtils.parseNumber(resource.getString("server.search.port.begin"), Integer.class);
        Integer portEnd = NumberUtils.parseNumber(resource.getString("server.search.port.end"), Integer.class);
        String[] visits = resource.getString("server.search.visit-list").split(",");
        
        // 线程池
        // ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
        ExecutorService cachedThreadPool = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60L, TimeUnit.SECONDS, new SynchronousQueue<Runnable>(), new CustomizableThreadFactory("cached-pool-"));
        for (int i = portBegin; i < portEnd; i++)
        {
            final int port = i;
            cachedThreadPool.execute(() -> openBrowser(baseUrl, port, visits));
        }
    }
    
    private void openBrowser(String baseUrl, int port, String[] visits)
    {
        Arrays.stream(visits).forEach(path -> {
            try
            {
                String url = baseUrl + ":" + port + "/" + path;
                ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class);
                if (responseEntity.getStatusCode() == HttpStatus.OK)
                {
                    log.info("now open url: {}", url);
                    Runtime.getRuntime().exec("cmd /c start /min " + url);
                }
            }
            catch (Exception e)
            {
            }
        });
    }
}
