package com.fly.hello.simple.input;

import java.util.Scanner;

import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LevelNumTest
{
    @Test
    public void testAdd()
    {
        try (Scanner sc = new Scanner(System.in))
        {
            int init = 600;
            do
            {
                int next = init % 600 + 100;
                log.info("next: {}", next);
                init = next;
                log.info("------------输入x退出,回车换行继续------------");
            } while (!"x".equalsIgnoreCase(sc.nextLine()));
            log.info("------------成功退出------------");
        }
    }
    
    @Test
    public void testSub()
    {
        try (Scanner sc = new Scanner(System.in))
        {
            int init = 600;
            do
            {
                int next = (init - 100 < 0 ? 600 : init - 100);
                log.info("next: {}", next);
                init = next;
                log.info("------------输入x退出,回车换行继续------------");
            } while (!"x".equalsIgnoreCase(sc.nextLine()));
            log.info("------------成功退出------------");
        }
    }
}
