#  Java实现文件目录监听与数据处理

命令行方式传递监听文件目录参数： **java -jar files-listener-0.0.1.jar --path=/work/test**

数据处理结果文件位置在`config.properties`配置：

```
out = /work/docker/blog-all-in-one/compose/upload/result.json

```

如何运行JsonDataInit

 - **运行时指定启动类**

```shell
#指定mainClass
java -cp files-listener-0.0.1.jar com.fly.simple.JsonDataWrite

```

 - **编译时指定启动类**

```shell
mvn clean package -f pom-write.xml

```