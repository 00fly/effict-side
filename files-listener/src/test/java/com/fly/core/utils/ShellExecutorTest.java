package com.fly.core.utils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import com.fly.simple.utils.ShellExecutor;

public class ShellExecutorTest
{
    /**
     * 获取接口数据写入文件
     */
    @Test
    public void test001()
        throws IOException
    {
        String response = ShellExecutor.execute("curl https://00fly.online/upload/data.json");
        FileUtils.writeStringToFile(new File("test.json"), response, StandardCharsets.UTF_8, false);
    }
    
    /**
     * 直接输出到文件
     */
    @Test
    public void test002()
        throws IOException
    {
        ShellExecutor.exec("curl https://00fly.online/upload/data.json  -o test.json");
        ShellExecutor.exec("curl https://00fly.online/upload/data.json  --output test2.json");
    }
}
