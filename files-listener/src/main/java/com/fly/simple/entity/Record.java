package com.fly.simple.entity;

import java.util.List;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Record
{
    private List<Article> list;
}
