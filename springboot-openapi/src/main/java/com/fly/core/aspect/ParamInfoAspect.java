package com.fly.core.aspect;

import java.lang.reflect.Method;
import java.util.stream.Stream;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.fly.openapi.annotation.OpenApi;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Aspect
@Order(-1)
@Component
public class ParamInfoAspect
{
    /**
     * 使用OpenApi注解标注的类
     */
    @Before("@annotation(openApi)")
    public void doBefore(JoinPoint joinPoint, OpenApi openApi)
        throws Throwable
    {
        log.info("###### ParamInfoAspect");
        String className = joinPoint.getTarget().getClass().getSimpleName();
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature)signature;
        Method method = methodSignature.getMethod();
        Class<?> methodReturnType = method.getReturnType();
        String methodName = new StringBuffer(className).append('.').append(method.getName()).toString();
        
        log.info("全限定类名：{}", signature.getDeclaringTypeName());
        log.info("执行方法：{}", methodName);
        log.info("返回类型：{}", methodReturnType.getName());
        Stream.of(methodSignature.getParameterTypes()).forEach(arg -> log.info("参数类型:{}", arg));
        Stream.of(methodSignature.getParameterNames()).forEach(arg -> log.info("参数名:{}", arg));
        Stream.of(joinPoint.getArgs()).forEach(arg -> log.info("参数值:{}", arg));
    }
}
