package com.fly.core.exception;

import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.fly.core.entity.JsonResult;
import com.fly.core.utils.HttpServletUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 统一异常处理器
 * 
 * @author 00fly
 * @version [版本号, 2018-09-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler
{
    /**
     * 参考：https://blog.csdn.net/Android_Michael/article/details/128833296
     * 
     * @param dataBinder
     */
    @InitBinder
    public void processParam(WebDataBinder dataBinder)
    {
        // 注意：此处无法获取SpringBean
        String token = HttpServletUtils.getRequest().getHeader("token");
        log.info("#### token: {}", token);
        
        Object object = dataBinder.getTarget();
        if (object != null)
        {
            // 获取绑定到实体的对象
            log.info("#### {}", object);
        }
    }
    
    @ExceptionHandler(Exception.class)
    public JsonResult<?> handleBadRequest(Exception exception)
    {
        // JSR303参数校验异常
        if (exception instanceof BindException)
        {
            BindingResult bindingResult = ((BindException)exception).getBindingResult();
            if (null != bindingResult && bindingResult.hasErrors())
            {
                return JsonResult.error(bindingResult.getFieldErrors().stream().map(e -> e.getDefaultMessage()).sorted().collect(Collectors.joining(",")));
            }
        }
        if (exception instanceof MethodArgumentNotValidException)
        {
            BindingResult bindingResult = ((MethodArgumentNotValidException)exception).getBindingResult();
            if (null != bindingResult && bindingResult.hasErrors())
            {
                return JsonResult.error(bindingResult.getFieldErrors().stream().map(e -> e.getDefaultMessage()).sorted().collect(Collectors.joining(",")));
            }
        }
        
        // 手动抛出的ValidateException
        if (exception instanceof ValidateException)
        {
            return JsonResult.error(exception.getMessage());
        }
        
        // 其余情况
        log.error("Error: handleBadRequest StackTrace : {}", exception.getMessage());
        return JsonResult.error(StringUtils.defaultString(exception.getMessage(), "系统异常，请联系管理员"));
    }
}
