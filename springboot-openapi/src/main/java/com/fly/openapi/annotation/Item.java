package com.fly.openapi.annotation;

public enum Item
{
    /**
     * 鉴权
     **/
    TOKEN,
    /**
     * 防重放，不可重复提交
     **/
    NO_REPEAT,
    /**
     * 防篡改
     **/
    UN_MODIFY,
    /**
     * 全部检查
     **/
    ALL,;
}