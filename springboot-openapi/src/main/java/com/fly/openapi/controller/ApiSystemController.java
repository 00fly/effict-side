package com.fly.openapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fly.core.entity.JsonResult;
import com.fly.openapi.interceptor.ApiVersionInterceptor;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags = "自动化接口后台系统控制")
@RestController
@RequestMapping("/auto/system")
public class ApiSystemController
{
    @Autowired
    ApiVersionInterceptor apiVersionInterceptor;
    
    @ApiOperationSupport(order = 10)
    @ApiOperation("启用接口版本控制")
    @PostMapping("/version/enable")
    public JsonResult<?> enable()
    {
        log.info("启用接口版本控制");
        apiVersionInterceptor.setVersionEnable(true);
        return JsonResult.success();
    }
    
    @ApiOperationSupport(order = 10)
    @ApiOperation("禁用接口版本控制")
    @PostMapping("/version/disable")
    public JsonResult<?> disable()
    {
        log.info("禁用接口版本控制");
        apiVersionInterceptor.setVersionEnable(false);
        return JsonResult.success();
    }
}
