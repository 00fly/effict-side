package com.fly.openapi.controller.v2;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fly.core.entity.JsonResult;
import com.fly.openapi.annotation.Item;
import com.fly.openapi.annotation.OpenApi;
import com.fly.openapi.entity.UserInfo;
import com.fly.openapi.feign.MyFeignClient;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

@Api(tags = "测试@OpenApi-v2")
@RestController
@RequestMapping("/openapi/v2/")
public class OpenApiV2Controller
{
    Resource resource = new ClassPathResource("pic/001.jpg");
    
    @Autowired
    private MyFeignClient feignClient;
    
    /**
     * Valid的验证先于切面执行,不符合业务习惯,待调整
     * 
     * @param user
     * @param token
     * @param userInfo
     * @return
     */
    @OpenApi(checkItems = Item.TOKEN)
    @ApiOperation("测试提交数据")
    @PostMapping("/test")
    @ApiImplicitParam(name = "token", value = "鉴权token", required = true)
    public JsonResult<UserInfo> test(@RequestHeader String user, @RequestHeader String token, @Valid @RequestBody UserInfo userInfo, BindingResult bindingResult)
    {
        return feignClient.test(userInfo);
    }
}
