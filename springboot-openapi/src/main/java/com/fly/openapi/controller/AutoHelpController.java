package com.fly.openapi.controller;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fly.core.entity.JsonResult;
import com.fly.openapi.entity.AuthToken;
import com.fly.openapi.service.AutoTokenService;
import com.fly.openapi.service.TimestampService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags = "接口辅助")
@RestController
@RequestMapping("/auto/help")
public class AutoHelpController
{
    @Autowired
    AutoTokenService autoTokenService;
    
    @Autowired
    TimestampService timestampService;
    
    /**
     * 获取用户token
     * 
     * @param user
     * @return
     * @see [类、类#方法、类#成员]
     */
    @ApiOperation("获取用户token")
    @GetMapping("/getToken")
    @ApiImplicitParam(name = "user", value = "用户名", required = true)
    public JsonResult<AuthToken> getToken(@RequestParam String user)
    {
        log.info("getToken for {}", user);
        return JsonResult.success(autoTokenService.getToken(user));
    }
    
    @ApiOperation("批量获取用户timestamp")
    @GetMapping("/getBatchTimestamps")
    public JsonResult<?> getBatchTimestamps(@RequestParam String user)
    {
        log.info("getBatchTimestamps for {}", user);
        return JsonResult.success(Collections.singletonMap("timestamps", timestampService.batchGet(user)));
    }
    
    @ApiOperation("消费timestamp")
    @GetMapping("/useTimestamp")
    public JsonResult<?> useTimestamp(@RequestParam String user, Long timestamp)
    {
        log.info("useTimestamp for {}", user);
        return JsonResult.success(Collections.singletonMap("isFirstUse", timestampService.isFirstUse(user, timestamp)));
    }
}