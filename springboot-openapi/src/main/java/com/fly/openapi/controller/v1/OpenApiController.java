package com.fly.openapi.controller.v1;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.fly.core.entity.JsonResult;
import com.fly.core.utils.JsonBeanUtils;
import com.fly.openapi.annotation.Item;
import com.fly.openapi.annotation.OpenApi;
import com.fly.openapi.entity.UserInfo;
import com.fly.openapi.feign.MyFeignClient;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags = "测试@OpenApi")
@RestController
@RequestMapping("/openapi/v1/")
public class OpenApiController
{
    Resource resource = new ClassPathResource("pic/001.jpg");
    
    @Autowired
    private MyFeignClient feignClient;
    
    @OpenApi(checkItems = Item.ALL)
    @ApiOperation("关键字搜索")
    @PostMapping("/post")
    public String post(@RequestHeader String user, @RequestHeader String token, @RequestHeader String timestamp, @RequestHeader String sign, String keyword)
    {
        log.info("request keyword: {}", keyword);
        return feignClient.post(keyword);
    }
    
    /**
     * Valid的验证先于切面执行,不符合业务习惯,待调整
     * 
     * @param user
     * @param token
     * @param userInfo
     * @return
     */
    @OpenApi(checkItems = Item.TOKEN)
    @ApiOperation("测试提交数据")
    @PostMapping("/test")
    @ApiImplicitParam(name = "token", value = "鉴权token", required = true)
    public JsonResult<UserInfo> test(@RequestHeader String user, @RequestHeader String token, @Valid @RequestBody UserInfo userInfo, BindingResult bindingResult)
    {
        return feignClient.test(userInfo);
    }
    
    /**
     * 无返回值aop无法改变返回结果
     * 
     * @param response
     * @throws IOException
     */
    @OpenApi(checkItems = Item.ALL)
    @ApiOperation("图片下载void")
    @GetMapping(value = "/pic/void", produces = {MediaType.APPLICATION_OCTET_STREAM_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public void pic1(@RequestHeader String user, @RequestHeader String token, @RequestHeader String timestamp, @RequestHeader String sign, HttpServletResponse response)
        throws IOException
    {
        if (RandomUtils.nextBoolean())
        {
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            response.setHeader("Content-Disposition", "attachment;filename=001.jpg");
            IOUtils.copy(resource.getInputStream(), response.getOutputStream());
            return;
        }
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setHeader("Content-Disposition", "attachment;filename=result.txt");
        response.getWriter().write(JsonBeanUtils.beanToJson(JsonResult.success()));
    }
    
    /**
     * aop改变返回结果
     * 
     * @param response
     * @throws IOException
     */
    @OpenApi(checkItems = Item.ALL)
    @ApiOperation("图片下载byte[]")
    @GetMapping(value = "/pic/bytes", produces = {MediaType.APPLICATION_OCTET_STREAM_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public byte[] pic2(@RequestHeader String user, @RequestHeader String token, @RequestHeader String timestamp, @RequestHeader String sign, HttpServletResponse response)
        throws IOException
    {
        if (RandomUtils.nextBoolean())
        {
            try (InputStream inputStream = resource.getInputStream())
            {
                response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
                response.setHeader("Content-Disposition", "attachment;filename=001.jpg");
                return IOUtils.toByteArray(inputStream);
            }
        }
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setHeader("Content-Disposition", "attachment;filename=result.txt");
        return JsonBeanUtils.beanToJson(JsonResult.success()).getBytes(StandardCharsets.UTF_8);
    }
    
    /**
     * aop里怎么新建StreamingResponseBody？答案：无需返回StreamingResponseBody，只需在response写入数据即可
     * 
     * @param response
     * @throws IOException
     */
    @OpenApi(checkItems = Item.ALL)
    @ApiOperation("图片下载Stream")
    @GetMapping(value = "/pic/stream", produces = {MediaType.APPLICATION_OCTET_STREAM_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public StreamingResponseBody pic3(@RequestHeader String user, @RequestHeader String token, @RequestHeader String timestamp, @RequestHeader String sign, HttpServletResponse response)
        throws IOException
    {
        return (output) -> {
            if (RandomUtils.nextBoolean())
            {
                try (InputStream input = resource.getInputStream())
                {
                    response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
                    response.setHeader("Content-Disposition", "attachment;filename=001.jpg");
                    output.write(IOUtils.toByteArray(input));
                }
            }
            else
            {
                response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
                response.setHeader("Content-Disposition", "attachment;filename=result.txt");
                output.write(JsonBeanUtils.beanToJson(JsonResult.success()).getBytes(StandardCharsets.UTF_8));
            }
        };
    }
}
