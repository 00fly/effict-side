package com.fly.openapi.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 
 * 接口用鉴权令牌
 * 
 * @author 00fly
 * @version [版本号, 2024年4月26日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Data
@Accessors(chain = true)
public class AuthToken
{
    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    private String userId;
    
    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String userName;
    
    /**
     * 令牌
     */
    @ApiModelProperty(value = "令牌")
    private String token;
    
    /**
     * 令牌有效时间
     */
    @ApiModelProperty(value = "令牌有效时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date tokenTime;
}