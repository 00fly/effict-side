package com.fly.openapi.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.fly.core.entity.JsonResult;
import com.fly.openapi.entity.UserInfo;

/**
 * name唯一，必填
 **/
@FeignClient(name = "myFeignClient", url = "http://127.0.0.1:${server.port}/native")
public interface MyFeignClient
{
    /**
     * OpenFeign在构造请求时需要@RequestMapping/@RequestParam/@PathVariable/@RequestHeader等来构造http请求<BR>
     * 否则报错 feign.FeignException: [405] during...
     * 
     * @param keyword
     * @return
     */
    @PostMapping("/post")
    public String post(@RequestParam String keyword);
    
    @PostMapping("/test")
    public JsonResult<UserInfo> test(@RequestBody UserInfo userInfo);
    
    @GetMapping("/pic/void")
    public void pic1();
    
    @GetMapping("/pic/bytes")
    public byte[] pic2();
    
    @GetMapping("/pic/stream")
    public StreamingResponseBody pic3();
}
