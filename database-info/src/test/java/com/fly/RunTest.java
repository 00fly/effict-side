package com.fly;

import java.sql.SQLException;

import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.keta.generate.MyDataSource;
import com.keta.generate.db.Mysql;
import com.keta.generate.db.Oracle;
import com.keta.generate.db.SqlServer;

/**
 * 
 * Junit 测试
 * 
 * @author 00fly
 * @version [版本号, 2018年11月14日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class RunTest
{
    private static final Logger LOGGER = LoggerFactory.getLogger(RunTest.class);
    
    /**
     * MYSQL
     * 
     * @throws SQLException
     * @see [类、类#方法、类#成员]
     */
    // @Ignore("数据源切换到mysql.properties,注释掉此行测试")
    @Test
    public void testMysql()
        throws SQLException
    {
        MyDataSource mysql = new Mysql();
        LOGGER.info("getTables={}", mysql.getTables());
        LOGGER.info("getAllTableName={}", mysql.getAllTableName());
        LOGGER.info("getTable={}", mysql.getTable("user"));
        LOGGER.info("queryColumnNames={}", mysql.queryColumnNames("user"));
    }
    
    /***
     * ORACLE
     * 
     * @throws SQLException
     * @see [类、类#方法、类#成员]
     */
    @Ignore("数据源切换到oracle.properties,注释掉此行测试")
    @Test
    public void testOracle()
        throws SQLException
    {
        MyDataSource oracle = new Oracle();
        LOGGER.info("getTables={}", oracle.getTables());
        LOGGER.info("getAllTableName={}", oracle.getAllTableName());
        LOGGER.info("getTable={}", oracle.getTable("QUES_INF"));
        LOGGER.info("queryColumnNames={}", oracle.queryColumnNames("QUES_INF"));
    }
    
    @Ignore("数据源切换到sqlserver.properties,注释掉此行测试")
    @Test
    public void testSqlServer()
        throws SQLException
    {
        MyDataSource sqlServer = new SqlServer();
        LOGGER.info("getTables={}", sqlServer.getTables());
        LOGGER.info("getAllTableName={}", sqlServer.getAllTableName());
        LOGGER.info("getTable={}", sqlServer.getTable("BN_INF_APPLY"));
        LOGGER.info("queryColumnNames={}", sqlServer.queryColumnNames("BN_INF_APPLY"));
    }
}
