package com.fly.core.utils;

/**
 * 
 * HtmlUtil
 * 
 * @author kailin.chen
 * @version [版本号, 2020年5月28日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class HtmlUtil
{
    private final static String BLANK = "";
    
    /**
     * 去除html代码中含有的标签
     * 
     * @param htmlStr
     * @return
     */
    public static String delHtmlTags(String htmlStr)
    {
        // 定义script的正则表达式，去除js可以防止注入
        String scriptRegex = "<script[^>]*?>[\\s\\S]*?<\\/script>";
        // 定义style的正则表达式，去除style样式，防止css代码过多时只截取到css样式代码
        String styleRegex = "<style[^>]*?>[\\s\\S]*?<\\/style>";
        // 定义HTML标签的正则表达式，去除标签，只提取文字内容
        String htmlRegex = "<[^>]+>";
        
        // 过滤script标签
        htmlStr = htmlStr.replaceAll(scriptRegex, BLANK);
        // 过滤style标签
        htmlStr = htmlStr.replaceAll(styleRegex, BLANK);
        // 过滤html标签
        htmlStr = htmlStr.replaceAll(htmlRegex, BLANK);
        htmlStr = htmlStr.replaceAll("&nbsp;", " ");
        htmlStr = htmlStr.replaceAll("，", ",");
        htmlStr = htmlStr.replaceAll("  ", " ");
        return htmlStr; // 返回文本字符串
    }
    
}
