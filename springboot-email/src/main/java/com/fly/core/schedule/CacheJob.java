package com.fly.core.schedule;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CacheJob
{
    /**
     * 定时清除缓存
     * 
     * @see [类、类#方法、类#成员]
     */
    @Scheduled(fixedRate = 30 * 60 * 1000)
    @CacheEvict(value = "demo", allEntries = true)
    public void clearCache()
    {
        log.info("★★★★★★★★★ clearCache at {} ★★★★★★★ ", DateFormatUtils.format(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss"));
    }
    
    @Scheduled(fixedRate = 10 * 1000L)
    public void schedule()
    {
        log.info("★★★★★★★★★ schedule ★★★★★★★ ");
    }
    
}
