package com.fly;

import java.io.IOException;

import org.apache.commons.lang3.SystemUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.EnableScheduling;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * SpringBootApplication 启动类
 * 
 * @author 00fly
 * @version [版本号, 2018-09-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@EnableCaching
@EnableScheduling
@SpringBootApplication
public class MailApplication implements ApplicationListener<WebServerInitializedEvent>, CommandLineRunner
{
    Integer port;
    
    public static void main(String[] args)
    {
        SpringApplication.run(MailApplication.class, args);
    }
    
    @Override
    public void onApplicationEvent(WebServerInitializedEvent event)
    {
        port = event.getWebServer().getPort();
    }
    
    @Override
    public void run(String... args)
        throws IOException
    {
        if (SystemUtils.IS_OS_WINDOWS_10 && port > 0)
        {
            log.info("★★★★★★★★  now open Browser ★★★★★★★★ ");
            Runtime.getRuntime().exec("cmd /c start /min http://127.0.0.1:" + port + "/doc.html");
        }
    }
}
