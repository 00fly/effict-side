package com.fly.mail.controller;

import java.net.UnknownHostException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fly.core.utils.HtmlUtil;
import com.fly.core.utils.IPUtils;
import com.fly.mail.model.JsonResult;
import com.fly.mail.model.MailBean;
import com.fly.mail.service.InfoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * IndexController
 * 
 * @author 00fly
 * @version [版本号, 2018-09-26]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@RestController
@Api(tags = "用户操作接口")
public class IndexController
{
    @Autowired
    protected ApplicationContext applicationContext;
    
    @Autowired
    protected HttpServletRequest request;
    
    @Autowired
    protected HttpSession session;
    
    @Value("${server.port}")
    private String port;
    
    @Autowired
    private InfoService infoService;
    
    /**
     * 首页
     * 
     * @param model
     * @return
     * @throws UnknownHostException
     * @throws Exception
     * @see [类、类#方法、类#成员]
     */
    @ApiOperation("系统信息")
    @GetMapping(value = "/")
    public Map<String, String> index()
        throws UnknownHostException
    {
        String ip = IPUtils.getIpAddr(request);
        Map<String, String> infoMap = infoService.queryInfo(ip, port);
        return infoMap;
    }
    
    @GetMapping("/work-item")
    public String create(@RequestBody Map<String, Object> map)
    {
        String html = (String)map.get("description");
        String text = HtmlUtil.delHtmlTags(html);
        text = StringEscapeUtils.unescapeHtml4(text);
        String[] textArr = StringUtils.split(text, "#");
        for (String line : textArr)
        {
            line = StringUtils.trimToEmpty(line);
            if (StringUtils.isNotBlank(line))
            {
                String[] value = StringUtils.split(line, " ");
                if (value.length == 2 && StringUtils.split(value[1], ",").length == 3)
                {
                    String[] workDays = StringUtils.split(value[1], ",");
                    log.info(value[0]);
                    log.info(workDays[0]);
                    log.info(workDays[1]);
                    log.info(workDays[2]);
                }
                else if (value.length == 2 && StringUtils.split(value[1], "，").length == 3) // 中文逗号
                {
                    String[] workDays = StringUtils.split(value[1], "，");
                    log.info(value[0]);
                    log.info(workDays[0]);
                    log.info(workDays[1]);
                    log.info(workDays[2]);
                }
            }
        }
        return text;
    }
    
    /**
     * sendMail
     * 
     * @return
     * @throws Exception
     * @see [类、类#方法、类#成员]
     */
    @ApiOperation("发送邮件")
    @PostMapping("/mail")
    public JsonResult<?> sendMail()
    {
        try
        {
            log.info("准备发送邮件");
            MailBean mailBean = new MailBean();
            mailBean.setRecipient("suerman_001@qq.com");
            mailBean.setSubject("测试");
            mailBean.setContent("测试内容");
            infoService.sendSimpleMail(mailBean);
            return JsonResult.success("邮件发送成功");
        }
        catch (Exception e)
        {
            log.error("邮件发送失败", e.getCause());
            return JsonResult.error("fail, 邮件发送失败");
        }
    }
}
