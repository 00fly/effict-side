package com.fly.mail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fly.mail.model.MailBean;
import com.fly.mail.service.InfoService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MailTest
{
    @Autowired
    InfoService infoService;
    
    @Test
    public void sendMail()
    {
        MailBean mailBean = new MailBean();
        mailBean.setRecipient("kalin001@qq.com");
        mailBean.setSubject("测试标题");
        mailBean.setContent("测试内容");
        infoService.sendSimpleMail(mailBean);
    }
}
