package cn.afterturn;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.SystemUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
public class Application implements CommandLineRunner
{
    @Value("${server.port}")
    Integer port;
    
    public static void main(String[] args)
    {
        SpringApplication.run(Application.class, args);
    }
    
    @Override
    public void run(String... args)
        throws IOException
    {
        File savefile = new File("D:/home/excel/");
        if (!savefile.exists())
        {
            savefile.mkdirs();
        }
        if (SystemUtils.IS_OS_WINDOWS && port > 0)
        {
            log.info("★★★★★★★★  now open Browser ★★★★★★★★ ");
            Runtime.getRuntime().exec("cmd.exe /c start /min http://127.0.0.1:" + port);
        }
    }
}