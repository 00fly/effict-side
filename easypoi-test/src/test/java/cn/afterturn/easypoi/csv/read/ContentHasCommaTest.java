package cn.afterturn.easypoi.csv.read;

import java.io.File;
import java.io.FileInputStream;
import java.util.Map;

import org.junit.Test;
import org.springframework.util.StopWatch;

import cn.afterturn.easypoi.csv.CsvImportUtil;
import cn.afterturn.easypoi.csv.entity.CsvImportParams;
import cn.afterturn.easypoi.handler.inter.IReadHandler;
import cn.afterturn.easypoi.test.excel.read.FileUtilTest;
import lombok.extern.slf4j.Slf4j;

/**
 * @author jueyue on 21-2-7.
 */
@Slf4j
public class ContentHasCommaTest
{
    @Test
    public void test()
    {
        try
        {
            StopWatch clock = new StopWatch();
            clock.start();
            log.debug(" ContentHasCommaTest start");
            CsvImportParams params = new CsvImportParams(CsvImportParams.UTF8);
            CsvImportUtil.importCsv(new FileInputStream(new File(FileUtilTest.getWebRootPath("csv/20181107202743_comma.csv"))), Map.class, params, new IReadHandler<Map>()
            {
                @Override
                public void handler(Map o)
                {
                    log.error("{} --- {}", o.get("商户号"), o.get("商户名称"));
                }
                
                @Override
                public void doAfterAll()
                {
                }
            });
            clock.stop();
            log.debug(" ContentHasCommaTest end,time is {}", clock.getTotalTimeMillis());
        }
        catch (Exception e)
        {
        }
    }
}
