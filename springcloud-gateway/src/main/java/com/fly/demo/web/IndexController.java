package com.fly.demo.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController
{
    @GetMapping("/show/test1")
    public String test1()
    {
        return "i am test1";
    }
    
    @GetMapping("/show2/test2")
    public String test2()
    {
        return "i am test2";
    }
}
