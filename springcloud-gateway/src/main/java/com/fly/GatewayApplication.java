package com.fly;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.core.env.Environment;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
@EnableDiscoveryClient
public class GatewayApplication implements CommandLineRunner
{
    @Value("${server.port}")
    Integer port;
    
    @Autowired
    Environment environment;
    
    public static void main(String[] args)
    {
        SpringApplication.run(GatewayApplication.class, args);
    }
    
    @Override
    public void run(String... args)
        throws Exception
    {
        if (SystemUtils.IS_OS_WINDOWS && port > 0)
        {
            log.info("★★★★★★★★  now open Browser ★★★★★★★★ ");
            log.info("请修改hosts: 127.0.0.1 test.00fly.online");
            String url = "https://test.00fly.online:" + port;
            
            Runtime.getRuntime().exec("cmd /c start " + url + "/show/test1");
            Runtime.getRuntime().exec("cmd /c start " + url + "/show2/test2");
            String profile = StringUtils.join(environment.getActiveProfiles());
            switch (profile)
            {
                case "httpbin":
                    Runtime.getRuntime().exec("cmd /c start " + url + "/user-agent");
                    Runtime.getRuntime().exec("cmd /c start " + url + "/headers");
                    Runtime.getRuntime().exec("cmd /c start " + url + "/get");
                    Runtime.getRuntime().exec("cmd /c start " + url);
                    break;
                
                case "demo":
                    Runtime.getRuntime().exec("cmd /c start " + url + "/get/method1");
                    break;
                
                case "dev":
                default:
                    Runtime.getRuntime().exec("cmd /c start " + url + "/gateway/show/test1");
                    Runtime.getRuntime().exec("cmd /c start " + url + "/show2/test2");
                    break;
            }
        }
    }
}
