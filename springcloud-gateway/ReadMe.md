## springcloud-gateway

```shell
docker run -it -d -p 8081:80 kennethreitz/httpbin

java -jar springcloud-gateway-0.0.1.jar --spring.profiles.active=httpbin
```

# 通过httpbin来测试发送的请求

## 1. 接口信息


|  接口地址|  请求方式|参数 | 
|--|--|--|
| https://test.00fly.online/get/method1 | get | param1、param2|
|  https://test.00fly.online/post/method1|  post| param1、param2、file|
|  https://test.00fly.online/json/method1|  jsonbody post| param1、param2|


## 2. 流程梳理

```mermaid
graph LR
A(发送get请求)--> B(接口https://test.00fly.online/get/method1)--> C(代理访问https://http.00fly.online/get) 
A1(发送post请求)--> B1(接口https://test.00fly.online/post/method1)--> C1(代理访问https://http.00fly.online/post) 
A2(发送json请求)--> B2(接口https://test.00fly.online/json/method1)--> C1(代理访问https://http.00fly.online/post) 
```