package com.fly.log.service;

import java.io.Serializable;

import lombok.Data;

@Data
public class User implements Serializable
{
    private static final long serialVersionUID = 3818685939210001653L;
    
    private Long id;
    
    private String userName;
    
    private String passWord;
}