package com.fly.log.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

@Component
public class UserManager
{
    @Autowired
    private MongoTemplate mongoTemplate;
    
    public void saveUser(User user)
    {
        mongoTemplate.save(user);
    }
}
