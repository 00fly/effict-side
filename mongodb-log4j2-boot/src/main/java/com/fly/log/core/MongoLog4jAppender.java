package com.fly.log.core;

import java.util.ResourceBundle;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public class MongoLog4jAppender
{
    private static String mongodbUri;
    
    // 静态初始化 DataSource
    static
    {
        ResourceBundle config = ResourceBundle.getBundle("jdbc");
        mongodbUri = config.getString("mongodb.uri");
    }
    
    public static MongoClient getMongoClient()
    {
        System.out.println("###### getMongoClient ######");
        MongoClientURI uri = new MongoClientURI(mongodbUri);
        return new MongoClient(uri);
    }
}