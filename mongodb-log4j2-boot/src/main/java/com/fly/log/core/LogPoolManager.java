package com.fly.log.core;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javax.sql.DataSource;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.util.Assert;

import com.alibaba.druid.pool.DruidDataSource;

/**
 * 
 * 日志数据库数据源
 * 
 * @author 00fly
 * @version [版本号, 2023年3月27日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class LogPoolManager
{
    private static DataSource dataSource;
    
    // 静态初始化 DataSource
    static
    {
        ResourceBundle config = ResourceBundle.getBundle("jdbc");
        dataSource = DataSourceBuilder.create().type(DruidDataSource.class).url(config.getString("jdbc.url")).username(config.getString("jdbc.username")).password(config.getString("jdbc.password")).build();
    }
    
    /**
     * getConnection
     * 
     * @return
     * @throws SQLException
     * @see [类、类#方法、类#成员]
     */
    public static Connection getConnection()
        throws SQLException
    {
        Assert.notNull(dataSource, "dataSource is null");
        return dataSource.getConnection();
    }
}
