package com.fly.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.InputStream;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.apache.commons.io.FileUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.ResourceUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 工具UI简化版本
 * 
 * @author 00fly
 * @version [版本号, 2023年3月3日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
public class SimpleCodeUI extends JFrame
{
    private static final long serialVersionUID = -2606565438929405844L;
    
    JFrame frame = this;
    
    public SimpleCodeUI()
        throws HeadlessException
    {
        initComponent();
        this.setVisible(true);
        this.setResizable(true);
        this.setAlwaysOnTop(true);
        this.setTitle("应用工具 V1.0");
        this.setBounds(400, 200, 1200, 550);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        try
        {
            // 设定用户界面的外观
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            SwingUtilities.updateComponentTreeUI(this);
        }
        catch (Exception e)
        {
        }
    }
    
    /**
     * 组件初始化
     * 
     * @see [类、类#方法、类#成员]
     */
    private void initComponent()
    {
        // 加载图标
        URL imgURL = this.getClass().getResource("/img/icon.gif");
        if (imgURL != null)
        {
            setIconImage(getToolkit().createImage(imgURL));
        }
        
        JTextArea textArea = new JTextArea();
        textArea.setToolTipText("提示文本内容");
        
        // JTextArea不自带滚动条，因此就需要把文本区放到一个滚动窗格中
        JScrollPane scroll = new JScrollPane(textArea);
        scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        
        JButton button = new JButton("按 钮");
        button.setPreferredSize(new Dimension(super.getWidth(), 50));
        this.add(scroll, BorderLayout.CENTER);
        this.add(button, BorderLayout.SOUTH);
        button.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent event)
            {
                try
                {
                    URL url = getClass().getProtectionDomain().getCodeSource().getLocation();
                    StringBuffer sb = new StringBuffer();
                    sb.append(" url: ").append(url).append("\n");
                    sb.append("file: ").append(url.getFile()).append("\n");
                    sb.append("path: ").append(url.getPath()).append("\n");
                    sb.append("protocol: ").append(url.getProtocol()).append("\n");
                    sb.append("isJarURL: ").append(ResourceUtils.isJarURL(url)).append("\n\n");
                    
                    url = getClass().getResource("");
                    sb.append(" url: ").append(url).append("\n");
                    sb.append("file: ").append(url.getFile()).append("\n");
                    sb.append("path: ").append(url.getPath()).append("\n");
                    sb.append("protocol: ").append(url.getProtocol()).append("\n");
                    sb.append("isJarURL: ").append(ResourceUtils.isJarURL(url)).append("\n\n");
                    
                    // 普通java工程中new ClassPathResource("")会报错class path resource [] cannot be resolved to URL because it does not exist
                    url = new ClassPathResource("img").getURL();
                    sb.append(" url: ").append(url).append("\n");
                    sb.append("file: ").append(url.getFile()).append("\n");
                    sb.append("path: ").append(url.getPath()).append("\n");
                    sb.append("protocol: ").append(url.getProtocol()).append("\n");
                    sb.append("isJarURL: ").append(ResourceUtils.isJarURL(url)).append("\n\n");
                    textArea.setText(sb.toString());
                    
                    // 协议： file、jar、rsrc-》org.eclipse.jdt.internal.jarinjarloader.JarRsrcLoader
                    if (!"file".equals(url.getProtocol())) // 拷贝jar内部文件
                    {
                        // 注意springboot、gui下以下语句不完全等同
                        // try (InputStream is = getClass().getResourceAsStream("/img/18.jpg"))
                        // try (InputStream is = ClassLoader.getSystemResourceAsStream("img/18.jpg"))
                        // try (InputStream is = getClass().getClassLoader().getResourceAsStream("img/18.jpg"))
                        try (InputStream is = getClass().getResourceAsStream("/img/18.jpg"))
                        {
                            File file = new File("img/18.jpg");
                            FileUtils.copyInputStreamToFile(is, file);
                            log.info("###### file path: {}", file.getCanonicalPath());
                        }
                    }
                    FileUtils.forceDeleteOnExit(new File("img"));
                }
                catch (Exception e)
                {
                    JOptionPane.showMessageDialog(frame, e.getMessage(), "系統异常", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }
    
    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(() -> new SimpleCodeUI());
    }
}
