package com.fly.ui;

import javax.swing.SwingUtilities;

public class MainRun
{
    /**
     * 主启动类
     * 
     * @param args
     */
    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(() -> {
            new SimpleCodeUI();
            new SimpleCodeUI2();
        });
    }
}
