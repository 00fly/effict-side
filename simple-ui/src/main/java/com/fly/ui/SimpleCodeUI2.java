package com.fly.ui;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 工具UI简化版本
 * 
 * @author 00fly
 * @version [版本号, 2023年3月3日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
public class SimpleCodeUI2 extends JFrame
{
    private static final long serialVersionUID = 4473263931999056564L;
    
    JFrame frame = this;
    
    public SimpleCodeUI2()
        throws HeadlessException
    {
        initComponent();
        this.setVisible(true);
        this.setResizable(true);
        this.setAlwaysOnTop(true);
        this.setTitle("应用工具 V2.0");
        this.setBounds(600, 400, 1200, 550);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        try
        {
            // 设定用户界面的外观
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            SwingUtilities.updateComponentTreeUI(this);
        }
        catch (Exception e)
        {
        }
    }
    
    /**
     * 组件初始化
     * 
     * @see [类、类#方法、类#成员]
     */
    private void initComponent()
    {
        // 加载图标
        URL imgURL = this.getClass().getResource("/img/icon.gif");
        if (imgURL != null)
        {
            setIconImage(getToolkit().createImage(imgURL));
        }
        
        JList<String> list = new JList<>();
        list.setToolTipText("提示文本内容");
        
        // 滚动条
        JScrollPane scroll = new JScrollPane(list);
        scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        
        JButton button = new JButton("按 钮");
        button.setPreferredSize(new Dimension(super.getWidth(), 50));
        this.add(scroll, BorderLayout.CENTER);
        this.add(button, BorderLayout.SOUTH);
        button.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent event)
            {
                try
                {
                    setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    List<String> listData = getExecuteResult();
                    int size = listData.size();
                    listData.add("输出总行数：" + size);
                    list.setListData(listData.toArray(new String[0]));
                    list.setSelectedIndex(size);
                    scroll.getViewport().setViewPosition(list.indexToLocation(size)); // 滚动到最后
                    setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                }
                catch (Exception e)
                {
                    log.error(e.getMessage(), e);
                    JOptionPane.showMessageDialog(frame, e.getMessage(), "系統异常", JOptionPane.ERROR_MESSAGE);
                    setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                }
            }
            
            /**
             * 获取执行结果
             * 
             * @return
             * @throws IOException
             */
            private List<String> getExecuteResult()
                throws IOException
            {
                // 注意和Jar运行结果比较
                PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
                
                // 注意classpath:与classpath*的区别
                Resource[] classes1 = resolver.getResources("classpath:com/**/*.class");
                Resource[] classes2 = resolver.getResources("classpath:com/fly/**/*.class");
                Resource[] jpgs = resolver.getResources("classpath:img/**/*.jpg");
                Stream<Resource> classes = Stream.of(classes1, classes2, jpgs).flatMap(Arrays::stream);
                return classes.map(c -> {
                    try
                    {
                        return c.getURL().getPath();
                    }
                    catch (IOException e)
                    {
                        log.error(e.getMessage(), e);
                        return null;
                    }
                }).filter(Objects::nonNull).collect(Collectors.toList());
            }
        });
    }
    
    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(() -> new SimpleCodeUI2());
    }
}
