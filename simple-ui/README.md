# simple-ui

工具UI简化版本

---

JAVA类加载时采用树形的委托机制，默认有三个类加载器，bootstrap：主要加载jdk中jre/lib/rt.jar中的类，ExtClassLoader加载jre/lib/ext/*.jar，AppClassLoader加载classpath指定的jar或者目录，
这三个类加载器bootstrap是处在树的根节点，然后是ExtClassLoader，接着是子节点AppClassLoader。
bootstrap不是一个java类，其他两个都是java类，他们执行顺序是，bootstrap最先加载，然后ExtClassLoader加载。再AppClassLoader。
他们是采取向上委托的，即，如果AppClassLoader需要加载类时，首先它自己不加载，它向ExtClassLoader询问，ExtClassLoader也不加载，直接向bootstrap询问。这时，bootstrap是根节点，它没有上级，它直接去它的目录加载，如果加载成功，那就ok了，如果没有加载成功，那bootstrap会退回到ExtClassLoader，ExtClassLoader去它目录加载，如果加载成功，那就ok，如果没有成功，退回到AppClassLoader，AppClassLoader去它目录加载，如果加载成功那ok，如果不成功，如果AppClassLoader还有子的类加载器，AppClassLoader也不会退回到他的子加载器，AppClassLoader会直接抛异常。

---

** Spring设计了一个Resource接口，该接口拥有对应不同资源类型的实现类 **

- ClassPathResource

类路径下的资源，资源以相对于类路径的方式表示

- FileSystemResource

文件系统资源，资源以文件系统路径的方式表示，如D:/conf/bean.xml

- InputStreamResource

- ServletContextResource

- UrlResource

封装了java.net.URL，能够访问任何可以通过URL表示的资源，如文件系统资源、HTTP资源、FTP资源

- PathResource

封装了java.net.URL、java.nio.file.path
