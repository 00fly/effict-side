package com.fly.stream;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StreamServer
{
    private static final int port = 8888;
    
    public static void main(String[] args)
        throws Exception
    {
        Server server = ServerBuilder.forPort(port).addService(new StreamServiceImpl()).build().start();
        log.info("grpc服务端启动成功, 端口: {}", port);
        server.awaitTermination();
    }
}
