package com.fly.stream;

import com.fly.stream.StreamServiceGrpc.StreamServiceImplBase;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StreamServiceImpl extends StreamServiceImplBase
{
    /**
     * 业务处理逻辑
     * 
     * @param request
     * @return
     */
    private String process(Request request)
    {
        String nick;
        switch (request.getSex())
        {
            case "0":
                nick = "girl";
                break;
            
            case "1":
                nick = "boy";
                break;
            
            default:
                nick = "person";
                break;
        }
        return String.format("you are welcome! %s %s", nick, request.getUserName());
    }
    
    /**
     * 一元RPC
     */
    @Override
    public void getDate(Request request, StreamObserver<Result> responseObserver)
    {
        Result result = null;
        try
        {
            String message = process(request);
            result = Result.newBuilder().setMessage(message).build();
        }
        catch (Exception e)
        {
            responseObserver.onError(e);
        }
        finally
        {
            responseObserver.onNext(result);
        }
        responseObserver.onCompleted();
    }
    
    /**
     * 流式输入
     */
    @Override
    public StreamObserver<Request> getDateInputStream(StreamObserver<Result> responseObserver)
    {
        return new StreamObserver<Request>()
        {
            @Override
            public void onCompleted()
            {
                responseObserver.onCompleted();
            }
            
            @Override
            public void onNext(Request request)
            {
                String message = process(request);
                
                // 接收请求后就返回一个响应
                responseObserver.onNext(Result.newBuilder().setMessage(message).build());
            }
            
            @Override
            public void onError(Throwable t)
            {
                log.error(t.getMessage(), t);
            };
        };
    }
    
    /**
     * 流式输出
     */
    @Override
    public void getDateOutputStream(Request request, StreamObserver<Result> responseObserver)
    {
        // 接收请求返回响应
        String message = process(request);
        responseObserver.onNext(Result.newBuilder().setMessage(message).build());
        responseObserver.onCompleted();
    }
    
    /**
     * 流式输入输出
     */
    @Override
    public StreamObserver<Request> getDateBothStream(StreamObserver<Result> responseObserver)
    {
        return new StreamObserver<Request>()
        {
            @Override
            public void onCompleted()
            {
                responseObserver.onCompleted();
            }
            
            @Override
            public void onNext(Request request)
            {
                // 接收请求后就返回一个响应
                String message = process(request);
                responseObserver.onNext(Result.newBuilder().setMessage(message).build());
            }
            
            @Override
            public void onError(Throwable t)
            {
                log.error(t.getMessage(), t);
            };
        };
    }
}
