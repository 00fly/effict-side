package com.shgx.server;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.shgx.grpc.api.RPCDateRequest;
import com.shgx.grpc.api.RPCDateResponse;
import com.shgx.grpc.api.RPCDateServiceGrpc;

import io.grpc.stub.StreamObserver;

/**
 * @author: guangxush
 * @create: 2019/07/07
 */
public class RPCDateServiceImpl extends RPCDateServiceGrpc.RPCDateServiceImplBase
{
    @Override
    public void getDate(RPCDateRequest request, StreamObserver<RPCDateResponse> responseObserver)
    {
        RPCDateResponse rpcDateResponse = null;
        Date now = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String nowTime = simpleDateFormat.format(now);
        try
        {
            rpcDateResponse = RPCDateResponse.newBuilder().setServerDate(nowTime + " ****** Welcome " + request.getUserName()).build();
        }
        catch (Exception e)
        {
            responseObserver.onError(e);
        }
        finally
        {
            responseObserver.onNext(rpcDateResponse);
        }
        responseObserver.onCompleted();
    }
}