package com.shgx.client;

import java.util.Scanner;

import com.fly.stream.Request;
import com.fly.stream.Result;
import com.fly.stream.StreamServiceGrpc;
import com.fly.stream.StreamServiceGrpc.StreamServiceBlockingStub;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StreamClient
{
    private static final String host = "localhost";
    
    private static final int serverPort = 8888;
    
    public static void main(String[] args)
        throws Exception
    {
        ManagedChannel managedChannel = ManagedChannelBuilder.forAddress(host, serverPort).usePlaintext().build();
        try
        {
            StreamServiceBlockingStub rpcDateService = StreamServiceGrpc.newBlockingStub(managedChannel);
            Scanner sc = new Scanner(System.in);
            do
            {
                Request request = Request.newBuilder().setUserName("shgx").setSex("1").build();
                Result result = rpcDateService.getDate(request);
                log.info(result.getMessage());
                log.info("------------输入x退出,回车换行继续------------");
            } while (!sc.nextLine().equals("x"));
            sc.close();
        }
        finally
        {
            managedChannel.shutdown();
        }
    }
}
