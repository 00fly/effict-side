package com.shgx.client;

import java.util.Scanner;

import com.shgx.grpc.api.RPCDateRequest;
import com.shgx.grpc.api.RPCDateResponse;
import com.shgx.grpc.api.RPCDateServiceGrpc;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;

/**
 * @author: guangxush
 * @create: 2019/07/07
 */
@Slf4j
public class GRPCClient
{
    private static final String host = "localhost";
    
    private static final int serverPort = 9999;
    
    public static void main(String[] args)
        throws Exception
    {
        ManagedChannel managedChannel = ManagedChannelBuilder.forAddress(host, serverPort).usePlaintext().build();
        try
        {
            RPCDateServiceGrpc.RPCDateServiceBlockingStub rpcDateService = RPCDateServiceGrpc.newBlockingStub(managedChannel);
            Scanner sc = new Scanner(System.in);
            do
            {
                RPCDateRequest rpcDateRequest = RPCDateRequest.newBuilder().setUserName("shgx").build();
                RPCDateResponse rpcDateResponse = rpcDateService.getDate(rpcDateRequest);
                log.info(rpcDateResponse.getServerDate());
                log.info("------------输入x退出,回车换行继续------------");
            } while (!sc.nextLine().equals("x"));
            sc.close();
        }
        finally
        {
            managedChannel.shutdown();
        }
    }
}
