package com.fly;

import javax.annotation.PreDestroy;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.fly.core.utils.SpringContextUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@EnableAsync
@EnableScheduling
@ServletComponentScan
@SpringBootApplication
public class HelloApplication
{
    @Autowired
    SpringContextUtils springContextUtils;
    
    public static void main(String[] args)
    {
        // args = new String[] {"--noweb"};
        boolean web = !ArrayUtils.contains(args, "--noweb");
        log.info("############### with Web Configuration: {} #############", web);
        if (RandomUtils.nextBoolean())
        {
            new SpringApplicationBuilder(HelloApplication.class).web(web ? WebApplicationType.SERVLET : WebApplicationType.NONE).run(args);
        }
        else
        {
            SpringApplication application = new SpringApplication(HelloApplication.class);
            application.setWebApplicationType(web ? WebApplicationType.SERVLET : WebApplicationType.NONE);
            application.run(args);
        }
    }
    
    @PreDestroy
    public void destroy()
    {
        log.info("###### destroy ######");
    }
}