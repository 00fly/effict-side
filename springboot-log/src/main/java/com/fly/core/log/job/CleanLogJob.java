package com.fly.core.log.job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fly.core.utils.SpringContextUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * CleanLogJob
 * 
 * @author 00fly
 * @version [版本号, 2022年11月30日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@Component
public class CleanLogJob
{
    private String welcome = "Hello 00fly in CleanLogJob, profile: " + SpringContextUtils.getActiveProfile();
    
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Scheduled(fixedRate = 60000L)
    public void clear()
    {
        final long count = jdbcTemplate.queryForObject("select count(*) from boot_log", Long.class);
        log.info("------------------- boot_log count: {} --------------------", count);
        if (count > 100)
        {
            log.info("###### clean table boot_log ######");
            
            // truncate 执行后将重新水平线和索引（id从零开始）
            // MySQL5.5版本开始引入了MDL锁（metadata lock），来保护表的元数据信息，用于解决或者保证DDL操作与DML操作之间的一致性
            // 如果表上有活动事务（未提交或回滚）,执行truncate table，请求写入的会话会等待在Metadata lock wait
            // 故尽量不要使用truncate table
            // jdbcTemplate.execute("truncate table boot_log");
            jdbcTemplate.execute("delete from boot_log");
        }
    }
    
    /**
     * 测试日志打印
     */
    @Scheduled(fixedRate = 1000L)
    public void run()
    {
        log.info("★★★★★★★★ {}", welcome);
    }
}
