package com.fly.core.log;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * 日志数据源注入，注意不能依赖web事件
 */
@Component
public class Log4j2Configuration implements ApplicationListener<ContextRefreshedEvent>
{
    @Autowired
    DataSource dataSource;
    
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent)
    {
        // 初始化日志数据源
        Assert.notNull(dataSource, "dataSource can not be null");
        RefreshLogPoolManager.init(dataSource);
    }
}