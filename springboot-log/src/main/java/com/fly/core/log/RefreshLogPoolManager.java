package com.fly.core.log;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.fly.core.utils.SpringContextUtils;

/**
 * 
 * 可刷新日志数据库数据源
 * 
 * @author 00fly
 * @version [版本号, 2023年3月27日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public final class RefreshLogPoolManager
{
    private static DataSource dataSource;
    
    public static void init(DataSource ds)
    {
        if (dataSource == null)
        {
            System.out.println("------------------- dataSource inited -------------------" + ds);
            dataSource = ds;
        }
    }
    
    /**
     * getConnection
     * 
     * @return
     * @throws SQLException
     * @see [类、类#方法、类#成员]
     */
    public static Connection getConnection()
        throws SQLException
    {
        if (dataSource == null)
        {
            dataSource = SpringContextUtils.getBean(DataSource.class);
            System.out.println("------------------- dataSource get by springContextUtils -------------------" + dataSource);
            return dataSource.getConnection();
        }
        System.out.println(dataSource);
        return dataSource.getConnection();
    }
}
