package com.fly.hello.runner;

import java.io.IOException;

import javax.servlet.ServletContext;

import org.apache.commons.lang3.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.fly.core.utils.SpringContextUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@Configuration
@ConditionalOnWebApplication
public class WebStartedRunner implements ApplicationListener<WebServerInitializedEvent>
{
    @Value("${server.port}")
    Integer port;
    
    @Autowired
    ServletContext servletContext;
    
    @Override
    public void onApplicationEvent(WebServerInitializedEvent event)
    {
        int port = event.getWebServer().getPort();
        log.info("#### server.port: {} ####", port);
    }
    
    @Bean
    CommandLineRunner run()
    {
        return args -> {
            openBrowser();
        };
    }
    
    private void openBrowser()
        throws IOException
    {
        String url;
        switch (SpringContextUtils.getActiveProfile())
        {
            case "prod":
                log.info("请修改hosts: 127.0.0.1 test.00fly.online");
                url = "https://test.00fly.online:" + port + servletContext.getContextPath();
                break;
            
            default:
                url = SpringContextUtils.getServerBaseURL();
                break;
        }
        if (SystemUtils.IS_OS_WINDOWS && port > 0)
        {
            log.info("★★★★★★★★  now open Browser ★★★★★★★★ ");
            Runtime.getRuntime().exec("cmd /c start /min " + url);
            Runtime.getRuntime().exec("cmd /c start /min " + url + "/index");
            if (SpringContextUtils.getActiveProfile().contains("dev"))
            {
                Runtime.getRuntime().exec("cmd /c start /min " + url + "/h2-console");
            }
        }
    }
}
