package com.fly.hello.web;

import java.net.UnknownHostException;

import javax.annotation.PostConstruct;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import com.fly.core.utils.SpringContextUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
@Api(tags = "WebClient接口")
@RequestMapping("/webClient")
public class WebClientController
{
    WebClient webClient;
    
    @PostConstruct
    public void init()
    {
        try
        {
            String baseUrl = SpringContextUtils.getServerBaseURL();
            this.webClient = WebClient.builder().baseUrl(baseUrl).build();
        }
        catch (UnknownHostException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @ApiOperation("测试")
    @GetMapping("/test")
    public Mono<String> hello()
    {
        return webClient.get().uri("/demo/first").exchange().flatMap(clientResponse -> clientResponse.bodyToMono(String.class));
    }
}
