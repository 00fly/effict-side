
#### 原理待澄清

##  spring-boot-devtools启用时

**getConnection 不会被执行**

```java 
    public static Connection getConnection()
        throws SQLException
    {
        if (dataSource == null)
        {
            dataSource = SpringContextUtils.getBean(DataSource.class);
            System.out.println("------------------- dataSource get by springContextUtils -------------------" + dataSource);
            return dataSource.getConnection();
        }
        System.out.println(dataSource);
        return dataSource.getConnection();
    }
 ```
 
 
## spring-boot-devtools不启用时

**不建议在ApplicationListener中调用init(DataSource ds), 原因是ContextRefreshedEvent后于DataSource初始化，并且此时日志需要保存的话dataSource为null** 

最佳实践是废弃init方法在getConnection中直接执行 `dataSource = SpringContextUtils.getBean(DataSource.class);`


### ApplicationListener注入日志数据源会导致查询与删除日志truncate table加上的MDL锁冲突致报错

具体可通过navicat运行 `show processlist` 查看

```
127	root	172.20.0.1:54678	hello	Query	3	Waiting for table metadata lock	select event_id,event_date,thread,class,`function`,message,exception,level,time from boot_log where 
128	root	172.20.0.1:54680	hello	Query	6	Waiting for table metadata lock	truncate table boot_log
```

### 问题重现步骤： 

RefreshLogPoolManager

```java
public final class RefreshLogPoolManager
{
    private static DataSource dataSource;
    
    public static void init(DataSource ds)
    {
        if (dataSource == null)
        {
            System.out.println("------------------- dataSource get in init -------------------" + ds);
            dataSource = ds;
        }
    }
    
    /**
     * getConnection
     * 
     * @return
     * @throws SQLException
     * @see [类、类#方法、类#成员]
     */
    public static Connection getConnection()
        throws SQLException
    {
        System.out.println(dataSource);
        return dataSource.getConnection();
    }
}

```

Log4j2Configuration

```java
/**
 * 日志数据源注入，注意不能依赖web事件
 */
@Component
public class Log4j2Configuration implements ApplicationListener<ContextRefreshedEvent>
{
    @Autowired
    DataSource dataSource;
    
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent)
    { 
        // 初始化日志数据源
        Assert.notNull(dataSource, "dataSource can not be null");
        RefreshLogPoolManager.init(dataSource);
    }    
}          
```

CleanLogJob

```java
    @Scheduled(fixedRate = 10000L)
    public void run()
    {
        final long count = jdbcTemplate.queryForObject("select count(*) from boot_log", Long.class);
        log.info("------------------- boot_log count: {} --------------------", count);
        if (count > 100)
        {
            log.info("###### clean table boot_log ######");
            
            // truncate 执行后将重新水平线和索引（id从零开始）
            // MySQL5.5版本开始引入了MDL锁（metadata lock），来保护表的元数据信息，用于解决或者保证DDL操作与DML操作之间的一致性
            // 如果表上有活动事务（未提交或回滚）,执行truncate table，请求写入的会话会等待在Metadata lock wait
            // 故尽量不要使用truncate table
            jdbcTemplate.execute("truncate table boot_log");
        }
    }
```


----
## 延伸的一些思考

### MDL简述

​为了在并发环境下维护表元数据的数据一致性，在表上有活动事务（显式或隐式）的时候，不可以对元数据进行写入操作。因此从MySQL5.5版本开始引入了MDL锁（metadata lock），来保护表的元数据信息，用于解决或者保证DDL操作与DML操作之间的一致性。

​对于引入MDL，其主要解决了2个问题，一个是事务隔离问题，比如在可重复隔离级别下，会话A在2次查询期间，会话B对表结构做了修改，两次查询结果就会不一致，无法满足可重复读的要求；另外一个是数据复制的问题，比如会话A执行了多条更新语句期间，另外一个会话B做了表结构变更并且先提交，就会导致slave在重做时，先重做alter，再重做update时就会出现复制错误的现象。所以在对表进行上述操作时，如果表上有活动事务（未提交或回滚），请求写入的会话会等待在Metadata lock wait 。

​支持事务的InnoDB引擎表和不支持事务的MyISAM引擎表，都会出现Metadata Lock Wait等待现象。一旦出现Metadata Lock Wait等待现象，后续所有对该表的访问都会阻塞在该等待上，导致连接堆积，业务受影响。