
# springboot项目frame

## Java命令运行

```
#只加载springBoot jar外部的配置文档
java -jar springboot-hello.jar --spring.config.location=./application-other.yml

#先加载dev配置文件，再加载springBoot jar外部的配置文档，级别要高于里面的文档，所以外面的属性会覆盖里面的，当然所有文档都是互补的，没有的话就相互补充。
java -jar -Dspring.profiles.active=dev springboot-hello.jar --spring.config.location=./application-other.yml
java -jar springboot-hello.jar --spring.profiles.active=dev --spring.config.location=./application-other.yml

#Spring Boot Jar运行时指定Start-Class类
java -cp springboot-hello-1.0.0.jar -Dloader.main=com.fly.HelloApplication org.springframework.boot.loader.PropertiesLauncher

#命令行下运行JUnit测试用例（注释掉pom.xml中的<scope>test</scope>保证执行mvn clean package后生成的jar包含test依赖，发布包解压classes、lib文件，进入classes目录运行命令）
java -Djava.ext.dirs=../lib org.junit.runner.JUnitCore com.fly.hello.LevelNumTest
java -Djava.ext.dirs=../lib org.junit.runner.JUnitCore com.fly.hello.WebApplicationTest
```
## apidoc

**[apidoc](https://apidocjs.com/) 是一个可以将源代码中的注释直接生成api接口文档的工具,对现有代码无侵入**

### 1.安装
```
#安装apidoc之前要先安装node.js

#安装apidoc
npm install -g apidoc

#验证
apidoc -v
apidoc -h

```
### 2.配置
典型 apidoc.json

```json
{
	"name": "接口文档",
	"version": "1.0.0",
	"description": "接口文档",
	"title": "接口演示文档",
	"url": "http://127.0.0.1:8080",
	"sampleUrl": "http://127.0.0.1:8080",
	"template": {
		"forceLanguage": "zh_cn",
		"showRequiredLabels": true,
		"withCompare": false,
		"withGenerator": true
	}
}

```

### 3.生成文档

```shell
#扫描src目录在doc目录生成接口文档(default)
apidoc
apidoc -i src -o doc

#扫描src目录在apidoc目录生成接口文档
apidoc -i src -o apidoc
```


## wait-for测试

```bash
docker run -it -d --name jdk openjdk:8-jre-alpine
docker cp wait-for.sh jdk:/
docker exec -it jdk sh
sh wait-for.sh www.baidu.com:80 -- echo "baidu is up"
```

## 待解决（RefreshLogPoolManager已解决）

1. databaseAppender参数bufferSize不起作用

2. 数据源通过spring.profiles.active指定或docker-compose环境变量注入，怎么改写 LogPoolManager日志初始化逻辑

