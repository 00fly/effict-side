package com.fly.core.base;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ThreadPoolTest
{
    ExecutorService executorService =
        new ThreadPoolExecutor(10, 10, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(), new BasicThreadFactory.Builder().namingPattern("sum-thread-%03d").daemon(true).priority(Thread.MAX_PRIORITY).build());
    
    /**
     * 线程池执行数据求和后汇总
     * 
     * @throws InterruptedException
     */
    @Test
    public void testSum()
        throws InterruptedException
    {
        StopWatch clock = new StopWatch();
        clock.start();
        List<Callable<Long>> tasks = new ArrayList<>();
        IntStream.range(0, 10).forEach(i -> {
            tasks.add(() -> {
                TimeUnit.MILLISECONDS.sleep(100); // 模拟耗时操作
                return LongStream.range(1000L * i, 1000L * (i + 1)).sum();
            });
        });
        
        // 汇总
        long sum = executorService.invokeAll(tasks).stream().map(future -> {
            try
            {
                return future.get();
            }
            catch (InterruptedException | ExecutionException e)
            {
                throw new RuntimeException(e);
            }
        }).mapToLong(v -> v).sum();
        clock.stop();
        log.info("处理完成，返回数据 {}, 用时: {}ms", sum, clock.getTime());
    }
    
    @Test
    public void testSum2()
    {
        StopWatch clock = new StopWatch();
        clock.start();
        long sum = LongStream.range(0, 10).map(i -> {
            try
            {
                TimeUnit.MILLISECONDS.sleep(100); // 模拟耗时操作
            }
            catch (InterruptedException e)
            {
            }
            return LongStream.range(1000L * i, 1000L * i + 1000L).sum();
        }).sum();
        clock.stop();
        log.info("处理完成，返回数据 {}, 用时: {}ms", sum, clock.getTime());
    }
}
