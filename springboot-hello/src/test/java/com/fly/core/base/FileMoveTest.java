package com.fly.core.base;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.util.ResourceUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FileMoveTest
{
    Resource[] resources = {};
    
    public void selectFile(Scanner sc)
        throws IOException, InterruptedException
    {
        resources = new PathMatchingResourcePatternResolver().getResources(ResourceUtils.CLASSPATH_URL_PREFIX + "data/pic/**/*.jpg");
        int index = 1;
        for (Resource it : resources)
        {
            log.info("{}. {}", index++, it.getURI().getPath());
        }
        log.info("请输入序号【1-{}】选择文件", --index);
        int input = NumberUtils.toInt(sc.nextLine());
        if (input > 0 && input <= resources.length)
        {
            String path = resources[input - 1].getFile().getCanonicalPath();
            log.info("你选择了文件{}：{} ", input, path);
            moveFile(path);
        }
        else
        {
            log.info("你未选中任何文件");
        }
        log.info("########## 当前文件处理完成 ##########\n");
    }
    
    /**
     * 移动文件
     * 
     * @param path
     * @throws IOException
     * @throws InterruptedException
     */
    private void moveFile(String path)
        throws IOException, InterruptedException
    {
        String to = new File("/").getCanonicalPath();
        log.info("【即将移动文件】 {} ===> {}", path, to);
        if (SystemUtils.IS_OS_WINDOWS)
        {
            String cmd = String.format("cmd /c copy %s %s", path, to);
            log.info(cmd);
            Runtime.getRuntime().exec(cmd);
            Runtime.getRuntime().exec("cmd /c start " + to);
        }
        if (SystemUtils.IS_OS_UNIX)
        {
            String cmd = String.format("/bin/sh -c cp %s %s", path, to);
            Runtime.getRuntime().exec(cmd);
        }
    }
    
    @Test
    public void test()
        throws Exception
    {
        try (Scanner sc = new Scanner(System.in))
        {
            String input;
            do
            {
                selectFile(sc);
                log.info("------------输入q退出,输入其他值继续------------");
                input = StringUtils.trimToEmpty(sc.nextLine());
            } while (!input.equalsIgnoreCase("q"));
            log.info("----------系统退出成功----------");
        }
    }
}
