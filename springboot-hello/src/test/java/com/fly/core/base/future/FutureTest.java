package com.fly.core.base.future;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FutureTest
{
    ExecutorService executorService =
        new ThreadPoolExecutor(10, 10, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(), new BasicThreadFactory.Builder().namingPattern("sum-thread-%03d").daemon(true).priority(Thread.MAX_PRIORITY).build());
    
    /**
     * 线程池执行数据求和后汇总
     * 
     * @throws InterruptedException
     * @throws ExecutionException
     */
    @Test
    public void testSum()
        throws InterruptedException, ExecutionException
    {
        StopWatch clock = new StopWatch();
        clock.start();
        List<CompletableFuture<Long>> futures = new ArrayList<>();
        IntStream.range(0, 10).forEach(i -> {
            CompletableFuture<Long> future = CompletableFuture.supplyAsync(() -> {
                try
                {
                    TimeUnit.MILLISECONDS.sleep(100); // 模拟耗时操作
                }
                catch (InterruptedException e)
                {
                }
                return LongStream.range(1000L * i, 1000L * (i + 1)).sum();
            }, executorService);
            futures.add(future);
            
            // 方法处理完成回调
            future.thenAccept((Long sum) -> {
                log.info("get sum: {}", sum);
            });
        });
        
        // 等待所有线程执行完毕
        CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()])).whenComplete((v, e) -> {
            log.info("所有任务执行完成触发,返回： {}", v);
        }).join();
        clock.stop();
        log.info("处理完成，用时: {}ms", clock.getTime());
    }
}
