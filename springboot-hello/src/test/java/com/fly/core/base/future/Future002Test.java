package com.fly.core.base.future;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * CompletableFuture详解 https://www.jianshu.com/p/6f3ee90ab7d3
 */
@Slf4j
public class Future002Test
{
    ExecutorService executorService = Executors.newFixedThreadPool(10);
    
    /**
     * 进行变换,入参是上一个阶段计算后的结果，返回值是经过转化后结果
     */
    @Test
    public void thenApply()
    {
        // 同步执行
        String result = CompletableFuture.supplyAsync(() -> "hello").thenApply(s -> s + " world").join();
        log.info(result);
        
        // 默认线程池ForkJoinPool.commonPool()中执行
        result = CompletableFuture.supplyAsync(() -> "hello").thenApplyAsync(s -> s + " world").join();
        log.info(result);
        
        // 指定线程池，异步执行
        result = CompletableFuture.supplyAsync(() -> "hello", executorService).thenApplyAsync(s -> s + " world").join();
        log.info(result);
    }
    
    /**
     * 进行消耗
     */
    @Test
    public void thenAccept()
    {
        CompletableFuture.supplyAsync(() -> "hello").thenAccept(s -> log.info("{} world", s));
        CompletableFuture.supplyAsync(() -> "hello").thenAcceptAsync(s -> log.info("{} world", s));
        CompletableFuture.supplyAsync(() -> "hello").thenAcceptAsync(s -> log.info("{} world", s), executorService);
    }
    
    /**
     * 对上一步的计算结果不关心，执行下一个操作
     */
    @Test
    public void thenRun()
    {
        CompletableFuture.supplyAsync(() -> "hello").thenRun(() -> log.info("hello world"));
        CompletableFuture.supplyAsync(() -> "hello").thenRunAsync(() -> log.info("hello world"));
        CompletableFuture.supplyAsync(() -> "hello").thenRunAsync(() -> log.info("hello world"), executorService);
    }
    
    /**
     * 结合两个CompletionStage的结果，进行转化后返回
     */
    @Test
    public void thenCombine()
    {
        String result = CompletableFuture.supplyAsync(() -> {
            sleepSeconds(2L);
            return "hello";
        }).thenCombine(CompletableFuture.supplyAsync(() -> {
            sleepSeconds(3L);
            return "world";
        }), (s1, s2) -> s1 + " " + s2).join();
        log.info(result);
        
        result = CompletableFuture.supplyAsync(() -> {
            sleepSeconds(2L);
            return "hello";
        }).thenCombineAsync(CompletableFuture.supplyAsync(() -> {
            sleepSeconds(3L);
            return "world";
        }), (s1, s2) -> s1 + " " + s2).join();
        log.info(result);
    }
    
    /**
     * 结合两个CompletionStage的结果，进行消耗
     * 
     * @throws InterruptedException
     */
    @Test
    public void thenAcceptBoth()
        throws InterruptedException
    {
        CompletableFuture.supplyAsync(() -> {
            sleepSeconds(2L);
            return "hello";
        }).thenAcceptBoth(CompletableFuture.supplyAsync(() -> {
            sleepSeconds(2L);
            return "world";
        }), (s1, s2) -> log.info(s1 + " " + s2));
        sleepSeconds(2L);
    }
    
    /**
     * 在两个CompletionStage都运行完执行
     */
    @Test
    public void runAfterBoth()
    {
        CompletableFuture.supplyAsync(() -> {
            sleepSeconds(2L);
            return "s1";
        }).runAfterBothAsync(CompletableFuture.supplyAsync(() -> {
            sleepSeconds(3L);
            return "s2";
        }), () -> log.info("hello world"));
        sleepSeconds(2L);
    }
    
    /**
     * 两个CompletionStage，谁计算的快，我就用那个CompletionStage的结果进行下一步的转化操作
     */
    @Test
    public void applyToEither()
    {
        String result = CompletableFuture.supplyAsync(() -> {
            sleepSeconds(3L);
            return "s1";
        }).applyToEither(CompletableFuture.supplyAsync(() -> {
            sleepSeconds(2L);
            return "hello world";
        }), s -> s).join();
        log.info(result);
    }
    
    /**
     * 两个CompletionStage，谁计算的快，我就用那个CompletionStage的结果进行下一步的消耗操作
     */
    @Test
    public void acceptEither()
    {
        CompletableFuture.supplyAsync(() -> {
            sleepSeconds(3L);
            return "s1";
        }).acceptEither(CompletableFuture.supplyAsync(() -> {
            sleepSeconds(2L);
            return "hello world";
        }), log::info);
        sleepSeconds(2L);
    }
    
    /**
     * 两个CompletionStage，任何一个完成了都会执行下一步的操作（Runnable）
     */
    @Test
    public void runAfterEither()
    {
        CompletableFuture.supplyAsync(() -> {
            sleepSeconds(3L);
            return "s1";
        }).runAfterEither(CompletableFuture.supplyAsync(() -> {
            sleepSeconds(2L);
            return "s2";
        }), () -> log.info("hello world"));
        sleepSeconds(2L);
    }
    
    /**
     * 当运行时出现了异常，可以通过exceptionally进行补偿
     */
    @Test
    public void exceptionally()
    {
        String result = CompletableFuture.supplyAsync(() -> {
            sleepSeconds(3L);
            if (RandomUtils.nextBoolean())
            {
                throw new RuntimeException("测试一下异常情况");
            }
            return "s1";
        }).exceptionally(e -> {
            log.info(e.getMessage());
            return "hello world";
        }).join();
        log.info(result);
    }
    
    /**
     * 当运行完成时，对结果的记录,这里的完成时有两种情况，一种是正常执行，返回值。另外一种是遇到异常抛出造成程序的中断。这里为什么要说成记录，因为这几个方法都会返回CompletableFuture，当Action执行完毕后它的结果返回原始的CompletableFuture的计算结果或者返回异常。所以不会对结果产生任何的作用
     */
    @Test
    public void whenComplete()
    {
        String result = CompletableFuture.supplyAsync(() -> {
            sleepSeconds(3L);
            if (RandomUtils.nextBoolean())
            {
                throw new RuntimeException("测试一下异常情况");
            }
            return "s1";
        }).whenComplete((s, t) -> {
            log.info(s);
            log.info(t.getMessage());
        }).exceptionally(e -> {
            log.info(e.getMessage());
            return "hello world";
        }).join();
        log.info(result);
    }
    
    /**
     * 运行完成时，对结果的处理。这里的完成时有两种情况，一种是正常执行，返回值。另外一种是遇到异常抛出造成程序的中断
     */
    @Test
    public void handle001()
    {
        String result = CompletableFuture.supplyAsync(() -> {
            sleepSeconds(3L);
            if (RandomUtils.nextBoolean())
            {
                throw new RuntimeException("测试一下异常情况");
            }
            return "s1";
        }).handle((s, t) -> {
            if (t != null)
            {
                return "hello world";
            }
            return s;
        }).join();
        log.info(result);
    }
    
    @Test
    public void handle002()
    {
        String result = CompletableFuture.supplyAsync(() -> {
            sleepSeconds(3L);
            return "s1";
        }).handle((s, t) -> {
            if (t != null)
            {
                return "hello world";
            }
            return s;
        }).join();
        log.info(result);
    }
    
    /**
     * 线程等待
     * 
     * @param seconds
     */
    private void sleepSeconds(Long seconds)
    {
        try
        {
            TimeUnit.SECONDS.sleep(seconds);
        }
        catch (InterruptedException e)
        {
        }
    }
}
