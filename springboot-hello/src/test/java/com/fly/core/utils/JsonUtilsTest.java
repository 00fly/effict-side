package com.fly.core.utils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.fly.core.utils.jackson.JsonUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JsonUtilsTest
{
    static String jsonText;
    
    @BeforeAll
    public static void init()
    {
        try
        {
            jsonText = IOUtils.resourceToString("/data.json", StandardCharsets.UTF_8);
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @Test
    public void jsonToPropText()
        throws IOException
    {
        String propText = JsonUtils.jsonToPropText(jsonText);
        log.info("jsonToPropText: {}", propText);
    }
    
    @Test
    public void jsonToXml()
        throws IOException
    {
        String xml = JsonUtils.jsonToXml(jsonText);
        log.info("jsonToXML: {}", xml);
    }
    
    @Test
    public void jsonToYaml()
        throws IOException
    {
        String yaml = JsonUtils.jsonToYaml(jsonText);
        log.info("jsonToYaml: {}", yaml);
    }
}
