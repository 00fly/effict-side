package com.fly.core.utils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ShellExecutorTest
{
    @Test
    public void test001()
        throws IOException
    {
        String response = ShellExecutor.execute("curl https://00fly.online/upload/data.json");
        log.info(response);
    }
    
    @Test
    public void test002()
        throws IOException
    {
        ShellExecutor.exec("curl -X GET -H \"Accept:image/jpeg\" -H \"Content-Type:application/x-www-form-urlencoded\" \"https://00fly.online/upload/2019/02/201902262129360274AKuFZcUfip.jpg\" --output flower.jpg");
    }
    
    @Test
    public void test003()
        throws IOException
    {
        String response = ShellExecutor.execute("curl https://00fly.online/upload/data.json");
        FileUtils.writeStringToFile(new File("test.json"), response, StandardCharsets.UTF_8, false);
    }
    
    @Test
    public void test004()
        throws IOException
    {
        ShellExecutor.exec("curl https://00fly.online/upload/data.json -o test.json");
        ShellExecutor.exec("curl https://00fly.online/upload/data.json --output test2.json");
    }
    
    @Test
    public void test005()
        throws IOException
    {
        // 模仿浏览器、伪造referer
        String response = ShellExecutor
            .execute("curl -A \"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.0)\" -e \"blog.csdn.net\" \"https://blog.csdn.net/community/home-api/v1/get-business-list?page=1&size=20&businessType=lately&noMore=false&username=qq_16127313\"");
        log.info(response);
    }
    
    @Test
    public void test006()
        throws IOException
    {
        // 伪造referer
        String response = ShellExecutor.execute("curl -e \"blog.csdn.net\" \"https://blog.csdn.net/community/home-api/v1/get-business-list?page=1&size=20&businessType=lately&noMore=false&username=qq_16127313\"");
        log.info(response);
    }
}
