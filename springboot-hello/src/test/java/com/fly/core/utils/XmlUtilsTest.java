package com.fly.core.utils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.fly.core.utils.jackson.XmlUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class XmlUtilsTest
{
    static String xmlContent;
    
    @BeforeAll
    public static void init()
    {
        try
        {
            Resource resource = new ClassPathResource("log4j2.xml");
            xmlContent = IOUtils.toString(resource.getURL(), StandardCharsets.UTF_8);
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @Test
    public void xmlToJson()
        throws IOException
    {
        String json = XmlUtils.xmlToJson(xmlContent);
        log.info("xmlToJson: {}", json);
    }
    
    @Test
    public void xmlToProperties()
        throws IOException
    {
        Properties properties = XmlUtils.xmlToProperties(xmlContent);
        log.info("xmlToProperties: {}", properties);
    }
    
    @Test
    public void xmlToPropText()
        throws IOException
    {
        String propText = XmlUtils.xmlToPropText(xmlContent);
        log.info("xmlToPropText: {}", propText);
    }
}
