package com.fly.hello.xml;

import java.io.IOException;
import java.util.Iterator;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Namespace;
import org.dom4j.Node;
import org.dom4j.VisitorSupport;
import org.dom4j.io.SAXReader;
import org.dom4j.tree.DefaultElement;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Dom4jTest
{
    Document document;
    
    // 初始化代码块
    {
        try
        {
            document = new SAXReader().read(new ClassPathResource("data.xml").getFile());
            document.accept(new Dom4jNameSpaceCleaner());
        }
        catch (DocumentException | IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    /**
     * DOM4J解析
     */
    @Test
    public void testByDom4J()
        throws Exception
    {
        Element root = document.getRootElement();
        Iterator<Element> iterator = root.elementIterator();
        int index = 1;
        while (iterator.hasNext())
        {
            log.info("###### node: {}", index++);
            Element e1 = iterator.next();
            log.info("\tL1: {} ==> {}", e1.getName(), e1.getTextTrim());
            Iterator<Element> iterator2 = e1.elementIterator();
            while (iterator2.hasNext())
            {
                Element e2 = iterator2.next();
                log.info("\t\tL2: {} ==> {}", e2.getName(), e2.getTextTrim());
            }
        }
    }
    
    /**
     * dom4j使用XPath发现不能获取到节点，可能是命名空间的问题
     */
    @Test
    public void testByXpath001()
    {
        // 取list下任意节点
        document.getRootElement().selectNodes("//list/*").stream().forEach(node -> log.info("{} ===> {}", node.getName(), node.getText()));
    }
    
    @Test
    public void testByXpath002()
    {
        // 取list下title或viewCount节点
        document.selectNodes("//list/title|//list/viewCount").stream().forEach(node -> log.info("{} ===> {}", node.getName(), node.getText()));
    }
    
    @Test
    public void testByXpath003()
    {
        // 取节点
        Node node = document.selectSingleNode("//list[articleId='135418954']/title");
        if (node != null)
        {
            log.info("{} ===> {}", node.getName(), node.getText());
        }
    }
    
    @Test
    public void testByXpath004()
    {
        // 取节点
        Node node = document.selectSingleNode("//list[articleId='135418954']/viewCount");
        if (node != null)
        {
            log.info("{} ===> {}", node.getName(), node.getText());
        }
    }
}

/**
 * dom4j xpath 清理namespace
 */
final class Dom4jNameSpaceCleaner extends VisitorSupport
{
    @Override
    public void visit(Document document)
    {
        ((DefaultElement)document.getRootElement()).setNamespace(Namespace.NO_NAMESPACE);
        document.getRootElement().additionalNamespaces().clear();
    }
    
    @Override
    public void visit(Namespace namespace)
    {
        namespace.detach();
    }
    
    @Override
    public void visit(Attribute node)
    {
        String content = node.toString();
        if (content.contains("xmlns") || content.contains("xsi:"))
        {
            node.detach();
        }
    }
    
    @Override
    public void visit(Element node)
    {
        if (node instanceof DefaultElement)
        {
            ((DefaultElement)node).setNamespace(Namespace.NO_NAMESPACE);
        }
    }
}
