package com.fly.hello.xml;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import lombok.extern.slf4j.Slf4j;

/**
 * 测试
 */
@Slf4j
public class JdomTest
{
    Document document;
    
    // 初始化代码块
    {
        try
        {
            File xmlFile = new ClassPathResource("data.xml").getFile();
            SAXBuilder saxBuilder = new SAXBuilder();
            document = saxBuilder.build(xmlFile);
        }
        catch (JDOMException | IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    /**
     * JDOM解析
     * 
     * @throws IOException
     * @throws JDOMException
     */
    @Test
    public void testByJDOM()
        throws JDOMException, IOException
    {
        for (Element element : (List<Element>)document.getRootElement().getChildren())
        {
            for (Element e : (List<Element>)element.getChildren())
            {
                log.info("{} ==> {}", e.getName(), e.getValue());
            }
        }
    }
    
    @Test
    @SuppressWarnings("unchecked")
    public void testByXpath001()
        throws JDOMException
    {
        // 取list下任意节点
        List<Element> list = XPath.selectNodes(document, "//list/*");
        list.stream().forEach(element -> log.info("{} ===> {}", element.getName(), element.getText()));
    }
    
    @Test
    @SuppressWarnings("unchecked")
    public void testByXpath002()
        throws JDOMException
    {
        // 取list下title或viewCount节点
        List<Element> list = XPath.selectNodes(document, "//list/title|//list/viewCount");
        list.stream().forEach(element -> log.info("{} ===> {}", element.getName(), element.getText()));
    }
    
    @Test
    public void testByXpath003()
        throws JDOMException
    {
        Element element = (Element)XPath.selectSingleNode(document, "//list[articleId='135418954']/title");
        log.info("{} ===> {}", element.getName(), element.getText());
    }
    
    @Test
    public void testByXpath004()
        throws JDOMException
    {
        Element element = (Element)XPath.selectSingleNode(document, "//list[articleId='135418954']/description");
        log.info("{} ===> {}", element.getName(), element.getText());
    }
}