package com.fly.hello.xml.bean;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Root
{
    private Config config;
}
