package com.fly.hello.xml.bean;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Config
{
    List<Item> property = new ArrayList<>();
}
