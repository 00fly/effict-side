package com.fly.hello.xml;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import lombok.extern.slf4j.Slf4j;

/**
 * 测试
 */
@Slf4j
public class DomTest
{
    Document document;
    
    XPath xPath;
    
    // 初始化代码块
    {
        try
        {
            File xmlFile = new ClassPathResource("data.xml").getFile();
            document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xmlFile);
            xPath = XPathFactory.newInstance().newXPath();
        }
        catch (ParserConfigurationException | SAXException | IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    /**
     * DOM解析
     */
    @Test
    public void testByDom()
        throws Exception
    {
        NodeList nodeList = document.getElementsByTagName("list");
        for (int i = 0; i < nodeList.getLength(); i++)
        {
            Node node = nodeList.item(i);
            NodeList childList = node.getChildNodes();
            for (int j = 0; j < childList.getLength(); j++)
            {
                Node childNode = childList.item(j);
                if (childNode.getNodeType() == Node.ELEMENT_NODE)
                {
                    log.info("{} ==> {}", childNode.getNodeName(), childNode.getTextContent());
                }
            }
        }
    }
    
    @Test
    public void testXpath001()
        throws Exception
    {
        NodeList nodeList = (NodeList)xPath.compile("//list").evaluate(document, XPathConstants.NODESET);
        for (int i = 0; i < nodeList.getLength(); i++)
        {
            Node node = nodeList.item(i);
            NodeList childList = node.getChildNodes();
            for (int j = 0; j < childList.getLength(); j++)
            {
                Node childNode = childList.item(j);
                if (childNode.getNodeType() == Node.ELEMENT_NODE)
                {
                    log.info("//list: {} ==> {}", childNode.getNodeName(), childNode.getTextContent());
                }
            }
        }
    }
    
    @Test
    public void testXpath002()
        throws XPathExpressionException
    {
        NodeList list = (NodeList)xPath.compile("//list/*").evaluate(document, XPathConstants.NODESET);
        for (int j = 0; j < list.getLength(); j++)
        {
            Node childNode = list.item(j);
            if (childNode.getNodeType() == Node.ELEMENT_NODE)
            {
                log.info("//list/*: {} ==> {}", childNode.getNodeName(), childNode.getTextContent());
            }
        }
    }
    
    @Test
    public void testXpath003()
        throws XPathExpressionException
    {
        NodeList list = (NodeList)xPath.compile("//list/title|//list/viewCount").evaluate(document, XPathConstants.NODESET);
        for (int j = 0; j < list.getLength(); j++)
        {
            Node childNode = list.item(j);
            if (childNode.getNodeType() == Node.ELEMENT_NODE)
            {
                log.info("{} ==> {}", childNode.getNodeName(), childNode.getTextContent());
            }
        }
    }
    
    @Test
    public void testXpath004()
        throws XPathExpressionException
    {
        // 取节点articleId='135418954'的list节点内容
        Node node = (Node)xPath.compile("//list[articleId='135418954']").evaluate(document, XPathConstants.NODE);
        log.info("{}", node.getTextContent());
        
        // 注意比较异同
        // 取属性articleId='135418954'的list节点内容
        node = (Node)xPath.compile("//list[@articleId='135418954']").evaluate(document, XPathConstants.NODE);
        if (node != null)
        {
            log.info("{}", node.getTextContent());
        }
        
        // 取节点articleId='135418954'的//list/title节点内容
        node = (Node)xPath.compile("//list[articleId='135418954']/title").evaluate(document, XPathConstants.NODE);
        log.info("name: {}", node.getTextContent());
        
        // 取节点articleId='135418954'的//list/viewCount节点内容
        node = (Node)xPath.compile("//list[articleId='135418954']/viewCount").evaluate(document, XPathConstants.NODE);
        log.info("value: {}", node.getTextContent());
    }
}