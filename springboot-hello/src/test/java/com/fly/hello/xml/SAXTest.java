package com.fly.hello.xml;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import lombok.extern.slf4j.Slf4j;

/**
 * 测试
 */
@Slf4j
public class SAXTest
{
    File xmlFile;
    
    SAXParser saxParser;
    
    // 初始化代码块
    {
        try
        {
            xmlFile = new ClassPathResource("data.xml").getFile();
            SAXParserFactory factory = SAXParserFactory.newInstance();
            saxParser = factory.newSAXParser();
        }
        catch (ParserConfigurationException | SAXException | IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    /**
     * SAX解析
     */
    @Test
    public void testBySAX()
        throws Exception
    {
        saxParser.parse(xmlFile, new XmlParseHandler());
    }
}

@Slf4j
class XmlParseHandler extends DefaultHandler
{
    private String tagName;
    
    /**
     * 节点解析开始调用
     * 
     * @param uri 命名空间uri
     * @param localName 标签名称
     * @param qName 带命名空间的标签名称
     * @param attributes
     * @throws SAXException
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
        throws SAXException
    {
        super.startElement(uri, localName, qName, attributes);
        // log.info("{} - startElement", qName);
        this.tagName = qName;
    }
    
    @Override
    public void endElement(String uri, String localName, String qName)
        throws SAXException
    {
        this.tagName = null;
    }
    
    @Override
    public void characters(char ch[], int start, int length)
        throws SAXException
    {
        // if (StringUtils.equalsAny(tagName, "title", "viewCount"))
        if (this.tagName != null)
        {
            String data = new String(ch, start, length);
            if (StringUtils.isNotBlank(data))
            {
                log.info("{} ===> {}", tagName, data);
            }
        }
    }
}