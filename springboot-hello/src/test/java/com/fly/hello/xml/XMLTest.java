package com.fly.hello.xml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fly.core.utils.jackson.JsonBeanUtils;
import com.fly.core.utils.jackson.JsonNodeUtils;
import com.fly.hello.xml.bean.Config;
import com.fly.hello.xml.bean.Item;
import com.fly.hello.xml.bean.Root;

/**
 * 测试
 */
public class XMLTest
{
    private XmlMapper xmlMapper = new XmlMapper();
    
    @Test
    public void test()
        throws IOException
    {
        List<Item> data = new ArrayList<>();
        data.add(new Item().setName("k1").setValue("v1"));
        data.add(new Item().setName("k2").setValue("v2"));
        Root root = new Root().setConfig(new Config().setProperty(data));
        System.out.println(JsonNodeUtils.jsonNodeToXml(JsonNodeUtils.jsonToJsonNode(JsonBeanUtils.beanToJson(root, true))));
        
        // xml与对象转换
        String xml = xmlMapper.writeValueAsString(root);
        Root result = xmlMapper.readValue(xml, Root.class);
        System.out.println(xml);
        System.out.println(result);
    }
}