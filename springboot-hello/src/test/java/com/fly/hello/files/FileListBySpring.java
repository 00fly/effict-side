package com.fly.hello.files;

import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FileListBySpring
{
    PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
    
    String userProfile = "file:" + System.getenv().get("USERPROFILE");
    
    /**
     * 遍历文件
     * 
     * @throws IOException
     */
    @Test
    public void testList()
        throws IOException
    {
        // 外部文件
        Resource[] jpgs = resolver.getResources(userProfile + "/Pictures/**/*.jpg");
        Arrays.stream(jpgs).forEach(System.out::println);
        
        // 外部文件
        Resource[] pngs = resolver.getResources(userProfile + "/Pictures/**/*.png");
        Arrays.stream(pngs).forEach(System.out::println);
        
        // 工程或jar内文件
        Resource[] xmls = resolver.getResources("classpath*:**/*.xml");
        Arrays.stream(xmls).forEach(System.out::println);
        
        // 合并
        log.info("################################################## 合并后 ##################################################");
        Stream.of(jpgs, pngs, xmls).flatMap(Arrays::stream).forEach(System.out::println);
    }
}
