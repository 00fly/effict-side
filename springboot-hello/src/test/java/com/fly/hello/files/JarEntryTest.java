package com.fly.hello.files;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;

import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * @author 00fly
 *
 */
@Slf4j
public class JarEntryTest
{
    String src = getClass().getResource("/servlet-api.jar").getPath();
    
    @Test
    public void testCopy()
    {
        try
        {
            String newJar = src.replace(".jar", DateFormatUtils.format(System.currentTimeMillis(), "_HHmmssSSS") + ".jar");
            log.info("源文件: {}", src);
            log.info("新文件: {}", newJar);
            copyJar(src, newJar);
            
            if (SystemUtils.IS_OS_WINDOWS)
            {
                Runtime.getRuntime().exec("cmd /c start " + new File(newJar).getParentFile().getCanonicalPath());
            }
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @Test
    public void testList()
        throws IOException
    {
        try (JarInputStream jis = new JarInputStream(new BufferedInputStream(new FileInputStream(src))))
        {
            JarEntry je;
            while ((je = jis.getNextJarEntry()) != null)
            {
                System.out.println(je.getName());
            }
            jis.closeEntry();
        }
    }
    
    /**
     * 拷贝 Jar
     * 
     * @param fromJar
     * @param toJar
     * @throws IOException
     */
    public static void copyJar(String fromJar, String toJar)
        throws IOException
    {
        try (JarInputStream jis = new JarInputStream(new BufferedInputStream(new FileInputStream(fromJar))); JarOutputStream jos = new JarOutputStream(new BufferedOutputStream(new FileOutputStream(toJar)), jis.getManifest()))
        {
            JarEntry je;
            while ((je = jis.getNextJarEntry()) != null)
            {
                jos.putNextEntry(je);
                int iRead;
                while ((iRead = jis.read()) != -1)
                {
                    jos.write(iRead);
                }
            }
            jis.closeEntry();
            jos.finish();
        }
    }
}
