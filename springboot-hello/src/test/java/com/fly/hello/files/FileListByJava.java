package com.fly.hello.files;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.stream.Stream;

import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.FileFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * 文件遍历
 */
@Slf4j
public class FileListByJava
{
    String path = System.getenv().get("USERPROFILE") + "\\Pictures\\";
    
    File searchFile = new File(path);
    
    @BeforeEach
    public void before()
    {
        log.info(path);
    }
    
    /**
     * 遍历文件
     */
    @Test
    public void testList()
    {
        Arrays.stream(searchFile.list()).forEach(System.out::println);
    }
    
    /**
     * 遍历文件
     */
    @Test
    public void testFile()
    {
        File[] files = searchFile.listFiles((FileFilter)FileFileFilter.FILE);
        Stream.of(files).forEach(System.out::println);
        
        // 等价lambda写法
        files = searchFile.listFiles((file) -> file.isFile());
        Stream.of(files).forEach(System.out::println);
    }
    
    /**
     * 遍历目录
     */
    @Test
    public void testDirectory()
    {
        File[] files = searchFile.listFiles((FileFilter)DirectoryFileFilter.DIRECTORY);
        Stream.of(files).forEach(System.out::println);
        
        // 等价lambda写法
        files = searchFile.listFiles((file) -> file.isDirectory());
        Stream.of(files).forEach(System.out::println);
    }
    
    /**
     * 自定义文件名过滤遍历
     */
    @Test
    public void testFileName()
    {
        File[] files = searchFile.listFiles(new FilenameFilter()
        {
            @Override
            public boolean accept(File dir, String name)
            {
                // 广义的文件包含目录，name为文件（包含目录）名称
                return StringUtils.endsWithAny(name, "abc", ".jpg", ".png");
            }
        });
        Stream.of(files).forEach(System.out::println);
        
        // 等价lambda写法
        files = searchFile.listFiles((d, s) -> StringUtils.endsWithAny(s, "abc", ".jpg", ".png"));
        Stream.of(files).forEach(System.out::println);
    }
}
