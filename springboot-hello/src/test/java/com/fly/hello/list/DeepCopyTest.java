package com.fly.hello.list;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import com.fly.core.utils.jackson.JsonBeanUtils;
import com.fly.hello.entity.Article;
import com.fly.hello.entity.BlogData;

import lombok.extern.slf4j.Slf4j;

/**
 * 浅拷贝（Shallow Copy）和深拷贝（Deep Copy）
 */
@Slf4j
public class DeepCopyTest
{
    private List<Article> articles;
    
    {
        try
        {
            String jsonText = IOUtils.toString(new ClassPathResource("data.json").getURL(), StandardCharsets.UTF_8);
            BlogData blogData = JsonBeanUtils.jsonToBean(jsonText, BlogData.class, true);
            articles = blogData.getData().getList();
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    /**
     * 深拷贝
     * 
     * @throws IOException
     */
    @Test
    public void testDeepCopy001()
        throws IOException
    {
        List<Article> articles2 = deepCopy(articles);
        
        // 修改原始数据
        articles.get(0).setTitle("修改title");
        log.info("######原始###### \n{}", JsonBeanUtils.beanToJson(articles, true));
        log.info("######拷贝###### \n{}", JsonBeanUtils.beanToJson(articles2, true));
    }
    
    /**
     * 对集合进行深拷贝,注意需要泛型类进行序列化（实现serializable）
     *
     * @param src
     * @param <T>
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> deepCopy(List<T> src)
    {
        try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream(); ObjectOutputStream outputStream = new ObjectOutputStream(byteOut);)
        {
            outputStream.writeObject(src);
            try (ByteArrayInputStream byteIn = new ByteArrayInputStream(byteOut.toByteArray()); ObjectInputStream inputStream = new ObjectInputStream(byteIn);)
            {
                return (List<T>)inputStream.readObject();
            }
        }
        catch (IOException | ClassNotFoundException e)
        {
            log.error(e.getMessage(), e);
            return Collections.emptyList();
        }
    }
}
