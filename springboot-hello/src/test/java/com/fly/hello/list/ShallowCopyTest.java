package com.fly.hello.list;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import com.fly.core.utils.jackson.JsonBeanUtils;
import com.fly.hello.entity.Article;
import com.fly.hello.entity.BlogData;

import lombok.extern.slf4j.Slf4j;

/**
 * 浅拷贝（Shallow Copy）和深拷贝（Deep Copy）
 */
@Slf4j
public class ShallowCopyTest
{
    private List<Article> articles;
    
    {
        try
        {
            String jsonText = IOUtils.toString(new ClassPathResource("data.json").getURL(), StandardCharsets.UTF_8);
            BlogData blogData = JsonBeanUtils.jsonToBean(jsonText, BlogData.class, true);
            articles = blogData.getData().getList();
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    /**
     * 浅拷贝,循环遍历复制
     * 
     * @throws IOException
     */
    @Test
    public void testShallowCopy001()
        throws IOException
    {
        List<Article> articles2 = new ArrayList<>();
        for (Article article : articles)
        {
            articles2.add(article);
        }
        // 修改原始数据
        articles.get(0).setTitle("修改title");
        log.info("######原始###### \n{}", JsonBeanUtils.beanToJson(articles, true));
        log.info("######拷贝###### \n{}", JsonBeanUtils.beanToJson(articles2, true));
    }
    
    /**
     * 浅拷贝,使用 List 实现类的构造方法
     * 
     * @throws IOException
     */
    @Test
    public void testShallowCopy002()
        throws IOException
    {
        List<Article> articles2 = new ArrayList<>(articles);
        
        // 修改原始数据
        articles.get(0).setTitle("修改title");
        log.info("######原始###### \n{}", JsonBeanUtils.beanToJson(articles, true));
        log.info("######拷贝###### \n{}", JsonBeanUtils.beanToJson(articles2, true));
    }
    
    /**
     * 浅拷贝,使用list.addAll() 方法
     * 
     * @throws IOException
     */
    @Test
    public void testShallowCopy003()
        throws IOException
    {
        List<Article> articles2 = new ArrayList<>();
        articles2.addAll(articles);
        
        // 修改原始数据
        articles.get(0).setTitle("修改title");
        log.info("######原始###### \n{}", JsonBeanUtils.beanToJson(articles, true));
        log.info("######拷贝###### \n{}", JsonBeanUtils.beanToJson(articles2, true));
    }
    
    /**
     * 浅拷贝,使用 java.util.Collections.copy() 方法
     * 
     * @throws IOException
     */
    @Test
    public void testShallowCopy004()
        throws IOException
    {
        List<Article> articles2 = new ArrayList<>();
        IntStream.range(0, articles.size()).forEach(i -> articles2.add(new Article()));
        Collections.copy(articles2, articles);
        
        // 修改原始数据
        articles.get(0).setTitle("修改title");
        log.info("######原始###### \n{}", JsonBeanUtils.beanToJson(articles, true));
        log.info("######拷贝###### \n{}", JsonBeanUtils.beanToJson(articles2, true));
    }
    
    /**
     * 浅拷贝,使用 Java 8 Stream API 将 List 复制到另一个 List 中
     * 
     * @throws IOException
     */
    @Test
    public void testShallowCopy005()
        throws IOException
    {
        List<Article> articles2 = articles.stream().collect(Collectors.toList());
        
        // 修改原始数据
        articles.get(0).setTitle("修改title");
        log.info("######原始###### \n{}", JsonBeanUtils.beanToJson(articles, true));
        log.info("######拷贝###### \n{}", JsonBeanUtils.beanToJson(articles2, true));
    }
}
