package com.fly.hello.template;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Transactional;

import com.fly.hello.template.bean.StudentVO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Transactional
@SpringBootTest(webEnvironment = WebEnvironment.NONE)
public class JdbcTemplateTest
{
    @Autowired
    JdbcTemplate jdbcTemplate;
    
    RowMapper<StudentVO> rowMapper = new BeanPropertyRowMapper<>(StudentVO.class);
    
    @BeforeEach
    public void init()
    {
        // execute可以执行所有SQL语句，因为没有返回值，一般用于执行DDL语句
        jdbcTemplate.execute("drop table if exists student");
        jdbcTemplate.execute("create table student(id bigint NOT NULL AUTO_INCREMENT, stu_name varchar(50), primary key(id))");
        jdbcTemplate.execute("insert into student(stu_name) values('Jack')");
        jdbcTemplate.execute("insert into student(stu_name) values('Phil')");
        jdbcTemplate.execute("insert into student(stu_name) values('Jenny')");
        
        // batchUpdate
        jdbcTemplate.batchUpdate("insert into student(stu_name) values(?)", Arrays.asList(new Object[][] {{"Tom"}, {"Jerry"}}));
        
        jdbcTemplate.execute("delete from student where id > 100");
        log.info("::: init success!!");
        log.info(">>>>> before: {}", jdbcTemplate.queryForObject("select count(*) from student", Long.class));
    }
    
    @AfterEach
    public void after()
    {
        log.info(">>>>> after : {}", jdbcTemplate.queryForObject("select count(*) from student", Long.class));
    }
    
    @Test
    public void testUpdate()
    {
        // update方法进行增删改
        int count = jdbcTemplate.update("insert into student(stu_name) values(?)", RandomStringUtils.randomAlphabetic(5));
        log.info("{}", count);
    }
    
    @Test
    public void testQueryBean()
    {
        // 读取单个对象
        StudentVO studentVO = jdbcTemplate.queryForObject("select id, stu_name from student where id=?", rowMapper, 1);
        log.info("{}", studentVO);
        
        // 读取多个对象
        List<StudentVO> list = jdbcTemplate.query("select id, stu_name from student", rowMapper);
        log.info("{}", list);
    }
    
    @Test
    public void testQueryBase1()
    {
        // 返回 List<Map<String, Object>>
        List<Map<String, Object>> data = jdbcTemplate.queryForList("select id, stu_name from student");
        log.info("{}", data);
        
        // 返回 Map<String, Object>
        Map<String, Object> map = jdbcTemplate.queryForMap("select * from student where id=?", 2);
        log.info("{}", map);
    }
    
    @Test
    public void testQueryBase2()
    {
        // 返回指定类型
        int count = jdbcTemplate.queryForObject("select count(*) from student", Integer.class);
        log.info("{}", count);
        
        String stuName = jdbcTemplate.queryForObject("select stu_name from student where id=?", String.class, 2);
        log.info("{}", stuName);
    }
    
    @Test
    public void testBatchUpdate()
    {
        String sql = "insert into student(stu_name) values(?)";
        List<Object[]> batchArgs = new ArrayList<Object[]>();
        batchArgs.add(new Object[] {"小马"});
        batchArgs.add(new Object[] {"大马"});
        batchArgs.add(new Object[] {"小刘"});
        batchArgs.add(new Object[] {"大刘"});
        batchArgs.add(new Object[] {"小强"});
        batchArgs.add(new Object[] {"大强"});
        jdbcTemplate.batchUpdate(sql, batchArgs);
    }
    
    @Test
    public void testLike()
    {
        testBatchUpdate();
        log.info("{}", jdbcTemplate.queryForList("select * from student where stu_name like ?", "%小%"));
        log.info("{}", jdbcTemplate.queryForList("select * from student where stu_name like concat('%', ?, '%')", "小"));
        
        // bean对象
        log.info("{}", jdbcTemplate.query("select * from student where stu_name like ?", rowMapper, "%小%"));
        log.info("{}", jdbcTemplate.query("select * from student where stu_name like concat('%', ?, '%')", rowMapper, "小"));
    }
}
