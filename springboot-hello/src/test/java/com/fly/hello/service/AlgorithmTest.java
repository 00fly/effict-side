package com.fly.hello.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.Test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 模拟测试访问量算法具有补偿功能
 */
@Slf4j
public class AlgorithmTest
{
    @Test
    public void test()
    {
        int length = 50;
        List<Visit> list = new ArrayList<>(length);
        IntStream.range(0, length).forEach(n -> list.add(new Visit(RandomStringUtils.randomAlphanumeric(10), RandomUtils.nextLong(0, 1000))));
        AtomicInteger count1 = new AtomicInteger(0);
        
        // 写法1
        Collections.sort(list, Comparator.comparing(Visit::getCount));
        list.stream().forEach(v -> log.info("{}. {}", count1.incrementAndGet(), v));
        
        // 写法2，与追加后的数据输出比较，注意细微区别
        // list.stream().sorted(Comparator.comparing(Visit::getCount)).forEach(v -> log.info("{}. {}", count1.incrementAndGet(), v));
        
        long before = list.stream().mapToLong(Visit::getCount).sum();
        
        log.info("******随机选择2条，对count较小的执行追加******");
        LongStream.range(0, 30000).forEach(num -> new Random().ints(2, 0, length).mapToObj(index -> list.get(index)).min(Comparator.comparing(Visit::getCount)).get().addCount());
        AtomicInteger count2 = new AtomicInteger(0);
        list.stream().forEach(v -> log.info("{}. {}", count2.incrementAndGet(), v));
        long after = list.stream().mapToLong(Visit::getCount).sum();
        log.info("SUM: {} ---> {}", before, after);
    }
}

@Data
@AllArgsConstructor
class Visit
{
    /**
     * 链接地址
     */
    private String url;
    
    /**
     * 访问量
     */
    private Long count;
    
    /**
     * 新增访问量
     */
    public void addCount()
    {
        count++;
    }
}