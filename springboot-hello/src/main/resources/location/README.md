
#### 说明

##### application-location.yml 用来测试加载外部配置文档。

```
java -jar springboot-hello-1.0.0.jar --spring.config.location=./application-location.yml
```

#### 怎么实现通过外部配置文件初始化日志数据源？