package com.fly.hello.runner;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.fly.core.utils.SpringContextUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class StaticCode
{
    @Autowired
    ApplicationContext context;
    
    static
    {
        log.info("==================类初次被加载时执行，仅执行一次");
        log.info("==================StaticCode static");
    }
    
    @PostConstruct
    private void init()
    {
        log.info("==================非spring管理，永远不会执行，除非加上@Component注解");
        log.info("==================StaticCode @PostConstruct, profile: {}", SpringContextUtils.getActiveProfile(context));
    }
    
    public void print()
    {
        log.info("==================StaticCode print: {}", SpringContextUtils.getActiveProfile());
    }
}
