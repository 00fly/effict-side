package com.fly.hello.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.fly.core.JsonResult;
import com.fly.core.annotation.Item;
import com.fly.core.annotation.OpenApi;
import com.fly.hello.feign.MyFeignClient;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags = "测试FeignClient")
@RestController
public class FeignClientController
{
    @Autowired
    private MyFeignClient feignClient;
    
    @OpenApi(checkItems = Item.ALL)
    @ApiOperation("关键字搜索")
    @PostMapping("query")
    public String query(@RequestHeader String token, @RequestHeader String timestamp, @RequestHeader String sign, String keyword)
    {
        log.info("####### feignClient.search({})", keyword);
        return feignClient.search(keyword);
    }

    @OpenApi(checkItems = Item.ALL)
    @ApiOperation("测试")
    @GetMapping("/test")
    public JsonResult<?> test()
    {
        return JsonResult.success();
    }
}
