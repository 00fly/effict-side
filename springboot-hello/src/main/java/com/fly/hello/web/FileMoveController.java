package com.fly.hello.web;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fly.core.JsonResult;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/filemove")
@Api(tags = "CSV演示接口")
public class FileMoveController
{
    @ApiOperationSupport(order = 20)
    @ApiOperation("目录csv搜索")
    @PostMapping("/listCsv")
    public JsonResult<?> listCsv(@Valid DirRequest request)
    {
        File dir = new File(request.getFileDirPath());
        if (!dir.exists() || !dir.isDirectory())
        {
            return JsonResult.error("文件不存在或不是一个目录");
        }
        // 检索当前目录及子目录
        Collection<File> files = FileUtils.listFiles(dir, new String[] {"csv", "jpg"}, true);
        if (files.isEmpty())
        {
            return JsonResult.error("当前目录及子目录不存在csv文件");
        }
        
        // 收集路径后过滤排序返回
        List<String> paths = files.stream().map(f -> f.getAbsolutePath()).filter(StringUtils::isNotBlank).sorted().collect(Collectors.toList());
        return JsonResult.success(paths);
    }
    
    @ApiOperationSupport(order = 30)
    @ApiOperation("csv文件移动")
    @PostMapping("/moveCsv")
    public JsonResult<?> moveCsv(@Valid MoveRequest request)
        throws IOException
    {
        // 判断是否合法
        String from = request.getCsvPath();
        String to = request.getDestDirPath();
        File src = new File(from);
        if (!src.exists() || !StringUtils.endsWithAny(from, ".csv", ".jpg") || !src.isFile())
        {
            return JsonResult.error("csv文件不存在或非法的csv文件");
        }
        File dir = new File(to);
        if (!dir.exists() || !dir.isDirectory())
        {
            return JsonResult.error("目录不存在或不是一个目录");
        }
        // 移动文件
        log.info("【即将移动文件】 {} ===> {}", from, to);
        if (SystemUtils.IS_OS_WINDOWS)
        {
            String cmd = String.format("copy %s %s", from, to);
            Runtime.getRuntime().exec(new String[] {"cmd", "/c", cmd});
            
            // 打印命令并打开目录
            log.info("--------- windows will run: {}", cmd);
            Runtime.getRuntime().exec("cmd /c start " + to);
        }
        if (SystemUtils.IS_OS_UNIX)
        {
            String cmd = String.format("cp %s %s", from, to);
            Runtime.getRuntime().exec(new String[] {"/bin/sh", "-c", cmd});
        }
        return JsonResult.success("csv文件移动成功");
    }
}

@Data
@ApiModel("文件目录")
class DirRequest
{
    @NotBlank(message = "文件目录不能为空")
    @ApiModelProperty(value = "文件目录", required = true)
    private String fileDirPath;
}

@Data
@ApiModel("csv文件信息")
class MoveRequest
{
    @NotBlank(message = "csv文件不能为空")
    @ApiModelProperty(value = "原始csv文件", required = true)
    private String csvPath;
    
    @NotBlank(message = "目标文件夹不能为空")
    @ApiModelProperty(value = "移动后的目标文件夹", required = true)
    private String destDirPath;
}