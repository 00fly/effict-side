package com.fly.hello.web.swagger;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

@Api(tags = "Knife演示接口")
@RestController
@RequestMapping("/knife")
public class KnifeController
{
    /**
     * @api {post} /knife/post/test post测试
     * @apiName post演示
     * @apiGroup 演示API
     * @apiParam {String} name="msg" 名称
     * @apiParam {String} [value] 取值
     * @apiSuccess {String} name 名称
     * @apiSuccess {String} value 取值
     * 
     */
    @ApiOperation("post测试")
    @PostMapping("/post/test")
    public OutPutBean test(InputBean input)
    {
        return new OutPutBean();
    }
    
    /**
     * @api {post} /knife/json/test json测试
     * @apiGroup 演示API
     * @apiBody {String} name="msg" 名称
     * @apiBody {String} [value] 取值
     * @apiSuccess {String} name 名称
     * @apiSuccess {String} value 取值
     * 
     */
    @ApiOperation("json测试")
    @PostMapping("/json/test")
    public OutPutBean jsonTest(@RequestBody InputBean input)
    {
        return new OutPutBean();
    }
}

@Data
@ApiModel("入参模型")
class InputBean
{
    @ApiModelProperty("名称")
    private String name;
    
    @ApiModelProperty("取值")
    private String value;
}

@Data
@ApiModel("出参模型")
class OutPutBean
{
    @ApiModelProperty("名称")
    private String name;
    
    @ApiModelProperty("取值")
    private String value;
}