package com.fly.hello.web.swagger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.LongStream;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fly.core.JsonResult;
import com.fly.hello.web.model.DataEntity;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * Swagger 注解演示
 * 
 * @author 00fly
 * @version [版本号, 2021年9月11日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Api(tags = "Swagger演示-api")
@RestController
@RequestMapping("/swagger")
@ConditionalOnWebApplication
public class RestDataController
{
    @Autowired
    HttpServletRequest request;
    
    /**
     * 新增
     */
    @ApiOperationSupport(order = 10)
    @ApiOperation("新增")
    @PostMapping("/insert")
    public JsonResult<?> insert(@Valid DataEntity entity)
    {
        return JsonResult.success();
    }
    
    /**
     * 新增
     */
    @ApiOperationSupport(order = 20)
    @ApiOperation("Json传参新增")
    @PostMapping("/insertByJson")
    public JsonResult<?> insertByJson(@Valid @RequestBody DataEntity entity)
    {
        return JsonResult.success();
    }
    
    /**
     * 将/insert3请求发送到此接口，演示map接受json传参
     * 
     * @param map
     * @return
     * @see [类、类#方法、类#成员]
     */
    @ApiOperationSupport(order = 30)
    @ApiOperation("map接受json传参")
    @PostMapping("/insert31")
    public JsonResult<?> insert31(@RequestBody Map<String, Object> map)
    {
        return JsonResult.success();
    }
    
    /**
     * 新增
     */
    @ApiOperationSupport(order = 40)
    @ApiOperation("Json数组传参新增")
    @PostMapping("/insert4")
    public JsonResult<?> insert4(@RequestBody List<DataEntity> entities)
    {
        return JsonResult.success();
    }
    
    /**
     * 删除
     */
    @ApiOperationSupport(order = 50)
    @ApiOperation("删除")
    @ApiImplicitParam(name = "id", value = "对象id", required = true, paramType = "path")
    // @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "对象id", required = true, paramType = "path")}) // 亦可
    @DeleteMapping("/delete/{id:\\d+}")
    public JsonResult<?> delete(@PathVariable Long id)
    {
        return JsonResult.success();
    }
    
    /**
     * Get查询
     */
    @ApiOperationSupport(order = 60)
    @ApiOperation("Get查询")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "对象id", required = true, paramType = "query")})
    @GetMapping("/query")
    public JsonResult<DataEntity> query(@RequestParam Long id)
    {
        DataEntity dataEntity = new DataEntity();
        dataEntity.setId(1L).setAge(18).setSex("F").setSalary(10000.00F).setGame1(99.999D).setGame2(new BigDecimal("65536.2356655"));
        return JsonResult.success(dataEntity);
    }
    
    /**
     * Path查询
     */
    @ApiOperationSupport(order = 70)
    @ApiOperation("Path查询")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "对象id", required = true, paramType = "path")})
    @GetMapping("/query/{id:\\d+}")
    public JsonResult<?> query1(@PathVariable Long id)
    {
        return JsonResult.success();
    }
    
    /**
     * 列表查询
     */
    @ApiOperationSupport(order = 80)
    @ApiOperation("列表查询")
    @GetMapping("/queryList")
    public List<DataEntity> queryList()
    {
        List<DataEntity> list = new ArrayList<>();
        LongStream.range(0, 5).forEach(i -> list.add(new DataEntity().setId(i).setName("jack" + i)));
        return list;
    }
}
