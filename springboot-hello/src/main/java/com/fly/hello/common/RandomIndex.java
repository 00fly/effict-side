package com.fly.hello.common;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.commons.lang3.RandomUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 随机索引<br>
 */
@Slf4j
public class RandomIndex
{
    int length;
    
    byte[] lock = new byte[0];
    
    Queue<Integer> quque = new ConcurrentLinkedQueue<>();
    
    /**
     * 实例化
     * 
     * @param length 总数据长度
     */
    public RandomIndex(int length)
    {
        super();
        this.length = length;
    }
    
    /**
     * 返回与最近数据不重复的随机索引值
     * 
     * @return
     */
    public int getIndex()
    {
        // 集中1次生成不超过20条数据，多次使用
        int ququeMaxSize = Math.min(length, 20);
        if (quque.size() < 3 || quque.size() < ququeMaxSize / 2)
        {
            synchronized (lock)
            {
                int add;
                while (quque.size() < ququeMaxSize)
                {
                    add = RandomUtils.nextInt(0, length);
                    if (!quque.contains(add))
                    {
                        quque.add(add);
                    }
                }
            }
            log.info("{}", quque);
        }
        int index = quque.poll();
        log.info("{} <= {}", index, quque);
        return index;
    }
}
