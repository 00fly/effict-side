package com.fly.hello.entity;

import java.io.Serializable;

import lombok.Data;

@Data
public class Article implements Serializable
{
    private static final long serialVersionUID = 6956152942848229905L;
    
    String title;
    
    String url;
    
    Long viewCount;
}
