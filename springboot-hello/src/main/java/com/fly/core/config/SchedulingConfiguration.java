package com.fly.core.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 当app.scheduling.enable设置为true时启用@EnableScheduling
 */
@ConditionalOnProperty(value = "app.scheduling.enable", havingValue = "true", matchIfMissing = false)
@Configuration
@EnableScheduling
public class SchedulingConfiguration
{
    
}