package com.fly.core.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Aspect
@Component
public class ShowAspect
{
    /**
     * com.fly.hello.job包及子包注解和使用OnceSchedule注解标注的类
     */
    @Pointcut("within(com.fly.hello.job..*) && @annotation(com.fly.core.annotation.OnceSchedule)")
    public void pointcut()
    {
    }
    
    /**
     * within：匹配指定类型中的方法
     */
    @Before("pointcut()")
    public void before()
    {
        log.info("=== before ===");
    }
    
    @Around("pointcut()")
    public Object around(ProceedingJoinPoint joinPoint)
        throws Throwable
    {
        log.info("------ around before ------");
        Object object = joinPoint.proceed();
        log.info("------ around after  ------\n");
        return object;
    }
}
