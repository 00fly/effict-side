package com.fly.core.utils.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.dataformat.javaprop.JavaPropsMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;

/**
 * JsonUtils 转换工具基类
 * 
 * @author 00fly
 *
 */
public abstract class BaseJackson
{
    protected static ObjectMapper objectMapper = new ObjectMapper();
    
    protected static JavaPropsMapper javaPropsMapper = new JavaPropsMapper();
    
    protected static JsonMapper jsonMapper = new JsonMapper();
    
    protected static XmlMapper xmlMapper = new XmlMapper();
    
    protected static YAMLMapper yamlMapper = new YAMLMapper();
}
