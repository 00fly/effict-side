package com.fly.core.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * HttpServletUtils
 */
public class HttpServletUtils
{
    /**
     * 获取HttpServletRequest
     * 
     * @return
     */
    public static HttpServletRequest getHttpServletRequest()
    {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes != null)
        {
            return servletRequestAttributes.getRequest();
        }
        return null;
    }
    
    /**
     * 获取HttpServletResponse
     * 
     * @return
     */
    public static HttpServletResponse getHttpServletResponse()
    {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes != null)
        {
            return servletRequestAttributes.getResponse();
        }
        return null;
    }
}
