package com.fly.core.utils.jackson;

import java.io.IOException;
import java.util.Properties;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Properties 转换工具
 * 
 * @author 00fly
 *
 */
public class PropertiesUtils extends BaseJackson
{
    /**
     * properties对象转Json字符串
     * 
     * @param properties对象
     * @return
     * @throws IOException
     */
    public static String propertiesToJson(Properties properties)
        throws IOException
    {
        JsonNode jsonNode = javaPropsMapper.readPropertiesAs(properties, JsonNode.class);
        return jsonNode.toPrettyString();
    }
    
    /**
     * properties对象转xml字符串
     * 
     * @param properties对象
     * @return
     * @throws IOException
     */
    public static String propertiesToXml(Properties properties)
        throws IOException
    {
        JsonNode jsonNode = javaPropsMapper.readPropertiesAs(properties, JsonNode.class);
        return xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode);
    }
    
    /**
     * properties对象转yaml
     * 
     * @param properties对象
     * @return
     * @throws IOException
     */
    public static String propertiesToYaml(Properties properties)
        throws IOException
    {
        JsonNode jsonNode = javaPropsMapper.readPropertiesAs(properties, JsonNode.class);
        return yamlMapper.writeValueAsString(jsonNode);
    }
    
    /**
     * properties字符串转json字符串
     * 
     * @param propText
     * @return
     * @throws IOException
     */
    public static String propTextToJson(String propText)
        throws IOException
    {
        JsonNode jsonNode = javaPropsMapper.readTree(propText);
        return jsonNode.toPrettyString();
    }
    
    /**
     * properties字符串转xml字符串
     * 
     * @param propText
     * @return
     * @throws IOException
     */
    public static String propTextToXml(String propText)
        throws IOException
    {
        JsonNode jsonNode = javaPropsMapper.readTree(propText);
        return xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode);
    }
    
    /**
     * properties字符串转yaml
     * 
     * @param propText
     * @return
     * @throws IOException
     */
    public static String propTextToYaml(String propText)
        throws IOException
    {
        JsonNode jsonNode = javaPropsMapper.readTree(propText);
        return yamlMapper.writeValueAsString(jsonNode);
    }
}
