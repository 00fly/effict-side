package com.fly.test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.fly.sample.bean.Dependency;
import com.fly.sample.utils.ReadXML;

/**
 * 
 * 单元测试
 * 
 * @author 00fly
 * @version [版本号, 2021年2月4日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class MainTest
{
    
    /**
     * 测试解析pom
     * 
     * @throws IOException
     * @see [类、类#方法、类#成员]
     */
    @Test
    public void test2()
        throws IOException
    {
        String path = MainTest.class.getResource("/pom-install.xml").getPath();
        String text = FileUtils.readFileToString(new File(path), StandardCharsets.UTF_8);
        List<Dependency> data = ReadXML.parse(text);
        String cmd;
        for (Dependency it : data)
        {
            // System.out.println(ToStringBuilder.reflectionToString(it,
            // ToStringStyle.MULTI_LINE_STYLE));
            cmd = String.format("mvn install:install-file -DgroupId=%s -DartifactId=%s -Dversion=%s  -Dfile=%s  -Dpackaging=jar -DgeneratePom=true",
                it.getGroupId(),
                it.getArtifactId(),
                it.getVersion(),
                StringUtils.substringAfter(it.getSystemPath(), "/lib/"));
            System.out.println(cmd);
        }
    }
    
}