package com.fly;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.fly.core.util.Executor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MainRun
{
    /**
     * 启动
     * 
     * @param args
     * @see [类、类#方法、类#成员]
     */
    public static void main(String[] args)
    {
        // ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        ScheduledExecutorService service = new ScheduledThreadPoolExecutor(1);
        
        String command = input();
        service.scheduleAtFixedRate(() -> {
            try
            {
                log.info(" ========>>>>> 即将执行命令：{}", command);
                Executor.execute(command);
            }
            catch (IOException e)
            {
                log.error(e.getMessage(), e);
            }
        }, 2, 10, TimeUnit.SECONDS);
    }
    
    /**
     * 获取输入命令
     * 
     * @return
     * @see [类、类#方法、类#成员]
     */
    private static String input()
    {
        try (Scanner sc = new Scanner(System.in))
        {
            do
            {
                log.info("请输入command！");
                String input = sc.nextLine();
                if (input != null && input.trim().length() > 0)
                {
                    return input;
                }
                log.info("请继续输入command！");
            } while (true);
        }
    }
}
