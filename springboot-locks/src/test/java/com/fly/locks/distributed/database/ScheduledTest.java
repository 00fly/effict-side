package com.fly.locks.distributed.database;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

import com.fly.LockBootApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest(classes = LockBootApplication.class)
public class ScheduledTest
{
    private String key = "job002";
    
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    ScheduledExecutorService executorService = Executors.newScheduledThreadPool(5);
    
    /**
     * 初始化
     */
    @BeforeEach
    public void init()
    {
        jdbcTemplate.update("DELETE FROM t_schedule_lock WHERE key_name=?", key);
        jdbcTemplate.update("INSERT INTO t_schedule_lock(key_name, time) VALUES(?, now())", key);
    }
    
    @Test
    public void test()
        throws InterruptedException
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                if (getLock(4L))
                {
                    log.info("#### hello world ***");
                }
                else
                {
                    log.info("没抢到，骂骂咧咧的走了...");
                }
            }
        };
        
        int i = 0;
        while (i++ < 5)
        {
            executorService.scheduleAtFixedRate(runnable, 2, 5, TimeUnit.SECONDS);
        }
        TimeUnit.SECONDS.sleep(300);
    }
    
    /**
     * 获取lock,并更新time为下次执行时间 now()+ seconds<BR>
     * 
     * 注意: 为了保证jdbcTemplate.update执行成功，一般设置seconds稍小于@Scheduled设置的运行间隔秒数
     * 
     * @return
     */
    private boolean getLock(long seconds)
    {
        return jdbcTemplate.update("UPDATE t_schedule_lock SET time = adddate(now(), INTERVAL ? SECOND) WHERE key_name = ? AND now() > time", seconds, key) > 0;
    }
}
