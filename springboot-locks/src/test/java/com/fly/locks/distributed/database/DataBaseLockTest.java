package com.fly.locks.distributed.database;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.fly.LockBootApplication;

/**
 * 
 * DataBaseLockTest
 * 
 * @author 00fly
 * @version [版本号, 2022年11月30日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@SpringBootTest(classes = LockBootApplication.class)
public class DataBaseLockTest
{
    @Test
    public void test()
    {
    }
}
