package com.fly.locks.thread.atomic;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.Test;

/**
 * 
 * AomicInteger计数器限流示例
 * 
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class AtomicIntegerTest
{
    /**
     * 线程池方式测试
     * 
     * @throws InterruptedException
     * @see [类、类#方法、类#成员]
     */
    @Test
    public void test()
        throws InterruptedException
    {
        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
        Runnable runnable = new AtomicIntegerThread();
        while (true)
        {
            TimeUnit.MILLISECONDS.sleep(RandomUtils.nextInt(100, 1000));
            cachedThreadPool.execute(runnable);
        }
    }
}
