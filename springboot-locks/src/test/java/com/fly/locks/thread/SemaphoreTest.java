package com.fly.locks.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

import com.fly.locks.thread.semaphore.SemaphoreThread;

public class SemaphoreTest
{
    ExecutorService fixExecutorService = Executors.newFixedThreadPool(5);
    
    @Test
    public void test()
        throws InterruptedException
    {
        Runnable runnable = new SemaphoreThread();
        IntStream.range(0, 10).forEach(num -> fixExecutorService.execute(runnable));
        TimeUnit.SECONDS.sleep(60);
    }
}
