package com.fly.locks.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

import com.fly.locks.thread.sync.SynCodeAreaThread;
import com.fly.locks.thread.sync.SynMethodThread;
import com.fly.locks.thread.sync.SynObjThread;

/**
 * 
 * synchronized线程锁测试
 * 
 * @author 00fly
 */
public class SynThreadLockTest
{
    ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
    
    @Test
    public void testSynObjThread()
    {
        Runnable runnable = new SynObjThread();
        IntStream.range(0, 100).forEach(num -> cachedThreadPool.execute(runnable));
    }
    
    @Test
    public void testSynMethodThread()
    {
        Runnable runnable = new SynMethodThread();
        IntStream.range(0, 100).forEach(num -> cachedThreadPool.execute(runnable));
    }
    
    @Test
    public void testSynCodeAreaThread()
    {
        Runnable runnable = new SynCodeAreaThread();
        IntStream.range(0, 100).forEach(num -> cachedThreadPool.execute(runnable));
    }
}
