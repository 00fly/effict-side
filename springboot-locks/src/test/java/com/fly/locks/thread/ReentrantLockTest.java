package com.fly.locks.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

import com.fly.locks.thread.reentrant.ReentrantLockThread;

/**
 * 
 * ReentrantLock线程锁测试
 * 
 * @author 00fly
 */
public class ReentrantLockTest
{
    ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
    
    @Test
    public void testReentrantLockThread()
    {
        Runnable runnable = new ReentrantLockThread();
        IntStream.range(0, 100).forEach(num -> cachedThreadPool.execute(runnable));
    }
}
