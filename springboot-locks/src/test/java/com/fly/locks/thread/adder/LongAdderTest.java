package com.fly.locks.thread.adder;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.Test;

/**
 * 压测LongAdder的原子操作性能
 **/
public class LongAdderTest
{
    /**
     * 线程池方式测试
     * 
     * @throws InterruptedException
     * @see [类、类#方法、类#成员]
     */
    @Test
    public void test()
        throws InterruptedException
    {
        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
        Runnable runnable = new LongAdderThread();
        while (true)
        {
            TimeUnit.MILLISECONDS.sleep(RandomUtils.nextInt(100, 1000));
            cachedThreadPool.execute(runnable);
        }
    }
}