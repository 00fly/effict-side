package com.fly.show.job;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnNotWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * SimpleJob
 * 
 * @author 00fly
 * @version [版本号, 2022年11月30日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@Component
@Configuration
@ConditionalOnNotWebApplication
public class SimpleJob
{
    /**
     * 无默认值会抛出IllegalArgumentException
     */
    @Value("${welcome.message:hello, 00fly in java!}")
    private String welcome;
    
    /**
     * 7-18点每5分钟执行<br>
     * 
     */
    @Scheduled(cron = "0 0/5 7-18 * * ?")
    public void run1()
    {
        try
        {
            TimeUnit.SECONDS.sleep(5); // 注意这儿的休眠时间
            log.info("---- {}", welcome);
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @Bean
    public ScheduledExecutorService scheduledExecutorService()
    {
        // return Executors.newScheduledThreadPool(10);
        return new ScheduledThreadPoolExecutor(10, new CustomizableThreadFactory("schedule-pool-"));
    }
}
