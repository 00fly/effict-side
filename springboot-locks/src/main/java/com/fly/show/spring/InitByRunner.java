package com.fly.show.spring;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.fly.core.util.SpringContextUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class InitByRunner implements CommandLineRunner, ApplicationRunner
{
    @Override
    public void run(String... args)
        throws Exception
    {
        String welcome = "implements CommandLineRunner, profile: " + SpringContextUtils.getActiveProfile();
        log.info("---- {}", welcome);
    }
    
    @Override
    public void run(ApplicationArguments args)
        throws Exception
    {
        String welcome = "implements ApplicationRunner, profile: " + SpringContextUtils.getActiveProfile();
        log.info("---- {}", welcome);
    }
}
