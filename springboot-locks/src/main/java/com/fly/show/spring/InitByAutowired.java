package com.fly.show.spring;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.fly.core.util.SpringContextUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class InitByAutowired
{
    @Autowired
    ApplicationContext context;
    
    /**
     * PostConstruct注解的方法将会在依赖注入完成后被自动调用<BR>
     * Constructor >> Autowired >> PostConstruct
     */
    @PostConstruct
    private void init()
    {
        String welcome = "@Autowired ApplicationContext, profile: " + SpringContextUtils.getActiveProfile(context);
        log.info("---- {}", welcome);
    }
}
