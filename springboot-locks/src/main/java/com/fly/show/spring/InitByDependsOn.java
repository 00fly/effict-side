package com.fly.show.spring;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.fly.core.util.SpringContextUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@DependsOn("springContextUtils")
public class InitByDependsOn
{
    /**
     * PostConstruct注解的方法将会在依赖注入完成后被自动调用<BR>
     * Constructor >> Autowired >> PostConstruct
     */
    @PostConstruct
    private void init()
    {
        String welcome = "@DependsOn(\"springContextUtils\"), profile: " + SpringContextUtils.getActiveProfile();
        log.info("---- {}", welcome);
    }
}
