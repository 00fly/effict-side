package com.fly.core.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:jdbc-mysql.properties")
public class DataSourcePoolConfig
{
}
