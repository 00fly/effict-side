package com.fly.locks.thread.sync;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SynCodeAreaThread implements Runnable
{
    static int num = 100;
    
    /**
     * synchronized修饰同步代码块
     */
    @Override
    public void run()
    {
        synchronized (this)
        {
            if (num > 0)
            {
                log.info("{} ==> num: {}", Thread.currentThread().getName(), num);
                num--;
            }
        }
    }
}
