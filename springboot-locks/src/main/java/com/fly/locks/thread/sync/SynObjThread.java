package com.fly.locks.thread.sync;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SynObjThread implements Runnable
{
    static int num = 100;
    
    /**
     * lock零长度的byte数组对象创建起来将比任何对象都经济
     */
    private byte[] lock = new byte[0];
    
    /**
     * synchronized修饰对象
     */
    @Override
    public void run()
    {
        synchronized (lock)
        {
            if (num > 0)
            {
                log.info("{} ==> num: {}", Thread.currentThread().getName(), num);
                num--;
            }
        }
    }
}
