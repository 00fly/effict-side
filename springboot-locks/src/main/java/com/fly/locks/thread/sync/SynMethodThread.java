package com.fly.locks.thread.sync;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SynMethodThread implements Runnable
{
    static int num = 100;
    
    @Override
    public void run()
    {
        sell();
    }
    
    /**
     * synchronized修饰方法
     * 
     * @see [类、类#方法、类#成员]
     */
    private synchronized void sell()
    {
        if (num > 0)
        {
            log.info("{} ==> num: {}", Thread.currentThread().getName(), num);
            num--;
        }
    }
}
