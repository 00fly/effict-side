package com.fly.locks.thread.reentrant;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import lombok.extern.slf4j.Slf4j;

/**
 * 公平锁的实现就是谁等待时间最长，谁就先获取锁<br>
 * 非公平锁就是随机获取<br>
 * ReentrantLock给定参数为（true）则为公平锁
 * 
 * @author 00fly
 * @version [版本号, 2022年12月16日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
public class ReentrantLockThread implements Runnable
{
    static int num = 100;
    
    Lock lock = new ReentrantLock();
    
    @Override
    public void run()
    {
        lock.lock();
        try
        {
            if (num > 0)
            {
                log.info("{} ==> num: {}", Thread.currentThread().getName(), num);
                num--;
            }
        }
        finally
        {
            lock.unlock();
        }
    }
}
