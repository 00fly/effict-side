package com.fly.locks.distributed.memcache;

import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.danga.MemCached.MemCachedClient;
import com.danga.MemCached.SockIOPool;

/**
 * 
 * MemCached 分布式锁演示
 * 
 * @author 00fly
 * @version [版本号, 2021年8月12日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Component
public class MemcachedLockClient
{
    @Value("${memcached.serverUrl:127.0.0.1:11211}")
    private String server;
    
    /**
     * 构建缓存客户端
     */
    private MemCachedClient cachedClient;
    
    @PostConstruct
    public void init()
    {
        cachedClient = new MemCachedClient();
        // 获取连接池实例
        SockIOPool pool = SockIOPool.getInstance();
        // 设置缓存服务器地址，可以设置多个实现分布式缓存
        pool.setServers(new String[] {server});
        // 设置初始连接5
        pool.setInitConn(5);
        // 设置最小连接5
        pool.setMinConn(5);
        // 设置最大连接250
        pool.setMaxConn(250);
        // 设置每个连接最大空闲时间3个小时
        pool.setMaxIdle(1000 * 60 * 60 * 3);
        // 设置连接池维护线程的睡眠时间
        // 设置为0，维护线程不启动
        // 维护线程主要通过log输出socket的运行状况，监测连接数目及空闲等待时间等参数以控制连接创建和关闭。
        pool.setMaintSleep(30);
        // 设置是否使用Nagle算法，因为我们的通讯数据量通常都比较大（相对TCP控制数据）而且要求响应及时，因此该值需要设置为false（默认是true）
        pool.setNagle(false);
        // 设置socket的读取等待超时值
        pool.setSocketTO(3000);
        // 设置socket的连接等待超时值
        pool.setSocketConnectTO(0);
        // 设置完pool参数后最后调用该方法，启动pool。
        pool.initialize();
    }
    
    public void add(String key, Object value)
    {
        cachedClient.set(key, value);
    }
    
    public boolean add(String key, Object value, int milliseconds)
    {
        return cachedClient.add(key, value, milliseconds);
    }
    
    public boolean add(String key, Object value, Date milliseconds)
    {
        return cachedClient.add(key, value, milliseconds);
    }
    
    public void remove(String key)
    {
        cachedClient.delete(key);
    }
    
    public Object get(String key)
    {
        return cachedClient.get(key);
    }
}
