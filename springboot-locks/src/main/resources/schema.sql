CREATE TABLE IF NOT EXISTS t_schedule_lock (
`id`  bigint(20) NOT NULL AUTO_INCREMENT ,
`key_name`  varchar(20) NOT NULL ,
`time`  datetime NOT NULL ,
PRIMARY KEY (id)
);