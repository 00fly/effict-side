
#普通JAVA工程中，运行class文件/运行jar文件中的class

```
java com.fly.schedule.simple.MainRun

java -cp schedule-1.0.0.jar com.fly.schedule.simple.MainRun

```

不幸的是，以上方法窗口关闭就会杀死进程。


##### 现改造为使用 nohup 后台执行命令：

因为nohup会忽略输入输出，需要将 String command = input(); 改为某命令, 如：  String command = "ls";

```
nohup java -cp schedule-1.0.0.jar com.fly.schedule.simple.MainRun &
```

#Springboot工程中，Jar运行时指定Start-Class类

```
java -cp schedule-springboot.jar -Dloader.main=com.fly.schedule.simple.MainRun org.springframework.boot.loader.PropertiesLauncher
```

