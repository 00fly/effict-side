package com.fly.memcache.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;

import io.swagger.v3.oas.annotations.Operation;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * knife4j
 *
 * @author jack
 */
@EnableKnife4j
@EnableOpenApi
@Configuration
@ConditionalOnWebApplication
@Import(BeanValidatorPluginsConfiguration.class)
public class Knife4jConfig
{
    @Value("${knife4j.enable: true}")
    private boolean enable;
    
    @Bean
    public Docket defaultApi3()
    {
        return new Docket(DocumentationType.OAS_30).enable(enable).apiInfo(apiInfo()).groupName("0.1版").select().apis(RequestHandlerSelectors.withMethodAnnotation(Operation.class)).paths(PathSelectors.any()).build();
    }
    
    private ApiInfo apiInfo()
    {
        return new ApiInfoBuilder().title("Swagger3接口文档").description("如有疑问，请联系开发。").termsOfServiceUrl("http://00fly.online/").contact(new Contact("00fly", "https://www.00fly.online/", "00fly@qq.com")).version("1.0").build();
    }
}
