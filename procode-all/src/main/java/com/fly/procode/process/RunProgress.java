package com.fly.procode.process;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;

import com.fly.procode.core.Service;

/**
 * 
 * 创建代码进度条线程
 * 
 * @author 00fly
 * @version [版本号, 2017年5月3日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class RunProgress implements IRunnableWithProgress, Observer
{
    private String sourcePath;
    
    private String bakFileName;
    
    private List<String> fileFilter;
    
    private String pwdtext;
    
    private IProgressMonitor monitor;
    
    public RunProgress(String bootPath, String bakFileName, List<String> fileFilter, String pwdtext)
    {
        super();
        this.sourcePath = bootPath;
        this.bakFileName = bakFileName;
        this.fileFilter = fileFilter;
        this.pwdtext = pwdtext;
    }
    
    @Override
    public void run(IProgressMonitor monitor)
        throws InvocationTargetException, InterruptedException
    {
        // 在当前目录，创建并运行脚本
        try
        {
            monitor.beginTask("代码备份", IProgressMonitor.UNKNOWN);
            monitor.subTask("代码备份中......");
            creatAndRun(sourcePath, bakFileName, fileFilter, pwdtext, monitor);
            monitor.done();
        }
        catch (Exception e)
        {
            throw new InvocationTargetException(e.getCause(), e.getMessage());
        }
    }
    
    // 运行代码创建程序
    private void creatAndRun(String sourcePath, String bakFileName, List<String> fileFilter, String pwdtext, IProgressMonitor monitor)
    {
        this.monitor = monitor;
        Service service = new Service();
        service.setRunProcess(this);
        service.addObserver(this);
        service.setSourcePath(sourcePath);
        service.createBakFile(sourcePath, bakFileName, fileFilter, pwdtext);
    }
    
    @Override
    public void update(Observable observable, Object msg)
    {
        if (msg instanceof String)
        {
            monitor.subTask((String)msg);
        }
    }
}