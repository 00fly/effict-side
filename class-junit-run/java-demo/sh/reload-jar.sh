#!/bin/bash
# get pid

pname="java-demo-0.0.1.jar"
echo  -e  "jar-name=$pname\r\n"


get_pid(){
 pid=`ps -ef | grep $pname | grep -v grep | awk '{print $2}'`
 echo "$pid"
}

ps -ef|grep $pname

PID=$(get_pid)
if [ -z "${PID}" ] 
then
 echo -e "\r\nJava Application already stop!"
else
 echo -e '\r\nkill -9  '${PID} '\r\n'
 kill -9 ${PID}
 echo -e "Java Application is stop!"
fi

rm -rf info.log

echo -e "\r\nJava Application will startup!\r\n"
jar_path=`find . -maxdepth 1 -name $pname`

#echo "jarfile=$jar_path"

nohup java -jar $jar_path --server.port=8181 >>./info.log 2>&1 &

ps -ef|grep $pname