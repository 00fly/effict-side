package com.fly.tcp.run;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import com.fly.tcp.bio.TcpServer;

public class StartServer
{
    public static void main(String[] args)
    {
        TcpServer server = new TcpServer();
        if (server.startServer("0.0.0.0", 8000))
        {
            Executors.newScheduledThreadPool(2).scheduleAtFixedRate(() -> {
                int index = RandomUtils.nextInt(1, 4);
                server.sendMsg("CLIENT_" + index, "random: " + RandomStringUtils.randomAlphanumeric(10));
            }, 10, 60, TimeUnit.SECONDS);
            new Thread(server).start();
        }
    }
}
