package com.fly.main;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Main1
{
    public static void main(String[] args)
    {
        Executors.newScheduledThreadPool(2).scheduleAtFixedRate(() -> log.info(StringUtils.repeat("hello!", 10)), 5, 10, TimeUnit.SECONDS);
    }
}
