# java工程


## 一、lunix调试
**注意分割符lunix:windows;**

查看目录结构 `tree -L 1`

```bash
├── classes
├── lib
└── test-classes
```


### 1. class文件运行

**支持指定MainClass**

```bash
#!bin/bash
cd classes
java -cp .:../lib/* com.fly.main.Main1
java -cp .:../lib/* com.fly.main.Main2

```


### 2. junitTest 运行

**支持指定Class**

```bash
#!bin/bash
cd test-classes
java -cp .:../lib/* org.junit.runner.JUnitCore com.fly.test.TestCase1
java -cp .:../lib/* org.junit.runner.JUnitCore com.fly.test.TestCase2

#可以指定多个
java -cp .:../lib/* org.junit.runner.JUnitCore com.fly.test.TestCase1 com.fly.test.TestCase2

```