package com.fly.test;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestCase2
{
    @Test
    public void test1()
    {
        log.info("{}", StringUtils.repeat("hello!", 10));
    }
    
    @Test
    public void test2()
    {
        log.info("{}", StringUtils.repeat("world!", 10));
    }
}
