package com.fly;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest
@WebAppConfiguration
public class ApplicationContextTest
{
    @Autowired
    WebApplicationContext wac;
   
    @Test
    public void setup()
    {
        log.info("★★★★★★★★ WebApplicationContext = {}", wac);
        int i = 1;
        for (String beanName : wac.getBeanDefinitionNames())
        {
            log.info("{}.\t{}", i++, beanName);
        }
    }
}
