package com.fly.main;

import org.apache.commons.lang3.StringUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Main2
{
    public static void main(String[] args)
    {
        log.info("{}", StringUtils.repeat("world!", 10));
    }
}
