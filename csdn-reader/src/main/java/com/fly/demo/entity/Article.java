package com.fly.demo.entity;

import lombok.Data;

@Data
public class Article
{
    String title;
    
    String url;
    
    Long viewCount;
}
