package com.fly.demo.entity;

import java.util.List;

import lombok.Data;

@Data
public class Record
{
    private List<Article> list;
}
