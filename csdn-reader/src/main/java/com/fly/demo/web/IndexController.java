package com.fly.demo.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import com.fly.demo.sse.SSEServer;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@Api(tags = "SSE接口")
public class IndexController
{
    @CrossOrigin
    @ApiOperation("SSE初始化")
    @GetMapping("/sse/connect/{userId}")
    public SseEmitter connect(@PathVariable String userId)
    {
        return SSEServer.connect();
    }
}
