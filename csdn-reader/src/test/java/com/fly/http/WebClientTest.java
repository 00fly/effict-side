package com.fly.http;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.UrlResource;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import com.fly.core.utils.JsonBeanUtils;
import com.fly.demo.entity.Article;
import com.fly.demo.entity.BlogData;

import lombok.extern.slf4j.Slf4j;

/**
 * https://blog.csdn.net/zzhongcy/article/details/105412842
 * 
 */
@Slf4j
@DisplayName("WebClient测试")
public class WebClientTest
{
    private static WebClient webClient;
    
    String url = "https://00fly.online/upload/data.json";
    
    @BeforeAll
    public static void init()
    {
        // 默认底层使用Netty
        log.info("--- init ---");
        webClient = WebClient.builder().codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(-1)).build();
        
        // 内置支持Jetty反应性HttpClient实现，手工编码指定
        // webClient = WebClient.builder().baseUrl("https://blog.csdn.net").clientConnector(new JettyClientHttpConnector()).build();
    }
    
    @Test
    @DisplayName("getUrls请求")
    public void testGetUrls()
        throws IOException
    {
        AtomicInteger count = new AtomicInteger(0);
        getBlogData().getData().getList().stream().map(Article::getTitle).forEach(title -> log.info("{}. {}", count.incrementAndGet(), title));
    }
    
    private BlogData getBlogData()
        throws IOException
    {
        log.info("★★★★★★★★ getData from webApi ★★★★★★★★");
        String resp = webClient.get().uri(url).acceptCharset(StandardCharsets.UTF_8).accept(MediaType.APPLICATION_JSON).retrieve().bodyToMono(String.class).block();
        return JsonBeanUtils.jsonToBean(resp, BlogData.class, true);
    }
    
    @Test
    public void getData()
        throws MalformedURLException, IOException
    {
        try (InputStream inputStream = new UrlResource(url).getInputStream())
        {
            String content = IOUtils.toString(inputStream, Charset.defaultCharset());
            log.info(content);
        }
        
        String content1 = IOUtils.toString(new URL(url), Charset.defaultCharset());
        log.info(content1);
    }
}
