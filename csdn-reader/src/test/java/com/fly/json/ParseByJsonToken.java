package com.fly.json;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingJsonFactory;

/**
 * 每个json数组中包含的json对象太多，导致用流和按行读取时加载到内存会导致内存溢出。<br>
 * <br>
 * https://blog.csdn.net/qq_40280582/article/details/121886428
 */
public class ParseByJsonToken
{
    /**
     * 代码中使用流和树模型解析的组合读取此文件。 <BR>
     * 每个单独的记录都以树形结构读取，但文件永远不会完整地读入内存，因此JVM内存不会爆炸。
     * 
     * @throws IOException
     */
    @Test
    public void test()
        throws IOException
    {
        JsonFactory f = new MappingJsonFactory();
        JsonParser jp = f.createParser(new ClassPathResource("data.json").getURL());
        JsonToken current = jp.nextToken();
        if (current != JsonToken.START_OBJECT)
        {
            System.out.println("Error: root should be object: quiting.");
            return;
        }
        while (jp.nextToken() != JsonToken.END_OBJECT)
        {
            String fieldName = jp.getCurrentName();
            current = jp.nextToken();
            if ("data".equals(fieldName))
            {
                if (current == JsonToken.START_OBJECT)
                {
                    while (jp.nextToken() != JsonToken.END_OBJECT)
                    {
                        JsonNode node = jp.readValueAsTree();
                        System.out.println(node);
                    }
                }
                else if (current == JsonToken.START_ARRAY)
                {
                    while (jp.nextToken() != JsonToken.END_ARRAY)
                    {
                        JsonNode node = jp.readValueAsTree();
                        System.out.println("field1: " + node.get("title").asText());
                        System.out.println("field2: " + node.get("url").asText());
                    }
                }
                else
                {
                    System.err.println("Error: records should be an array: skipping.");
                    jp.skipChildren();
                }
            }
            else
            {
                System.out.println("Unprocessed property: " + fieldName);
                jp.skipChildren();
            }
        }
    }
}
