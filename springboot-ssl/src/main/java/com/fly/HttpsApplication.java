package com.fly;

import javax.servlet.ServletContext;

import org.apache.commons.lang3.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
public class HttpsApplication implements CommandLineRunner
{
    @Value("${server.port}")
    Integer port;
    
    @Autowired
    ServletContext servletContext;
    
    public static void main(String[] args)
    {
        SpringApplication.run(HttpsApplication.class, args);
    }
    
    @Override
    public void run(String... args)
        throws Exception
    {
        if (SystemUtils.IS_OS_WINDOWS && port > 0)
        {
            log.info("★★★★★★★★  now open Browser ★★★★★★★★ ");
            log.info("请修改hosts: 127.0.0.1 test.00fly.online");
            String url = "https://test.00fly.online:" + port + servletContext.getContextPath();
            Runtime.getRuntime().exec("cmd /c start " + url);
        }
    }
}