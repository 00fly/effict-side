package com.fly.web;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;

import org.apache.commons.lang3.RandomUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.http.MediaType;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * RestPicController
 * 
 * @author 00fly
 * @version [版本号, 2021年9月28日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@RestController
@RequestMapping("/show")
@ConditionalOnWebApplication
public class RestPicController
{
    byte[] lock = new byte[0];
    
    Resource[] resources = {};
    
    Queue<Integer> quque = new ConcurrentLinkedQueue<>();
    
    @PostConstruct
    private void init()
    {
        try
        {
            resources = new PathMatchingResourcePatternResolver().getResources(ResourceUtils.CLASSPATH_URL_PREFIX + "data/pic/**/*.jpg");
            if (resources.length < 4)
            {
                log.error("############### 请在[resources/data/pic/]目录放入不少于4张jpg图片 ###############");
            }
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    @GetMapping(value = {"/girl", "/pic"}, produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] showPic()
        throws IOException
    {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(createImage(), "jpg", os);
        return os.toByteArray();
    }
    
    /**
     * createImage 生成图片
     * 
     * @return
     * @throws IOException
     * @see [类、类#方法、类#成员]
     */
    private BufferedImage createImage()
        throws IOException
    {
        if (resources.length < 4)
        {
            log.info("############### 请在[resources/data/pic/]目录放入不少于4张jpg图片 ###############");
            return new BufferedImage(400, 400, BufferedImage.TYPE_BYTE_GRAY);
        }
        // 取图片
        int index = getIndex();
        return ImageIO.read(resources[index].getInputStream());
    }
    
    private int getIndex()
    {
        int index;
        if (quque.size() < 3)
        {
            synchronized (lock)
            {
                // 集中1次生成，多次使用
                int max = resources.length;
                while (quque.size() < max)
                {
                    index = RandomUtils.nextInt(0, max);
                    if (!quque.contains(index))
                    {
                        quque.add(index);
                    }
                }
            }
            log.info("{}", quque);
        }
        index = quque.poll();
        log.info("{} <= {} , ", index, quque);
        return index;
    }
}
