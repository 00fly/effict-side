package com.fly.base;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FileMoveTest
{
    Scanner sc;
    
    Collection<File> pics;
    
    /**
     * 移动文件
     * 
     * @param path
     * @throws IOException
     */
    private void moveFile(String path)
        throws IOException
    {
        String to = new File("/").getCanonicalPath();
        log.info("【即将移动文件】 {} ===> {}", path, to);
        if (SystemUtils.IS_OS_WINDOWS)
        {
            String cmd = String.format("cmd /c copy %s %s", path, to);
            log.info(cmd);
            Runtime.getRuntime().exec(cmd);
            Runtime.getRuntime().exec("cmd /c start " + to);
        }
        if (SystemUtils.IS_OS_UNIX)
        {
            String cmd = String.format("cp %s %s", path, to);
            Runtime.getRuntime().exec(new String[] {"/bin/sh", "-c", cmd});
        }
    }
    
    public void selectFile(String srcDir)
        throws IOException
    {
        // 获取目录下或者子目录下的文件，文件类型可以指定
        File file = new File(srcDir);
        pics = FileUtils.listFiles(file, new String[] {"jpg"}, true);
        int index = 1;
        for (File it : pics)
        {
            log.info("{}. {}", index++, it.getCanonicalPath());
        }
        
        // 选择文件
        log.info("请输入序号【1-{}】选择文件", --index);
        int input = NumberUtils.toInt(sc.nextLine());
        if (input > 0 && input <= pics.size())
        {
            String path = pics.toArray(new File[] {})[input - 1].getCanonicalPath();
            log.info("你选择了文件{}：{} ", input, path);
            moveFile(path);
        }
        else
        {
            log.info("你未选中任何文件");
        }
        log.info("########## 当前文件处理完成 ##########\n");
    }
    
    /**
     * getInput
     * 
     * @param msg 提示语
     * @return
     * @see [类、类#方法、类#成员]
     */
    private String getInput(String msg)
    {
        String value;
        do
        {
            log.info("请输入{}", msg);
            value = sc.nextLine();
        } while (StringUtils.isEmpty(value));
        log.info("你输入了{}：{}", msg, value);
        return value;
    }
    
    @Test
    public void test()
        throws Exception
    {
        String input;
        sc = new Scanner(System.in);
        String fileRoot = getInput("文件目录");
        do
        {
            selectFile(fileRoot);
            log.info("------------输入q退出,输入其他值继续------------");
            input = StringUtils.trimToEmpty(sc.nextLine());
        } while (!input.equalsIgnoreCase("q"));
        log.info("----------系统退出成功----------");
        sc.close();
    }
}
