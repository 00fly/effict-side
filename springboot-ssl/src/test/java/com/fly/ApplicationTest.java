package com.fly;

import java.util.Scanner;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest(classes = HttpsApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class ApplicationTest
{
    /**
     * 注意：测试定时任务时，请勿使用dev环境，因为每个应用访问的是自己的内存数据库
     */
    @Test
    public void test()
    {
        try (Scanner sc = new Scanner(System.in))
        {
            do
            {
                log.info("------------输入x退出,回车换行继续------------");
            } while (!"x".equalsIgnoreCase(sc.nextLine()));
            log.info("------------成功退出------------");
        }
    }
}
