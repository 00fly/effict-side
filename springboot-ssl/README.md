
# pem带key转pfx

执行需要输入密码，值配置在key-store-password

```shell
openssl pkcs12 -export -out test.00fly.online.pfx -inkey private.key -in full_chain.pem
```

配置ssl支持

```yaml
server:
  port: 443
  ssl:
    key-store: classpath:data/ssl/test.00fly.online.pfx
    key-store-password: 12345asdfg
    keyStoreType: PKCS12
```