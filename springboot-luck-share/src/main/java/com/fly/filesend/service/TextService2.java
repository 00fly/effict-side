package com.fly.filesend.service;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListMap;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.stereotype.Service;

@Service
public class TextService2
{
    Map<String, String> map = new ConcurrentSkipListMap<>();
    
    /**
     * 时间戳keys
     * 
     * @return
     */
    public Set<String> getKeys()
    {
        return map.keySet();
    }
    
    /**
     * 获取值
     * 
     * @param key
     * @return
     */
    public String getText(String key)
    {
        return StringUtils.trimToEmpty(map.get(key));
    }
    
    public void setText(String text)
    {
        if (StringUtils.isNotBlank(text))
        {
            String key = DateFormatUtils.format(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss.SSS");
            map.put(key, text);
            while (map.size() > 10)// 保留10次历史
            {
                String firstKey = map.keySet().stream().findFirst().get();
                map.remove(firstKey);
            }
        }
    }
}
