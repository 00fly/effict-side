package com.fly.filesend.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class TextService
{
    private String text;
    
    public String getText()
    {
        return StringUtils.trimToEmpty(text);
    }
    
    public void setText(String text)
    {
        this.text = text;
    }
}
