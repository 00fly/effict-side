package com.fly.filesend.controller.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.http.MediaType;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fly.core.exception.ValidateException;
import com.fly.filesend.entity.JsonResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags = "文件上传、下载接口")
@RestController
@RequestMapping("/rest/file")
public class RestFileController
{
    File root = new File("upload");
    
    @ApiOperation("文件下载, index取值 [0, files.length)")
    @ApiImplicitParam(name = "index", value = "文件索引,起始值0", required = true, allowableValues = "0,1,2,3,4,5,6,7,8,9,10")
    @GetMapping(value = "/down/{index}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public void down(@PathVariable int index, HttpServletResponse response)
        throws IOException
    {
        List<File> files = FileUtils.listFiles(root, null, true).stream().filter(f -> f.isFile()).sorted(Comparator.comparing(File::getAbsolutePath)).collect(Collectors.toList());
        if (index >= 0 && index < files.size())
        {
            File file = files.get(index);
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(file.getName(), StandardCharsets.UTF_8.name()));
            response.setHeader("Cache-Control", "no-store, no-cache");
            FileCopyUtils.copy(new FileInputStream(file), response.getOutputStream());
        }
    }
    
    @ApiOperation("文件搜索")
    @PostMapping("/list")
    public JsonResult<?> list()
    {
        if (!root.exists() || !root.isDirectory())
        {
            return JsonResult.error("文件目录不存在");
        }
        // 检索文件路径排序
        List<String> paths = FileUtils.listFiles(root, null, true).stream().filter(f -> f.isFile()).map(f -> f.getPath()).sorted().collect(Collectors.toList());
        return JsonResult.success(paths);
    }
    
    @ApiOperation("文件批量上传处理")
    @PostMapping("/upload")
    public JsonResult<?> upload(MultipartFile[] files)
        throws IOException
    {
        if (files == null || files.length == 0)
        {
            throw new ValidateException("文件不能为空");
        }
        String date = DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMdd");
        String dir = root.getCanonicalPath() + File.separator + date + File.separator;
        new File(dir).mkdirs();
        
        // 保存文件
        Arrays.stream(files).filter(file -> StringUtils.isNotBlank(file.getOriginalFilename())).forEach(file -> {
            try
            {
                File newFile = new File(dir + file.getOriginalFilename());
                FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(newFile));
                log.info("###### file upload to: {}", dir);
            }
            catch (IOException e)
            {
                log.error(e.getMessage(), e);
            }
        });
        if (SystemUtils.IS_OS_WINDOWS)
        {
            Runtime.getRuntime().exec("cmd /c start " + dir);
        }
        return JsonResult.success("文件上传成功，保存目录：" + dir);
    }
}
