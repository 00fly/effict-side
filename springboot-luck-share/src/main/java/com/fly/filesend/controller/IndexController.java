package com.fly.filesend.controller;

import java.io.File;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.fly.filesend.service.TextService;
import com.fly.filesend.service.TokenService;

@Controller
public class IndexController
{
    @Autowired
    HttpSession httpSession;
    
    File root = new File("upload");
    
    @Autowired
    TokenService tokenService;
    
    @Autowired
    TextService textService;
    
    @GetMapping({"/", "/index"})
    public String index(Model model)
    {
        String token = (String)httpSession.getAttribute("token");
        if (tokenService.valide(token))
        {
            model.addAttribute("text", textService.getText());
            model.addAttribute("isLogin", true);
            model.addAttribute("sysTokenTime", tokenService.getTokenTime());
            if (root.exists())
            {
                List<File> files = FileUtils.listFiles(root, null, true).stream().filter(f -> f.isFile()).sorted(Comparator.comparing(File::getAbsolutePath)).collect(Collectors.toList());
                model.addAttribute("files", files);
                model.addAttribute("size", FileUtils.byteCountToDisplaySize(FileUtils.sizeOfDirectory(root)));
            }
        }
        return "index";
    }
    
    @PostMapping("/login")
    public String login(String token)
    {
        if (tokenService.valide(token))
        {
            httpSession.setAttribute("token", token);
        }
        return "redirect:/index";
    }
    
    @GetMapping("/logout")
    public String logout()
    {
        httpSession.invalidate();
        return "redirect:/index";
    }
}
