package com.fly.filesend.controller.file;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fly.filesend.service.TextService;

@Controller
@RequestMapping("/text")
public class TextController
{
    @Autowired
    TextService textService;
    
    @PostMapping("/submit")
    public String upload(String text)
    {
        textService.setText(text);
        return "redirect:/index";
    }
}
