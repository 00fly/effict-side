package com.fly.demo.ui.base;

import java.awt.Dimension;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;

import org.apache.commons.lang3.RandomUtils;

public class ImageDialog extends JDialog
{
    private static final long serialVersionUID = -4922215816711409008L;
    
    private boolean hidden = true;
    
    public ImageDialog(BufferedImage image)
    {
        super();
        if (hidden) // 隐藏标题
        {
            setUndecorated(true);
            setSize(image.getWidth(), image.getHeight());
        }
        else
        {
            setTitle("图片");
            setSize(image.getWidth(), image.getHeight() + 30);
        }
        Dimension screenSize = getToolkit().getScreenSize();
        Dimension dialogSize = getSize();
        dialogSize.height = Math.min(screenSize.height, dialogSize.height);
        dialogSize.width = Math.min(screenSize.width, dialogSize.width);
        
        // 居中显示或随机
        // setLocation((screenSize.width - dialogSize.width) / 2, (screenSize.height - dialogSize.height) / 2);
        setLocation(RandomUtils.nextInt(0, screenSize.width - dialogSize.width), RandomUtils.nextInt(0, screenSize.height - dialogSize.height));
        add(new JLabel(new ImageIcon(image)));
        setVisible(true);
        setResizable(false);
        setAlwaysOnTop(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    
}
