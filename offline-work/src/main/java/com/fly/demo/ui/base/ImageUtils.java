package com.fly.demo.ui.base;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.imageio.ImageIO;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ImageUtils
{
    private static List<BufferedImage> images;
    
    static
    {
        try
        {
            images = getResources().map(r -> readImage(r)).filter(Objects::nonNull).collect(Collectors.toList());
            log.info("images.length: {}", images.size());
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    /**
     * 获取全部图片资源
     * 
     * @return
     * @throws IOException
     * @see [类、类#方法、类#成员]
     */
    private static Stream<Resource> getResources()
        throws IOException
    {
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        // 绝对路径写法 "file:C:/Users/DELL/Pictures/abc/*.jpg"
        Resource[] jpgs = resolver.getResources("classpath:images/*.jpeg");
        Resource[] pngs = resolver.getResources("classpath:images/*.png");
        try (InputStream is = new ClassPathResource("images/urls.txt").getInputStream())
        {
            List<Resource> urlResources = IOUtils.readLines(is, Charset.defaultCharset()).stream().filter(StringUtils::isNotBlank).map(url -> {
                try
                {
                    return new UrlResource(url);
                }
                catch (MalformedURLException e)
                {
                    log.error(e.getMessage(), e);
                    return null;
                }
            }).filter(Objects::nonNull).collect(Collectors.toList());
            
            // 本地与Url图片资源合并几种写法
            switch (RandomUtils.nextInt(0, 5))
            {
                case 0:
                    // 或者 return Stream.of(jpgs, pngs, urlResources.toArray(new Resource[0])).flatMap(Stream::of);
                    return Stream.of(jpgs, pngs, urlResources.toArray(new Resource[0])).flatMap(Arrays::stream);
                case 1:
                    return Stream.concat(Stream.concat(Stream.of(jpgs), Stream.of(pngs)), urlResources.stream());
                case 2:
                    return Stream.of(ArrayUtils.addAll(ArrayUtils.addAll(jpgs, pngs), urlResources.toArray(new Resource[0])));
                case 3:
                    return Arrays.stream(ArrayUtils.addAll(ArrayUtils.addAll(jpgs, pngs), urlResources.toArray(new Resource[0])));
                case 4:
                default:
                    urlResources.addAll(Arrays.asList(jpgs));
                    urlResources.addAll(Arrays.asList(pngs));
                    return urlResources.stream();
            }
        }
    }
    
    /**
     * 转换为BufferedImage
     * 
     * @param resource
     * @return
     */
    private static BufferedImage readImage(Resource resource)
    {
        try (InputStream is = resource.getInputStream())
        {
            return ImageIO.read(is);
        }
        catch (IOException e)
        {
            log.error(e.getMessage(), e);
            return null;
        }
    }
    
    /**
     * 返回随机图片
     * 
     * @return
     */
    public static BufferedImage getRandomImage()
    {
        int index = RandomUtils.nextInt(0, images.size());
        return images.get(index);
    }
}