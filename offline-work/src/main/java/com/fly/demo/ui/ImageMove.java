package com.fly.demo.ui;

import java.awt.Dimension;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.swing.WindowConstants;

import org.apache.commons.lang3.RandomUtils;

import com.fly.demo.ui.base.ImageDialog;
import com.fly.demo.ui.base.ImageUtils;

/**
 * 
 * 不断移动的图片
 * 
 * @author 00fly
 * @version [版本号, 2023年3月3日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class ImageMove
{
    private static ImageDialog dialog;
    
    private static int paramX = 1;
    
    private static int paramY = 1;
    
    static
    {
        openImage();
    }
    
    /**
     * 打开图片
     * 
     */
    private static void openImage()
    {
        dialog = new ImageDialog(ImageUtils.getRandomImage());
        dialog.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
    }
    
    /**
     * 位置移动，鼠标移动、点击
     */
    private static void move()
    {
        Dimension screenSize = dialog.getToolkit().getScreenSize();
        // x方向递增、递减
        if (dialog.getX() < 0)
        {
            paramX = 1;
        }
        else if (dialog.getX() > screenSize.width)
        {
            paramX = -1;
            openImage();
        }
        // y方向递增、递减
        if (dialog.getY() < 0)
        {
            paramY = 1;
        }
        else if (dialog.getY() > screenSize.height)
        {
            paramY = -1;
            openImage();
        }
        
        int x = dialog.getX() + paramX * RandomUtils.nextInt(10, screenSize.width / 10);
        int y = dialog.getY() + paramY * RandomUtils.nextInt(10, screenSize.height / 10);
        dialog.setLocation(x, y);
        dialog.setVisible(true); // 重新显示
    }
    
    public static void main(String[] args)
    {
        Executors.newScheduledThreadPool(2).scheduleAtFixedRate(ImageMove::move, 2, 5, TimeUnit.SECONDS);
    }
}
