package com.fly.demo.ui;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomUtils;

import com.fly.demo.ui.base.ImageDialog;
import com.fly.demo.ui.base.ImageUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 时隐时现的图片
 * 
 * @author 00fly
 * @version [版本号, 2023年3月3日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
public class ImageDispose
{
    static boolean mouseEnable = false;
    
    private static Robot myRobot;
    
    static
    {
        try
        {
            myRobot = new Robot();
        }
        catch (AWTException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    /**
     * 打开图片后延时销毁
     * 
     */
    private static void openImage()
    {
        try
        {
            ImageDialog dialog = new ImageDialog(ImageUtils.getRandomImage());
            
            if (mouseEnable)
            {
                // 移动并单击左键鼠标
                myRobot.mouseMove(dialog.getX() + RandomUtils.nextInt(0, dialog.getWidth()), dialog.getY() + RandomUtils.nextInt(0, dialog.getHeight()));
                myRobot.mousePress(KeyEvent.BUTTON1_DOWN_MASK);
                myRobot.mouseRelease(KeyEvent.BUTTON1_DOWN_MASK);
            }
            TimeUnit.SECONDS.sleep(8);
            dialog.dispose();
        }
        catch (InterruptedException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    public static void main(String[] args)
    {
        Executors.newScheduledThreadPool(2).scheduleAtFixedRate(ImageDispose::openImage, 2, 10, TimeUnit.SECONDS);
    }
}
