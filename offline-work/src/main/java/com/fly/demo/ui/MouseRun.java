package com.fly.demo.ui;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 随机游走的鼠标
 */
@Slf4j
public class MouseRun
{
    private static Robot robot;
    
    static
    {
        try
        {
            robot = new Robot();
        }
        catch (AWTException e)
        {
            log.error(e.getMessage(), e);
        }
    }
    
    /**
     * 位置移动，鼠标移动、点击
     */
    private static void move()
    {
        // 模拟按下鼠标左键后释放
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        robot.mouseMove(RandomUtils.nextInt(0, screenSize.width), RandomUtils.nextInt(0, screenSize.height));
        robot.mousePress(KeyEvent.BUTTON1_DOWN_MASK);
        robot.mouseRelease(KeyEvent.BUTTON1_DOWN_MASK);
        
        // 打印坐标
        Point point = MouseInfo.getPointerInfo().getLocation();
        log.info("position: {},{}", point.getX(), point.getY());
    }
    
    public static void main(String[] args)
    {
        Executors.newScheduledThreadPool(2).scheduleAtFixedRate(MouseRun::move, 2, 10, TimeUnit.SECONDS);
    }
}
