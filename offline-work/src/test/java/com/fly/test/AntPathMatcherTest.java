package com.fly.test;

import org.junit.Test;
import org.springframework.util.AntPathMatcher;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AntPathMatcherTest
{
    AntPathMatcher matcher = new AntPathMatcher();
    
    @Test
    public void test()
    {
        log.info("{}", matcher.match("test", "test"));
        log.info("{}", matcher.match("/test/*", "/test/111"));// true
        log.info("{}", matcher.match("/test/**", "/test/111/222"));// true
        log.info("{}", matcher.match("/test/{id}", "/test/111"));// true
        log.info("{}", matcher.match("/test/{id}/aa", "/test/111/aa"));// true
        log.info("{}", matcher.match("/test/{id}-{name}/aa", "/test/111-haha/aa"));// true
        log.info("{}", matcher.match("/test/{id}-{name}/aa", "/test/111-/aa"));// true
    }
}
