package com.fly.source.ui;

import javax.swing.SwingUtilities;

import org.apache.commons.lang3.RandomUtils;

/**
 * 随机启动
 */
public class MainRun
{
    public static void main(String[] args)
    {
        if (RandomUtils.nextBoolean())
        {
            SwingUtilities.invokeLater(() -> new QuickSourceUI());
            return;
        }
        SwingUtilities.invokeLater(() -> new SimpleSourceUI());
    }
}
