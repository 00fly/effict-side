package com.fly.core.utils;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class HttpRequestUtils
{
    public static HttpServletRequest getHttpServletRequest()
    {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes != null)
        {
            return servletRequestAttributes.getRequest();
        }
        return null;
    }
}
