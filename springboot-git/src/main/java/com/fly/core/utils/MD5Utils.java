package com.fly.core.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.springframework.util.DigestUtils;

public class MD5Utils
{
    /**
     * MD5加密
     * 
     * @explain spring实现
     * @param str 待加密字符串
     * @return 16进制加密字符串
     */
    public static String encryptToMD5(String str)
    {
        return DigestUtils.md5DigestAsHex(str.getBytes(StandardCharsets.UTF_8));
    }
    
    /**
     * MD5加密
     * 
     * @explain spring实现
     * @param file 待加密文件
     * @return 16进制加密字符串
     * @throws IOException
     */
    public static String encryptToMD5(File file)
        throws IOException
    {
        try (InputStream fis = new FileInputStream(file))
        {
            return DigestUtils.md5DigestAsHex(fis);
        }
    }
}
