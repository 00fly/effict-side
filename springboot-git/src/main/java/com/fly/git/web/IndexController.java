package com.fly.git.web;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fly.core.entity.JsonResult;
import com.fly.core.utils.MD5Utils;
import com.fly.git.model.GitInfo;
import com.fly.git.model.GitRunInfo;
import com.fly.git.service.GitClientService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "api接口")
@RestController
@RequestMapping("/git")
public class IndexController
{
    @Autowired
    private GitClientService gitClient;
    
    @ApiOperationSupport(order = 10)
    @ApiOperation("设置git参数")
    @PostMapping("init")
    public JsonResult<?> init(@Valid GitInfo gitInfo)
        throws FileNotFoundException, IOException
    {
        String project = MD5Utils.encryptToMD5(gitInfo.getRemoteGit());
        File file = new File("data/" + project + ".ser");
        file.getParentFile().mkdirs();
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file)))
        {
            oos.writeObject(gitInfo);
        }
        gitClient.init();
        return JsonResult.success();
    }
    
    @ApiOperationSupport(order = 20)
    @ApiOperation("设置运行时间")
    @PostMapping("configHours")
    public JsonResult<?> configHours()
    {
        List<GitRunInfo> gitRunInfos = gitClient.getGitRunInfos();
        if (gitRunInfos.isEmpty())
        {
            return JsonResult.error("请先设置git参数");
        }
        for (GitRunInfo gitRunInfo : gitRunInfos)
        {
            gitRunInfo.registerHours();
        }
        return JsonResult.success("设置运行时间成功");
    }
    
    @ApiOperationSupport(order = 30)
    @ApiOperation("测试远程提交")
    @PostMapping("remote-commit")
    public JsonResult<?> commit()
        throws IOException, GitAPIException
    {
        List<GitRunInfo> gitRunInfos = gitClient.getGitRunInfos();
        if (gitRunInfos.isEmpty())
        {
            return JsonResult.error("请先设置git参数");
        }
        gitClient.runGitAll();
        return JsonResult.success();
    }
    
    @ApiOperationSupport(order = 40)
    @ApiOperation("查询运行时间")
    @GetMapping("queryHours")
    public JsonResult<?> queryHours()
    {
        List<GitRunInfo> gitRunInfos = gitClient.getGitRunInfos();
        if (gitRunInfos.isEmpty())
        {
            return JsonResult.error("请先设置git参数");
        }
        Integer index = 1;
        Map<Integer, Set<Integer>> runHoursAll = new HashMap<>();
        for (GitRunInfo gitRunInfo : gitRunInfos)
        {
            runHoursAll.put(index++, gitRunInfo.getRunHours());
        }
        return JsonResult.success(runHoursAll);
    }
}
