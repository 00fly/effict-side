package com.fly.git.model;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.IntStream;

import org.apache.commons.lang3.RandomUtils;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import com.fly.core.utils.MD5Utils;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * git运行信息
 * 
 * @author 00fly
 *
 */
@Data
@Slf4j
public class GitRunInfo
{
    /**
     * git参数信息
     */
    private GitInfo gitInfo;
    
    /**
     * 代码本地目录
     */
    private String localPath;
    
    /**
     * 鉴权信息
     */
    private UsernamePasswordCredentialsProvider credentialsProvider;
    
    /**
     * 定时任务运行时间
     */
    private Set<Integer> runHours = new ConcurrentSkipListSet<>();
    
    /**
     * 上次运行时间
     */
    private Long lastRunTime;
    
    /**
     * 上次提交的内容
     */
    String commitText;
    
    public GitRunInfo(GitInfo gitInfo)
    {
        super();
        try
        {
            this.gitInfo = gitInfo;
            this.credentialsProvider = new UsernamePasswordCredentialsProvider(gitInfo.getUsername(), gitInfo.getPassword());
            this.localPath = new File("project/" + MD5Utils.encryptToMD5(gitInfo.getRemoteGit())).getCanonicalPath();
            registerHours();
        }
        catch (IOException e)
        {
            log.error("###### GitRunInfo init failure!", e.getMessage());
        }
    }
    
    /**
     * registerRunHours
     * 
     * @see [类、类#方法、类#成员]
     */
    public void registerHours()
    {
        runHours.clear();
        int curHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        IntStream.range(curHour + 1, 24).filter(n -> RandomUtils.nextInt(0, 10) > 6).forEach(runHours::add);
        log.info("---- runHours init success: {} ----", runHours);
    }
}
