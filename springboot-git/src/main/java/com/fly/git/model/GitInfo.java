package com.fly.git.model;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "git参数信息")
public class GitInfo implements Serializable
{
    private static final long serialVersionUID = -4864847400188602713L;
    
    @NotBlank(message = "用户名不能为空")
    @ApiModelProperty(value = "用户名", required = true)
    private String username;
    
    @NotBlank(message = "密码或个人令牌不能为空")
    @ApiModelProperty(value = "密码或个人令牌", required = true)
    private String password;
    
    @NotBlank(message = "注册邮箱不能为空")
    @Email(message = "注册邮箱不合法")
    @ApiModelProperty(value = "注册邮箱", required = true)
    private String email;
    
    @NotBlank(message = "git项目地址不能为空")
    @ApiModelProperty(value = "git项目地址", example = "https://gitee.com/00fly/git-test.git", required = true)
    private String remoteGit;
}
