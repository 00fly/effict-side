package com.fly.git;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ServiceTest
{
    @Test
    public void test1()
        throws Exception
    {
        try (Scanner sc = new Scanner(System.in))
        {
            do
            {
                updateReadMe();
                log.info("------------输入x退出,回车换行继续------------");
            } while (!"x".equalsIgnoreCase(sc.nextLine()));
            log.info("------------成功退出------------");
        }
    }
    
    private void updateReadMe()
        throws IOException
    {
        File rootDir = new File("project/README.md");
        List<String> lines = FileUtils.readLines(rootDir, StandardCharsets.UTF_8.toString());
        lines.forEach(System.out::println);
        
        // 替换最后一行内容
        int index = lines.size() - 1;
        lines.remove(index);
        lines.add(DateFormatUtils.format(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss"));
        FileUtils.writeLines(rootDir, StandardCharsets.UTF_8.toString(), lines);
    }
    
    @Test
    public void test()
    {
        // git隐藏文件夹 project\.git\objects无法删除，导致project删除失败
        File rootDir = new File("project");
        rootDir.mkdir();
        FileUtils.deleteQuietly(rootDir);
    }
}