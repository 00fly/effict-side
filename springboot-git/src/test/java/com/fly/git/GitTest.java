package com.fly.git;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.SystemUtils;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.api.errors.TransportException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fly.GitApplication;
import com.fly.git.service.GitClientService;

/**
 * GitClientService测试
 */
@SpringBootTest(classes = GitApplication.class)
public class GitTest
{
    @Autowired
    private GitClientService gitClient;
    
    /**
     * 克隆远程库
     * 
     * @throws GitAPIException
     * @throws TransportException
     * @throws InvalidRemoteException
     * @throws IOException
     * @throws JGitInternalException
     */
    @Test
    public void testClone()
        throws InvalidRemoteException, TransportException, GitAPIException, JGitInternalException, IOException
    {
        // gitClient.gitClone();
        // gitClient.localCommit();
        // gitClient.remotePush();
    }
    
    /**
     * 命令行删除目录及其中的所有文件
     * 
     * @throws Exception
     * @see [类、类#方法、类#成员]
     */
    @Test
    public void deleteDirectory()
        throws Exception
    {
        // gitClient.gitClone();
        TimeUnit.SECONDS.sleep(30);
        String localPath = new File("project").getCanonicalPath();
        if (SystemUtils.IS_OS_WINDOWS)
        {
            Runtime.getRuntime().exec("cmd /c start " + localPath);
            TimeUnit.SECONDS.sleep(1);
            Runtime.getRuntime().exec("cmd /c rd /s/q " + localPath);
        }
        if (SystemUtils.IS_OS_UNIX)
        {
            Runtime.getRuntime().exec(new String[] {"/bin/sh", "-c", "rm -rf " + localPath});
        }
    }
    
}