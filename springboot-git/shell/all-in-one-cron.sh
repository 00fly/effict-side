#!/bin/sh
# defines variable
git=`ls -d */` && path=`pwd` && gitpath=$path/$git

# git pull
cd $gitpath && git pull \
&& now=`git log -1 --format="%at"` \
&& last=`cat $path/last`

# check timestamp, if changed print git log
if [ ! $last ] || [ $now -gt $last ] ; then
  echo "git changed!!" && echo $now>$path/last && echo `git log -1`
else
  echo "git not change"
fi
