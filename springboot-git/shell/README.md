# **演示：通过crontab调用万能版shell脚本实现定时修改git代码并自动提交**

# 一，环境准备

centos7 操作系统
```bash
#安装git
yum install -y git
```
# 二，运行步骤

## 1. 下载任意git项目源码

```bash
cd /work/gitee
git clone https://gitee.com/00fly/docker-demo.git
```

## 2. 配置git用户名、密码（个人令牌）
以 gitcode.com 为例，全局或当前目录保存

```bash
#全局保存
git config --global user.name 00fly
git config --global user.email 00fly@noreply.gitcode.com
git config --global credential.helper store

#当前目录保存
cd /work/gitee
git config user.name 00fly
git config user.email 00fly@noreply.gitcode.com
git config credential.helper store
```

```保证设置后git提交代码无需输入用户名、密码```

## 3. 执行
```bash
cd /work/gitee
wget https://gitee.com/00fly/git-cron/raw/master/all-in-one-cron-push.sh -O all-in-one-cron-push.sh
```
## 4. 配置定时任务
```bash
#查看作业任务
crontab -l

0 */4 * * * cd /work/gitee&&sh all-in-one-cron-push.sh>>/work/logs/git-cron.log
*/5 * * * * cd /work/gitee&&sh run.sh>>/work/logs/run.log

#编辑作业任务
crontab -e
```
举例如下：

```0 */1 * * *  cd /work/gitee&&sh all-in-one-cron-push.sh``` 每1小时执行一次

```*/5 * * * * cd /work/gitee&&sh all-in-one-cron-push.sh``` 每5分钟执行一次

```0 */4 * * * cd /work/gitee&&sh all-in-one-cron-push.sh``` 每4小时执行一次

**```注意：直接写为0 */4 * * * /work/gitee/all-in-one-cron-push.sh，crontab执行pwd结果为/root```**

## 5. 核心代码

```mermaid
graph LR
A(1. 更新git项目源码)--> B(2.保存本地git更新时间)--> C(3.更新代码并提交)  
```
1. 进入本地项目目录更新源码（git pull）
2. 在本地项目目录检查本地git更新时间是否为空或者最新一次提交时间戳（git log -1）是否大于本地git更新时间戳，如成立则更新last文件内容并打印最新1条日志
3. 修改date.md 文件内容，调用git push推送代码

all-in-one-cron-push.sh
```bash
#!/bin/sh
# defines variable
git=`ls -d */` && path=`pwd` && gitpath=$path/$git

# git pull
cd $gitpath && git pull \
&& now=`git log -1 --format="%at"` \
&& last=`cat $path/last`

# check timestamp, if changed print git log
if [ ! $last ] || [ $now -gt $last ] ; then
  echo "git changed!!" && echo $now>$path/last && echo `git log -1`
else
  echo "git not change"
fi

# git push
date > date.md
if [ -n "$(git status -s)" ] ; then
    git add .
    git commit -m "update: `date +"%Y-%m-%d %H:%M:%S"` auto commit"
    git push
fi
```

**升级版： all-in-one-cron-batch-push.sh 支持多git项目**
```bash
#!/bin/sh
for dir in `ls -d */`
do
  if [ -d $dir/.git ]; then
    echo "will process git diretory： $dir"

    # defines variable
    git=$dir && path=`pwd` && gitpath=$path/$git

    # git pull
    cd $gitpath && git pull \
    && now=`git log -1 --format="%at"` \
    && last=`cat $path/last`

    # check timestamp, if changed print git log
    if [ ! $last ] || [ $now -gt $last ] ; then
      echo "git changed!!" && echo $now>$path/last && echo `git log -1`
    else
      echo "git not change"
    fi

    # git push
    date > date.md
    if [ -n "$(git status -s)" ] ; then
        git add .
        git commit -m "update: `date +"%Y-%m-%d %H:%M:%S"` auto commit"
        git push
    fi

  fi
done
```