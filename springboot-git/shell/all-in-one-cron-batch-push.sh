#!/bin/sh
for dir in `ls -d */`
do
  if [ -d $dir/.git ]; then
    echo "will process git diretory： $dir"

    # defines variable
    git=$dir && path=`pwd` && gitpath=$path/$git

    # git pull
    cd $gitpath && git pull \
    && now=`git log -1 --format="%at"` \
    && last=`cat $path/last`

    # check timestamp, if changed print git log
    if [ ! $last ] || [ $now -gt $last ] ; then
      echo "git changed!!" && echo $now>$path/last && echo `git log -1`
    else
      echo "git not change"
    fi

    # git push
    date > date.md
    if [ -n "$(git status -s)" ] ; then
        git add .
        git commit -m "update: `date +"%Y-%m-%d %H:%M:%S"` auto commit"
        git push
    fi

  fi
done

