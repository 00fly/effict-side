# springboot-git
 
springboot工程操作git
 
![输入图片说明](images/image.png)


最新版本已支持docker镜像部署，完整的编排文件以及启动脚本见docker目录。

# 一、docker部署过程

## 1. 上传docker目录到安装了docker环境的服务器

目录结构

```shell
├── docker-compose.yml
├── restart.sh
├── restart-simple.sh
└── stop.sh

```

## 2. 运行命令启动容器

```shell
sh restart.sh
```

输出结果如下：

```sh
[root@00fly docker-git]# sh restart.sh 
[+] Running 9/9
 ✔ git-web 7 layers [⣿⣿⣿⣿⣿⣿⣿]      0B/0B      Pulled                                                                                                                                                                                  21.6s 
   ✔ 97518928ae5f Already exists                                                                                                                                                                                                       0.0s 
   ✔ b109695f6e59 Already exists                                                                                                                                                                                                       0.0s 
   ✔ 3ac0199bf17e Already exists                                                                                                                                                                                                       0.0s 
   ✔ 14cce039e329 Already exists                                                                                                                                                                                                       0.0s 
   ✔ 1bc1e3a5cc47 Already exists                                                                                                                                                                                                       0.0s 
   ✔ afe7d63db613 Pull complete                                                                                                                                                                                                        0.2s 
   ✔ 18a218fb6784 Pull complete                                                                                                                                                                                                       20.3s 
 ✔ git-simple Pulled                                                                                                                                                                                                                  21.6s 
[+] Running 2/3
 ⠴ Network docker-git_default  Created                                                                                                                                                                                                 0.5s 
 ✔ Container git-web           Started                                                                                                                                                                                                 0.4s 
 ✔ Container git-simple        Started   
```

输入`docker ps`查看，可以看到启动了2个服务，如**8082**端口被占用，请自行修改`docker-compose.yml`

![docker-ps](images/docker-ps.png)

为什么启动2个服务？
- git-web是我们通过接口配置git参数，一旦参数配置完成后，便可以关了他。
- git-simple是日常后台执行git推送，无需开放端口。

# 二、操作指南

配置地址：http://{ip}:8082/doc.html

## 1. 设置请求token
如提示：禁止访问，token信息不合法，按下述步骤操作：

- 查看后台日志
![查看后台日志](images/log.png)

- 请求头设置token
![请求头设置token](images/token.png)


## 2. 设置git仓库参数，支持设置多个仓库
![设置git仓库参数](images/git-setup.png)

注意： **git项目根目录下一定要创建 README.md文件，此程序会自动替换最后一行内容为提交时间戳。**

## 3. 设置运行时间
![设置运行时间](images/times-setup.png)

## 4. 测试一次远程提交
![测试一次远程提交](images/remote-test.png)

## 5. 查看运行时间
![查看运行时间](images/times-show.png)


# 三.主要涉及技术点
 1.  jgit 
 2. swagger使用
 3. 定时任务cron  
 4. 文件读写  
 5. 统一异常处理
 6. log4j2日志

