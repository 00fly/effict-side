# springboot-git
 
springboot工程操作git
 
![输入图片说明](images/image.png)


最新版本已支持docker镜像部署，完整的编排文件以及启动脚本见docker目录。

docker简易操作流程请参考 [Docker 微服务部署入门到实践](https://blog.csdn.net/qq_16127313/article/details/124140533)


### Spring Boot Jar运行时指定Start-Class类

```
java -cp springboot-git-1.0.0.jar -Dloader.main=com.fly.GitApplication org.springframework.boot.loader.PropertiesLauncher
```

reload-jar.sh

```bash
#!/bin/bash
# get pid

pname="springboot-git-1.0.0.jar"
echo  -e  "jar-name=$pname\r\n"


get_pid(){
 pid=`ps -ef | grep $pname | grep -v grep | awk '{print $2}'`
 echo "$pid"
}

ps -ef|grep $pname

PID=$(get_pid)
if [ -z "${PID}" ] 
then
 echo -e "\r\nJava Application already stop!"
else
 echo -e '\r\nkill -9  '${PID} '\r\n'
 kill -9 ${PID}
 echo -e "Java Application is stop!"
fi

rm -rf info.log

echo -e "\r\nJava Application will startup!\r\n"
jar_path=`find .. -name $pname`

#echo "jarfile=$jar_path"

nohup java -jar $jar_path >>./info.log 2>&1 &

ps -ef|grep $pname

echo -e "\r\n======== Java Application logs ======== \r\n"
tail -f  info.log
```