# docker-compose
 
docker-compose.yml 使用 command 目的是避免覆盖 Dockerfile的 ENTRYPOINT 设置。


也就是说使用`entrypoint: java -jar app.jar --noweb`的话，会覆盖Dockerfile的

```shell
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-Xshareclasses", "-Xquickstart", "-jar", "/root/app.jar"]
``` 