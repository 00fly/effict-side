package com.fly.docker.web;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.apache.logging.log4j.spi.StandardLevel;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fly.core.JsonResult;

import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;

@Api(tags = "首页接口")
@Controller
public class IndexController
{
    @GetMapping("/index")
    public String sayHello()
    {
        return "index.html";
    }
    
    @Operation(summary = "更新日志level")
    @ResponseBody
    @PostMapping("/updateLogLevel")
    public JsonResult<String> updateLogLevel()
    {
        LoggerContext loggerContext = LoggerContext.getContext(false);
        LoggerConfig loggerConfig = loggerContext.getConfiguration().getRootLogger();
        Level now = loggerConfig.getLevel();
        
        // 0 -> 600
        int next = (now.getStandardLevel().intLevel() + 100) % 700;
        Level level = Level.toLevel(StandardLevel.getStandardLevel(next).name());
        loggerConfig.setLevel(level);
        loggerContext.updateLoggers();
        
        System.err.println("OFF(0), FATAL(100), ERROR(200), WARN(300), INFO(400), DEBUG(500), TRACE(600) => " + Level.toLevel(StandardLevel.getStandardLevel(next).name()));
        return JsonResult.success("系統当前日志级别：" + level.name());
    }
}
