package com.fly.docker.thread;

import java.util.concurrent.Callable;

import org.apache.commons.lang3.StringUtils;

import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.exceptions.DockerException;

import lombok.extern.slf4j.Slf4j;

/**
 * 带返回值的ping接口检查线程
 */
@Slf4j
public class PingCallable implements Callable<DockerClient>
{
    private DockerClient dockerClient;
    
    public PingCallable(DockerClient dockerClient)
    {
        super();
        this.dockerClient = dockerClient;
    }
    
    @Override
    public DockerClient call()
        throws DockerException, InterruptedException
    {
        if (StringUtils.equals("OK", dockerClient.ping()))
        {
            log.info("{} ping接口检查ok", dockerClient);
            return dockerClient;
        }
        return null;
    }
}
