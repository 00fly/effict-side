package com.fly.docker.thread;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.exceptions.DockerException;

import lombok.extern.slf4j.Slf4j;

/**
 * ping接口检查线程
 */
@Slf4j
public class PingRunable implements Runnable
{
    private DockerClient dockerClient;
    
    private List<DockerClient> liveServers;
    
    public PingRunable(DockerClient dockerClient, List<DockerClient> liveServers)
    {
        super();
        this.dockerClient = dockerClient;
        this.liveServers = liveServers;
    }
    
    @Override
    public void run()
    {
        try
        {
            if (StringUtils.equals("OK", dockerClient.ping()))
            {
                liveServers.add(dockerClient);
            }
        }
        catch (DockerException | InterruptedException e)
        {
            log.error(e.getMessage(), e);
        }
    }
}