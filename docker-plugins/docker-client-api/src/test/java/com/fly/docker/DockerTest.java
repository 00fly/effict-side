package com.fly.docker;

import java.net.URI;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.DockerClient.ListContainersParam;
import com.spotify.docker.client.DockerClient.LogsParam;
import com.spotify.docker.client.LogStream;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.Container;
import com.spotify.docker.client.messages.ContainerChange;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerCreation;
import com.spotify.docker.client.messages.ContainerInfo;
import com.spotify.docker.client.messages.ContainerStats;
import com.spotify.docker.client.messages.TopResults;

import lombok.extern.slf4j.Slf4j;

/**
 * 参考：https://github.com/spotify/docker-client/blob/master/docs/user_manual.md
 */
@Slf4j
public class DockerTest
{
    private static DockerClient dockerClient;
    
    /**
     * docker服务地址
     */
    private static String DOCKER_SERVER_URL = "http://192.168.182.10:2375";
    
    @BeforeAll
    public static void init()
    {
        dockerClient = DefaultDockerClient.builder().uri(URI.create(DOCKER_SERVER_URL)).connectionPoolSize(10).build();
        log.info("★★★★★★★★ DockerClient Successful inited ★★★★★★★★");
    }
    
    /**
     * 创建
     * 
     * @throws DockerException
     * @throws InterruptedException
     */
    @Test
    public void testCreateContainer()
        throws DockerException, InterruptedException
    {
        // 启动并创建
        ContainerCreation container = dockerClient.createContainer(ContainerConfig.builder().image("hello-world:latest").build());
        log.info("{}", ToStringBuilder.reflectionToString(container));
        
        dockerClient.startContainer(container.id());
        try (LogStream stream = dockerClient.logs(container.id(), LogsParam.stdout(), LogsParam.stderr()))
        {
            String logs = stream.readFully();
            log.info("{}", logs);
        }
    }
    
    /**
     * Inspect
     * 
     * @throws DockerException
     * @throws InterruptedException
     */
    @Test
    public void testInspectContainer()
        throws DockerException, InterruptedException
    {
        // 展示运行状态容器列表
        List<Container> containers = dockerClient.listContainers();
        int index = RandomUtils.nextInt(0, containers.size());
        log.info("index = {},  containers size = {}", index, containers.size());
        
        Container container = containers.get(index);
        log.info("{}", ToStringBuilder.reflectionToString(container, ToStringStyle.MULTI_LINE_STYLE));
        
        // 打印容器信息
        ContainerInfo info = dockerClient.inspectContainer(container.id());
        log.info("{}", ToStringBuilder.reflectionToString(info, ToStringStyle.MULTI_LINE_STYLE));
    }
    
    /**
     * inspectContainerChanges
     * 
     * @throws DockerException
     * @throws InterruptedException
     */
    @Test
    public void testInspectContainerChanges()
        throws DockerException, InterruptedException
    {
        // 展示运行状态容器列表
        List<Container> containers = dockerClient.listContainers();
        int index = RandomUtils.nextInt(0, containers.size());
        log.info("index = {},  containers size = {}", index, containers.size());
        
        Container container = containers.get(index);
        log.info("{}", ToStringBuilder.reflectionToString(container, ToStringStyle.MULTI_LINE_STYLE));
        
        // 容器文件系统变化
        List<ContainerChange> changes = dockerClient.inspectContainerChanges(container.id());
        AtomicInteger count = new AtomicInteger(0);
        changes.stream().forEach(c -> log.info("{}. {}", count.incrementAndGet(), c));
    }
    
    /**
     * 容器列表
     * 
     * @throws DockerException
     * @throws InterruptedException
     */
    @Test
    public void testListContainers()
        throws DockerException, InterruptedException
    {
        // 展示运行状态容器列表
        AtomicInteger count = new AtomicInteger(0);
        log.info("运行状态容器列表");
        dockerClient.listContainers().stream().forEach(c -> log.info("{}. {}", count.incrementAndGet(), c));
        
        // 展示全部状态容器列表
        count.set(0);
        log.info("全部状态容器列表");
        dockerClient.listContainers(ListContainersParam.allContainers()).stream().forEach(c -> log.info("{}. {}", count.incrementAndGet(), c));
    }
    
    /**
     * 查询logs
     * 
     * @throws DockerException
     * @throws InterruptedException
     */
    @Test
    public void testLogs()
        throws DockerException, InterruptedException
    {
        List<Container> containers = dockerClient.listContainers();
        int index = RandomUtils.nextInt(0, containers.size());
        log.info("index = {},  containers size = {}", index, containers.size());
        
        Container container = containers.get(index);
        log.info("{}", ToStringBuilder.reflectionToString(container, ToStringStyle.MULTI_LINE_STYLE));
        try (LogStream stream = dockerClient.logs(container.id(), LogsParam.stdout(), LogsParam.stderr()))
        {
            String logs = stream.readFully();
            log.info("{}", logs);
        }
    }
    
    /**
     * 拉取image
     * 
     * @throws DockerException
     * @throws InterruptedException
     */
    @Test
    public void testPull()
        throws DockerException, InterruptedException
    {
        dockerClient.pull("hello-world:latest");
        dockerClient.pull("registry.cn-shanghai.aliyuncs.com/00fly/springboot-hello:1.0.0");
    }
    
    /**
     * removeContainer
     * 
     * @throws DockerException
     * @throws InterruptedException
     */
    @Test
    public void testRemoveContainer()
        throws DockerException, InterruptedException
    {
        List<Container> containers = dockerClient.listContainers();
        int index = RandomUtils.nextInt(0, containers.size());
        log.info("index = {},  containers size = {}", index, containers.size());
        
        Container container = containers.get(index);
        String image = container.image();
        log.info("{}", ToStringBuilder.reflectionToString(container, ToStringStyle.MULTI_LINE_STYLE));
        
        // stop then remove
        dockerClient.stopContainer(container.id(), 5); // stop after 10 seconds
        TimeUnit.SECONDS.sleep(10);
        dockerClient.removeContainer(container.id());
        
        // create then start
        ContainerCreation containerCreation = dockerClient.createContainer(ContainerConfig.builder().image(image).build());
        log.info("{}", ToStringBuilder.reflectionToString(containerCreation, ToStringStyle.MULTI_LINE_STYLE));
        dockerClient.startContainer(containerCreation.id());
        TimeUnit.SECONDS.sleep(5);
        try (LogStream stream = dockerClient.logs(containerCreation.id(), LogsParam.stdout(), LogsParam.stderr()))
        {
            String logs = stream.readFully();
            log.info("{}", logs);
        }
    }
    
    /**
     * StartStop
     * 
     * @throws DockerException
     * @throws InterruptedException
     */
    @Test
    public void testStartStop()
        throws DockerException, InterruptedException
    {
        List<Container> containers = dockerClient.listContainers();
        int index = RandomUtils.nextInt(0, containers.size());
        log.info("index = {},  containers size = {}", index, containers.size());
        
        Container container = containers.get(index);
        log.info("{}", ToStringBuilder.reflectionToString(container, ToStringStyle.MULTI_LINE_STYLE));
        
        // stop then start、restart
        dockerClient.stopContainer(container.id(), 10); // stop after 10 seconds
        TimeUnit.SECONDS.sleep(15);
        dockerClient.startContainer(container.id());
        TimeUnit.SECONDS.sleep(2);
        dockerClient.restartContainer(container.id(), 10);
        
        // kill then start
        dockerClient.killContainer(container.id());
        TimeUnit.SECONDS.sleep(2);
        dockerClient.startContainer(container.id());
    }
    
    /**
     * stats
     * 
     * @throws DockerException
     * @throws InterruptedException
     */
    @Test
    public void testStats()
        throws DockerException, InterruptedException
    {
        List<Container> containers = dockerClient.listContainers();
        int index = RandomUtils.nextInt(0, containers.size());
        log.info("index = {},  containers size = {}", index, containers.size());
        
        Container container = containers.get(index);
        log.info("{}", ToStringBuilder.reflectionToString(container, ToStringStyle.MULTI_LINE_STYLE));
        
        ContainerStats stats = dockerClient.stats(container.id());
        log.info("{}", stats);
    }
    
    /**
     * topContainer
     * 
     * @throws DockerException
     * @throws InterruptedException
     */
    @Test
    public void testTopContainer()
        throws DockerException, InterruptedException
    {
        List<Container> containers = dockerClient.listContainers();
        int index = RandomUtils.nextInt(0, containers.size());
        log.info("index = {},  containers size = {}", index, containers.size());
        
        Container container = containers.get(index);
        log.info("{}", ToStringBuilder.reflectionToString(container, ToStringStyle.MULTI_LINE_STYLE));
        
        TopResults topResults = dockerClient.topContainer(container.id(), "-ef");
        log.info("{}", ToStringBuilder.reflectionToString(topResults, ToStringStyle.MULTI_LINE_STYLE));
    }
}
