# 说明

- docker-client [https://github.com/spotify/docker-client](https://github.com/spotify/docker-client)

- docker-client-show依赖于docker-client-sdk模块Jar

## 适用场合

- 开发环境没有docker-client-sdk模块pom文件的依赖，只能通过docker-client-sdk.jar将所有的依赖包解压打包成一个整体jar供docker-client-show使用。

- docker-client-show可以正常编译使用docker-client-sdk模块的功能，因为完整的依赖均在docker-client-sdk.jar中，docker-client-sdk.jar可以被拷贝到其他项目lib中使用