## 三种自动化构建Docker镜像的Maven插件

- com.spotify.docker-maven-plugin  已停更,官方建议dockerfile-maven-plugin
- com.spotify.dockerfile-maven-plugin  近两年无更新，不支持操作容器
- io.fabric8.docker-maven-plugin  支持直接操作容器，且仍在持续更新，推荐使用
