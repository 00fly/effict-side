## docker-maven-plugin示例工程

### 注意点

- Maven的配置文件 setting.xml中server节id取值为 ${docker.hub}


显式调用

```shell
mvn clean package docker:build
mvn clean package docker:build docker:push
```

已绑定Docker命令到 Maven package 阶段

```shell
mvn clean package
```

跳过 docker 某个过程

```shell
mvn clean package 待确认
```
