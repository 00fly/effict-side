package com.fly.core.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;

import io.swagger.v3.oas.annotations.Operation;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseBuilder;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Response;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@EnableKnife4j
@EnableOpenApi
@Configuration
public class Knife4jConfig
{
    @Bean
    Docket createRestApi()
    {
        // 返回文档摘要信息
        return new Docket(DocumentationType.OAS_30).apiInfo(apiInfo())
            .select()
            .apis(RequestHandlerSelectors.withMethodAnnotation(Operation.class))
            .paths(PathSelectors.any())
            .build()
            .globalResponses(HttpMethod.GET, getGlobalResonseMessage())
            .globalResponses(HttpMethod.POST, getGlobalResonseMessage());
    }
    
    // 生成接口信息，包括标题、联系人等
    private ApiInfo apiInfo()
    {
        return new ApiInfoBuilder().title("数据接口API").description("如有疑问，请联系开发。").contact(new Contact("00fly", "https://www.00fly.online/", "00fly@qq.com")).version("1.0").build();
    }
    
    // 生成通用响应信息
    private List<Response> getGlobalResonseMessage()
    {
        List<Response> responseList = new ArrayList<>();
        responseList.add(new ResponseBuilder().code("404").description("找不到资源").build());
        return responseList;
    }
}