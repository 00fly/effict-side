package com.fly.docker.web;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.Operation;
import lombok.Data;
import lombok.experimental.Accessors;

@Api(tags = "Knife演示接口")
@RestController
@RequestMapping("/knife")
public class KnifeController
{
    @Operation(summary = "post测试")
    @PostMapping("/post/test")
    public OutPutBean test(@Valid InputBean input)
    {
        return new OutPutBean().setName(input.getName()).setValue(input.getValue());
    }
    
    @Operation(summary = "json测试")
    @PostMapping("/json/test")
    public OutPutBean jsonTest(@Valid @RequestBody InputBean input)
    {
        return new OutPutBean().setName(input.getName()).setValue(input.getValue());
    }
}

@Data
@ApiModel("入参模型")
class InputBean
{
    @NotBlank(message = "名称不能为空")
    @ApiModelProperty("名称")
    private String name;
    
    @NotBlank(message = "值不能为空")
    @ApiModelProperty("取值")
    private String value;
}

@Data
@ApiModel("出参模型")
@Accessors(chain = true)
class OutPutBean
{
    @ApiModelProperty("名称")
    private String name;
    
    @ApiModelProperty("取值")
    private String value;
}