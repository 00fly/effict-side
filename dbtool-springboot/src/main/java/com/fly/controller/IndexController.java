package com.fly.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fly.entity.DataBaseBean;

/**
 * 
 * IndexController
 * 
 * @author 00fly
 * @version [版本号, 2019年7月13日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Controller
public class IndexController
{
    @GetMapping("/")
    public String index()
    {
        return "index";
    }
    
    @PostMapping("/execute")
    public String execute(DataBaseBean dataBaseBean, HttpServletRequest request)
    {
        try
        {
            Object executeResult = dataBaseBean.execute();
            request.setAttribute("result", executeResult);
        }
        catch (Exception e)
        {
            request.setAttribute("promptInformation", e.getMessage());
        }
        return "index";
    }
}
