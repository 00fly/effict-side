package com.swagger3.demo;

import java.io.IOException;
import java.net.InetAddress;

import javax.servlet.ServletContext;

import org.apache.commons.lang3.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Swagger3Application implements CommandLineRunner
{
    @Value("${server.port}")
    Integer port;
    
    @Autowired
    ServletContext servletContext;
    
    public static void main(String[] args)
    {
        SpringApplication.run(Swagger3Application.class, args);
    }
    
    @Override
    public void run(String... args)
        throws IOException
    {
        if (SystemUtils.IS_OS_WINDOWS && port > 0)
        {
            String ip = InetAddress.getLocalHost().getHostAddress();
            String url = "http://" + ip + ":" + port + servletContext.getContextPath();
            // Runtime.getRuntime().exec("cmd /c start /min " + url + "/swagger-ui/index.html");
            Runtime.getRuntime().exec("cmd /c start /min " + url + "/doc.html");
        }
    }
}
