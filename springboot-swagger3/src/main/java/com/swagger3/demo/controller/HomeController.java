package com.swagger3.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;

@Api(tags = "首页信息管理")
@Slf4j
@RestController
@RequestMapping("/home")
public class HomeController
{
    @Operation(summary = "首页详情")
    @GetMapping("/home")
    public String home()
    {
        log.info("home");
        return "success......";
    }
    
    @Operation(summary = "文件上传")
    @PostMapping("/upload")
    public String upload(MultipartFile file)
    {
        return file.getOriginalFilename();
    }
}