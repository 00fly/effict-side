
package com.fly.simple.utils;

import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RegExUtils
{
    private static String EMPTY = "";
    
    /**
     * 移除正则内容
     * 
     * @param text
     * @param regex
     * @return
     */
    public static String removePattern(String text, String regex)
    {
        return replacePattern(text, regex, EMPTY);
    }
    
    public static String replacePattern(String text, String regex, String replacement)
    {
        if (text == null || regex == null || replacement == null)
        {
            return text;
        }
        return Pattern.compile(regex, Pattern.DOTALL).matcher(text).replaceAll(replacement);
    }
    
    /**
     * 返回版本号<br>
     * 
     * e.g: zipkin-server-2.10.4-exec.jar 返回 02.10.04
     * 
     * @param text
     * @return
     */
    public static String getVersion(String text)
    {
        text = removePattern(text, "[a-z]|-|\\.jar");
        return Stream.of(text.split("\\.")).map(v -> leftPad(v, 2, "0")).collect(Collectors.joining("."));
    }
    
    /**
     * 左补位padStr
     * 
     * @param input
     * @param size
     * @param padStr
     * @return
     */
    public static String leftPad(String input, int size, String padStr)
    {
        StringBuffer sb = new StringBuffer();
        while (sb.length() < size - input.length())
        {
            sb.append(padStr);
        }
        return sb.append(input).toString();
    }
}
