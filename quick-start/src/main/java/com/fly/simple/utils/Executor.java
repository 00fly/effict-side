package com.fly.simple.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Executor
{
    /**
     * execute命令
     * 
     * @param command 执行的命令
     * @param printLog 是否打印日志
     * @throws IOException
     */
    public static void execute(String command, boolean printLog)
        throws IOException
    {
        System.out.println(String.format("即将执行：%s\n", command));
        Process ps = Runtime.getRuntime().exec(command);
        if (!printLog)
        {
            return;
        }
        try (BufferedReader br = new BufferedReader(new InputStreamReader(ps.getInputStream(), "GBK")))
        {
            String line;
            while ((line = br.readLine()) != null)
            {
                System.out.println(line);
            }
        }
    }
}
