package com.fly.test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fly.simple.utils.RegExUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FilesSortedTest
{
    static int index = RandomUtils.nextInt(0, 3);
    
    /**
     * 获取版本号正则
     */
    Pattern versionPattern = Pattern.compile("-\\d(\\.\\d+)+(\\-|\\.)");
    
    @BeforeClass
    public static void init()
    {
        log.info("index: {}", index);
    }
    
    @Test
    public void testToMap()
        throws IOException
    {
        try (InputStream inputStream = FilesSortedTest.class.getResourceAsStream("/data"))
        {
            List<String> lines = IOUtils.readLines(inputStream, StandardCharsets.UTF_8);
            AtomicInteger count = new AtomicInteger(1);
            lines.stream().sorted(Comparator.comparing(RegExUtils::getVersion)).collect(Collectors.toMap(Function.identity(), s -> Collections.singletonList(count.getAndIncrement()))).forEach((k, v) -> log.info("{} {}", k, v));
            
            count.set(1);
            lines.stream().sorted(Comparator.comparing(RegExUtils::getVersion)).forEach(line -> log.info("{} {}", count.getAndIncrement(), line));
        }
    }
    
    /**
     * 按自然数版本号排序
     * 
     * @throws IOException
     */
    @Test
    public void test()
        throws IOException
    {
        try (InputStream inputStream = FilesSortedTest.class.getResourceAsStream("/data"))
        {
            List<String> lines = IOUtils.readLines(inputStream, StandardCharsets.UTF_8);
            log.info("默认排序：");
            lines.stream().sorted().forEach(log::info);
            
            log.info("改进排序结果：");
            lines.stream().sorted(Comparator.comparing(line -> leftPad(line, 2))).forEach(log::info);
        }
    }
    
    /**
     * 左补位处理
     * 
     * @param input
     * @param size
     * @return
     */
    private String leftPad(String input, int size)
    {
        switch (index)
        {
            case 0:
                return processAsDate(input);
            case 1:
                return processAsNum(input, size);
            case 2:
                return processAsNumV2(input, size);
            default:
                return input;
        }
    }
    
    /**
     * 转换为时间处理，取值有限制[0-23].[0-59].[0-59]
     * 
     * @throws ParseException
     */
    public String processAsDate(String input)
    {
        try
        {
            // 移除不需要的字符
            String date = RegExUtils.removePattern(input, "[a-z]|-|\\.jar");
            return DateFormatUtils.format(DateUtils.parseDate(date, "HH.mm.ss"), "HH.mm.ss");
        }
        catch (ParseException e)
        {
            log.error(e.getMessage(), e);
            return input;
        }
    }
    
    /**
     * 替换为新版本号（groupId、artifactId带数字的不适用）
     * 
     * @param input
     * @param size
     * @return
     */
    private String processAsNum(String input, int size)
    {
        // 移除不需要的字符
        String version = RegExUtils.removePattern(input, "[a-z]|-|\\.jar");
        String newVersion = Stream.of(version.split("\\.")).map(v -> StringUtils.leftPad(v, size, "0")).collect(Collectors.joining("."));
        String result = input.replace(version, newVersion);
        log.debug("{}\t===>\t{}", input, result);
        return result;
    }
    
    /**
     * 替换为新版本号
     * 
     * @param input
     * @param size
     * @return
     */
    private String processAsNumV2(String input, int size)
    {
        // 版本号,取"-"开始、"-"或"."结束的数字值,eg：-2.4.9-、-6.0.18.
        Matcher matcher = versionPattern.matcher(input);
        if (matcher.find())
        {
            // -6.0.18. => 6.0.18
            String find = matcher.group();
            String version = StringUtils.substring(find, 1, find.length() - 1);
            String newVersion = Stream.of(version.split("\\.")).map(v -> StringUtils.leftPad(v, size, "0")).collect(Collectors.joining("."));
            String result = input.replace(version, newVersion);
            log.debug("{}\t===>\t{}", input, result);
            return result;
        }
        return input;
    }
}