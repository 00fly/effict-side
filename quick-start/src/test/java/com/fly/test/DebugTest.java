package com.fly.test;

import java.util.Comparator;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * lambda借助peek()方法调试
 */
@Slf4j
public class DebugTest
{
    // 需要注意的坑
    // 坑一：peek() 不影响流的生成和消费
    // peek()是一个中间操作，它并不会终止流的处理流程，因此如果不跟一个终端操作（如collect(), forEach(), count()等），则peek()中的操作虽然会被执行，但整个流式处理链的结果不会有任何产出。换言之，只有当流被消耗时，peek()里的操作才会真正发生。
    //
    // 坑二：peek() 的执行次数取决于下游操作
    // peek()方法中的动作会在流的每个元素上执行一次，但具体执行多少次取决于下游的终端操作。例如，如果你在排序(sorted())前使用了peek()，而在排序后又使用了一次peek()，则同一个元素可能会被两次peek()。
    //
    // 坑三：并发流中的peek()行为
    // 对于并行流，peek()操作的执行顺序没有保证，而且可能会多次执行（取决于JVM的具体调度）。如果你在并行流中依赖peek()的顺序性或唯一性，可能会遇到意想不到的问题。
    //
    // 坑四：资源管理
    // 如果在peek()中打开了一些资源（如文件、数据库连接等），但在peek()内部并未妥善关闭它们，可能会导致资源泄露。因为在没有终端操作的情况下，流可能不会立即执行，资源也就无法及时释放。
    //
    // 坑五：对流元素的修改可能无效
    // peek()通常用于读取或打印流元素，而不是修改它们。虽然理论上可以尝试在peek()中修改元素，但由于流的惰性求值和可能的不可变性，这样的修改可能不会反映到源集合或后续流操作中。
    /**
     * 借助peek()方法调试
     */
    @Test
    public void testDebug()
    {
        // 会生成10个随机数再limit还是达到limit就不再生成随机数
        String result = new Random().ints(10, 0, 60)
            .peek(e -> System.out.println(e)) // debug
            .limit(RandomUtils.nextInt(1, 10))
            .mapToObj(String::valueOf)
            .collect(Collectors.joining("."));
        System.out.println(result);
    }
    
    /**
     * 不超过100个版本号字符串排序
     */
    @Test
    public void test()
    {
        IntStream.rangeClosed(1, 100).mapToObj(i -> toVersion()).sorted(Comparator.comparing(line -> leftPad(line, 2))).forEach(log::info);
    }
    
    /**
     * 生成不超过5位随机版本号
     * 
     * @return
     */
    private String toVersion()
    {
        // 生成1-5个0到60之间的随机数
        switch (RandomUtils.nextInt(0, 3))
        {
            case 0:
                // nextInt [1,6) 不包含6
                return new Random().ints(RandomUtils.nextInt(1, 6), 0, 60).mapToObj(String::valueOf).collect(Collectors.joining("."));
            case 1:
                // 如何debug，会生成5个随机数再limit?
                return new Random().ints(5, 0, 60).limit(RandomUtils.nextInt(1, 5)).mapToObj(String::valueOf).collect(Collectors.joining("."));
            case 2:
            default:
                return IntStream.rangeClosed(1, RandomUtils.nextInt(2, 6)).mapToObj(i -> String.valueOf(RandomUtils.nextInt(0, 60))).collect(Collectors.joining("."));
        }
    }
    
    /**
     * 左补0至长度size
     * 
     * @param input
     * @param size
     * @return
     */
    private String leftPad(String input, int size)
    {
        return Stream.of(input.split("\\.")).map(v -> StringUtils.leftPad(v, size, "0")).collect(Collectors.joining("."));
    }
}
