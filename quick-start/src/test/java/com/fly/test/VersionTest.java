package com.fly.test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fly.simple.utils.RegExUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class VersionTest
{
    /**
     * 获取版本号正则
     */
    Pattern versionPattern = Pattern.compile("-\\d(\\.\\d+)+(\\-|\\.)");
    
    static int index = RandomUtils.nextInt(1, 3);
    
    @BeforeClass
    public static void init()
    {
        log.info("index: {}", index);
    }
    
    @Test
    public void testSorted()
        throws IOException
    {
        // 搜索本地maven仓库
        List<String> lines = FileUtils.listFiles(new File("D:\\repo"), new String[] {"jar"}, true) // list
            .stream()
            .filter(f -> f.isFile() && !StringUtils.endsWithAny(f.getName(), "-sources.jar", "-javadoc.jar"))
            .map(f -> f.getName())
            .collect(Collectors.toList());
        
        log.info("默认排序：");
        lines.stream().sorted().forEach(System.out::println);
        
        log.info("改进排序结果：");
        lines.stream().sorted(Comparator.comparing(line -> leftPad(line, 2))).forEach(System.out::println);
    }
    
    /**
     * 左补位处理
     * 
     * @param input
     * @param size
     * @return
     */
    private String leftPad(String input, int size)
    {
        switch (index)
        {
            case 1:
                return processAsNum(input, size);
            case 2:
                return processAsNumV2(input, size);
            default:
                return input;
        }
    }
    
    /**
     * 替换为新版本号（groupId、artifactId带数字的不适用）
     * 
     * @param input
     * @param size
     * @return
     */
    private String processAsNum(String input, int size)
    {
        // 移除不需要的字符
        String version = RegExUtils.removePattern(input, "[a-z]|-|\\.jar");
        String newVersion = Stream.of(version.split("\\.")).map(v -> StringUtils.leftPad(v, size, "0")).collect(Collectors.joining("."));
        String result = input.replace(version, newVersion);
        log.debug("{}\t===>\t{}", input, result);
        return result;
    }
    
    /**
     * 替换为新版本号
     * 
     * @param input
     * @param size
     * @return
     */
    private String processAsNumV2(String input, int size)
    {
        // 版本号,取"-"开始、"-"或"."结束的数字值,eg：-2.4.9-、-6.0.18.
        Matcher matcher = versionPattern.matcher(input);
        if (matcher.find())
        {
            // -6.0.18. => 6.0.18 => 06.00.18
            String find = matcher.group();
            String version = StringUtils.substring(find, 1, find.length() - 1);
            String newVersion = Stream.of(version.split("\\.")).map(v -> StringUtils.leftPad(v, size, "0")).collect(Collectors.joining("."));
            String result = input.replace(version, newVersion);
            log.debug("{}\t===>\t{}", input, result);
            return result;
        }
        return input;
    }
    
    @Test
    public void testFind()
        throws IOException
    {
        try (InputStream inputStream = VersionTest.class.getResourceAsStream("/data2"))
        {
            List<String> lines = IOUtils.readLines(inputStream, StandardCharsets.UTF_8);
            for (String line : lines)
            {
                Matcher matcher = versionPattern.matcher(line);
                if (matcher.find())
                {
                    log.info("{}\t\t{}", matcher.group(), line);
                }
            }
        }
    }
    
    /**
     * 正则实现困难
     */
    @Test
    public void testByRegex()
    {
        System.out.println("zipkin-server-2.04.9-exec.jar".replaceAll("\\d", "0$0"));
        System.out.println("zipkin-server-2.5.3-exec.jar".replaceAll("(\\d{1,})", "0$0"));
        System.out.println("zipkin-server-2.4.12-exec.jar".replaceAll("\\d{1,}", "0$0"));
    }
}
