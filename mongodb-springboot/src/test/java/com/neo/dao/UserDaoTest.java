package com.neo.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.neo.entity.UserEntity;

/**
 * Created by summer on 2017/5/5.
 */
@SpringBootTest
public class UserDaoTest
{
    @Autowired
    private UserDao userDao;
    
    @Test
    public void testSaveUser()
        throws Exception
    {
        for (long i = 1; i < 10; i++)
        {
            UserEntity user = new UserEntity();
            user.setId(i);
            user.setUserName("小明");
            user.setPassWord("fffooo123");
            userDao.saveUser(user);
        }
    }
    
    @Test
    public void findUserByUserName()
    {
        UserEntity user = userDao.findUserByUserName("小明");
        System.out.println("user is " + user);
    }
    
    @Test
    public void updateUser()
    {
        UserEntity user = new UserEntity();
        user.setId(2l);
        user.setUserName("天空");
        user.setPassWord("fffxxxx");
        userDao.updateUser(user);
    }
    
    @Test
    public void deleteUserById()
    {
        userDao.deleteUserById(1l);
    }
}
