# effict side

#### 项目介绍
效率开发工具大杂烩


#### 使用说明

1. 数据库测试工具

![输入图片说明](https://images.gitee.com/uploads/images/2018/0707/124625_0a2e7f48_722815.png "db.png")

2.excel模板数据库语句转换工具
![excel模板](https://images.gitee.com/uploads/images/2018/0707/160833_d8d6e16b_722815.png "excel模板.png")
![运行界面](https://images.gitee.com/uploads/images/2018/0707/160905_5ba30063_722815.png "ui.png")
![运行界面](https://images.gitee.com/uploads/images/2018/0707/161044_02898204_722815.png "ui.png")

3.Java项目编码转换工具

![输入图片说明](https://gitee.com/uploads/images/2018/0703/085352_d4f8db2e_722815.png "encode_a.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0703/085406_5bb5c904_722815.png "encode_b.png")





欢迎点击链接加入技术讨论群【Java 爱码少年】：https://jq.qq.com/?_wv=1027&k=4AuWuZu
