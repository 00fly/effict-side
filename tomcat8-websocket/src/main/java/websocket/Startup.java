package websocket;

import javax.annotation.PostConstruct;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet(urlPatterns = "/startup", loadOnStartup = 1)
public class Startup extends HttpServlet
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -3530422820429952077L;
    
    @PostConstruct
    public void openBrowser()
    {
        try
        {
            Runtime.getRuntime().exec("cmd /c start /min http://192.168.1.175:8080/examples/");
        }
        catch (Exception e)
        {
        }
    }
}
