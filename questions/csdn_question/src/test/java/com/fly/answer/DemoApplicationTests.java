package com.fly.answer;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.fly.answer.model.Clazz;
import com.fly.answer.model.Score;
import com.fly.answer.model.Student;

@SpringBootTest
class DemoApplicationTests
{
    
    Clazz math = new Clazz("数学");
    
    Clazz physics = new Clazz("物理");
    
    @Test
    void test()
    {
        Student jack = new Student("Jack");
        Student john = new Student("John");
        Student mark = new Student("Mark");
        
        Score jackScore = new Score();
        jackScore.setStudent(jack);
        jackScore.addScore(math, 88);
        jackScore.addScore(physics, 79);
        
        Score johnScore = new Score();
        johnScore.setStudent(john);
        johnScore.addScore(math, 90);
        johnScore.addScore(physics, 82);
        
        Score markScore = new Score();
        markScore.setStudent(mark);
        markScore.addScore(math, 84);
        markScore.addScore(physics, 73);
        List<Score> list = new ArrayList<Score>();
        list.add(jackScore);
        list.add(johnScore);
        list.add(markScore);
        
        // step1
        printDetail(list);
        
        // step2
        printAverage(list);
        
        // step3
        printSortAverage(list);
        
        // step4
        markScore.addScore(math, 79);
        markScore.addScore(physics, 89);
        printDetail(list);
        
        // step5
        list.remove(johnScore);
        printDetail(list);
    }
    
    private void printDetail(List<Score> list)
    {
        list.forEach((score) -> {
            System.out.println(score.getStudent().getStudentName());
            score.getScoreList().forEach((k, v) -> {
                System.out.println(k.getClassName() + "\t" + v);
            });
        });
    }
    
    private void printAverage(List<Score> list)
    {
        list.forEach((score) -> {
            List<Integer> l = new ArrayList<Integer>();
            score.getScoreList().forEach((k, v) -> {
                l.add(v);
            });
            OptionalDouble avg = l.stream().mapToInt((i) -> i).average();
            System.out.println(avg.getAsDouble());
        });
    }
    
    private void printSortAverage(List<Score> list)
    {
        list.sort((a, b) -> a.getScoreList().get(math) + a.getScoreList().get(physics) - b.getScoreList().get(math) - b.getScoreList().get(physics));
        list.forEach((score) -> {
            List<Integer> l = new ArrayList<Integer>();
            score.getScoreList().forEach((k, v) -> {
                l.add(v);
            });
            OptionalDouble avg = l.stream().mapToInt((i) -> i).average();
            System.out.println(avg.getAsDouble());
        });
    }
}
