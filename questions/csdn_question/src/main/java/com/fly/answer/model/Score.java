package com.fly.answer.model;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 
 * <一句话功能简述>
 * 
 * @author 00fly
 * @version [版本号, 2020年6月16日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class Score
{
    private Student student;
    
    private Map<Clazz, Integer> scoreList = new LinkedHashMap<>();
    
    public Student getStudent()
    {
        return student;
    }
    
    public void setStudent(Student student)
    {
        this.student = student;
    }
    
    public Map<Clazz, Integer> getScoreList()
    {
        return scoreList;
    }
    
    public void setScoreList(Map<Clazz, Integer> scoreList)
    {
        this.scoreList = scoreList;
    }
    
    public void addScore(Clazz clazz, Integer scoreNum)
    {
        this.scoreList.put(clazz, scoreNum);
    }
}
