package com.fly.answer.model;

/**
 * 
 * 学生
 * 
 * @author 00fly
 * @version [版本号, 2020年6月16日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class Student
{
    private String studentName;
    
    public Student(String studentName)
    {
        super();
        this.studentName = studentName;
    }
    
    public String getStudentName()
    {
        return studentName;
    }
    
    public void setStudentName(String studentName)
    {
        this.studentName = studentName;
    }
    
}
