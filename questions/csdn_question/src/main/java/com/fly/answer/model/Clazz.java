package com.fly.answer.model;

/**
 * 班级
 * 
 * @author 00fly
 * @version [版本号, 2020年6月16日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class Clazz
{
    private String className;
    
    public Clazz(String className)
    {
        super();
        this.className = className;
    }
    
    public String getClassName()
    {
        return className;
    }
    
    public void setClassName(String className)
    {
        this.className = className;
    }
    
}
