package com.fly.answer.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;

public class Scores
{
    List<Score> data = new ArrayList<Score>();
    
    public void addStudent(Student student, Map<Clazz, Integer> scoreList)
    {
        Score score = new Score();
        score.setStudent(student);
        score.setScoreList(scoreList);
        data.add(score);
    }
    
    public void dropStudent(Student student, Clazz clazz, Integer scoreNum)
        throws Exception
    {
        for (Score score : data)
        {
            if (score.getStudent().equals(student))
            {
                if (scoreNum < 0)
                {
                    throw new Exception("scoreNum value Error");
                }
                else
                {
                    data.remove(score);
                }
            }
        }
    }
    
    public void getSortStudent()
    {
        Clazz math = new Clazz("数学");
        Clazz physics = new Clazz("物理");
        data.sort((a, b) -> a.getScoreList().get(math) + a.getScoreList().get(physics) - b.getScoreList().get(math) - b.getScoreList().get(physics));
        data.forEach((score) -> {
            List<Integer> l = new ArrayList<Integer>();
            score.getScoreList().forEach((k, v) -> {
                l.add(v);
            });
            OptionalDouble avg = l.stream().mapToInt((i) -> i).average();
            System.out.println(avg.getAsDouble());
        });
    }
    
    public Integer getNumberOfstudent()
    {
        return data.size();
    }
    
    public void clear()
    {
        data.clear();
    }
}
