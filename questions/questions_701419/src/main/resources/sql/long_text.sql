/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2018-09-30 13:51:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for long_text
-- ----------------------------
DROP TABLE IF EXISTS `long_text`;
CREATE TABLE `long_text` (
  `id` varchar(40) NOT NULL,
  `id_path` varchar(40) DEFAULT NULL,
  `content` varchar(1000) DEFAULT NULL,
  `sid` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pk_sid` (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of long_text
-- ----------------------------
