<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<style>
.blue-button {
	background: #25A6E1;
	filter: progid: DXImageTransform.Microsoft.gradient( startColorstr='#25A6E1',
		endColorstr='#188BC0', GradientType=0);
	padding: 3px 20px;
	color: #fff;
	font-family: 'Helvetica Neue', sans-serif;
	font-size: 12px;
	border-radius: 2px;
	-moz-border-radius: 2px;
	-webkit-border-radius: 4px;
	border: 1px solid #1A87B9
}

table {
	font-family: "Helvetica Neue", Helvetica, sans-serif;
	width: 50%;
}

th {
	background: SteelBlue;
	color: white;
}

td, th {
	border: 1px solid gray;
	width: 25%;
	text-align: left;
	padding: 5px 10px;
}
</style>
</head>
<body>
	<form:form method="post" modelAttribute="item" action="${pageContext.request.contextPath}/longText/add">
		<table>
			<tr>
				<th colspan="2">Add or Edit Item</th>
				<form:hidden path="id" />
			</tr>
			<tr>
				<td><form:label path="idPath">idPath:</form:label></td>
				<td><form:input path="idPath" size="30" maxlength="30"></form:input> varchar </td>
			</tr>
			<tr>
				<td><form:label path="content">content:</form:label></td>
				<td><form:input path="content" size="30" maxlength="30"></form:input> varchar </td>
			</tr>
			<tr>
				<td><form:label path="sid">sid:</form:label></td>
				<td><form:input path="sid" size="30" maxlength="30"></form:input> varchar </td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: center;"><input type="submit" class="blue-button" /></td>
			</tr>
		</table>
	</form:form>
	</br>
	<h3>Data List</h3>
	<c:if test="${!empty list}">
		<table class="tg">
			<tr>
				<th width="80">id</th>
				<th width="120">idPath</th>
				<th width="120">content</th>
				<th width="120">sid</th>
				<th width="60">Edit</th>
				<th width="60">Delete</th>
			</tr>
			<c:forEach items="${list}" var="it">
				<tr>
					<td>${it.id}</td>
					<td>${it.idPath}</td>
					<td>${it.content}</td>
					<td>${it.sid}</td>
					<td><a href="<c:url value='/longText/update/${it.id}' />">Edit</a></td>
					<td><a href="<c:url value='/longText/delete/${it.id}' />">Delete</a></td>
				</tr>
			</c:forEach>
		</table>
	</c:if>
</body>
</html>
