<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<style type="text/css">
.body {
	font-size: 15px;
	font-weight: normal;
	LINE-HEIGHT: 22px;
	color: navy;
}

a:hover {
	color: red;
}
</style>
<head>
<title>HomePage</title>
</head>
<body>
	<h1>Hello world!</h1>
	<c:forEach var="item" items="${urls}">
		<p>
			<a target="blank" href="${pageContext.request.contextPath}${item}/">点击访问 ${item}</a>
		</p>
	</c:forEach>
</body>
</html>
