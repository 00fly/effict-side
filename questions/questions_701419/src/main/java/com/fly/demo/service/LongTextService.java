package com.fly.demo.service;

import java.util.List;

import com.fly.common.PaginationSupport;
import com.fly.common.ServiceException;
import com.fly.demo.entity.LongText;

/**
 * 
 * LongTextService 接口
 * 
 * @author 00fly
 * @version [版本号, 2018-09-29]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public interface LongTextService
{
    /**
     * 新增
     * 
     * @param longText
     * @throws ServiceException
     * @see [类、类#方法、类#成员]
     */
    void insert(LongText longText)
        throws ServiceException;
    
    /**
     * 根据id删除
     * 
     * @param id
     * @throws ServiceException
     * @see [类、类#方法、类#成员]
     */
    void deleteById(String id)
        throws ServiceException;
    
    /**
     * 根据主键id列表删除数据
     * 
     * @param ids 主键列表
     * @return
     * @throws ServiceException
     */
    long deleteById(String[] ids)
        throws ServiceException;
    
    /**
     * 根据主键id列表删除数据
     * 
     * @param ids 主键列表
     * @return
     * @throws ServiceException
     */
    long deleteById(List<String> ids)
        throws ServiceException;
    
    /**
     * 根据id更新
     * 
     * @param longText
     * @throws ServiceException
     * @see [类、类#方法、类#成员]
     */
    void update(LongText longText)
        throws ServiceException;
    
    /**
     * 新增/根据id更新
     * 
     * @param longText
     * @throws ServiceException
     * @see [类、类#方法、类#成员]
     */
    void saveOrUpdate(LongText longText)
        throws ServiceException;
    
    /**
     * 根据id查询
     * 
     * @param id
     * @return
     * @throws ServiceException
     * @see [类、类#方法、类#成员]
     */
    LongText queryById(String id)
        throws ServiceException;
    
    /**
     * 查询全部
     * 
     * @return
     * @throws ServiceException
     */
    List<LongText> queryAll()
        throws ServiceException;
    
    /**
     * 根据条件分页查询
     * 
     * @param criteria 条件对象
     * @param pageNo 页号
     * @param pageSize 页大小
     * @return
     * @throws ServiceException
     */
    PaginationSupport<LongText> queryForPagination(LongText criteria, int pageNo, int pageSize)
        throws ServiceException;
    
    /**
     * 事务方法
     * 
     * @throws ServiceException
     * @see [类、类#方法、类#成员]
     */
    void testTrans()
        throws ServiceException;
}
