package com.fly.demo.entity;

/**
 * 
 * long_text表对应的LongText实体
 * 
 * @author 00fly
 * @version [版本号, 2018-09-29]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class LongText
{
    // id
    private String id;
    
    // id_path
    private String idPath;
    
    // content
    private String content;
    
    // sid
    private String sid;
    
    public String getId()
    {
        return id;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public void setIdPath(String idPath)
    {
        this.idPath = idPath;
    }
    
    public String getIdPath()
    {
        return this.idPath;
    }
    
    public void setContent(String content)
    {
        this.content = content;
    }
    
    public String getContent()
    {
        return this.content;
    }
    
    public void setSid(String sid)
    {
        this.sid = sid;
    }
    
    public String getSid()
    {
        return this.sid;
    }
}
