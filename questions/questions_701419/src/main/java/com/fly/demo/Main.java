package com.fly.demo;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import com.fly.common.ServiceException;
import com.fly.core.BaseDAO;
import com.fly.demo.entity.LongText;
import com.fly.demo.service.impl.LongTextServiceImpl;

/**
 * 
 * Main
 * 
 * @author 00fly
 * @version [版本号, 2018-09-29]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class Main
{
    /**
     * Main
     * 
     * @param args
     * @throws ServiceException
     * @throws SQLException
     * @see [类、类#方法、类#成员]
     */
    public static void main(String[] args)
        throws ServiceException, SQLException
    {
        String input = RandomStringUtils.randomAlphanumeric(100);
        int length = 20;
        System.out.println(input);
        System.out.println(length);
        String temp = input;
        
        if (input.length() > length)
        {
            // 计算分几组
            int arrLength = (input.length() - 1) / length + 1;
            String[] textArr = new String[arrLength];
            int index = 0;
            // 计算每组的字符串
            do
            {
                textArr[index] = input.substring(0, length);
                input = input.substring(length);
                index++;
            } while (input.length() > length);
            textArr[index] = input;
            
            // 打印分组数据
            for (int i = 0; i < arrLength; i++)
            {
                System.out.println(i + "---> " + textArr[i]);
            }
            
            // 插入数据库
            String id = "";
            LongText text;
            String parentId = "";
            String lastSid = "";
            for (int i = 0; i < arrLength; i++)
            {
                text = new LongText();
                if (lastSid.isEmpty())
                {
                    text.setId(UUID.randomUUID().toString());
                    id = text.getId();
                }
                else
                {
                    text.setId(lastSid);
                }
                if (parentId.isEmpty())
                {
                    parentId = text.getId() + "_";
                }
                // 左补齐0为3位，限定分组长度不能超过1000
                text.setIdPath(parentId + StringUtils.leftPad(String.valueOf(i), 3, "0"));
                text.setContent(textArr[i]);
                text.setSid(UUID.randomUUID().toString());
                lastSid = text.getSid();
                LongTextServiceImpl.getInstance().insert(text);
            }
            
            // 打印id
            System.out.println("id: " + id + " ---> " + temp);
            
            // SQL
            String sql = "select content from long_text where id_path like ? order by id_path";
            List<LongText> list = new BaseDAO().query(LongText.class, sql, new Object[] {id + "%"});
            for (LongText it : list)
            {
                System.out.print(it.getContent());
            }
        }
    }
}
