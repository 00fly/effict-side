package com.fly.demo.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fly.common.DaoException;
import com.fly.common.PaginationSupport;
import com.fly.core.BaseDAO;
import com.fly.demo.dao.LongTextDAO;
import com.fly.demo.entity.LongText;

/**
 * 
 * LongTextDAO 接口实现类
 * 
 * @author 00fly
 * @version [版本号, 2018-09-29]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */

@SuppressWarnings("unchecked")
public class LongTextDAOImpl extends BaseDAO<LongText> implements LongTextDAO
{
    private static final Logger LOGGER = LoggerFactory.getLogger(LongTextDAOImpl.class);
    
    /**
     * <默认构造函数>
     */
    private LongTextDAOImpl()
    {
        super();
    }
    
    private static final LongTextDAOImpl SINGLE = new LongTextDAOImpl();
    
    /**
     * 单例获取对象
     * 
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static LongTextDAOImpl getInstance()
    {
        return SINGLE;
    }
    
    /**
     * 构造whereClause
     * 
     * @param criteria
     * @return
     */
    private String buildWhereClause(LongText criteria)
    {
        StringBuilder whereClause = new StringBuilder();
        if (criteria.getId() != null)
        {
            whereClause.append(" and id=?");
        }
        if (criteria.getIdPath() != null)
        {
            whereClause.append(" and id_path=?");
        }
        if (criteria.getContent() != null)
        {
            whereClause.append(" and content=?");
        }
        if (criteria.getSid() != null)
        {
            whereClause.append(" and sid=?");
        }
        if (whereClause.length() > 4)
        {
            return whereClause.substring(4);
        }
        return "";
    }
    
    /**
     * 构造whereParams
     * 
     * @param criteria
     * @return
     * 
     */
    private List<Object> buildWhereParams(LongText criteria)
    {
        List<Object> whereParams = new ArrayList<>();
        if (criteria.getId() != null)
        {
            whereParams.add(criteria.getId());
        }
        if (criteria.getIdPath() != null)
        {
            whereParams.add(criteria.getIdPath());
        }
        if (criteria.getContent() != null)
        {
            whereParams.add(criteria.getContent());
        }
        if (criteria.getSid() != null)
        {
            whereParams.add(criteria.getSid());
        }
        return whereParams;
    }
    
    /**
     * 根据条件删除数据
     * 
     * @param criteria 条件对象
     * @return
     * @throws DaoException
     */
    @Override
    public long deleteByCriteria(LongText criteria)
        throws DaoException
    {
        try
        {
            StringBuilder sql = new StringBuilder("delete from long_text");
            String whereClause = buildWhereClause(criteria);
            List<Object> whereParams = buildWhereParams(criteria);
            if (StringUtils.isNotEmpty(whereClause))
            {
                sql.append(" where ").append(whereClause);
            }
            return update(sql.toString(), whereParams.toArray());
        }
        catch (SQLException e)
        {
            LOGGER.error(e.getMessage(), e);
            throw new DaoException(e.getMessage(), e.getCause());
        }
    }
    
    /**
     * 根据主键id删除数据
     * 
     * @param id 主键
     * @return
     * @throws DaoException
     */
    @Override
    public long deleteById(String id)
        throws DaoException
    {
        try
        {
            String sql = "delete from long_text where id=?";
            return update(sql, id);
        }
        catch (SQLException e)
        {
            LOGGER.error(e.getMessage(), e);
            throw new DaoException(e.getMessage(), e.getCause());
        }
    }
    
    /**
     * 根据主键id列表删除数据
     * 
     * @param ids 主键列表
     * @return
     * @throws DaoException
     */
    @Override
    public long deleteById(String[] ids)
        throws DaoException
    {
        try
        {
            String[][] idsArr = new String[ids.length][1];
            for (int i = 0; i < ids.length; i++)
            {
                idsArr[i][0] = ids[i];
            }
            String sql = "delete from long_text where id=?";
            return batch(sql, idsArr).length;
        }
        catch (SQLException e)
        {
            LOGGER.error(e.getMessage(), e);
            throw new DaoException(e.getMessage(), e.getCause());
        }
    }
    
    /**
     * 根据主键id列表删除数据
     * 
     * @param ids 主键列表
     * @return
     * @throws DaoException
     */
    @Override
    public long deleteById(List<String> ids)
        throws DaoException
    {
        try
        {
            String[][] idsArr = new String[ids.size()][1];
            for (int i = 0; i < ids.size(); i++)
            {
                idsArr[i][0] = ids.get(i);
            }
            String sql = "delete from long_text where id=?";
            return batch(sql, idsArr).length;
        }
        catch (SQLException e)
        {
            LOGGER.error(e.getMessage(), e);
            throw new DaoException(e.getMessage(), e.getCause());
        }
    }
    
    /**
     * 增加记录(插入全字段)
     * 
     * @param bean 待插入对象
     * @return
     * @throws DaoException
     */
    @Override
    public boolean insert(LongText bean)
        throws DaoException
    {
        try
        {
            String sql = "insert into long_text (id, id_path, content, sid ) values(?, ?, ?, ?)";
            return update(sql, bean.getId(), bean.getIdPath(), bean.getContent(), bean.getSid()) > 0;
        }
        catch (SQLException e)
        {
            LOGGER.error(e.getMessage(), e);
            throw new DaoException(e.getMessage(), e.getCause());
        }
    }
    
    /**
     * 增加记录(仅插入非空字段)
     * 
     * @param bean 待插入对象
     * @return
     * @throws DaoException
     */
    @Override
    public boolean insertSelective(LongText bean)
        throws DaoException
    {
        try
        {
            StringBuilder columns = new StringBuilder();
            StringBuilder values = new StringBuilder();
            List<Object> params = new ArrayList<>();
            if (bean.getIdPath() != null)
            {
                columns.append(", id_path");
                values.append(", ?");
                params.add(bean.getIdPath());
            }
            if (bean.getContent() != null)
            {
                columns.append(", content");
                values.append(", ?");
                params.add(bean.getContent());
            }
            if (bean.getSid() != null)
            {
                columns.append(", sid");
                values.append(", ?");
                params.add(bean.getSid());
            }
            StringBuilder sql = new StringBuilder("insert into long_text (").append(columns.substring(1)).append(")");
            sql.append(" values(").append(values.substring(1)).append(")");
            return update(sql.toString(), params.toArray()) > 0;
        }
        catch (SQLException e)
        {
            LOGGER.error(e.getMessage(), e);
            throw new DaoException(e.getMessage(), e.getCause());
        }
    }
    
    /**
     * 查询全部
     * 
     * @return
     * @throws DaoException
     */
    @Override
    public List<LongText> queryAll()
        throws DaoException
    {
        try
        {
            String sql = "select id, id_path, content, sid from long_text";
            return query(LongText.class, sql);
        }
        catch (SQLException e)
        {
            LOGGER.error(e.getMessage(), e);
            throw new DaoException(e.getMessage(), e.getCause());
        }
    }
    
    /**
     * 根据条件查询
     * 
     * @param criteria 条件对象
     * @return
     * @throws DaoException
     */
    @Override
    public List<LongText> queryByCriteria(LongText criteria)
        throws DaoException
    {
        try
        {
            StringBuilder sql = new StringBuilder("select id, id_path, content, sid from long_text ");
            String whereClause = buildWhereClause(criteria);
            List<Object> whereParams = buildWhereParams(criteria);
            if (StringUtils.isNotEmpty(whereClause))
            {
                sql.append(" where ").append(whereClause);
            }
            return query(LongText.class, sql.toString(), whereParams.toArray());
        }
        catch (SQLException e)
        {
            LOGGER.error(e.getMessage(), e);
            throw new DaoException(e.getMessage(), e.getCause());
        }
    }
    
    /**
     * 根据id查找数据
     * 
     * @param id 主键
     * @return
     * @throws DaoException
     */
    @Override
    public LongText queryById(String id)
        throws DaoException
    {
        try
        {
            String sql = "select id, id_path, content, sid from long_text where id=?";
            return (LongText)queryFirst(LongText.class, sql, id);
        }
        catch (SQLException e)
        {
            LOGGER.error(e.getMessage(), e);
            throw new DaoException(e.getMessage(), e.getCause());
        }
    }
    
    /**
     * 根据条件分页查询
     * 
     * @param criteria 条件对象
     * @param pageNo 页号
     * @param pageSize 页大小
     * @return
     * @throws DaoException
     */
    @Override
    public PaginationSupport<LongText> queryForPagination(LongText criteria, int pageNo, int pageSize)
        throws DaoException
    {
        try
        {
            StringBuilder sql = new StringBuilder("select id, id_path, content, sid from long_text");
            String whereClause = buildWhereClause(criteria);
            List<Object> whereParams = buildWhereParams(criteria);
            if (StringUtils.isNotEmpty(whereClause))
            {
                sql.append(" where ").append(whereClause);
            }
            return queryForPagination(LongText.class, sql.toString(), pageNo, pageSize, whereParams.toArray());
        }
        catch (SQLException e)
        {
            LOGGER.error(e.getMessage(), e);
            throw new DaoException(e.getMessage(), e.getCause());
        }
    }
    
    /**
     * 根据条件查询数据条数
     * 
     * @param criteria 条件对象
     * @return
     * @throws DaoException
     */
    @Override
    public long queryTotal(LongText criteria)
        throws DaoException
    {
        try
        {
            StringBuilder sql = new StringBuilder("select count(1) from long_text");
            String whereClause = buildWhereClause(criteria);
            List<Object> whereParams = buildWhereParams(criteria);
            if (StringUtils.isNotEmpty(whereClause))
            {
                sql.append(" where ").append(whereClause);
            }
            return queryForLong(sql.toString(), whereParams.toArray());
        }
        catch (SQLException e)
        {
            LOGGER.error(e.getMessage(), e);
            throw new DaoException(e.getMessage(), e.getCause());
        }
    }
    
    /**
     * 根据复杂条件更新全字段数据
     * 
     * @param bean 待更新对象
     * @param criteria 条件对象
     * @return
     * @throws DaoException
     */
    @Override
    public boolean updateByCriteria(LongText bean, LongText criteria)
        throws DaoException
    {
        try
        {
            StringBuilder sql = new StringBuilder("update long_text set id_path=?, content=?, sid=?");
            List<Object> params = new ArrayList<>();
            params.add(bean.getIdPath());
            params.add(bean.getContent());
            params.add(bean.getSid());
            String whereClause = buildWhereClause(criteria);
            List<Object> whereParams = buildWhereParams(criteria);
            if (StringUtils.isNotEmpty(whereClause))
            {
                sql.append(" where ").append(whereClause);
            }
            params.addAll(whereParams);
            return update(sql.toString(), params.toArray()) > 0;
        }
        catch (SQLException e)
        {
            LOGGER.error(e.getMessage(), e);
            throw new DaoException(e.getMessage(), e.getCause());
        }
    }
    
    /**
     * 根据复杂条件更新非空字段数据
     * 
     * @param bean 待更新对象
     * @param criteria 条件对象
     * @return
     * @throws DaoException
     */
    @Override
    public boolean updateByCriteriaSelective(LongText bean, LongText criteria)
        throws DaoException
    {
        try
        {
            StringBuilder sql = new StringBuilder("update long_text set");
            StringBuilder columns = new StringBuilder();
            List<Object> params = new ArrayList<>();
            if (bean.getIdPath() != null)
            {
                columns.append(", id_path=?");
                params.add(bean.getIdPath());
            }
            if (bean.getContent() != null)
            {
                columns.append(", content=?");
                params.add(bean.getContent());
            }
            if (bean.getSid() != null)
            {
                columns.append(", sid=?");
                params.add(bean.getSid());
            }
            sql.append(columns.substring(1));
            
            String whereClause = buildWhereClause(criteria);
            List<Object> whereParams = buildWhereParams(criteria);
            if (StringUtils.isNotEmpty(whereClause))
            {
                sql.append(" where ").append(whereClause);
            }
            params.addAll(whereParams);
            return update(sql.toString(), params.toArray()) > 0;
        }
        catch (SQLException e)
        {
            LOGGER.error(e.getMessage(), e);
            throw new DaoException(e.getMessage(), e.getCause());
        }
    }
    
    /**
     * 根据id更新全部数据
     * 
     * @param bean 待更新对象
     * @return
     * @throws DaoException
     */
    @Override
    public boolean updateById(LongText bean)
        throws DaoException
    {
        try
        {
            String sql = "update long_text set id_path=?, content=?, sid=? where id=?";
            return update(sql, bean.getIdPath(), bean.getContent(), bean.getSid(), bean.getId()) > 0;
        }
        catch (SQLException e)
        {
            LOGGER.error(e.getMessage(), e);
            throw new DaoException(e.getMessage(), e.getCause());
        }
    }
    
    /**
     * 根据id更新非空字段数据
     * 
     * @param bean 待更新对象
     * @return
     * @throws DaoException
     */
    @Override
    public boolean updateByIdSelective(LongText bean)
        throws DaoException
    {
        try
        {
            StringBuilder sql = new StringBuilder("update long_text set");
            StringBuilder columns = new StringBuilder();
            List<Object> params = new ArrayList<>();
            if (bean.getIdPath() != null)
            {
                columns.append(", id_path=?");
                params.add(bean.getIdPath());
            }
            if (bean.getContent() != null)
            {
                columns.append(", content=?");
                params.add(bean.getContent());
            }
            if (bean.getSid() != null)
            {
                columns.append(", sid=?");
                params.add(bean.getSid());
            }
            sql.append(columns.substring(1));
            sql.append(" where id=?");
            params.add(bean.getId());
            return update(sql.toString(), params.toArray()) > 0;
        }
        catch (SQLException e)
        {
            LOGGER.error(e.getMessage(), e);
            throw new DaoException(e.getMessage(), e.getCause());
        }
    }
    
}