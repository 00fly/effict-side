package com.fly.demo.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fly.common.ServiceException;
import com.fly.demo.entity.LongText;
import com.fly.demo.service.LongTextService;
import com.fly.demo.service.impl.LongTextServiceImpl;

/**
 * 
 * LongTextController
 * 
 * @author 00fly
 * @version [版本号, 2018-09-29]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Controller
@RequestMapping("/longText")
public class LongTextController
{
    LongTextService longTextService = LongTextServiceImpl.getInstance();
    
    /**
     * 新增/更新数据
     * 
     * @param longText
     * @return
     * @throws ServiceException
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(LongText longText)
        throws ServiceException
    {
        longTextService.saveOrUpdate(longText);
        return "redirect:/longText/list";
    }
    
    /**
     * 删除数据
     * 
     * @param id
     * @return
     * @throws ServiceException
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable String id)
        throws ServiceException
    {
        longTextService.deleteById(id);
        return "redirect:/longText/list";
    }
    
    /**
     * 列表展示
     * 
     * @param model
     * @return
     * @throws ServiceException
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = {"/", "/list"}, method = RequestMethod.GET)
    public String list(Model model)
        throws ServiceException
    {
        List<LongText> list = longTextService.queryAll();
        model.addAttribute("item", new LongText());
        model.addAttribute("list", list);
        return "/longText/show";
    }
    
    /**
     * 编辑数据
     * 
     * @param id
     * @param model
     * @return
     * @throws ServiceException
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
    public String update(@PathVariable String id, Model model)
        throws ServiceException
    {
        model.addAttribute("item", longTextService.queryById(id));
        model.addAttribute("list", longTextService.queryAll());
        return "/longText/show";
    }
    
}
