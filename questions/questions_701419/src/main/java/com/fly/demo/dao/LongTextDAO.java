package com.fly.demo.dao;

import java.util.List;

import com.fly.common.DaoException;
import com.fly.common.PaginationSupport;
import com.fly.demo.entity.LongText;

/**
 * 
 * LongTextDAO 接口
 * 
 * @author 00fly
 * @version [版本号, 2018-09-29]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public interface LongTextDAO
{
    
    /**
     * 根据条件删除数据
     * 
     * @param criteria 条件对象
     * @return
     * @throws DaoException
     */
    long deleteByCriteria(LongText criteria)
        throws DaoException;
    
    /**
     * 根据主键id删除数据
     * 
     * @param id 主键
     * @return
     * @throws DaoException
     */
    long deleteById(String id)
        throws DaoException;
    
    /**
     * 根据主键id列表删除数据
     * 
     * @param ids 主键列表
     * @return
     * @throws DaoException
     */
    long deleteById(String[] ids)
        throws DaoException;
    
    /**
     * 根据主键id列表删除数据
     * 
     * @param ids 主键列表
     * @return
     * @throws DaoException
     */
    long deleteById(List<String> ids)
        throws DaoException;
    
    /**
     * 增加记录(插入全字段)
     * 
     * @param bean 待插入对象
     * @return
     * @throws DaoException
     */
    boolean insert(LongText bean)
        throws DaoException;
    
    /**
     * 增加记录(仅插入非空字段)
     * 
     * @param bean 待插入对象
     * @return
     * @throws DaoException
     */
    boolean insertSelective(LongText bean)
        throws DaoException;
    
    /**
     * 查询全部
     * 
     * @return
     * @throws DaoException
     */
    List<LongText> queryAll()
        throws DaoException;
    
    /**
     * 根据条件查询
     * 
     * @param criteria 条件对象
     * @return
     * @throws DaoException
     */
    List<LongText> queryByCriteria(LongText criteria)
        throws DaoException;
    
    /**
     * 根据id查找数据
     * 
     * @param id 主键
     * @return
     * @throws DaoException
     */
    LongText queryById(String id)
        throws DaoException;
    
    /**
     * 根据条件分页查询
     * 
     * @param criteria 条件对象
     * @param pageNo 页号
     * @param pageSize 页大小
     * @return
     * @throws DaoException
     */
    PaginationSupport<LongText> queryForPagination(LongText criteria, int pageNo, int pageSize)
        throws DaoException;
    
    /**
     * 根据条件查询数据条数
     * 
     * @param criteria 条件对象
     * @return
     * @throws DaoException
     */
    long queryTotal(LongText criteria)
        throws DaoException;
    
    /**
     * 根据复杂条件更新全字段数据
     * 
     * @param bean 待更新对象
     * @param criteria 条件对象
     * @return
     * @throws DaoException
     */
    boolean updateByCriteria(LongText bean, LongText criteria)
        throws DaoException;
    
    /**
     * 根据复杂条件更新非空字段数据
     * 
     * @param bean 待更新对象
     * @param criteria 条件对象
     * @return
     * @throws DaoException
     */
    boolean updateByCriteriaSelective(LongText bean, LongText criteria)
        throws DaoException;
    
    /**
     * 根据id更新全部数据
     * 
     * @param bean 待更新对象
     * @return
     * @throws DaoException
     */
    boolean updateById(LongText bean)
        throws DaoException;
    
    /**
     * 根据id更新非空字段数据
     * 
     * @param bean 待更新对象
     * @return
     * @throws DaoException
     */
    boolean updateByIdSelective(LongText bean)
        throws DaoException;
    
}