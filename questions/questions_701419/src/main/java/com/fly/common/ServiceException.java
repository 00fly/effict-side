package com.fly.common;

/**
 * 
 * ServiceException
 * 
 * @author 00fly
 * @version [版本号, 2018-09-29]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class ServiceException extends Exception
{
    private static final long serialVersionUID = -901435156441815725L;
    
    public ServiceException()
    {
        super();
    }
    
    public ServiceException(String message, Throwable cause)
    {
        super(message, cause);
    }
    
    public ServiceException(String message)
    {
        super(message);
    }
    
    public ServiceException(Throwable cause)
    {
        super(cause);
    }
    
}
