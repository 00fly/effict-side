package com.fly.common;

/**
 * 
 * DaoException
 * 
 * @author 00fly
 * @version [版本号, 2018-09-29]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class DaoException extends Exception
{
    private static final long serialVersionUID = -4983182908260262069L;
    
    public DaoException()
    {
        super();
    }
    
    public DaoException(String message, Throwable cause)
    {
        super(message, cause);
    }
    
    public DaoException(String message)
    {
        super(message);
    }
    
    public DaoException(Throwable cause)
    {
        super(cause);
    }
    
}
