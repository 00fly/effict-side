package com.fly.core;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * TransactionProvider 事务支持提供类
 * 
 * @author 00fly
 * @version [版本号, 2018-09-29]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class TransactionProvider
{
    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionProvider.class);
    
    /**
     * 开启事务
     * 
     */
    public void startTransaction()
    {
        try
        {
            MySqlDBUtil.startTransaction();
        }
        catch (SQLException e)
        {
            LOGGER.error(e.getMessage(), e);
        }
    }
    
    /**
     * 提交事务并关闭连接
     * 
     * @see [类、类#方法、类#成员]
     */
    public void commitAndClose()
    {
        try
        {
            MySqlDBUtil.commitAndClose();
        }
        catch (SQLException e)
        {
            LOGGER.error(e.getMessage(), e);
        }
    }
    
    /**
     * 回滚事务并关闭连接
     * 
     * @see [类、类#方法、类#成员]
     */
    public void rollbackAndClose()
    {
        try
        {
            MySqlDBUtil.rollbackAndClose();
        }
        catch (SQLException e)
        {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
