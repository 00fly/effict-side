package com.fly.demo.controller;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/test")
public class TestController
{
    @GetMapping("/index")
    public String index(Model model)
    {
        model.addAttribute("msg", "来自spring-mvc的信息");
        return "index";
    }
    
    @ResponseBody
    @GetMapping("/rest")
    public Map<String, String> rest()
        throws UnknownHostException
    {
        Map<String, String> map = new HashMap<>();
        map.put("msg", "来自spring-mvc的信息");
        map.put("Server IP", InetAddress.getLocalHost().getHostAddress());
        return map;
    }
}