<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<style>
body {
	margin: 10;
	font-size: 62.5%;
	line-height: 1.5;
}
</style>
</head>
<title>Hello World!</title>
<script>
	let source = new EventSource("/servlet/sse")
	source.onmessage = function(event) {
		document.getElementById("result").innerText = event.data;
	}
</script>

<body>
	<div class="wrapper-page">
		<div class="ex-page-content text-center">
			<div>
				<span style="color: red; font-size: 18px;" id="result"></span>
			</div>
		</div>
	</div>
</body>

</html>