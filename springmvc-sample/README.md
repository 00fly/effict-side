
#### 项目介绍

##### springmvc-sample

- applicationContext.xml 典型配置
- spring-mvc.xml 典型配置




##### 注意点

 - spring和springMVC是两个不同的容器，无法互通，要使aop在controller中生效，spring-mvc.xml也需要配置 `aop:aspectj-autoproxy`
 
 
 - proxy-target-class是Spring AOP中的一个属性，用于指定是否使用CGLIB代理来创建代理对象。当proxy-target-class为true时，Spring将使用CGLIB代理来创建代理对象，否则将使用JDK动态代理。CGLIB代理可以代理类和接口，而JDK动态代理只能代理接口。因此，如果要代理类而不是接口，需要将proxy-target-class设置为true。
 
 
 - 返回图片的字节数组不可使用注解@Controller，返回值会被当作视图路径解析导致异常
 
 
 
 
 
#####  message-converters两种配置方式

 方式1
 
 配置必须在`<mvc:annotation-driven />`之前，否则将不会生效, `<mvc:annotation-driven />`会自动注册DefaultAnnotationHandlerMapping与AnnotationMethodHandlerAdapter。
 
```
<bean class="org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping" />
<bean class="org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter">
	<property name="messageConverters">
		<list>
			<ref bean="stringHttpMessageConverter" />
			<ref bean="jackson2HttpMessageConverter" />
		</list>
	</property>
</bean>

<mvc:annotation-driven />

```


方式2

```
<mvc:annotation-driven>
	<mvc:message-converters>
		<ref bean="stringHttpMessageConverter" />
		<ref bean="jackson2HttpMessageConverter" />
	</mvc:message-converters>
</mvc:annotation-driven>
```