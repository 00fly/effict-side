package com.fly.demo;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.JDialog;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import com.fly.demo.ui.ImageShowDialog;
import com.fly.qrcode.qr.QRCodeUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FileQrTest
{
    int size = 1120;
    
    @Test
    public void testPrintToFile()
        throws Exception
    {
        // TODO: 中文支持长度不足1120,待优化
        String text = IOUtils.resourceToString("/data.json", StandardCharsets.UTF_8);
        int start = 0;
        while (start < text.length())
        {
            String content = StringUtils.substring(text, start, start + size);
            log.info("length: {}", text.length());
            BufferedImage image = QRCodeUtil.createImage(content, null, false, 600);
            File output = new File(start + ".jpg");
            ImageIO.write(image, "JPG", output);
            start += size;
        }
    }
    
    @Test
    public void testShowImage()
        throws IOException
    {
        String all = IOUtils.resourceToString("/data.json", StandardCharsets.UTF_8);
        try (Scanner sc = new Scanner(System.in))
        {
            int repeat = 10;
            JDialog jDialog = null;
            do
            {
                if (jDialog != null)
                {
                    jDialog.dispose();
                }
                String text = StringUtils.substring(all, 0, 800 + 10 * (repeat++));
                log.info("length: {}", text.length());
                jDialog = new ImageShowDialog(QRCodeUtil.createImage(text, null, false, 600));
                log.info("------------输入x退出,回车换行继续------------");
            } while (!"x".equalsIgnoreCase(sc.nextLine()));
            log.info("------------成功退出------------");
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
        }
    }
}
