package com.fly.demo.ui;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 * 
 * 弹出窗口
 * 
 * @author 00fly
 * @version [版本号, 2023年3月3日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class ImageShowDialog extends JDialog
{
    private static final long serialVersionUID = -7240357454480002551L;
    
    public ImageShowDialog(BufferedImage image)
    {
        super();
        setTitle("图片查看工具");
        setSize(image.getWidth(), image.getHeight() + 30);
        Dimension screenSize = getToolkit().getScreenSize();
        Dimension dialogSize = getSize();
        dialogSize.height = Math.min(screenSize.height, dialogSize.height);
        dialogSize.width = Math.min(screenSize.width, dialogSize.width);
        setLocation((screenSize.width - dialogSize.width) / 2, (screenSize.height - dialogSize.height) / 2);
        add(new JLabel(new ImageIcon(image)));
        setVisible(true);
        setResizable(false);
        setAlwaysOnTop(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    
    public static void main(String[] args)
        throws IOException
    {
        Resource resources = new ClassPathResource("123.jpg");
        BufferedImage image = ImageIO.read(resources.getInputStream());
        new ImageShowDialog(image);
    }
}
